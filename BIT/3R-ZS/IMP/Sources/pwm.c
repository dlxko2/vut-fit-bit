void initPWM(void) 
{
  // Vipneme hodiny
  TPM1SC_CLKSA = 0;
  TPM1SC_CLKSB = 0;
  
  // Vynulujeme pocitac
  TPM1CNT = 0x00; 
  
  // Nadstavime cyklus
  TPM1MOD = 10000;
  
  // Nadstavime rezim
  TPM1SC_CPWMS = 0; 
  TPM1C2SC_MS2A = 1;
  TPM1C2SC_MS2B = 0;
  TPM1C2SC_ELS2A = 1;
  TPM1C2SC_ELS2B = 0;
  
  // Nadstavime interupt
  TPM1C2SC_CH2IE = 1;
  
  // Nadstavime stridu
  TPM1C2V = 1000; 
  
  // Pouzijeme bus clock
  TPM1SC_CLKSA = 1;
  TPM1SC_CLKSB = 0;
  
  // Delicka
  TPM1SC_PS0 = 1;
  TPM1SC_PS1 = 1;
  TPM1SC_PS2 = 1;
}