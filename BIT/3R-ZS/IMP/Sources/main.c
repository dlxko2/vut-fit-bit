/* Martin Pavelka (xpavel27)
 * Original 18.12.2015
 */
 
#include <hidef.h> 
#include "derivative.h" 

/* Zakladne deklaracie */
#define LeftDisplay  (*(volatile char*)(0x200))       
#define RightDisplay (*(volatile char*)(0x201))     
#define DILSwitch    (*(volatile char*)(0x202))      
                
#define TRUE 1
#define FALSE 0

#define numX 0x00
#define num0 0x3F
#define num1 0x06
#define num2 0x5B
#define num3 0x4F
#define num4 0x66
#define num5 0x6D
#define num6 0x7D
#define num7 0x07
#define num8 0x7F
#define num9 0x6F
#define numA 0x77
#define numB 0x7C
#define numC 0x39
#define numD 0x5E
#define numE 0x79
#define numF 0x71

/* Globalne premenne */
int waitStateCount;
int nibble0, nibble1, nibble2, nibble3;
int nibbleIndex;
int finishProgress;

/* Prototypy funkcii */
void    main                 (void);
void    repairUP             (void);
void    repairDOWN           (void);
void    initPWM              (void);
void    finishCount          (void);
void    runStart             (void);
int     checkReading         (void);
int     checkCountUp         (void);
void    runSettings          (void);
void    initSystem           (void);
int     checkStartStop       (void);
int     getNibbleCountIndex  (void);
int     checkRightDisplay    (void);
int     getNibble            (int index);
byte    convDecToDisplay     (int number);
int     getValue             (int ifIndex);
void    setNibble            (int index, int value);

// Vektor prerusenia
void interrupt VectorNumber_Vtpm1ch2 tpm1ch2_isr(void);

/* Hlavna funkcia programu */
void main(void) 
{  
  // Initialize PWM module and system
  initPWM();  
  initSystem();
  
  // Enable interupts
  EnableInterrupts;
  
  // Zasobovanie watchdogu spravami
  for(;;) {__RESET_WATCHDOG();}  
}

/* Funkcia implementujuca init systemu */
void initSystem(void) 
{
  // Init interface
  DILSwitch = 0b00000000;
  LeftDisplay = num0;
  RightDisplay = num0;

  // Init counters
  waitStateCount = 0;
  finishProgress = 0;

  // Init nibbles
  nibble0 = 0;
  nibble1 = 0;
  nibble2 = 0;
  nibble3 = 0;
  nibbleIndex = 0;
}

/* Funkcia implementujuca settings mode */
void runSettings(void) 
{  
  // Ak lavy displej potom ziskaj hodnotu a vypis ju s indexom
  if (!checkRightDisplay())
  {
    nibbleIndex = getValue(1);
    LeftDisplay = convDecToDisplay(nibbleIndex);
    RightDisplay = convDecToDisplay(getNibble(nibbleIndex));
    return;
  }

  // Ak pravy ulozime hodnotu do nibble a vypiseme 
  setNibble(nibbleIndex, getValue(0));
  RightDisplay = convDecToDisplay(getNibble(nibbleIndex));
}

/* Funkcia implementujuca start mode */
void runStart(void) 
{
  // Ak je pocitanie hore tak pripocitame a urobime prenos
  if (checkCountUp())
  {
    if ((nibble0 + 1) > 15) repairUP();
    else ++nibble0; 
  }

  // Ak je odpocitavanie odpocitame a urobime prenos
  else
  {
    if ((nibble0 - 1) < 0) repairDOWN();
    else --nibble0;
  }
  
  // Ak neni v rezime ukoncenia zobrazime zmeny
  if (finishProgress == 0) 
  {
    nibbleIndex = getNibbleCountIndex();
    LeftDisplay = convDecToDisplay(nibbleIndex);
    RightDisplay = convDecToDisplay(getNibble(nibbleIndex));
  }
}

/* Funkcia implementujuca dosiahnutie maximalnej hodnoty */
void finishCount() 
{
  // Nadstavime priznak ukoncenia
  finishProgress = 1;

  // Ak 3 cykly presli resetujeme system
  if (waitStateCount == 3) 
  { 
    initSystem();
    return;
  }

  // Implementacia blikania hodnoty na displeji
  if (waitStateCount == 0 || waitStateCount == 2)
    RightDisplay = numX;
  else
    RightDisplay = convDecToDisplay(getNibble(nibbleIndex));
        
  // Zvysime pocitadlo uplynulych cyklov                                                            
  ++waitStateCount; 
}

/* Implementacia prenosu o rad pri pripocitavani */
void repairUP()
{
  // Ak maximalna hodnota skoncime
  if (nibble3 == 15) 
  {
    finishCount();
    return;
  }
      
  // Pripocitanie hodnoty o jedna
  ++nibble1;
  nibble0 = 0; 

  // Oprava nibble s index 2
  if (nibble1 > 15)
  {
    ++nibble2;
    nibble1 = 0;
  }

  // Oprava nibble s index 3
  if (nibble2 > 15)
  {
    ++nibble3;
    nibble2 = 0;
  }
}

/* Implementacia prenosu o rad pri odpocte */
void repairDOWN()
{
  // Ak maximalna hodnota tak skoncime
  if (nibble3 == 0) 
  {
    finishCount();
    return;
  }
 
  // Odpocitame jednicku
  --nibble1;
  nibble0 = 15; 

  // Oprava hodnoty nibble s index 2
  if (nibble1 < 0)
  {
    --nibble2;
    nibble1 = 15;
  }

  // Oprava hodnoty s nibble index 3
  if (nibble2 < 0)
  {
    --nibble3;
    nibble2 = 15;
  }
}

/* Funkcia implementujuca PWM modul */
void initPWM(void) 
{
  // Vipneme hodiny
  TPM1SC_CLKSA = 0;
  TPM1SC_CLKSB = 0;
  
  // Vynulujeme pocitac
  TPM1CNT = 0x00; 
  
  // Nadstavime cyklus
  TPM1MOD = 10000;
  
  // Nadstavime rezim
  TPM1SC_CPWMS = 0; 
  TPM1C2SC_MS2A = 1;
  TPM1C2SC_MS2B = 0;
  TPM1C2SC_ELS2A = 1;
  TPM1C2SC_ELS2B = 0;
  
  // Nadstavime interupt
  TPM1C2SC_CH2IE = 1;
  
  // Nadstavime stridu
  TPM1C2V = 1000; 
  
  // Pouzijeme bus clock
  TPM1SC_CLKSA = 1;
  TPM1SC_CLKSB = 0;
  
  // Delicka
  TPM1SC_PS0 = 1;
  TPM1SC_PS1 = 1;
  TPM1SC_PS2 = 1;
}

/* Funkcia implementujuca vektor prerusenia z PWM */
void interrupt VectorNumber_Vtpm1ch2 tpm1ch2_isr(void) 
{
  // Nadstavenie dalsieho cyklu
  TPM1C2SC_CH2F = 0;
  TPM1C2V = 10000;
  
  // Ak ukoncujeme potom skocime do ineho rezimu
  if (finishProgress == 1)
  {
    finishCount();
    return;
  }
 
  // Ak je zapnuty system
  if(checkStartStop()) 
  {
      // Ak je v rezime pocitania alebo nadstavenia
      if (checkReading()) runStart();
      else runSettings();
  } 
  
  // Ak vypnuty obnovime stav
  else initSystem(); 
}

/* Funkcia ktora ziska najpomalsie sa meniaci index */
int getNibbleCountIndex()
{
  if      (nibble3 > 0) return 3;
  else if (nibble2 > 0) return 2;
  else if (nibble1 > 0) return 1;
  else                  return 0;
}

/* Funkcia, ktora nadstavuje hodnotu nibbla */
void setNibble(int index, int value)
{
  switch(index)
  {
    case 0: nibble0 = value; break;
    case 1: nibble1 = value; break;
    case 2: nibble2 = value; break;
    case 3: nibble3 = value; break;
  }
}

/* Ziskanie hodnoty zo Switchu(0,1,2,3) */
int getValue(int ifIndex)
{
  // Inicializacia vyslednej hodnoty
  int tmpValue = 0;

  // Ziskanie hodnoty z prvych dvoch prepinacov
  tmpValue += ((DILSwitch & 0b00000001) == 0b00000001) ? 1 : 0; 
  tmpValue += ((DILSwitch & 0b00000010) == 0b00000010) ? 2 : 0;

  // Ak nejde o index ukonci funkciu
  if (ifIndex == 0)
    return tmpValue;

  // Ak ide o hodnotu ziskanie hodnoty aj dalsich dvoch prepinacov
  tmpValue += ((DILSwitch & 0b00000100) == 0b00000100) ? 4 : 0;
  tmpValue += ((DILSwitch & 0b00001000) == 0b00001000) ? 8 : 0;
  return tmpValue;
}

/* Funkcia na ziskanie hodnoty nibblu */
int getNibble(int index)
{
  switch(index)
  {
    case 0: return nibble0;
    case 1: return nibble1;
    case 2: return nibble2;
    case 3: return nibble3;
    default: return 0;
  }
}

/* Konvertovanie dec na displej */
byte convDecToDisplay(int number) 
{
  switch (number) 
  {
    case 0:  return num0;
    case 1:  return num1;
    case 2:  return num2;
    case 3:  return num3;
    case 4:  return num4;
    case 5:  return num5;
    case 6:  return num6;
    case 7:  return num7;
    case 8:  return num8;
    case 9:  return num9;
    case 10: return numA;
    case 11: return numB;
    case 12: return numC;
    case 13: return numD;
    case 14: return numE;
    case 15: return numF;
  } 
}

/* Check Switchu(7) */
int checkStartStop(void) 
{
  return ((DILSwitch & 0b10000000) == 0b10000000) ? TRUE : FALSE; 
}

/* Check Switchu(6) */
int checkReading(void) 
{
  return ((DILSwitch & 0b01000000) == 0b01000000) ? TRUE : FALSE; 
}

/* Check Switchu(5) */
int checkCountUp(void) 
{
  return ((DILSwitch & 0b00100000) == 0b00100000) ? TRUE : FALSE;
}
  
/* Check Switchu(4) */
int checkRightDisplay(void) 
{
  return ((DILSwitch & 0b00010000) == 0b00010000) ? TRUE : FALSE;
}

