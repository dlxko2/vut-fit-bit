#ifndef messages_h
#define messages_h

#define STATUS_220          "220"
#define STATUS_250          "250"
#define STATUS_221          "221"
#define STATUS_354          "354"
#define MSG_EHLO            "EHLO xpavel27\x0d\x0a"
#define MSG_FROM            "MAIL FROM:xpavel27@isa.local\x0d\x0a"
#define MSG_TO              "RCPT TO:"
#define CHAR_CLRF           "\x0d\x0a"
#define CHAR_CLRF_CLRF      "\x0d\x0a.\x0d\x0a"
#define MSG_DATA            "DATA\x0d\x0a"
#define MSG_QUIT            "QUIT\x0d\x0a"
#define MSG_NOOP            "NOOP\x0d\x0a"

#endif 
