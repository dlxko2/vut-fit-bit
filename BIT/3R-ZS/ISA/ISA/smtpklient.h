#ifndef main_h
#define main_h

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>

#define SIZE_EMAIL 256 /* 256 RC email size */
#define SIZE_TEXT 1000 /* 1000 RFC msg size */
#define SIZE_BUFFER 1000 /* 512 RFC respond size */
#define WAIT_INTERVAL 270
#define SUCCESS 0
#define FAILED 1

typedef enum
{
    START,
    EMAIL,
    TEXT
} states;

typedef struct
{
    char *aIP;
    char *bPort;
    char *iFile;
    char *wTime;
} arguments;

typedef struct
{
    char buffer[SIZE_EMAIL];
    int position;
} email;

typedef struct
{
    char buffer[SIZE_TEXT];
    int position;
} text;

bool getCheckedRespond(char *inputBuffer, char checkMsg[]);
bool sendCheckedQuestion(char MsgToSend[]);
void prepend(char* s, const char* t);
bool connetMe(void);
bool getArguments(int argc, char * argv[]);
bool getFirstRespond(char *inputBuffer);
bool sendEhlo(char *inputBuffer);
bool sendFrom(char *inputBuffer);
bool sendTo(char *inputEmail, char *inputBuffer);
bool sendData(char *inputBuffer, bool dataSentState);
bool sendText(char *inputBuffer, char *inputText, bool ifChunk);
bool sendQuit(char *inputBuffer);
bool sendNoop(char *inputBuffer);
void handleSignal(int signal);
bool finiteMachine(char *inputBuffer, FILE *inputFile);
void stuckInConnection(char *inputBuffer);
void closeSocketWithMsg(char MsgToPrint[]);

#endif
