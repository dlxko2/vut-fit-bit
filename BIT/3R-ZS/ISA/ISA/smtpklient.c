// Project ISA
// Martin Pavelka
// 3BIT 2015

#include "smtpklient.h"
#include "messages.h"

#define DEBUG_MODE false

int mySocket;
arguments myArgs;
bool mySignal;
int currentState;

void closeSocketWithMsg(char MsgToPrint[])
{
    if (mySocket >= 0) close(mySocket);
    fprintf(stderr, "%s\n", MsgToPrint);
}

/* Function to get respond and check it */
bool getCheckedRespond(char *inputBuffer, char checkMsg[])
{
    bzero(inputBuffer, SIZE_BUFFER);
    recv(mySocket, inputBuffer, SIZE_BUFFER, 0);
    if (DEBUG_MODE) printf("%s\n", inputBuffer);
    return (strncmp(inputBuffer, checkMsg, strlen(checkMsg)) != 0) ? false : true;
}

/* Function to send message and check send process */
bool sendCheckedQuestion(char MsgToSend[])
{
    ssize_t sendResult = send(mySocket, MsgToSend, strlen(MsgToSend), 0);
    if (DEBUG_MODE) printf("%s\n", MsgToSend);
    return (sendResult < (ssize_t)strlen(MsgToSend)) ? false : true;
}

/* Function to prepend text */
void prepend(char* s, const char* t)
{
    size_t len = strlen(t);
    size_t i;
    
    memmove(s + len, s, strlen(s) + 1);
    for (i = 0; i < len; ++i) s[i] = t[i];
}

/* Function to create connection */
bool connetMe()
{
    struct addrinfo hints;
    struct addrinfo *result;
    int tmpSocket, adressResult;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = 0;
    
    // Fill address info
    adressResult = getaddrinfo(myArgs.aIP, myArgs.bPort, &hints, &result);
    if (adressResult != 0)
    {
        fprintf(stderr, "Getaddrinfo: %s\n", gai_strerror(adressResult));
        return false;
    }
    
    // Create socket
    tmpSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (tmpSocket < 0)
    {
        fprintf(stderr, "Socket: can't create socket \n");
        return false;
    }
    
    // Connect to server
    if (connect(tmpSocket, result->ai_addr, result->ai_addrlen) < 0)
    {
        perror("Connet: can't connect to server \n");
        return false;
    }
    
    freeaddrinfo(result);
    mySocket = tmpSocket;
    return true;
}

/* Function to get arguments */
bool getArguments(int argc, char * argv[])
{
    myArgs.aIP = "127.0.0.1";
    myArgs.bPort = "25";
    int c;
    
    while ((c = getopt (argc, argv, "a:p:i:w:")) != -1)
        switch (c)
    {
        case 'a': myArgs.aIP = optarg;   break;
        case 'p': myArgs.bPort = optarg; break;
        case 'i': myArgs.iFile = optarg; break;
        case 'w': myArgs.wTime = optarg; break;
        default: return false;
    }
    
    if (myArgs.wTime != false && atoi(myArgs.wTime) > 3600)
    {
        fprintf(stderr, "Arguments: Time 3600 is maximum and you have %d!\n", atoi(myArgs.wTime));
        return false;
    }
    
    if (optind < argc) {
        fprintf (stderr, "Arguments: Wrong number of arguments parsed %s!\n", argv[optind]);
        return false;
    }
    
    if (myArgs.iFile == NULL) {
        fprintf(stderr, "Arguments: Wrong file path defined\n");
        return false;
    }
    
    return true;
}

/* Function to get First respond from server */
bool getFirstRespond(char *inputBuffer)
{
    return (!getCheckedRespond(inputBuffer, STATUS_220)) ? false : true;
}

/* Function to send ehlo message */
bool sendEhlo(char *inputBuffer)
{
    if (!sendCheckedQuestion(MSG_EHLO)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_250)) ? false : true;
}

/* Function to send from message */
bool sendFrom(char *inputBuffer)
{
    if (!sendCheckedQuestion(MSG_FROM)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_250)) ? false : true;
}

/* Function to send receiver mail */
bool sendTo(char *inputEmail, char *inputBuffer)
{
    char mail[SIZE_EMAIL+sizeof(MSG_TO)+sizeof(CHAR_CLRF)+10];
    strcpy(mail, inputEmail);
    prepend(mail, MSG_TO);
    strcat(mail, CHAR_CLRF);
    
    if (!sendCheckedQuestion(mail)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_250)) ? false : true;
}

/* Function to send email data msg */
bool sendData(char *inputBuffer, bool dataSentState)
{
    // Check if skipped
    if (dataSentState) return true;
    if (!sendCheckedQuestion(MSG_DATA)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_354)) ? false : true;
}

/* Function to send email body */
bool sendText(char *inputBuffer, char *inputText, bool ifChunk)
{
    if (!ifChunk) strcat(inputText, CHAR_CLRF_CLRF);
    if (!sendCheckedQuestion(inputText)) return false;
    if (!ifChunk) return (!getCheckedRespond(inputBuffer, STATUS_250)) ? false : true;
    return true;
}

/* Function to send quit message */
bool sendQuit(char *inputBuffer)
{
    if (!sendCheckedQuestion(MSG_QUIT)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_221)) ? false : true;
}

/* Function to send noop message */
bool sendNoop(char *inputBuffer)
{
    if (!sendCheckedQuestion(MSG_NOOP)) return false;
    return (!getCheckedRespond(inputBuffer, STATUS_250)) ? false : true;
}

/* Function to handle signals */
void handleSignal(int signal)
{
    if (currentState == TEXT)
    {
        if (signal) mySignal = true;
        return;
    }
    
    char inputBuffer[SIZE_BUFFER];
    sendQuit(inputBuffer);
    closeSocketWithMsg("Received kill signal!\n");
    exit(SUCCESS);
}

/* Function of finite signals */
bool finiteMachine(char *inputBuffer, FILE *inputFile)
{
    // Initialize state machine
    int currentChar = 0, currentLine = 1;
    currentState = START;
    bool dataSendState = false, recptsState = false, textSent = true, skipRecipent = false;
    
    // Initialize buffers
    email tmpEmail;
    text tmpText;
    tmpEmail.position = 0;
    tmpText.position = 0;
    bzero(tmpEmail.buffer, SIZE_EMAIL);
    bzero(tmpText.buffer, SIZE_TEXT);
    
    while (true)
    {
        currentChar = getc(inputFile);
        switch (currentState)
        {
            case START:
                
                // Check if end of file
                if (currentChar == EOF) return true;
                else if (currentChar != '\n') currentState = EMAIL;
                
                // Send mail from
                if (textSent && !sendFrom(inputBuffer))
                    return false;
                
                textSent = false;
                
                // Save character
                tmpEmail.buffer[tmpEmail.position] = (char)currentChar;
                tmpEmail.position++;
                break;
                
            case EMAIL:
                
                // If some action required
                if (currentChar == ',' || currentChar == ' ')
                {
                    // Send recipient
                    if (!skipRecipent)
                    {
                        // Send and check recipient
                        bool rcptResult = sendTo(tmpEmail.buffer, inputBuffer);
                        if (!rcptResult)
                            fprintf(stderr, "Client: Wrong recipient %s line %d!\n", tmpEmail.buffer, currentLine);
                        
                        // Set state as recepient correct
                        if (rcptResult && !recptsState)
                            recptsState = true;
                    }
                    
                    bzero(tmpEmail.buffer, SIZE_EMAIL);
                    tmpEmail.position = 0;
                    
                    // Choose if next mail or text and refresh recipient skipping
                    if (skipRecipent) skipRecipent = false;
                    if (currentChar == ',') currentState = EMAIL;
                    else
                    {
                        if (!recptsState) fprintf(stderr, "Client: None of the recipients correct line %d!\n", currentLine);
                        currentState = TEXT;
                    }
                    break;
                }
                
                // Add character to buffer
                if (!skipRecipent)
                {
                    tmpEmail.buffer[tmpEmail.position] = (char)currentChar;
                    tmpEmail.position++;
                }
                
                // Check for long recipient
                if (tmpEmail.position > SIZE_EMAIL && !skipRecipent)
                {
                    fprintf(stderr, "Client: Too long recipient %s line %d!\n", tmpEmail.buffer, currentLine);
                    skipRecipent = true;
                }
                break;
                
            case TEXT:
                
                // If any action required
                if (currentChar == '\n' || currentChar == EOF)
                {
                    // Send data and text when any reciptient correct
                    if (recptsState)
                    {
                        sendData(inputBuffer, dataSendState);
                        sendText(inputBuffer, tmpText.buffer, false);
                        recptsState = false;
                        textSent = true;
                    }
                    
                    // If end of file quit
                    if(currentChar == EOF || mySignal)
                        return true;
                    
                    // If newline continue to email
                    bzero(tmpText.buffer, SIZE_TEXT);
                    tmpText.position = 0;
                    dataSendState = false;
                    currentState = START;
                    ++currentLine;
                    break;
                }
                
                // Add character to buffer
                tmpText.buffer[tmpText.position] = (char)currentChar;
                tmpText.position++;
                
                // If end of chunk
                if (tmpText.position >= SIZE_TEXT - 5)
                {
                    // Send data and text when any reciptient correct
                    if (recptsState)
                    {
                        sendData(inputBuffer, dataSendState);
                        sendText(inputBuffer, tmpText.buffer, true);
                        textSent = true;
                    }
                    
                    // If next chunk continue
                    dataSendState = true;
                    bzero(tmpText.buffer, SIZE_TEXT);
                    tmpText.position = 0;
                }
                break;
        }
        
    }
    return false;
}

void stuckInConnection(char *inputBuffer)
{
    unsigned int timeChunk = (unsigned int)atoi(myArgs.wTime);
    while (timeChunk > 0)
    {
        if (timeChunk < WAIT_INTERVAL)
        {
            sleep(timeChunk);
            break;
        }
        
        timeChunk = timeChunk - WAIT_INTERVAL;
        sleep(WAIT_INTERVAL);
        sendNoop(inputBuffer);
    }
}

int main(int argc,  char * argv[])
{
    // Initialize buffer
    char inputBuffer[1000];
    
    // Handle termination signals
    signal(SIGTERM, handleSignal);
    signal(SIGINT, handleSignal);
    signal(SIGQUIT, handleSignal);
    
    // Initialize arguments
    if (!getArguments(argc, argv))
    {
        closeSocketWithMsg("Arguments: Wrong arguments");
        return FAILED;
    }
    
    // Get file
    FILE * inputFile = fopen(myArgs.iFile, "r");
    if (inputFile == NULL)
    {
        closeSocketWithMsg("Clinet: Can't open data file");
        return FAILED;
    }
    
    // Create connection
    if (!connetMe())
    {
        closeSocketWithMsg("Connection: Can't create connection");
        return FAILED;
    }
    
    // Get first respond
    if (!getFirstRespond(inputBuffer))
    {
        closeSocketWithMsg("Connection: Wrong respond from server");
        return FAILED;
    }
    
    // Send ehlo
    if (!sendEhlo(inputBuffer))
    {
        closeSocketWithMsg("Clinet: Sending ehlo failed");
        return FAILED;
    }
    
    // Call finite machine
    if (!finiteMachine(inputBuffer, inputFile))
    {
        closeSocketWithMsg("Clinet: Problem while sending/receiving packets");
        return FAILED;
    }
    
    // Wait some time with connection
    if (myArgs.wTime != NULL)
        stuckInConnection(inputBuffer);
    
    // Send quit message
    sendQuit(inputBuffer);
    return SUCCESS;
}
