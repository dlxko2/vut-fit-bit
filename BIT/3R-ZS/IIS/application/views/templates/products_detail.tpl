<!-- PRODUCTS DETAIL HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
    <script src="{$basepath}/myjs/products_detail.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- HEADER -->
<h2>
  Product details: {$product_info.NAME} ({$product_id})
</h2> 
<br>

<!-- CONTENT -->
<div style="width:100%">

<!-- LEFT INFO -->
<div class="col-xs-6 col-md-3" style="float: left; width:25%;">
  <a href="#" class="thumbnail"><img src="{$basepath}/images/products/1.jpeg" alt="..."></a>
  
  <!-- EXTRACT -->
  <div class="col-md-6">
    <form method="post" accept-charset="utf-8" action="{$indexpath}/products/productDetail/{$product_id}">
    <button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
    </form>
  </div>

  <!-- STOCK -->
  <div class="col-md-6">
    {if $product_info.STOCK == "Y"}
    <button class="btn btn-success">On Stock</button>
    {else}
    <button class="btn btn-danger">Inavaiable</button>
    {/if} 
  </div>

  <!-- MODIFY -->
  <div class="col-md-12">
    {if $ismerch && $product_info.COMPANY == $merchant.COMPANY}
    <button class="btn btn-warning" name="Edit" id="toogle">Modify this as merchant !</button>
    {elseif $isadm}
    <button class="btn btn-warning" name="Edit" id="toogle">Modify this as admin !</button>
    {else}
    <button class="btn btn-warning" name="Edit" id="tooglefeed">Give customer feedback !</button>
    {/if}
  </div>

  {if !$ismerch && !$isadm}
  <div class="col-md-12"><br>
  <div id="editInfoShowFeed" style="display:none;">
    <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/feedbacks/addFeedback/{$product_id}">
    <div class="form-group">
    <p class="bg-danger" style="padding: 10px 10px 10px 10px">
    <b>Please select the mark</b><br>
    <select class="form‐control" id="mark" name="mark">
      <option value="0">Select the mark</option>
      <option value="1">Excelent</option>
      <option value="2">Super</option>
      <option value="3">Good</option>
      <option value="4">Bad</option>
      <option value="5">Worst</option>
    </select>
    <br><br>
    <button type="submit" class="btn btn-primary">Submit</button>
    </p>
    </div>
    </form>
  </div>
  </div>
  {/if}
</div>
  
<!-- INFO SHOW RIGHT --> 
<div id="getInfoShow">
<div class="panel panel-default" style="float: right; width:75%;">

  <!-- RIGHT PANNEL 1 -->
  <div class="panel-heading"><b>Description</b></div>
  <div class="panel-body">{$product_info.DESCRIPTION}</div>
  <div class="panel-heading"><b>Pricing</b></div>
  <div class="panel-body">
  <b>Price:&nbsp; </b>{$product_info.PRICE}<br>
  <b>VAT:&nbsp; </b> {$product_info.VAT}<br>
  <b>Count:&nbsp; </b> {$product_info.COUNT}<br>
  </div>

  <!-- RIGHT PANNEL 2 -->
  <div class="panel-heading"><b>More infos</b></div>
  <div class="panel-body">
  <b>Brand:&nbsp; </b> {$product_info.BRAND} <br>
  <b>Category:&nbsp; </b> <td>{$product_info.CATEGORY}</td> <br>
  <b>Company:&nbsp; </b> <td>{$product_info.COMPANY}</td><br>
  <b>Delivery:&nbsp; </b> <td><font color="red">{$product_info.DELIVERY}</font> days</td>
  <br>
  </div>

  <!-- RIGHT PANNEL 3 -->
  <div class="panel-heading"><b>More infos</b></div>
  <div class="panel-body">
  {if $product_info.SOFTWARE}
  <b>Licence type:&nbsp; </b> {$product_info.LICENCE_TYPE} <br>
  <b>Configuration:&nbsp; </b> <td>{$product_info.CONFIGURATION}</td> <br>
  {else}
  <b>Size:&nbsp; </b> {$product_info.SIZE} <br>
  <b>Weight:&nbsp; </b> {$product_info.WEIGHT} <br>
  {/if}
  </div>

</div>
</div>

<!-- EDIT INFO RIGHT -->
{if $ismerch || $isadm}
<div id="editInfoShow" style="display:none;">
<div style="width:75%;float:right" >
  
  <!-- HEAD -->
  <form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/products/editProduct/{$product_id}">
  <div class="form-group">

  <!-- BASIC INFO -->
  <p class="bg-danger" style="padding: 10px 10px 10px 10px">
  <b>Basic Info</b><br>
  <input type="text" class="form-control" id="edit_name" placeholder="Name" name="edit_name" value="{$product_info.NAME}"> 
  <input type="text" class="form-control" id="edit_count" placeholder="Count" name="edit_count" value="{$product_info.COUNT}"> 
  <br><br>
  <input type="text" class="form-control" id="edit_brand" placeholder="Brand" name="edit_brand" value="{$product_info.BRAND}"> 
  <input type="text" class="form-control" id="edit_price" placeholder="Price" name="edit_price" value="{$product_info.PRICE}">
  <br><br>
  </p>

  <!-- HARDWARE / SOFTWARE -->
  <p class="bg-warning" style="padding: 10px 10px 10px 10px">
  <b>Specify Info</b><br>
  {if $product_info.SOFTWARE}
  	<input type="text" class="form-control" id="edit_licence_type" placeholder="Licence" name="edit_licence_type" value="{$product_info.LICENCE_TYPE}">
  	<input type="text" class="form-control" id="edit_configuration" placeholder="Configuration" name="edit_configuration" value="{$product_info.CONFIGURATION}">
  	<input type="hidden" name="edit_type" value="Software">
  {else}
  	<input type="text" class="form-control" id="edit_size" placeholder="Size" name="edit_size" value="{$product_info.SIZE}">
  	<input type="text" class="form-control" id="edit_weight" placeholder="Weight" name="edit_weight" value="{$product_info.WEIGHT}">
  	<input type="hidden" name="edit_type" value="Hardware">
  {/if}
  <br><br>
  </p>

  <!-- EDIT FORM STOCK -->
  <p class="bg-info" style="padding: 10px 10px 10px 10px">
  <b>Stock Info</b><br>
  <select class="form‐control" id="edit_stock" name="edit_stock">
    <option value="0">Select stock</option>
    <option value="Y" {if $product_info.STOCK == "Y"}selected{/if}>YES</option>
    <option value="N" {if $product_info.STOCK == "N"}selected{/if}>NO</option>
  </select>
  <br><br>
  </p>

  <!-- DESCRIPTION -->
  <p class="bg-success" style="padding: 10px 10px 10px 10px">
  <b>Description</b><br>
  <textarea class="form-control" id="edit_description" rows="4" placeholder="Description" name="edit_description" style="width:100%">{$product_info.DESCRIPTION}
  </textarea>
  <br><br>
  <button type="submit" class="btn btn-primary">Submit</button>
  </p>
  </div>
  </form>
  </p>
</div>
</div>
{/if}

<!-- FINISH -->
</div>
</div>
</div>
</body>
</html>