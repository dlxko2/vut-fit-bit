<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>

</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="{$basepath}/images/feedbacks.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

<!-- SEARCH FEEDBACK -->
<h2>Search for the feedback</h2>  

<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/feedbacks/show">
<div class="form-group">
<input type="text" class="form-control" id="seach_feedback_product" placeholder="Product name" name="seach_feedback_product" value="{$SearchByProduct}"> 
{if $isadm || $ismerch}
<input type="text" class="form-control" id="seach_feedback_user" placeholder="User name" name="seach_feedback_user" value="{$SearchByUser}">
{/if}
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>
  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>Product</th>
<th>User</th>
<th>Mark</th>
<th>Company</th>
{if !$ismerch}
	<th>DETAILS</th>
{/if}
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
{foreach from=$feedbacks item=feedback}
{if $feedback.MARK == "1" || $feedback.MARK == "2"}
<tr class="danger">
{elseif $feedback.MARK == "3"}
<tr class="info">
{elseif $feedback.MARK > "4"}
<tr class="success">
{/if}

<td>{$feedback.ID}</td>     
<td>{$feedback.PRODUCT}</td>          
<td>{$feedback.USER}</td>      
<td>{$feedback.MARK}</td>
<td>{$feedback.COMPANY}</td>
  
<!-- OPTIONS -->
{if !$ismerch}
	<td width="5%">
	<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/feedbacks/deleteFeedback/{$feedback.ID}" ></a>
	</td>
{/if}
</tr>
{/foreach}
</tbody>
</table>

<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html>