<!-- FEEDBACKS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <link href="{$basepath}/mycss/trashes.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>

</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">
  	 
<!-- IMAGE -->
<img src="{$basepath}/images/trashes.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

{if $items == false || $items|@count lt 5}
<br>
<p class="bg-primary" align="center"><b>
<span style="font-size:large">Oh no you have an empty trash :(</span><br>
<span style="font-size:middle">Please add some products!</span><br><br>
<a href="{$indexpath}/products/show/1"><span class="glyphicon glyphicon-list-alt" style="color:white"></span></a><br><br>


{else}  
<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>Name</th>
<th>Price</th>
<th>Vat</th>
<th>Count</th>
<th>Delivery</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
	{foreach from=$items key=mykey item=item}

	{if is_int($mykey) == false}
	{continue}
	{/if}

	{if $item.STOCK == "Y"}
	<tr class="success">
	{else}
	<tr class="danger">
	{/if}

	<td>{$item.NAME}</td>     
	<td>{$item.PRICE}</td>          
	<td>{$item.VAT}</td>      
	<td>{$item.COUNT}</td>
	<td>{$item.DELIVERY}</td>

	  
	<!-- OPTIONS -->
	<td width="5%" align="center">
	<a class="glyphicon glyphicon-trash" href="{$indexpath}/trashes/deleteItem/{$item.ID}" style="font-size:20px"></a>
	</td>
	</tr>
	{/foreach}

<tr class="info">
<td style="vertical-align: middle;font-size:large" valign="bottom"><b>TOTAL:</b></td>
<td style="vertical-align: middle;font-size:large"><b>{$items.TOTAL}</b></td>
<td style="vertical-align: middle;font-size:large"><b>{$items.TOTALV}</b></td>
<td style="vertical-align: middle;font-size:large"><b>{$items.TOTALC}</b></td>
<td style="vertical-align: middle;font-size:large"><b>{$items.TOTALD}</b></td>
<form>
<input type="hidden" name="finishOrder" id="finishOrder" value="1">
<td><button type="submit" class="btn btn-success" formaction="{$indexpath}/trashes/processOrder/">Complete!</button></td>
</form>
</tr>
</tbody>
</table>

{/if}


<!-- FINNISH -->
</div>
</div>
</div>
</div>
</body>
</html>