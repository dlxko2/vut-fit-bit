<!-- DASHBOARD HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <link href="{$basepath}/mycss/dashboard.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="{$basepath}/images/dashboard.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

<!-- PAYED MONTHLY -->
<div class="panel panel-red" style="width:20%">
<div class="panel-heading">
<div class="row">
<div class="col-xs-3">
</div>
<div class="col-xs-9 text-right">
<div class="huge">{$monthPayed} </div> CZK
</div>
</div>
</div> 
<div class="panel-footer">
<span class="pull-left">
  Payed this month!
</span>
<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
<div class="clearfix"></div>
</div>
</div>

<!-- FINISH -->
</div>
</div>
</div>
</body>
</html>