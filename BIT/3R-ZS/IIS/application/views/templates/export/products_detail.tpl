<html>
<head>
</head>

<body>

  <h2>Product details: {$product_info.NAME} ({$product_id}) 
  {if $product_info.STOCK == "Y"}
      <font color="green"><b>On Stock</b></font>
    {else}
      <font color="red"><b>Inavaiable</b></font>
    {/if}
  </h2> 
 
  <b>Description</b><br>{$product_info.DESCRIPTION} <br><br>
  
  <font color="red"><b>Pricing</b></font><br>
  <b>Price: </b>{$product_info.PRICE}<br>
  <b>VAT: </b> {$product_info.VAT}<br>
  <b>Count: </b> {$product_info.COUNT}<br><br>
 
 <font color="red"><b>More infos</b></font><br>
 <b>Brand: </b> {$product_info.BRAND} <br>
 <b>Category: </b>{$product_info.CATEGORY}</td> <br>
 <b>Company: </b> {$product_info.COMPANY}</td><br>
 <b>Delivery: </b> <font color="red">{$product_info.DELIVERY}</font> days <br><br>

</body>
</html>