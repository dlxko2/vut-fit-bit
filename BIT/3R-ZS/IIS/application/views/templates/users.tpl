<!-- USERS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
    <script src="{$basepath}/myjs/users.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="{$basepath}/images/users.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

<!-- NAVIGATION BAR -->  
<ul class="nav nav-tabs">
<li><a data-toggle="pill" href="#search">Find</a></li>
<li><a data-toggle="pill" href="#create">Create</a></li>
</ul>

<!-- SEARCH PRODUCT -->
<div class="tab-content">
{include file='resources/users_search.tpl'}
{include file='resources/users_create.tpl'}
</div>

<!-- FINISH -->
</div>
</div>
</div>

<!-- CONTENT SELECTOR -->
{literal}
<script>
$(document).ready(function(){
  {/literal}
  {if $pageid == "1"} activaTab('search');
  {else} activaTab('create');
  {/if}
  {literal}
});
</script>
{/literal}

</body>
</html>