<!-- CREATE PANEL -->
<div id="create" class="tab-pane fade">
<h3>Create new merchant</h3>

<!-- CREATE FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/merchants/createNewMerchant">
<div class="form-group">
<div class="has-error">

	<p class="bg-danger" style="padding: 10px 10px 10px 10px">
	<label for="merchant_company">Company:</label>
	<input type="text" class="form-control" id="merchant_company" placeholder="Company name" name="merchant_company" value="{$inputdata.company}"> 
	<br><br>
	<label for="merchant_delivery">Delivery:</label>
	<input type="text" class="form-control" id="merchant_delivery" placeholder="Delivery days" name="merchant_delivery" value="{$inputdata.delivery}">
	<br><br>
	<label for="merchant_website">Website:</label>
	<input type="text" class="form-control" id="merchant_website" placeholder="Website" name="merchant_website" value="{$inputdata.website}"> 
	</p>

	<!-- ACM SELECTION -->
	<p class="bg-warning" style="padding: 10px 10px 10px 10px">
	<label for="merchant_acm">Merchant ACM:</label>
	<select class="form‐control" id="merchant_acm" name="merchant_acm">
		<option value="0">Select ACM</option>
		{foreach from=$acms item=acm}
	  		<option value="{$acm.ID}" {if $inputdata.user_id == $acm.ID}selected{/if}>{$acm.LOGIN}</option>
		{/foreach}
	</select>
	</p>

<!-- SUBMIT BUTTON -->
<button type="submit" class="btn btn-primary">Submit</button>
</div>
</div>
</form>
</div>