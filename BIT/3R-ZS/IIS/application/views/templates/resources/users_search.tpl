<!-- SEARCH USER PANEL -->
<div id="search" class="tab-pane fade in active"> 
<h2>Search for the user</h2>

<!-- SEARCH USER FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/users/show/1">
<div class="form-group">
<input type="text" class="form-control" id="seach_user_id" placeholder="User id" name="seach_user_id" value="{$SearchByID}"> 
<input type="text" class="form-control" id="seach_user_login" placeholder="User login" name="seach_user_login" value="{$SearchByLogin}">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER -->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>LOGIN</th>
<th>NAME</th>
<th>SURNAME</th>
<th>PERMISSIONS</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY -->
<tbody>
{foreach from=$users item=user}
<tr class="success">
<td>{$user.ID}</td>
<td>{$user.LOGIN}</td>
<td>{$user.PERS_NAME}</td>
<td>{$user.SURNAME}</td>
<td>{$user.PERM_NAME}</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="{$indexpath}/users/userDetail/{$user.ID}"></a>&nbsp;&nbsp;
<a class="glyphicon glyphicon-trash" aria-hidden="true" href="{$indexpath}/users/deleteUser/{$user.ID}" ></a>
</td>

</tr>
{/foreach}
</tbody>
</table>
</div>
</div>