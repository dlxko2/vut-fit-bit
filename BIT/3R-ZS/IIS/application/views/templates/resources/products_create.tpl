<!-- CREATE PRODUCT -->
<div id="create" class="tab-pane fade">
<h3>Create new product 
{if $ismerch} as {$merchant.COMPANY} {/if}
</h3>
<br>

  
<!-- CREATE FORM BASIC -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/products/createNewProduct">
<div class="form-group">
<div class="has-error">
	
	<!-- NAME -->
	<p class="bg-danger" style="padding: 10px 10px 10px 10px">
	<label for="product_name">Name:</label>
	<input type="text" class="form-control" id="product_name" placeholder="Name" name="product_name" value="{$inputdata.Name}"> 
	
	<!-- COUNT -->
	<label for="product_count">Count:</label>
	<input type="text" class="form-control" id="product_count" placeholder="Count" name="product_count" value="{$inputdata.Count}">  
	<br><br>

	<!-- BRAND -->
	<label for="product_brand">Brand:</label>
	<input type="text" class="form-control" id="product_brand" placeholder="Brand" name="product_brand" value="{$inputdata.Brand}">
	
	<!-- PRICE -->
	<label for="product_price">Price:</label>
	<input type="text" class="form-control" id="product_price" placeholder="Price" name="product_price" value="{$inputdata.Price}"> 
	</p> 

	<!-- TYPE -->
	<p class="bg-warning" style="padding: 10px 10px 10px 10px">
	<label for="product_type">Type:</label>
	<select class="form‐control" id="product_type" name="product_type">
  		<option value="0">Select type</option>
  		<option value="1" {if $inputdata.ProductType == "1"}selected{/if}>Hardware</option>
  		<option value="2" {if $inputdata.ProductType == "2"}selected{/if}>Software</option>
	</select>

	<!-- CATEGORY -->
	<span id="softwareC"  {if $inputdata.ProductType != "2"} style="display:none" {/if}>
		<label for="product_category">Category:</label>
		<select class="form‐control" id="product_category" name="product_category">
		  <option value="0">Select category</option>
		  {foreach from=$categories item=category}
		  {if $category.TYPE == "S"}
		  	<option value="{$category.ID}" {if $inputdata.Category == $category.ID}selected{/if}>{$category.NAME}</option>
		  {/if}
		  {/foreach}
		</select>
	</span>
	<span id="hardwareC" {if $inputdata.ProductType != "1"} style="display:none" {/if}>
		<label for="product_category">Category:</label>
		<select class="form‐control" id="product_category" name="product_category">
		  <option value="0">Select category</option>
		  {foreach from=$categories item=category}
		  {if $category.TYPE == "H"}
		  	<option value="{$category.ID}" {if $inputdata.Category == $category.ID}selected{/if}>{$category.NAME}</option>
		  {/if}
		  {/foreach}
		</select>
	</span>

	<!-- MERCHANT -->
	{if $ismerch}
		<input type="hidden" name="product_merchant" id="product_merchant" value="{$merchant.ID}">
	{else}
		<label for="product_merchant">Merchant:</label>
		<select class="form‐control" id="product_merchant" name="product_merchant">
		<option value="0">Select merchant</option>
		{foreach from=$merchants item=merchant}
  			<option value="{$merchant.ID}" {if $inputdata.Merchant == $merchant.ID}selected{/if}>{$merchant.COMPANY}</option>
		{/foreach}
		</select>
	{/if}
	</p>

	<!-- WEIGHT -->
	<div id="hardware" {if $inputdata.ProductType != "1"} style="display:none" {/if}>
	<p class="bg-info" style="padding: 10px 10px 10px 10px">
	<label for="product_weight">Weight:</label>
	<input type="text" class="form-control" id="product_weight" placeholder="Weight" name="product_weight" value="{$inputdata.Weight}"> 
	
	<!-- SIZE -->
	<label for="product_size">Size:</label>
	<input type="text" class="form-control" id="product_size" placeholder="Size" name="product_size" value="{$inputdata.Size}"> 
	</p>
	</div>

	<!-- LICENCE -->
	<div id="software" {if $inputdata.ProductType != "2"} style="display:none" {/if}>
	<p class="bg-info" style="padding: 10px 10px 10px 10px">
	<label for="product_licence">Licence:</label>
	<input type="text" class="form-control" id="product_licence" placeholder="Licence" name="product_licence" value="{$inputdata.Licence}"> 

	<!-- CONFIGURATION -->
	<label for="product_config">Configuration:</label>
	<input type="text" class="form-control" id="product_config" placeholder="Config" name="product_config" value="{$inputdata.Config}"> 
	</p>
	</div>

	<!-- DESCRIPTION -->
	<p class="bg-success" style="padding: 10px 10px 10px 10px">
	<label for="product_description">Description:</label>
	<textarea class="form-control" id="product_description" rows="4" placeholder="Description" name="product_description">{$inputdata.Description}
	</textarea>
	</p>

	<button type="submit" class="btn btn-primary">Submit</button>

</div>
</form>

</div>
</div>