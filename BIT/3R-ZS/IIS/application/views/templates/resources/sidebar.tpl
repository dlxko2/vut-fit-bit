<!-- SIDEBAR MENU -->
<div id="wrapper">
<div id="sidebar-wrapper">
<ul class="sidebar-nav">
  
<!-- SIDEBAR HEADER -->
<li class="sidebar-brand"><a href="#">

<!-- SIDEBAR NAME -->
{if $isadm}<b><font color="orange">Admin's</font></b>
{elseif $ismerch}<b><font color="orange">Merchant's</font></b>
{else}<b><font color="orange">Customer's</font></b>
{/if}

<font color="green">BackOffice</font></a></li>



<!-- ADMIN PANEL -->
{if $isadm}
<li><a href="{$indexpath}/dashboard/show">Dashboard</a></li>
<li><a href="{$indexpath}/users/show/1">Users</a></li>
<li><a href="{$indexpath}/merchants/show/1">Merchants</a></li>
<li><a href="{$indexpath}/categories/show/">Categories</a></li>
{/if}

<!-- CUSTOMER PANNEL -->
{if $isadm == false && $ismerch == false}
<li><a href="{$indexpath}/trashes/show">Trash <font color="#3399FF"><b>({$trashcount})</b></font></a></li>
{/if}

<!-- PROFILE -->
<li><a href="{$indexpath}/users/userDetail/{$userID}">
Profile
</a></li>

<!-- PRODUCTS -->  
<li><a href="{$indexpath}/products/show/1">
List Products
</a></li>
  
<!-- FEEDBACKS -->
<li><a href="{$indexpath}/feedbacks/show">
{if $isadm == true}Feedbacks
{else} My Feedbacks {/if}
</a></li>

<!-- ORDERS -->
<li><a href="{$indexpath}/orders/show">
{if $isadm == true}Orders
{else} My Orders {/if}
</a></li>

<!-- LOGOUT -->
<li><a href="{$indexpath}/login/outdex">
Logout
</a></li>

</ul>
</div>