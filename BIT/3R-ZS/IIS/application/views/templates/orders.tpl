<!-- ORDERS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
</head>
<body>

<!-- SIDEBAR MENU -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="{$basepath}/images/orders.jpg" alt="..." class="img-rounded" style="width:100%">
<br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>
  
<!-- SEARCH PANEL -->
<div class="tab-content">
<h2>Search for the order</h2>

<!-- SEARCH FORM -->
<form class="form-inline" role="form" method="post" accept-charset="utf-8" action="{$indexpath}/orders/show">
	<div class="form-group">
<input type="text" class="form-control" id="search_order_id" placeholder="Order id" name="search_order_id" value="{$SearchByID}"> 
<input type="text" class="form-control" id="search_order_login" placeholder="Customer login" name="search_order_login" value="{$SearchByLogin}">
</div>
<button type="submit" class="btn btn-primary" name="Search" value="0">Submit</button>
<button type="submit" class="btn btn-info" name="Search" value="1">Extract</button>
</form>

<!-- SEARCH TABLE HEADER-->
<div class="span3">
<table class="table">
<thead>
<tr>
<th>ID</th>
<th>LOGIN</th>
<th>ASIGNEE</th>
<th>PRICE</th>
<th>PAYED</th>
<th>EXPANDED</th>
<th>DETAILS</th>
</tr>
</thead>

<!-- SEARCH TABLE BODY-->
<tbody>
{foreach from=$orders item=order}
{if $order.EXPANDED == "Y"}
<tr class="success">
{elseif $order.PAYED == "Y"}
<tr class="info">
{else}
<tr class="danger">
{/if}
<td>{$order.ID}</td>
<td>{$order.LOGIN}</td>

{if $order.EMPLOYEE_ID}
	{foreach from=$users item=user}
	{if $user.ID == $order.EMPLOYEE_ID}
		<td>{$user.LOGIN}</td>
	{/if}
	{/foreach}
{else}
<td>Not assigned</td>
{/if}

<td>{$order.PRICE}</td>
<td>{$order.PAYED}</td>
<td>{$order.EXPANDED}</td>

<!-- OPTIONS -->
<td width="5%">
<a class="glyphicon glyphicon-search" aria-hidden="true" href="{$indexpath}/orders/orderDetail/{$order.ID}"></a>
{if $isadm || $ismerch}
<a class="glyphicon glyphicon-eur" aria-hidden="true" href="{$indexpath}/orders/orderPay/{$order.ID}/{$userID}"></a>
<a class="glyphicon glyphicon-road" aria-hidden="true" href="{$indexpath}/orders/orderShip/{$order.ID}/{$userID}"></a>
{/if}
</td>
</tr>
{/foreach}
</tbody>
</table>
</div>
</div>

<!-- FINISH -->
</div>
</div>
</div>
</div>
</body>
</html>