<!-- PRODUCTS HEADER -->
<html>
<head>
    <link href="{$basepath}/css/bootstrap.css" rel="stylesheet">
    <link href="{$basepath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$basepath}/css/simple-sidebar.css" rel="stylesheet">
    <link href="{$basepath}/mycss/products.css" rel="stylesheet">
    <script src="{$basepath}/jquery.js"></script>
    <script src="{$basepath}/myjs/products.js"></script>
    <script src="{$basepath}/js/bootstrap.min.js"></script>
</head>
<body>

<!-- SIDEBAR -->
{include file='resources/sidebar.tpl'}

<!-- PAGE CONTENT -->
<div id="page-content-wrapper">
<div class="container-fluid">

<!-- IMAGE -->
<img src="{$basepath}/images/products.jpg" alt="..." class="img-rounded" style="width:100%">
<br><br>

<!-- ERRORS -->
<div class="alert alert-danger" role="alert" style="display:{if $errors == ""}none{/if}">
{$errors} 
</div>

<!-- NAVIGATION BAR -->  
<ul class="nav nav-tabs">
<li><a data-toggle="pill" href="#search">Find</a></li>
{if $isadm == true || $ismerch == true}
<li><a data-toggle="pill" href="#create">Create</a></li>
{/if}
</ul>

<!-- CONTENT -->
<div class="tab-content">
{include file='resources/products_search.tpl'}
{if $isadm == true || $ismerch == true}
{include file='resources/products_create.tpl'}
{/if}
</div>
</div>
</div>

<!-- CONTENT SELECTOR + TYPE SELECTOR -->
{literal}
<script>
 $("#product_type").change(function(){changeboxes();});
 $(document).ready(function(){
    {/literal}
    {if $pageid == "1"} activaTab('search');
    {else} activaTab('create');
    {/if}
    {literal}
});
</script>
{/literal}

</body>
</html>