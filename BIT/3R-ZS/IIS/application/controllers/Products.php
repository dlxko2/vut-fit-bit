<?php

class products extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/products_model');
        $this->load->model('database/categories_model');
        $this->load->model('checks/products_check_model');
        $this->load->model('database/merchants_model');
        $this->load->model('database/trashes_model');
        $this->load->model('database/feedbacks_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");
    }
	
	/* The main function to show products 
	 * @parameter - Page ID (show/create) 
	 * @return - boolean with success */
	public function show($PageID)
	{
		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Load previous input datas
		$InputData = $this->session->userdata('productInput');
		$this->session->unset_userdata('productInput');

		// Process post datas
		$Export = $this->input->post('Search');
    	$SearchByID = $this->input->post('seach_product_id');
    	$SearchByName = $this->input->post('seach_product_name');
    	$Merchant = $this->session->userdata('merchant');

    	// Check post datas
    	$Errors = "";
    	if (!empty($SearchByID) && !is_numeric($SearchByID))
    		$Errors .= "Error in search product id!<br>";
    	
    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/products/show/1', 'location');
    		return false;
    	}
    	
		// Load model datas
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Merchants = $this->merchants_model->getRequestedMerchants();
		$Categories = $this->categories_model->getAllCategories();
		
		// Call model to get products for each role
		if (!$this->isADM() && !$this->isMERCH())
			$Products = $this->products_model->getRequestedProducts($SearchByID, $SearchByName, true);
		else if ($this->isMERCH())
			$Products = $this->products_model->getRequestedProducts($SearchByID, $SearchByName, false, $Merchant);
		else
			$Products = $this->products_model->getRequestedProducts($SearchByID, $SearchByName, false);

		// Process Smarty
		$this->mysmarty->assign('SearchByID', $SearchByID);
		$this->mysmarty->assign('SearchByName', $SearchByName);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('inputdata', $InputData);
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('merchant', $Merchant);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('pageid', $PageID);
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('merchants',$Merchants);
		$this->mysmarty->assign('categories',$Categories);
		$this->mysmarty->assign('products',$Products);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process exports
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/products.tpl');
			CreatePDF($ExportHTML, 'Products.pdf', true);
		}
		else $this->mysmarty->display('products.tpl');
	}

	/* Function to create the new product 
	 * @parameter - void 
	 * @return - boolean with success */
	public function createNewProduct()
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');

		// Process input datas
		$NewProductInfo = array
		(
			'ProductType' => $this->input->post('product_type'),
			'Weight' 	  => $this->input->post('product_weight'),
			'Size' 		  => $this->input->post('product_size'),
			'Licence' 	  => $this->input->post('product_licence'),
			'Config' 	  => $this->input->post('product_config'),
			'Name' 		  => $this->input->post('product_name'),
			'Description' => $this->input->post('product_description'),
			'Count'		  => $this->input->post('product_count'),
			'Brand'		  => $this->input->post('product_brand'),
			'Price'		  => $this->input->post('product_price'),
    		'Category'	  => $this->input->post('product_category'),
    		'Merchant'	  => $this->input->post('product_merchant')
    	);

		// Save to session actual input
		$this->session->set_userdata(array('productInput' => $NewProductInfo));

		// Check input datas in model
		$Errors = $this->products_check_model->checkCreateData($NewProductInfo);
    	
    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/products/show/2', 'location');
  			return false;
    	}

    	// Create new product
    	$Result = $this->products_model->createNewProduct($NewProductInfo);
    	redirect('/products/show/1', 'location');
	}

	/* Function to delete product 
	 * @parameter - Product ID to delete
	 * @return - void */
	public function deleteProduct($ProductId)
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');

		// FK FEEDBACKS
		if (!$this->feedbacks_model->fkProduct($ProductId))
		{
			$this->session->set_userdata('errors', "Product used in feedback record!");
			redirect('/products/show/1', 'location');
		}

		// FK TRASH ITEMS
		if (!$this->trashes_model->fkProduct($ProductId))
		{
			$this->session->set_userdata('errors', "Product used in trash items record!");
			redirect('/products/show/1', 'location');
		}

		$Result = $this->products_model->deleteProduct($ProductId);
        redirect('/products/show/1', 'location');
	}

	/* Function to show product details 
	 * @parameter - Product ID to show details
	 * @return - void */
	public function productDetail($ProductId)
	{		
		// Call model functions
		$Merchant = $this->session->userdata('merchant');
		$Export = $this->input->post('Search');
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Result = $this->products_model->getRequestedProduct($ProductId);

		// Process smarty
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('merchant', $Merchant);
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('product_info',$Result);
		$this->mysmarty->assign('product_id',$ProductId);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process exports
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/products_detail.tpl');
			CreatePDF($ExportHTML, 'ProductsDetail.pdf', true);
		}
		else $this->mysmarty->display('products_detail.tpl');
	}

	/* Function to edit the selected product
	 * @parameter - Product ID of product to edit
	 * @return - void just redirect */
	public function editProduct($ProductId)
	{
		if (!$this->isMERCH() && !$this->isADM()) redirect('/login/noAccess', 'location');
		
		// Process input datas
		$EditProductInfo = array
		(
			'Weight' 	  => trim($this->input->post('edit_weight')),
			'Size' 		  => trim($this->input->post('edit_size')),
			'Licence' 	  => trim($this->input->post('edit_licence')),
			'Config' 	  => trim($this->input->post('edit_config')),
			'Name' 		  => trim($this->input->post('edit_name')),
			'Description' => trim($this->input->post('edit_description')),
			'Count'		  => trim($this->input->post('edit_count')),
			'Brand'		  => trim($this->input->post('edit_brand')),
			'Price'		  => trim($this->input->post('edit_price'))
    	);

		// Check for errors
    	$Errors = $this->products_check_model->checkEditData($EditProductInfo);

    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/products/show/1', 'location');
  			return false;
    	}

    	// Edit the selected product
    	$Result = $this->products_model->editProduct($EditProductInfo, $ProductId);
    	redirect('/products/show/1', 'location');
	}
}

?>
