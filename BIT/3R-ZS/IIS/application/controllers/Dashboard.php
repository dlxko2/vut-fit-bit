<?php

class dashboard extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/trashes_model');
        $this->load->model('database/dashboard_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");

		// No access for any if not ADM
		if (!$this->isADM())
			redirect("login/noAccess");
    }
	
	/* The main function showing dashboard 
	 * @parameter: void
	 * @return: void */
	public function show()
	{
		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Get datas from models
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$MonthPayed = $this->dashboard_model->getMonthPayed();

		// Process Smarty
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('monthPayed',$MonthPayed);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));
		$this->mysmarty->display('dashboard.tpl');
	}
}

?>
