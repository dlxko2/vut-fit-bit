<?php

class merchants extends CI_Controller
{
	/* Constructor */
	function __construct()
    {
        parent::__construct();
        $this->load->model('database/products_model');
        $this->load->model('database/merchants_model');
        $this->load->model('database/trashes_model');

        // Check if logged
		if ($this->session->userdata('login') == false)
			redirect("login/index/0");

		if (!$this->isADM())
			redirect("login/noAccess");
    }
	
	/* The function which shows the merchants 
	 * @parameter - Page ID (show/create) 
	 * @return - void */
	public function show($PageID)
	{
		// Load previous input datas
		$InputData = $this->session->userdata('merchantInput');
		$this->session->unset_userdata('merchantInput');

		// Process errors 
		$DisErrors = $this->session->userdata('errors');
		$this->session->unset_userdata('errors');

		// Process input 
		$Export = $this->input->post('Search');
    	$SearchByID = $this->input->post('seach_merchant_id');
    	$SearchByName = $this->input->post('seach_merchant_company');

    	// Check post datas
    	$Errors = "";
    	if (!empty($SearchByID) && !is_numeric($SearchByID))
    		$Errors .= "Error in search for merchant id!<br>";
    	
    	// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/merchants/show/1', 'location');
    		return false;
    	}

		// Load models
		$TrashCount = $this->trashes_model->getTrashCount($this->getUSR());
		$Merchants = $this->merchants_model->getRequestedMerchants($SearchByID, $SearchByName);
		$Acms = $this->merchants_model->getAllAcms();

		// Process Smarty
		$this->mysmarty->assign('SearchByID', $SearchByID);
		$this->mysmarty->assign('SearchByName', $SearchByName);
		$this->mysmarty->assign('userID', $this->getUSR());
		$this->mysmarty->assign('inputdata', $InputData);
		$this->mysmarty->assign('trashcount', $TrashCount);
		$this->mysmarty->assign('ismerch', $this->isMERCH());
		$this->mysmarty->assign('isadm', $this->isADM());
		$this->mysmarty->assign('pageid', $PageID);
		$this->mysmarty->assign('errors', $DisErrors);
		$this->mysmarty->assign('merchants',$Merchants);
		$this->mysmarty->assign('acms',$Acms);
		$this->mysmarty->assign('basepath',base_url('assets/'));
		$this->mysmarty->assign('indexpath',base_url('index.php/'));

		// Process export
		if ($Export == '1') 
		{
			$ExportHTML = $this->mysmarty->fetch('export/merchants.tpl');
			CreatePDF($ExportHTML, 'Merchants.pdf', true);
		}
		else $this->mysmarty->display('merchants.tpl');
	}

	/* Function to create new merchant 
	 * @parameter - void 
	 * @return - boolean with success */
	public function createNewMerchant()
	{
		// Get input datas from post
		$MerchantInfo = array 
    	(
    		'company' => $this->input->post('merchant_company'),
            'delivery' => $this->input->post('merchant_delivery'),
            'website' => $this->input->post('merchant_website'),
            'user_id' => $this->input->post('merchant_acm')
    	);

    	// Save to session actual input
		$this->session->set_userdata(array('merchantInput' => $MerchantInfo));

    	// Check input datas in model
		$Errors = $this->merchants_model->checkCreateData($MerchantInfo);

		// Resolve errors
    	if (!empty($Errors)) 
    	{
    		$this->session->set_userdata('errors', $Errors);
    		redirect('/merchants/show/2', 'location');
  			return false;
    	}

    	// Create new merchant
		$this->merchants_model->createNewMerchant($MerchantInfo);
    	redirect('/merchants/show/1', 'location');
	}

	public function deleteMerchant($MerchantId)
	{
		// FK PRODUCTS
		if (!$this->products_model->fkMerchant($MerchantId))
		{
			$this->session->set_userdata('errors', "Merchant used in product record!");
			redirect('/merchants/show/1', 'location');
		}

		$this->merchants_model->deleteMerchant($MerchantId);
        redirect('/merchants/show/1', 'location');
	}
}

?>
