<?php

class Users_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to get details for selected user
     * @parameter - ID of the user for details
     * @return - Array with the user infos */
    public function getRequestedUser($UserID) 
    {
        // Initialize function variables
        $this->db->select('us.id, pers.id pers_id, us.login, pe.name perm_name, pers.name pers_name, pers.surname, pers.telephone, pers.mail, pers.address');
        $this->db->from('users us');
        $this->db->join('permissions pe', 'us.permission_id = pe.id');
        $this->db->join('persons pers', 'us.person_id = pers.id');
        $this->db->where('us.id', $UserID);
        $query = $this->db->get();
        $ResultQ = $query->result();
        $Result = array();

        // Create result array
        $Result['ID'] = $ResultQ[0]->id;
        $Result['PERS_ID'] = $ResultQ[0]->pers_id;
        $Result['LOGIN'] = $ResultQ[0]->login;
        $Result['PERM_NAME'] = $ResultQ[0]->perm_name;
        $Result['PERS_NAME'] = $ResultQ[0]->pers_name;
        $Result['SURNAME'] = $ResultQ[0]->surname;
        $Result['TELEPHONE'] = $ResultQ[0]->telephone;
        $Result['MAIL'] = $ResultQ[0]->mail;
        $Result['ADDRESS'] = $ResultQ[0]->address;
        return $Result;
    }

    /* The main function to show all users depending on conditions
     * @parameter - User ID to search by 
     * @parameter - User login to search by 
     * @return - Array with datas for users */
    public function getRequestedUsers($SearchByID = "", $SearchByLogin = "") 
    {
        // Prepare sql query
        $this->db->select('us.id, us.login, pe.name perm_name, pers.name pers_name, pers.surname');
        $this->db->from('users us');
        $this->db->join('permissions pe', 'us.permission_id = pe.id');
        $this->db->join('persons pers', 'us.person_id = pers.id');

        // Process where condition
    	if (!empty($SearchByID))
            $this->db->where('us.id', $SearchByID);
    	if (!empty($SearchByLogin))
            $this->db->like('us.login', $SearchByLogin);
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
		   	(
		   		'ID' => $row->id,
		   		'LOGIN' => $row->login,
		   		'PERM_NAME' => $row->perm_name,
		   		'PERS_NAME' => $row->pers_name,
		   		'SURNAME' => $row->surname
		   	));
       	}
       	return $Result;
    }

    /* Function to create new user
     * @parameter - Informations about person table
     * @parameter - Informations about user table 
     * @return - void */
    public function createNewUser($PersonInfo, $UserInfo) 
    {
    	// Initialize variables
    	$SpecifyInfo = array();
    	$PersonID = 0;
    	
    	// Proces specified query
    	$ResultPerson = $this->db->insert('persons', $PersonInfo);
    	$PersonID = $this->db->insert_id();
    	
    	// Update persons info
    	$UserInfo['person_id'] = $PersonID;
    	
    	// Process user query
    	$ResultUser = $this->db->insert('users', $UserInfo);
    }

    /* Function to get all permissions 
     * @parameter - void
     * @return - Array with all permissions */
    public function getAllPermissions()
    {
        // Process query and prepare array
        $query = $this->db->get('permissions');
		$Result = array();

        // Fill the output result array
		foreach ($query->result() as $row)
		{
		    array_push($Result, array
			(
		   		'ID' => $row->id,
		   		'NAME' => $row->name,
		   	));
       	}
       	return $Result;
    }

    /* Function to delete the specified user 
     * @parameter - User ID to delete 
     * @return - void */
    public function deleteUser($UserID) 
    {
        $this->db->delete('users', array('id' => $UserID));
        $this->db->delete('persons', array('user_id' => $UserID));
    }

    public function editUser($PersonInfo, $UserInfo, $UserId, $PersonID)
    {
        // If not empty update info update it in database
        if(!empty($PersonInfo['Name'])) $this->db->update('persons', array('name' => $PersonInfo['Name']), "id = " . $PersonID);
        if(!empty($PersonInfo['Surname'])) $this->db->update('persons', array('surname' => $PersonInfo['Surname']), "id = " . $PersonID);
        if(!empty($PersonInfo['Mail'])) $this->db->update('persons', array('mail' => $PersonInfo['Mail']), "id = " . $PersonID);
        if(!empty($PersonInfo['Telephone'])) $this->db->update('persons', array('telephone' => $PersonInfo['Telephone']), "id = " . $PersonID);
        if(!empty($PersonInfo['Address'])) $this->db->update('persons', array('address' => $PersonInfo['Address']), "id = " . $PersonID);
        if(!empty($UserInfo['Permission'])) $this->db->update('users', array('permission_id' => $UserInfo['Permission']), "id = " . $UserId);
        return true;
    }
}

?>