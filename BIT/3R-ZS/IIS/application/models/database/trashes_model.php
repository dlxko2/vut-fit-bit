<?php

class Trashes_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* The main core function to get datas about feedbacks 
     * @parameter1 - Product name
     * @parameter2 - User login
     * @parameter3 - Product company ID 
     * @parameter4 - Feedback commenter user ID 
     * @return - array with result datas */
    public function getRequestedItems($UserID, $ActiveTrash) 
    {
        // Create base SQL query 
        $this->db->select('merch.delivery, trit.id, pro.count, pro.name, pro.price, pro.stock, pro.vat');
        $this->db->from('trash_items trit');
    	$this->db->join('trashes tre', 'trit.trash_id = tre.id');
        $this->db->join('users usr', 'tre.user_id = usr.id');
        $this->db->join('products pro', 'trit.product_id = pro.id');
        $this->db->join('merchants merch', 'pro.merchant_id = merch.id');
        $this->db->where(array('usr.id' => $UserID, 'trit.trash_id' => $ActiveTrash));      
    	
    	// Process the query and create result
    	$query = $this->db->get();
		$Result = array();

        // Create price store variables
        $TotalPrice = 0;
        $TotalPriceVAT = 0;
        $TotalCount = 0;
        $Delivery = 0;

		// Fill result with output datas
		foreach ($query->result() as $row)
		{
            // Count prices
            $TotalCount = $TotalCount + 1;
            $TotalPrice = $TotalPrice + intval($row->price);
            $TotalPriceVAT = $TotalPriceVAT + intval($row->price) + intval($row->vat);

            if ($Delivery < $row->delivery)
                $Delivery = $row->delivery;

            // Push infos for item
		    array_push($Result, array
		   	(
		   		'ID' => $row->id,
		   		'COUNT' => $row->count,
		   		'NAME' => $row->name,
		   		'PRICE' => $row->price,
                'STOCK' => $row->stock,
                'DELIVERY' => $row->delivery,
                'VAT' => $row->vat
		   	));
       	}

        // Save prices into output
        $Result['TOTALC'] = $TotalCount;
        $Result['TOTAL'] = $TotalPrice;
        $Result['TOTALV'] = $TotalPriceVAT;
        $Result['TOTALD'] = $Delivery;
       	return $Result;
    }

    public function getTrashCount($UserID) 
    {
        $ActiveTrash = $this->getActiveTrash($UserID);
        if ($ActiveTrash < 1) return 0;
        
        // Create base SQL query 
        $this->db->from('trash_items trit');
        $this->db->join('trashes tre', 'trit.trash_id = tre.id');
        $this->db->where(array('tre.user_id' => $UserID, 'trit.trash_id' => $ActiveTrash));      
        return $this->db->count_all_results();
    }

    public function getActiveTrash($UserID)
    {
        $this->db->select('id');
        $this->db->from('trashes');
        $this->db->where(array('status' => 'INP', 'user_id' => $UserID));  
        $query = $this->db->get();
        $ResultQ = $query->result();
        return ($ResultQ == false ? false : $ResultQ[0]->id);
    }

    public function newActiveTrash($UserID)
    {
        // Create info about insert
        $TrashInfo = array
        (
            'user_id' => $UserID,
            'status' => 'INP'
        );

        // Insert into table and get id 
        $Result = $this->db->insert('trashes', $TrashInfo);
        return $this->db->insert_id();
    }

    public function addItemToTrash($TrashID, $ProductID)
    {
        // Create info about insert
        $ItemTrashInfo = array
        (
            'product_id' => $ProductID,
            'trash_id' => $TrashID
        );

        // Insert into table and get id 
        $Result = $this->db->insert('trash_items', $ItemTrashInfo);
        return true;
    }

    public function getTrashPrice($ActiveTrash)
    {
        $this->db->select("SUM(pro.price) + SUM(pro.vat) trash_total", FALSE);
        $this->db->from('trash_items trit');
        $this->db->join('products pro', 'trit.product_id = pro.id');
        $this->db->where('trit.trash_id', $ActiveTrash);  
        $query = $this->db->get();
        $ResultQ = $query->result();
        return $ResultQ[0]->trash_total;
    }

    public function createNewOrder($ActiveTrash, $UserID, $TrashPrice)
    {
        // Create info about insert
        $OrderInfo = array
        (
            'expanded' => 'N',
            'user_id' => $UserID,
            'payed' => 'N',
            'trash_id' => intval($ActiveTrash),
            'price' => $TrashPrice
        );

        // Insert into table and get id 
        $Result = $this->db->insert('orders', $OrderInfo);
        return true;
    }

    public function closeTrash($ActiveTrash)
    {
        $this->db->where('id', $ActiveTrash);
        $this->db->update('trashes', array('status' => 'CRE'));
        return true;
    }

    public function deleteItemFromTrash($ItemID)
    {
        $this->db->delete('trash_items', array('id' => $ItemID)); 
    }

    public function removeFromCount($ProductIDArray)
    {
        $this->db->where_in('id', $ProductIDArray);
        $this->db->set('count', 'count-1', FALSE);
        $this->db->update('products');
    }

    public function getProductsIDsFromTrash($ActiveTrash)
    {
        $this->db->select('product_id');
        $this->db->from('trash_items');
        $this->db->where('trash_id', $ActiveTrash);
        $query = $this->db->get();

        $ProductsArray = array();

        foreach ($query->result() as $row)
            array_push($ProductsArray, intval($row->product_id));

        return $ProductsArray;
    }

    public function fkUser($UserID)
    {
        $query = $this->db->query('SELECT * FROM trashes WHERE user_id =' . $UserID);
        return ($query->num_rows() > 0 ? false : true);
    }

    public function fkProduct($ProductID)
    {
        $query = $this->db->query('SELECT * FROM trash_items WHERE product_id =' . $ProductID);
        return ($query->num_rows() > 0 ? false : true);
    }
}

?>