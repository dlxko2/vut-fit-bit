<?php

class Login_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to get user datas during loggin in 
     * @parameter1 - User login 
     * @parameter2 - User password 
     * @return - false if not found else result data array */
    public function getUser($User, $Password) 
    {
        // Initialize function variables
        $query = $this->db->get_where('users', array('login' => $User, 'password' => md5($Password)));
        $ResultQ = $query->result();
        
        // Check if user found
        if ($ResultQ == false)
            return false;

        // Create result array
        $Result = array();
        $Result['ID'] = $ResultQ[0]->id;
        $Result['PERSON'] = $ResultQ[0]->person_id;
        $Result['PERMISSION'] = $ResultQ[0]->permission_id;
        $Result['LOGIN'] = $ResultQ[0]->login;
        return $Result;
    }

    public function getUsername($User) 
    {
        $query = $this->db->get_where('users', array('login' => $User));
        $ResultQ = $query->result();
        return ($ResultQ == false ? false : true);
   }

    /* Function to get user datas during loggin in about merchant 
     * @parameter1 - User login 
     * @return - false if not found else result data array */
    public function getMerchant($User) 
    {
        // Initialize function variables
        $this->db->select('id, company');
        $this->db->where('user_id', $User);
        $query = $this->db->get('merchants');
        $ResultQ = $query->result();
        
        // Check if user found
        if ($ResultQ == false)
            return false;

        // Create result array
        $Result = array();
        $Result['ID'] = $ResultQ[0]->id;
        $Result['COMPANY'] = $ResultQ[0]->company;
        return $Result;
    }
}

?>