<?php

class Categories_check_model extends CI_Model {

    /* Constructor */
    function __construct()
    {
        parent::__construct();
    }

    /* Function to check datas for user creation 
     * @parameter - Informations about person table
     * @parameter - Informations about user table 
     * @return - String with errors */
    public function checkCreateData($CategoryInfo)
    {
        $Errors = "";
        
        if (empty($CategoryInfo['name']) || strlen($CategoryInfo['name']) < 5)
            $Errors .= "Error in create category name!<br>";
        if (empty($CategoryInfo['type']) || $CategoryInfo['type'] == "0")
            $Errors .= "Error in create category type!<br>";
        return $Errors;
    }
}