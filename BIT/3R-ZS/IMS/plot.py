#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import datetime
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange

def strings2dates(strDatesArr):
    for s in strDatesArr:
        yield datetime.datetime.strptime(s, "%d.%m.%Y").date()

def ldData(filename):
    with open(filename) as f:
        data = f.read()
    data = list(filter(None, data.split('\n')))
    
    x = [float(row.split(' ')[0])/(24*60) for row in data]
    y = [float(row.split(' ')[1]) for row in data]
    return (x, y)


if __name__ == "__main__":
    attackersX =  list(strings2dates([#'30.08.2001',\
                                 '28.07.2015',\
                                 '29.07.2015',\
                                 '31.07.2015',\
                                 '03.08.2015',\
                                 '24.08.2015',\
                                 '02.10.2015',\
                                 '04.10.2015']))
    
    attackersY = [#44,
                  2000,1500,800,1700,2000,200,120 ]
    
    
    campersX =  list(strings2dates([#'15.09.2009',\
                                    '15.10.2014',\
                                    '24.10.2014',\
                                    '15.01.2015',\
                                    '15.06.2015',\
                                    '31.07.2015',\
                                    '19.10.2015',\
                                    '02.12.2015']))
    campersY = [#300,
                1450,2300,600,300,5000,6000,4500]
    
    campersAproxX_str = []
    year = 2014
    for m in range(24):
        if m == 12:
            year +=1 
        campersAproxX_str.append('15.%d.%d' % ((m % 12 +1), year)) 

    
    campersAproxX = list(strings2dates(campersAproxX_str))
    campersAproxY = [m for m in range(24)]
    
    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1)
    fig.tight_layout()
    
    
    ax1.set_title("Train delays")    
    ax1.set_xlabel('day')
    ax1.set_ylabel('time[min]')
    d1 = ldData("delays.dat")
    ax1.plot(d1[0], d1[1], c='r', label='delay')
    #leg = ax1.legend()
    
    ax2.set_title("Attack strength")    
    ax2.set_xlabel('day')
    ax2.set_ylabel('number of intruders')
    d2 = ldData("attackStrength.dat")
    ax2.plot(d2[0], d2[1], c='r', label='attack')
    
    ax3.set_title("Intruders cnt")    
    ax3.set_ylabel('man')
    ax3.plot_date(attackersX, attackersY, 'bo')
    ax3.xaxis.set_major_formatter(DateFormatter('%d.%m.%Y'))
    ax3.grid(True)
    ax3.set_xlim(attackersX[0], attackersX[-1])
    
    ax4.set_title("Camp population")    
    ax4.set_ylabel('man')
    ax4.autoscale_view()
    ax4.plot_date(campersX, campersY, '-')
    ax4.xaxis.set_major_formatter(DateFormatter('%d.%m.%Y'))
    ax4.grid(True)
    ax4.set_xlim(campersX[0], campersX[-1])
    
    ax5.set_title("Camp aproximation")    
    ax5.set_xlabel('time')
    ax5.set_ylabel('man')
    ax5.autoscale_view()
    ax5.plot_date(campersAproxX, campersAproxY, '-')
    ax5.xaxis.set_major_formatter(DateFormatter('%d.%m.%Y'))
    ax5.grid(True)
    ax5.set_xlim(campersAproxX[0], campersAproxX[-1])
    
    plt.show()
