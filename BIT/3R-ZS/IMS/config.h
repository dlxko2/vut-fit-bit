#pragma once

// time[min]

#define DAY               (24*60)

#define SIM_DURATION      (30*DAY)

#define TRAINS_CNT        30
#define TRAIN_MIN_SPACING 3
#define TRAIN_TRIP_LEN    35
#define TRAIN_DELAY_UK    30
#define TRAIN_INTERVAL    60

//=165/day
#define INTRUDER_INTERVAL 8.7
#define INVASION_INTERVAL abs(Normal(5, 2*DAY))

#define SECURITY_CNT            300
#define SECURITY_CHECK_INTERVAL 10
#define SECURITY_REACTION_TIME  Exponential(5)

#define INTRUDER_WILL_TRY_AGAIN (Random() <= 0.6)

#define SIM_START_DATE
