gnuplot <<- \n
        set xlabel "delay[min]"
        set ylabel "time[min]"
        set term png
        set output "delays.png"
        plot "delays.dat"
