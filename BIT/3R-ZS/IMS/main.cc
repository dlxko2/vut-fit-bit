#include <simlib.h>
#include "config.h"
#include <iostream>
#include <fstream>

Facility tunnelEntrance("Tunnel entrance");
//Store trains("Trains", TRAINS_CNT);
Stat trainDelay("Delay of trains");
Histogram intruderDetected("Intruder detected", 0, SIM_DURATION / 30, 30);
Histogram intruderDeported("Intruder deported", 0, SIM_DURATION / 30, 30);
//Facility invasion("This is trigger for all invaders to make an attack");
//Queue intruderHomeland(
//		"intruderHomeland- Place from intruders planing attacks");
Queue railway("railway - Place where intruders trying to get");
static double nextInvasionTime;
static int attackStrength = 0;

std::ofstream delaysFile;
std::ofstream attackStrengthFile;

class Trip: public Process {
	double designedTime;
	void Behavior() {
		designedTime = Time;
		//Enter(trains, 1); // take train in FR
		Seize(tunnelEntrance);
		delaysFile << designedTime << " " << (Time - designedTime) << std::endl;
		trainDelay(Time - designedTime);
		Wait(TRAIN_MIN_SPACING);
		Release(tunnelEntrance);
		Wait(TRAIN_TRIP_LEN - TRAIN_MIN_SPACING);
		//now in UK
		Wait(TRAIN_DELAY_UK);
		Wait(TRAIN_TRIP_LEN);
		//now back in FR
		//Leave(trains, 1);
	}
};

class Intruder: public Process {
	void Behavior() {
		Intruder_start: ;
		Wait(nextInvasionTime - Time);
		intruderDetected(Time);
		Seize(tunnelEntrance, 1);
		attackStrength += 1;
		Passivate();
		Release(tunnelEntrance);
		intruderDeported(Time);
		if (INTRUDER_WILL_TRY_AGAIN){
			Wait(INVASION_INTERVAL);
			goto Intruder_start;
		}
		// removed by security
	}
};

class IntruderGenerator: public Event {
	void Behavior() {
		(new Intruder())->Activate();
		Activate(Time + INTRUDER_INTERVAL);
	}
};

class InvasionGenerator: public Event {
	void Behavior() {
		nextInvasionTime = Time + INVASION_INTERVAL;
		attackStrengthFile << Time << " " << attackStrength << std::endl;
		attackStrength = 0;
		Activate(nextInvasionTime);
	}
};

class TrainDispatcher: public Event {
	void Behavior() {
		(new Trip())->Activate();
		Activate(Time + TRAIN_INTERVAL);
	}
};

class SecurityCheck: public Process {
	void Behavior() {
		Intruder * intruderInTunnel = dynamic_cast<Intruder*>(tunnelEntrance.in);
		if (tunnelEntrance.Busy() && intruderInTunnel) {
			intruderInTunnel->Activate();
		}
	}
};

class SecurityGenerator: public Event {
	void Behavior() {
		for (int i = 0; i < SECURITY_CNT; i++)
			(new SecurityCheck)->Activate(Time + SECURITY_REACTION_TIME);
		Activate(Time + SECURITY_CHECK_INTERVAL);
	}
};

int main() {
	delaysFile.open("delays.dat");
	attackStrengthFile.open("attackStrength.dat");
	tunnelEntrance.SetQueue(railway);
	Init(0, SIM_DURATION);
	(new TrainDispatcher)->Activate();
	(new InvasionGenerator)->Activate();
	(new IntruderGenerator)->Activate();
	(new SecurityGenerator)->Activate();
	Run();
	trainDelay.Output();
	intruderDetected.Output();
	delaysFile.close();
	attackStrengthFile.close();
	//intruderDeported.Output();
}
