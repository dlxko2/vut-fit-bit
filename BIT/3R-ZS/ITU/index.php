<html>
<head>
    <title>ITU Projekt</title>
    <meta charset="utf-8">
    <meta name="author" content="Martin Pavelka">

    <script src="assets/jquery-2.1.4.min.js"></script>
    <script src="assets/jquery.knob.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/jquery.playSound.js"></script>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
  
   
    <script>
        var Interval;
        var Mode = 1;
        var DefHours;
        var DefMinutes;
        var DefSeconds;
        var DefHoursI;
        var DefMinutesI;
        var DefSecondsI;
        var Set = 0;
        var UseInterval = 0;
        var Playing = 0;
        var Shake = 0;
        var Information = 0;

        function ShakeFunc()
        {
          if (Playing == 0)
            return;
          
          if (Shake == 0)
          {
            Shake = 1;
             $( "#BellElem" ).fadeOut( 200, "linear");
             setTimeout(function(){ $("#BellElem").css('color', '#ffa31a'); }, 200);
             $( "#BellElem" ).css( 'left', "-4");
          }
          else
          {
            Shake = 0;
            $( "#BellElem" ).fadeIn( 200, "linear");
            setTimeout(function(){ $("#BellElem").css('color', 'red'); }, 200);
            $( "#BellElem" ).css( 'left', "4");
          }
          if (Playing == 1)
            setTimeout("ShakeFunc()", 200);
        }

         function clock() 
         {
            if (Mode == 0 && Set == 1)
            {
              var $s = $(".second"),
                  $m = $(".minute"),
                  $h = $(".hour");

              DefSeconds = DefSeconds + 1;

              if (DefSeconds == 60)
              { 
                DefMinutes = DefMinutes + 1;
                DefSeconds = 0
              }

              if (DefMinutes == 60)
              {
                DefHours = DefHours + 1;
                DefMinutes = 0;
              }

              if (DefHours == 25)
              {
                DefHours = 0;
              }

                  
              s = DefSeconds,
              m = DefMinutes,
              h = DefHours;
              $s.val(s).trigger("change");
              $m.val(m).trigger("change");
              $h.val(h).trigger("change");
              
               $('#idTimeM').text((m<10?'0':'') + m  + ":");
               $('#idTimeS').text((s<10?'0':'') + s  + "");
               $('#idTimeH').text((h<10?'0':'') + h  + ":");

               if ((DefSeconds == DefSecondsI) && (DefMinutes == DefMinutesI) && (DefHours == DefHoursI) && (Playing == 1))
               {
                  $.playSound('http://127.0.0.1:8080/itu/assets/audio/omfg');
                  setTimeout("ShakeFunc()", 200);
                  $("#WakeMe").text("Stop me!");
               }
               
              setTimeout("clock()", 1000);
            }
        }
        
        $(function($) 
        {
            $(".knobh").knob({
              'release' : function (v) 
              { 
                if (Set == 0 && Mode == 0)
                {
                  if (UseInterval == 1)
                  {
                     DefHoursI = v; 
                     SelHoursI = v + " hours ";
                     $( "#SelHoursI" ).text(SelHoursI).show(500, "linear");
                  }
                  else
                  {
                    DefHours = v; 
                    SelHours = "You have selected " + v + " hours ";
                    $( "#SelHours" ).text(SelHours).show(500, "linear");
                  } 
                  
                  
                  $( "#idHour" ).fadeOut( 1000, "linear");
                  $( "#idMinute" ).fadeIn( 1000, "linear");
                  if (UseInterval == 1) 
                    $('#idTimeH').text((DefHoursI<10?'0':'') + DefHoursI  + ":").css('color', 'red');    
                  else
                    $('#idTimeH').text((DefHours<10?'0':'') + DefHours  + ":").css('color', 'red');        
                }
              },
              'change' : function (v) 
              {
                if (Set == 0 && Mode == 0)
                {
                  var value = Math.round(v);
                  $('#idTimeH').text((value<10?'0':'') + value  + ":").css('color', 'red');
                }
              }
            });

            $(".knobm").knob({
              'release' : function (v) 
              { 
                if (Set == 0 && Mode == 0)
                {
                  if (UseInterval == 1)
                  {
                    DefMinutesI = v; 
                    SelMinutesI = " " + v + " minutes ";
                    $( "#SelMinutesI" ).text(SelMinutesI).show(500, "linear");
                  }
                  else
                  {
                    DefMinutes = v; 
                    SelMinutes = " " + v + " minutes ";
                    $( "#SelMinutes" ).text(SelMinutes).show(500, "linear");
                  }
                  
                  $( "#idMinute" ).fadeOut( 1000, "linear");
                  $( "#idSecond" ).fadeIn( 1000, "linear");

                  if (UseInterval == 1)
                  {
                    $('#idTimeH').text((DefHoursI<10?'0':'') + DefHoursI  + ":").css('color', 'white');
                    $('#idTimeM').text((DefMinutesI<10?'0':'') + DefMinutesI  + ":").css('color', 'red');
                  }
                  else
                  {
                    $('#idTimeH').text((DefHours<10?'0':'') + DefHours  + ":").css('color', 'white');
                    $('#idTimeM').text((DefMinutes<10?'0':'') + DefMinutes  + ":").css('color', 'red');
                  }

                }
              },
              'change' : function (v) 
              {
                if (Set == 0 && Mode == 0)
                {
                  var value = Math.round(v);
                  $('#idTimeH').css('color', 'white');
                  $('#idTimeM').text((value<10?'0':'') + value  + ":").css('color', 'red');
                }
              }
            });

            $(".knobs").knob({
              'release' : function (v) 
              { 
                if (Set == 0 && Mode == 0)
                {
                  if (UseInterval == 1)
                  {
                    DefSecondsI = v; 
                    SelSecondsI = " " + v + " seconds... ";
                    $( "#SelSecondsI" ).text(SelSecondsI).show(500, "linear");
                  }
                  else
                  {
                    DefSeconds = v; 
                    SelSeconds = " " + v + " seconds... ";
                    $( "#SelSeconds" ).text(SelSeconds).show(500, "linear");
                  }
                  
                  $( "#idMinute" ).fadeIn( 1000, "linear");
                  $( "#idHour" ).fadeIn( 1000, "linear");
                  $('#idTimeS').css('color', 'white');

                  if (UseInterval == 1)
                    $('#idTimeM').text((DefMinutesI<10?'0':'') + DefMinutesI  + ":").css('color', 'white');
                  else
                    $('#idTimeM').text((DefMinutes<10?'0':'') + DefMinutes  + ":").css('color', 'white');

                  Set = 1;
                  console.log("start");
                  clock();
                }
              },
              'change' : function (v) 
              {
                if (Set == 0 && Mode == 0)
                {
                  var value = Math.round(v);
                  $('#idTimeM').css('color', 'white');
                  $('#idTimeS').text((value<10?'0':'') + value).css('color', 'red');
                }
              }
            });
          
        });

        function Setting()
        {
          console.log("ssss");
            var $s = $(".second"), 
                $m = $(".minute"),
                $h = $(".hour");

            $s.val(0).trigger("change");
            $m.val(0).trigger("change");
            $h.val(0).trigger("change");

            Mode = 0;
            Set = 0;
            UseInterval = 0;
            clearInterval(Interval);
            $( "#idSecond" ).fadeOut( 1000, "linear");
            $( "#idMinute" ).fadeOut( 1000, "linear");
            $('#idTimeH').text("00:");
            $('#idTimeM').text("00:");
            $('#idTimeS').text("00");

            $('#SelWelcome').text("Select the time...").show(500,"linear");
          
        }

        function IntervalFunc()
        {
      
            if (Set != 1)
            {
              alert("Please set the time first!");
              console.log("sracka");
              return;
            }
            var $s = $(".second"), 
                $m = $(".minute"),
                $h = $(".hour");

            $s.val(0).trigger("change");
            $m.val(0).trigger("change");
            $h.val(0).trigger("change");

            Mode = 0;
            Set = 0;
            UseInterval = 1;
            clearInterval(Interval);
            $( "#idSecond" ).fadeOut( 1000, "linear");
            $( "#idMinute" ).fadeOut( 1000, "linear");
            $('#idTimeH').text("00:");
            $('#idTimeM').text("00:");
            $('#idTimeS').text("00");

            $('#FinishText').text("with finish time as ").show(500,"linear");
          
        }

        function WakeMe()
        {
          if (Playing == 0)
          {
            Playing = 1;

            if (UseInterval == 1)
            {
              $("#WakeMeText").text("and Wake me!");
              $("#WakeMeText").fadeIn(200, "linear");
            }
            else $("#WakeMeText").fadeOut(200, "linear");
              
            $( "#BellElem" ).fadeOut( 500, "linear");

            setTimeout(function(){ $("#BellElem").css('color', '#ffa31a'); }, 500);

            
            $( "#BellElem" ).fadeIn( 500, "linear");
            $("#WakeMe").text("Don't wake me!");
          }
          else
          {
            Playing = 0;

            if (UseInterval == 1)
            {
              $("#WakeMeText").text("and Don't Wake me!");
              $("#WakeMeText").fadeIn(200, "linear");
            }
            else $("#WakeMeText").fadeOut(200, "linear");
          

            $( "#BellElem" ).fadeOut( 500, "linear");
            setTimeout(function(){ $("#BellElem").css('color', 'white'); }, 500);
            $( "#BellElem" ).css( 'left', "0");
            $( "#BellElem" ).fadeIn( 500, "linear");
            $("#WakeMe").text("Wake me!");
            $('audio').remove();
          }
        }
        function Help()
        {
          if (Information == 0)
          {
            $( ".Inform" ).fadeIn( 500, "linear");
            Information = 1;
          }
          else
          {
            $( ".Inform" ).fadeOut( 500, "linear");
            Information = 0;
          }
        }

        function UseTime()
        {
          if (Mode == 1)
          {
            var d = new Date();
            var $s = $(".second"), 
                $m = $(".minute"),
                $h = $(".hour");

            s = d.getSeconds();
            m = d.getMinutes();
            h = d.getHours();
            
            $s.val(s).trigger("change");
            $m.val(m).trigger("change");
            $h.val(h).trigger("change");

            $('#idTimeH').text((h<10?'0':'') + h  + ":");
            $('#idTimeM').text((m<10?'0':'') + m + ":");
            $('#idTimeS').text((s<10?'0':'') + s);

            console.log("CURR: hours: " + h + " minutes: " + m + " seconds: " + s);
          }
        }

    </script>

    <style>
        body
        {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 300;
            text-rendering: optimizelegibility;
            background-image: url("assets/images/bg.jpg");
           
        }
    </style>

</head>
<body onload="Interval = setInterval(UseTime, 1000);">
         
    <div class="container-fluid">

    <div class="row">
    
     <div id="Setting">
     <button type="button" class="btn btn-success" style="float:right;border-radius: 0 !important;" onclick="Setting()">Set the time</button>
     <button type="button" class="btn btn-danger" style="float:right;border-radius: 0 !important;" onclick="IntervalFunc()">Set the finish</button>
     <button type="button" id ="WakeMe" class="btn btn-primary" style="float:right;border-radius: 0 !important;" onclick="WakeMe()">Wake Me!</button>
     <button type="button" id ="WakeMe" class="btn btn-warning" style="float:right;border-radius: 0 !important;" onclick="Help()">Help</button>
    
   </div>

   <div style="position:absolute;left:73%;top:40px; padding-right:10px;color:yellow; display:none; font-size:20px" class="Inform">
    You can switch some functionality 
    here. Set the time updates the actual 
    time of clock. Finish time sets the time 
    for alarm clock and Wake Me starts the song 
    to play. </div>

    <div style="position:absolute;left:3%; color:yellow; font-size:20px; display:none; " class="Inform">
    <br>
    Here is clock element where you can see and set the time. Just press mouse <br>
    button and release on selected value or press and move your mouse there! <br>
    The biggest circle is for hours, smaller for minutes and the smallest for seconds  </div>


    <div class="col-md-7" style="margin-top:10%">


        <div style="position:absolute;left:10px;top:10px;padding:0% 10%" id="idHour">
        <input class="knobh hour" 
               data-min="0" 
               data-max="23" 
               data-bgColor="#333" 
               data-fgColor="#FF6347" 
               data-displayInput=false 
               data-width="500" 
               data-height="500" 
               data-thickness=".3">
        </div>
        
        <div style="position:absolute;left:80px;top:80px;padding:0% 10%" id="idMinute">
        <input class="knobm minute" 
               data-min="0" 
               data-max="59" 
               data-bgColor="#333" 
               data-displayInput=false 
               data-width="360" 
               data-height="360" 
               data-thickness=".30">
        </div>
                
        <div style="position:absolute;left:133px;top:133px;padding:0% 10%" id="idSecond">
        <input class="knobs second" 
               data-min="0" 
               data-max="59" 
               data-bgColor="#333" 
               data-fgColor="rgb(127, 255, 0)" 
               data-displayInput=false 
               data-width="255" 
               data-height="255" 
               data-thickness=".3">
        </div>

         <div style="position:absolute;left:250px;top:205px;padding:0% 10%;color:white;font-size:25px" id="idTime">
        <span class="glyphicon glyphicon-bell" aria-hidden="true" id="BellElem"></span>
        </div>

        <div style="position:absolute;left:210px;top:240px;padding:0% 10%;color:white;font-size:25px" id="idTime">
        <span id="idTimeH"></span><span id="idTimeM"></span><span id="idTimeS"></span>
        </div>

  
    </div>

    <div class="col-md-5" style="margin-top:25%; padding-right:10%">

      
      

      <span id="SelWelcome" style="color:white; font-size:40">Welcome...</span>

      <span id="SelHours" style="display:none; color:#ADFF2F; font-size:20px"></span>
      <span id="SelMinutes" style="display:none; color:#ADFF2F; font-size:20px"></span>
       <span id="SelSeconds" style="display:none; color:#ADFF2F; font-size:20px"></span>
       <span id="FinishText" style="display:none; color:#FF6347; font-size:20px"></span>
        <span id="SelHoursI" style="display:none; color:#FF6347; font-size:20px"></span>
      <span id="SelMinutesI" style="display:none; color:#FF6347; font-size:20px"></span>
       <span id="SelSecondsI" style="display:none; color:#FF6347; font-size:20px"></span>
       <span id="WakeMeText" style="display:none; color:#ffa31a; font-size:20px"></span>

       <br><br>
       <span id="WakeMeText" style="display:block; color:yellow; font-size:20px;display:none; " class="Inform">
       Here you can see any informations about selected time or alarm. 
       You have to do some changes to see anything here :) </span>
      


    </div>

    </div>
    </div>


</body>
</html>
