#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/* Code defines of pixel types
 * @DEF1: Char for white pixel
 * @DEF2: Char for black pixel */
#define WHITE_PIXEL '0'
#define BLACK_PIXEL '1'

/* Enumeration of other things
 * @ENUM1: Code if object not found
 * @ENUM2: Convert int to char value */
enum
{
    OBJECT_NOT_FOUND = 0,
    ASCI_BONUS = 48,
} OtherEnums;

/* Enumeration of function codes
 * @ENUM1: ERROR code 
 * @ENUM2: SUCCESS code */
enum
{
    ERROR = 1,
    SUCCESS = 0,
}   FuncCodes;

/* Enumeration of position types
 * @ENUM1: For move to the RIGHT
 * @ENUM2: For move to the LEFT
 * @ENUM3: For move UP
 * @ENUM4: For move DOWN */
enum
{
    RIGHT = 1,
    LEFT = -1,
    UP = -1,
    DOWN = 1,
}   PosCodes;

/* Structure of BitMap image
 * @VAR1: Number of rows
 * @VAR2: Number of cols
 * @VAR3: Array of BitMap */
typedef struct
{
    int rows;
    int cols;
    char *cells;
}   Bitmap;

/* Enumeration of Arg types
 * @ENUM1: For help page
 * @ENUM2: For horizontal line
 * @ENUM3: For vertical line
 * @ENUM4: For biggest square
 * @ENUM5: For test script */
enum
{
    HELP = 2,
    HLINE = 3,
    VLINE = 4,
    SQUARE = 5,
    TEST = 6,
}   ArgTypes;

/* Arguments structure
 * @VAR1: Type of argument
 * @VAR2: Name of the file */
typedef struct
{
    int ArgumentType;
    char *FileName;
}   MyArgs;

/* Function to print help page */
void MyHelpPage()
{
    printf("*--------------------- ProjectThree-2.1 --------------------*\n"
           "| Project created by Martin Pavelka 10.12.2013 (5:27 GMT+1) |\n"
           "| Argumenty:                                                |\n"
           "| --help                = print manual page of program      |\n"
           "| --vline $FileName     = get the longest vertical line     |\n"
           "| --hline $FileName     = get the longest horizontal line   |\n"
           "| --square $FileName    = get the biggest square            |\n"
           "| --test $FileName      = test if image file is corrupted   |\n"
           "*-----------------------------------------------------------*\n\n");
}

/* Function to get color of selected pixel
 * @PARAM1:   Structure of BitMap image
 * @PARAM2-3: Coords of selected pixel 
 * @RETURN:   Color of selected pixel */
char getcolor(Bitmap *bitmap, int x, int y)
{
    return bitmap->cells[y * bitmap->cols + x ];
}

/* Function to initialize BitMap array structure 
 * @PARAM1: Structure of BitMap where allocate 
 * @RETURN: TRUE sucess else FALSE */
bool MyInitBitmapData(Bitmap *FcBitmap)
{
    // Allocate 2D array with size for ROWS*COLS
    FcBitmap->cells = malloc(FcBitmap->rows * FcBitmap->cols * sizeof(char));
    
    // Test if array is already created
    if (FcBitmap->cells == NULL) return false;
    
    // Return success
    return true;
}

/* Function to open BitMap file
 * @PARAM1: Name of the BitMap file
 * @RETURN: File descriptor pointer */
FILE *MyFileOpen (char *FileName)
{
    /* Local variable 
     * @VAR1: File storage */
    FILE *InputFile = NULL;
    
    // Open File with Name for Read
    InputFile = fopen(FileName, "r");
    
    // Return FILE
    return InputFile;
}

/* Function to load BitMap info from file 
 * @PARAM1: BitMap structure of picture
 * @PARAM2: File descriptor pointer 
 * @RETURN: TRUE is sucess else FALSE */
bool MyLoadBitmapInfo(Bitmap *FcBitmap, FILE *FcFile)
{
    /* Local variables
     * @VAR1-2: Infos ROWS, COLS */
    int InfoX = 0;
    int InfoY = 0;
    
    // Load BitMap info from file and check
    if (fscanf(FcFile, "%d %d", &InfoX, &InfoY) != 2)
        return false;
    
    // Check variables
    if (InfoX == 0 || InfoY == 0) return false;
    
    // Save bitmap info
    FcBitmap->rows = InfoX;
    FcBitmap->cols = InfoY;
    
    return true;
}

/* Function to load BitMap data into structure 
 * @PARAM1: BitMap structure of picture
 * @PARAM2: File descriptor pointer
 * @PARAM3: If test mode is set 
 * @RETURN: TRUE - If ok else FALSE */
bool MyLoadBitmapData(Bitmap *FcBitmap, FILE *FcFile, bool TestMode)
{
    /* Local variables
     * @VAR1: Temp character */
    int TestChar = 0;
    
    // For every ROW do the cycle
    for (int CurrRows = 0; CurrRows < FcBitmap->rows; ++CurrRows)
    {
        // For every COL do the cycle
        for (int CurrCol = 0; CurrCol < FcBitmap->cols; ++CurrCol)
        {
            // Get one integer information from file
            if(fscanf(FcFile, "%d", &TestChar) != 1)
                return false;
            
            // Test if integer is only 1 or 0
            if (TestChar + ASCI_BONUS != '1' && TestChar + ASCI_BONUS != '0')
                return false;
            
            // Save character if not TestMode
            if (!TestMode)
                FcBitmap->cells[CurrRows * FcBitmap->cols + CurrCol] = TestChar + ASCI_BONUS;
        }
    }
    
    // Chceck if it is nothing behind
    while (fscanf(FcFile, "%d", &TestChar) > 0)
            return false;

    // Success
    return true;
}

/* Function to dealocate BitMap structure
 * @PARAM1: BitMap structure of picture */
void MyRemoveBitmap(Bitmap *FcBitmap)
{
    free(FcBitmap->cells);
    FcBitmap->cells = NULL;
    FcBitmap->cols = 0;
    FcBitmap->rows = 0;
}

/* Function to find biggest horizontal line
 * @PARAM1:   Structure of BitMap picture
 * @PARAM2-5: Start and End coords of line as result 
 * @RETURN:   0 if not found else size of max sized line */
int find_hline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    /* Local variables
     * @VAR1:   Current line size
     * @VAR2:   Max line size
     * @VAR3-4: Temp start coords
     * @VAR5:   If line started */
    int CurrLine       = 0;
    int MaxLine        = 0;
    int TempX1, TempY1 = 0;
    bool LineStarted   = false;
    
    // For every ROW of BitMap picture
    for (int CurrRow = 0; CurrRow < bitmap->rows; ++CurrRow)
    {
        // Reset line
        CurrLine = 0;
        LineStarted = false;
        
        // For every COL of BitMap picture
        for (int CurrCol = 0; CurrCol < bitmap->cols; ++CurrCol)
        {
            // If black pixel found - line started or continue
            if (getcolor(bitmap, CurrCol, CurrRow) == BLACK_PIXEL)
            {
                // Update line size
                ++CurrLine;
                
                // If line started update coords
                if (!LineStarted)
                {
                    LineStarted = true;
                    TempX1 = CurrCol;
                    TempY1 = CurrRow;
                }
                
                // If max line save coords
                if (CurrLine > MaxLine)
                {
                    MaxLine = CurrLine;
                    *x2 = CurrCol;
                    *y2 = CurrRow;
                    *x1 = TempX1;
                    *y1 = TempY1;
                    
                    // If total max line return it
                    if (MaxLine == bitmap->cols)
                        return MaxLine;
                }
            }
            else // Reset line
            {
                CurrLine = 0;
                LineStarted = false;
            }
        }
    }
    
    // Return 0 or line size 
    return (MaxLine <= 1) ? OBJECT_NOT_FOUND : MaxLine;
}

/* Function to find biggest vertical line
 * @PARAM1:   Structure of BitMap picture
 * @PARAM2-5: Start and End coords of line as result
 * @RETURN:   0 if not found else size of max sized line */
int find_vline(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    /* Local variables
     * @VAR1:   Current line size
     * @VAR2:   Max line size
     * @VAR3-4: Temp start coords
     * @VAR5:   If line started */
    int CurrLine = 0;
    int MaxLine = 0;
    int TempX1, TempY1 = 0;
    bool LineStarted = false;
    
    // For every COL of BitMap picture
    for (int CurrCol = 0; CurrCol < bitmap->cols; ++CurrCol)
    {
        // Reset line size
        CurrLine = 0;
        LineStarted = false;
        
        // For every ROW of BitMap picture
        for (int CurrRow = 0; CurrRow < bitmap->rows; ++CurrRow)
        {
            // If black pixel found - line started or continue
            if (getcolor(bitmap, CurrCol, CurrRow) == BLACK_PIXEL)
            {
                // Update line size
                ++CurrLine;
                
                // If line started update coords
                if (!LineStarted)
                { 
                    LineStarted = true;
                    TempX1 = CurrCol;
                    TempY1 = CurrRow;
                }
                
                // If max line save coords
                if (CurrLine > MaxLine)
                {
                    MaxLine = CurrLine;
                    *x2 = CurrCol;
                    *y2 = CurrRow;
                    *x1 = TempX1;
                    *y1 = TempY1;
                    
                    // If total max line return it
                    if (MaxLine == bitmap->rows)
                        return MaxLine;
                }
            }
            else // Reset line
            {
                CurrLine = 0;
                LineStarted = false;
            }
        }
    }
    
    // Return 0 or line size
    return (MaxLine <= 1) ? OBJECT_NOT_FOUND : MaxLine;
}

/* Function to find the biggest SQUARE in bitmap file
 * @PARAM1:   BitMap structure of image
 * @PARAM2-5: Start and End coords of square
 * @RETURN:   Size of found one OR 0 if not found */
int find_square(Bitmap *bitmap, int *x1, int *y1, int *x2, int *y2)
{
    /* Local variables
     * @VAR1:   maximal offset done
     * @VAR2-3: temp start coords */
    int MxOffset = 0;
    int TempX    = 0;
    int TempY    = 0;
    
    // Do cycle for the all ROWS of BitMap picture
    for (int Height = 0; Height < bitmap->rows; ++Height)
    {
        // Do cycle for the all COLS of BitMap picture
        for (int Width = 0; Width < bitmap->cols; ++Width)
        {
            // If actual pixel is white stop looking for anothers
            if (getcolor(bitmap, Width, Height) == WHITE_PIXEL) continue;

            // For every it's offset move RIGHT starting with offset 1 ending with MAX COLS
            for (int ROffWidth = Width + RIGHT; ROffWidth < bitmap->cols; ++ROffWidth)
            {
                // If actual pixel is white stop looking for anothers
                if (getcolor(bitmap, ROffWidth, Height) == WHITE_PIXEL) break;
                
                // For every it's offset move DOWN starting with offset 1 ending with MAX ROWS 
                for (int DOffHeight = Height + DOWN; (DOffHeight < bitmap->rows) && (ROffWidth-Width+1 > DOffHeight-Height); ++DOffHeight)
                {
                    // If actual pixel is white stop looking for anothers
                    if (getcolor(bitmap, ROffWidth, DOffHeight) == WHITE_PIXEL) break;
                    
                    // Save actual END coords
                    TempX = ROffWidth;
                    TempY = DOffHeight;
                    
                    // For every it's offset move LEFT starting with offset -1 ending with 
                    for (int LROffWidth = ROffWidth + LEFT; (LROffWidth > Width-1) && (DOffHeight-Height+1 > ROffWidth-LROffWidth); --LROffWidth)
                    {
                        // If actual pixel is white stop looking for anothers
                        if (getcolor(bitmap, LROffWidth, DOffHeight) == WHITE_PIXEL) break;
                        
                        // For every it's offset move UP starting with offset -1 ending with 
                        for (int UOffHeight = DOffHeight + UP; (UOffHeight > Height-1) && (ROffWidth-LROffWidth+1 > DOffHeight-UOffHeight); --UOffHeight)
                        {
                            // If actual pixel is white stop looking for anothers
                            if (getcolor(bitmap, LROffWidth, UOffHeight) == WHITE_PIXEL) break;
                            
                            // Only if the spiral is complete
                            if (LROffWidth == Width && UOffHeight == Height)
                            {
                                // Only if the square is the biggest one
                                if (MxOffset < (DOffHeight-UOffHeight))
                                {
                                    // Save the biggest offset
                                    MxOffset = DOffHeight-UOffHeight;

                                    // Save result
                                    *x1 = Width;
                                    *y1 = Height;
                                    *x2 = TempX;
                                    *y2 = TempY;
    }   }   }   }   }   }   }   }
    
    // Return 0 if not found else Offset
    return (MxOffset == 0) ? OBJECT_NOT_FOUND : MxOffset;
}

/* Function to get arguments 
 * @PARAM1: number of arguments
 * @PARAM2: arguments structure 
 * @RETURN: structure of arguments */
MyArgs ProcArgs(int argc, char * argv[])
{
    // Variable
    MyArgs Arguments;
    
    // Chceck argument count
    if (argc != 2 && argc != 3)
    {
        Arguments.ArgumentType = ERROR;
        return Arguments;
    }
    
    // Do the help script
    if (argc == 2 && strcmp(argv[1], "--help") == 0)
    {
        Arguments.ArgumentType = HELP;
        return Arguments;
    }
    
    // Inicialize filename
    if (argc == 3)
        Arguments.FileName = malloc(strlen(argv[2])*sizeof(char));
    if (Arguments.FileName == NULL)
    {
        Arguments.ArgumentType = ERROR;
        return Arguments;
    }

    // Do the test script
    if (argc == 3 && strcmp(argv[1], "--test") == 0)
    {
        strcpy(Arguments.FileName, argv[2]);
        Arguments.ArgumentType = TEST;
        return Arguments;
    }
    
    // Do the vline script
    if (argc == 3 && strcmp(argv[1], "--vline") == 0)
    {
        strcpy(Arguments.FileName, argv[2]);
        Arguments.ArgumentType = VLINE;
        return Arguments;
        
    }
    
    // Do the hline script
    if (argc == 3 && strcmp(argv[1], "--hline") == 0)
    {
        strcpy(Arguments.FileName, argv[2]);
        Arguments.ArgumentType = HLINE;
        return Arguments;
        
    }
    
    // Do the square script
    if (argc == 3 && strcmp(argv[1], "--square") == 0)
    {
        strcpy(Arguments.FileName, argv[2]);
        Arguments.ArgumentType = SQUARE;
        return Arguments;
        
    }

    Arguments.ArgumentType = ERROR;
    return Arguments;
}

/* Main function of program
 * @PARAM1: number of arguments
 * @PARAM2: structure of arguments
 * @RETURN: SUCESS or ERROR code */
int main(int argc, char * argv[])
{
    /* Local variables
     * @VAR1:   File variable
     * @VAR2:   Bitmap variable
     * @VAR3:   Args variable
     * @VAR4-7: Coords variable
     * @VAR8:   If Test argument set */
    FILE *MyFile = NULL;
    Bitmap MyBitmap;
    MyArgs Arguments;
    int x1, y1, x2, y2;
    bool TestMode = false;
    
    // Process arguments
    Arguments = ProcArgs(argc, argv);
    if (Arguments.ArgumentType == ERROR)
    {
        printf("Can't get arguments!\n");
        return ERROR;
    }
    
    // Set test mode if requested
    if (Arguments.ArgumentType == TEST)
        TestMode = true;

    // Print help page if requested
    if (Arguments.ArgumentType == HELP)
    {
        MyHelpPage();
        return ERROR;
    }
    
    // Open file of BitMap picture
    MyFile = MyFileOpen(Arguments.FileName);
    if (MyFile == NULL)
    {
        printf("Can't open File %s!\n", Arguments.FileName);
        return ERROR;
    }
    free(Arguments.FileName);
    
    // Load Bitmap informations 
    if (!MyLoadBitmapInfo(&MyBitmap, MyFile))
    {
        if (Arguments.ArgumentType == TEST)
            printf("Invalid\n");
        else
            printf("Can't load Bitmap info!\n");
        fclose(MyFile);
        return ERROR;
    }
    
    // Init Bitmap 2D array structure
    if(!MyInitBitmapData(&MyBitmap))
    {
        printf("Can't init Bitmap array!\n");
        return ERROR;
        fclose(MyFile);
    }
    
    // Lead Bitmap data to structure from file
    if (!MyLoadBitmapData(&MyBitmap, MyFile, TestMode))
    {
        if (Arguments.ArgumentType == TEST)
            printf("Invalid\n");
        else
            printf("Can't load Bitmap data!\n");
        MyRemoveBitmap(&MyBitmap);
        fclose(MyFile);
        return ERROR;
    }
    
    // If test argument processed
    if (Arguments.ArgumentType == TEST)
    {
        printf("Valid\n");
        return SUCCESS;
    }
   
    // Find HLINE in the BitMap picture
    if (Arguments.ArgumentType == HLINE)
    {
        if (find_hline(&MyBitmap, &x1, &y1, &x2, &y2) == 0)
            printf("Can't find object!\n");
        else
            printf("%d %d %d %d\n", y1,x1,y2,x2);

            MyRemoveBitmap(&MyBitmap);
            fclose(MyFile);
            return SUCCESS;
    }
    
    // Find VLINE in the BitMap picture
    if (Arguments.ArgumentType == VLINE)
    {
        if(find_vline(&MyBitmap, &x1, &y1, &x2, &y2) == 0)
            printf("Can't find object!\n");
        else
            printf("%d %d %d %d\n", y1,x1,y2,x2);
        
        MyRemoveBitmap(&MyBitmap);
        fclose(MyFile);
        return SUCCESS;
    }
    
    // Find SQUARE in the BitMap picture
    if (Arguments.ArgumentType == SQUARE)
    {
        
        if(find_square(&MyBitmap, &x1, &y1, &x2, &y2) == 0)
            printf("Can't find object!\n");
        else
            printf("%d %d %d %d\n", y1,x1,y2,x2);
        MyRemoveBitmap(&MyBitmap);
        fclose(MyFile);
        return SUCCESS;
    }
    
    return SUCCESS;
}

