#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>

/* Definicia kodov pouzitych v programe
 @DEF01 => kod uspesneho ukoncenia programu
 @DEF02 => kod chybneho ukoncenia programu
 @DEF03 => kod pretecenia pocitadla pozicie v slove
 @DEF04 => kod pretecenia pocitadla najdenych znakov
 @DEF05 => kod pri vrateni hodnoty NaN funkciou strtoint
 @DEF06 => kod pri preteceni vo funkcii strtoint
 @DEF07 => kod pri nenadstaveni ziadneho argumentu pozicie
 @DEF08 => kod pri nenadstaveni ziadneho specifeckeho znaku hladania
 @DEF09 => maximalna dlzka vypisu pri debug mode
 @DEF10 => dekadicka hodnota pre presun na dalsiu cifru
 @DEF11 => hodnota najmensieho ASCI cisla
 @DEF12 => argument - znak hladania vsetkych znakov
 @DEF13 => argument - znak hladania velkych znakov
 @DEF14 => argument - znak hladania ciselnych znakov
 */
#define CODE_FUNCTION_SUCESS 0
#define CODE_FUNCTION_ERROR 1
#define CODE_ERR_POZ_OWF -1
#define CODE_ERR_FOUND_OWF -2
#define CODE_NOT_A_NUMBER -1
#define CODE_NUMBER_OWF -2
#define CODE_ARG_NO_POSITION -4
#define CODE_ARG_NO_SELECTED_CHAR '*'
#define CODE_MAX_WORD_LEN 80
#define CODE_DEKADIC_NUMBER 10
#define CODE_BASE_NUMBER_ASCI_POS 48
#define CODE_ARG_ALLCHARS '.'
#define CODE_ARG_CAPCHARS '^'
#define CODE_ARG_NUMCHARS '@'

/* Struktura argumentov 
 @VAR1 => arument pozicie
 @VAR2 => argument hladaneho znaku (uzivatelom zadaneho)
 @VAR3 => argument debug modu (true ak zapnuty)
 @VAR4 => argument hladania vsetkych znakov (true ak zapnuty) 
 @VAR5 => argument hladania velkych pismien (true ak zapnuty)
 @VAR6 => argument hladania ciselnych znakov 0-9 (true ak zapnuty) */
typedef struct 
{
    int position;
    char sel_chars;
    bool debug_mode, all_chars, cap_chars, num_chars;
} arguments ; 

/* Funkcia, ktora vypise help stranku programu */
static void function_printf_help()
{
    printf("-------------------------- ProjectOne-FINAL --------------------------\n"
           "|                                                                    |\n"
           "|           Projekt vytvoril Martin Pavelka 24. zari 2013            |\n"
           "|                                                                    |\n"
           "| Argumenty:                                                         |\n"
           "| -d         = spustit program v ladiacom mode                       |\n"
           "| --help     = spustit manualovu stranku                             |\n"
           "| $number    = vyhladat znak na tejto pozicii alebo dlzku slova      |\n"
           "| $character = vyhladat tento znak v zadanom textet                  |\n"
           "| \"$text\"    = text, ktory je odovzdany programu na spracovanie      |\n"
           "|                                                                    |\n"
           "| Error codes:                                                       |\n"
           "| [-1] Nastalo pretecenie cisla citajuceho poziciu v slove           |\n"
           "| [-2] Nastalo pretecenie cisla pocitajuceho pocet najdenych znakov  |\n"
           "|                                                                    |\n"
           "----------------------------------------------------------------------\n\n");
}

/* Funkcia pre overenie ci je znak akceptovany alebo je oddelovacom
 * @PARAM1 => znak, ktory sa ma overit
 * @RETURN => TRUE ak je znakom, FALSE ak je oddelovaci znak */
static bool function_acceptable_character(char rq_char)
{
    return ((rq_char >= 'A' && rq_char <= 'Z') || (rq_char >= 'a' && rq_char <= 'z') ||
            (rq_char >= '0' && rq_char <= '9') || (rq_char == '_' || rq_char == '-'))
            ? true : false;
}

/* Funkcia pre overenie ci je znak velke alebo male pismeno
 * @PARAM1 => znak, ktory sa ma overit
 * @RETURN => TRUE ak je znakom velke pismeno od A do Z, inak FALSE */
static bool function_if_capital (char rq_char)
{
    return (rq_char >= 'A' && rq_char <= 'Z') ? true : false;
}

/* Funkcia na kovertovanie string-u do integer-u
 * @PARAM1 => slovo, ktore chceme zmenit na cislo 
 * @RETURN => vysledne cislo ziskane pocas procesu funkcie alebo kod NaN */
static int function_strtoint (char req_word[])
{
    /* @VAR1 => pocitadlo pre cyklus for
     * @VAR2 => docasne ulozisko vysledneho cisla */
    unsigned int counter, temp_number = 0;
    
    // Spracovanie kazdeho znaku v argumente
    for (counter = 0; req_word[counter] != '\0'; counter++)
    {
        if(req_word[counter]< '0' || req_word[counter] > '9')
            return CODE_NOT_A_NUMBER;
        else
        {   // Overenie ci nedoslo k preteceniu cisla
            if (temp_number > (temp_number * CODE_DEKADIC_NUMBER + (req_word[counter] - CODE_BASE_NUMBER_ASCI_POS)))
                return CODE_NUMBER_OWF;
            
            // Priradenie dekadickej hodnoty a samotneho cisla
            temp_number = temp_number * CODE_DEKADIC_NUMBER + (req_word[counter] - CODE_BASE_NUMBER_ASCI_POS);
        }
    }
    return temp_number;
}


/* Funkcia, ktora spracuje aktualne pismeno spracovavaneho slova
 * @PARAM1 => pozicia znaku v spracovavanom slove
 * @PARAM2 => aktualne spracovavany znak 
 * @PARAM3 => argumenty zadane pri starte programu
 * @RETURN => TRUE ak znak splna niektore poziadavky, FALSE ak nie */
static bool function_proc_character(int char_pos, char act_char, arguments def_args)
{
    // Overenie moznosti s pouzitelnym argumentom pozicie
    // 1. Overenie pri zadani specifickeho znaku na hladanie uzivatelom
    // 2. Overenie pri zadani ciselneho hladaneho znaku
    // 3. Overenie pri zadani hladania velkych pismien
    if (def_args.position != CODE_ARG_NO_POSITION && char_pos == def_args.position)
    {
        if ((def_args.sel_chars != CODE_ARG_NO_SELECTED_CHAR) && (act_char == def_args.sel_chars))
                return true;
        if ((def_args.num_chars) && (act_char >= '0' && act_char <= '9'))
                return true;
        if ((def_args.cap_chars) && (function_if_capital(act_char)))
                return true;
    }
    
    // Overenie moznosti s nezadanym argumentom pozicie
    // 1. Overenie pri zadani hladania velkych pismien
    // 2. Overenie pri zadani ciselneho hladaneho znaku
    // 3. Overenie pri zadani specifickeho znaku na hladanie uzivatelom
    if (def_args.position == CODE_ARG_NO_POSITION)
    {
        if ((def_args.cap_chars) && (function_if_capital(act_char)))
                return true;
        if ((def_args.num_chars) && (act_char >= '0' && act_char <= '9'))
                return true;
        if ((def_args.sel_chars != CODE_ARG_NO_SELECTED_CHAR) && (act_char == def_args.sel_chars))
                return true;
    }
    return false;
}

/* Funkcia, ktora spracuje aktualne dokoncene slovo
 * @PARAM1 => dlzka spracovaneho slova 
 * @PARAM2 => argumenty zadane pri starte programu
 * @RETURN => true ak slovo splna niektore poziadavky, false ak nie */
static bool function_proc_word(int word_size, arguments def_args)
{
    // Ak je zadane pocitanie slov istej dlzky
    // Overime aj hladanie vsetkych znakov a samotnu dlzku slova
    if ((def_args.position != CODE_ARG_NO_POSITION) && ((def_args.all_chars) && (word_size >= def_args.position)))
        return true;
    
    // Ak pocitame vsetky slova
    // Overime len hladanie vsetkych znakov
    if ((def_args.position == CODE_ARG_NO_POSITION) && (def_args.all_chars))
            return true;
    
    return false;
}


/* Funkcia, ktora spracuje prichadzajuci vstup 
 * @PARAM1 => argumenty zadane pri starte programu
 * @RETURN => pocet najdenych alebo chybovy kod */
static int function_process_input(arguments def_args)
{
    /* @VAR1 => aktualne spracovavany znak
     * @VAR2 => overenie ci dany znak uz bol precitany
     * @VAR3 => pozicia znaku v aktualnom slove
     * @VAR4 => pocet najdenych znakov podla poziadoviek */
    int act_char = 0;
    unsigned int alrdy_found = 0, act_char_pos = 0, found_num = 0;
    
    // Cyklus vykonavany pokial neskonci vstup
    while (act_char != EOF)
    {
        // Precitanie aktualneho pismena 
        act_char = getchar();
        
        // Overenie, ci je spracovavavany platny znak 
        if (function_acceptable_character(act_char))
        {
            // Ulozenie predchadzajuceho stavu najdenych 
            alrdy_found = found_num;
            
            // Cyklus vykonavany pokial neskonci slovo 
            while (function_acceptable_character(act_char))
            {
                // Upravy aktualnu poziciu pismena 
                if (act_char_pos < INT_MAX)
                    ++act_char_pos;
                else return CODE_ERR_POZ_OWF;
                
                // Spracuje aktualne pismeno ak nebolo v slove este nic najdene 
                if ((alrdy_found == found_num) && (function_proc_character(act_char_pos, act_char, def_args)))
                {
                    // Upravy pocet najdenych
                    if(found_num < INT_MAX)
                        ++found_num;
                    else return CODE_ERR_FOUND_OWF;
                }
                
                // Vypis ak je zapaty debug mod
                if (def_args.debug_mode && act_char_pos < CODE_MAX_WORD_LEN+1)
                    printf("%c", act_char);
                
                // Ak nebolo este precitane pismeno nacita dalsie 
                act_char = getchar();
            }
            
            // Spracuje aktualne slovo 
            if ((act_char_pos != 0) && (function_proc_word(act_char_pos, def_args)))
            {
                // Upravy pocet najdenych
                if (found_num < INT_MAX)
                    ++found_num;
                else return CODE_ERR_FOUND_OWF;
            }
            
            // Novy riadok pre debug mod 
            if (def_args.debug_mode)
                printf("\n");
            
            // Resetnutie aktualnej pozicie 
            act_char_pos = 0;
        }
    }
    return found_num;
}


/* Definicia hlavnej funkcie programu
 * @PARAM1 => pocet argumentov
 * @PARAM2 => pole argumentov
 * @RETURN => CODE_FUNCTION_SUCESS v poriadku, CODE_FUNCTION_ERROR pri chybe */
int main(int argc, char * argv[])
{
    /* @VAR1 => pocitadlo pre cyklus for
     * @VAR2 => ulozisko cisla vrateneho strtoint funkciou
     * @VAR3 => struktura uloziska argumentov */
    unsigned short int counter = 0;
    short int pos_num = 0;
    arguments defined_arguments;
    
    // Nadstavenie defaultnych hodnot
    defined_arguments.position = CODE_ARG_NO_POSITION;
    defined_arguments.sel_chars = CODE_ARG_NO_SELECTED_CHAR;
    defined_arguments.all_chars = false;
    defined_arguments.cap_chars = false;
    defined_arguments.debug_mode = false;
    defined_arguments.num_chars = false;
    
    // Overenie poctu argumetov
    if (argc == 1 || argc > 4)
    {
        function_printf_help();
        printf("ERROR: Zly pocet [%d] argumentov!\n", argc);
        return CODE_FUNCTION_ERROR;
    }
    
    // Vypisanie help stranky programu
    if (argc == 2 && strcmp(argv[1], "--help") == 0)
    {
        function_printf_help();
        return CODE_FUNCTION_SUCESS;
    }
    
    // Nacitanie znaku na prvej pozicii alebo vypisanie chyby
    if ((strlen(argv[1]) == 1))
    {
        if (argv[1][0] == CODE_ARG_ALLCHARS)
            defined_arguments.all_chars = true;
        else if (argv[1][0] == CODE_ARG_CAPCHARS)
            defined_arguments.cap_chars = true;
        else if (argv[1][0] == CODE_ARG_NUMCHARS)
            defined_arguments.num_chars = true;
        else
            defined_arguments.sel_chars = argv[1][0];
    }
    else
    {
        function_printf_help();
        printf("Error: Zly prvy argument! - [%s]\n", argv[1]);
        return CODE_FUNCTION_ERROR;
    }
    
    // Overenie ostatnych argumentov
    for (counter = 2; counter < argc; ++counter)
    {
        // Spracovanie debug modu
        if (strcmp(argv[counter], "-d") == 0)
        {
            defined_arguments.debug_mode = true;
            continue;
        }
        
        // Ziskanie argumentu pozicie
        pos_num = function_strtoint(argv[counter]);
        
        // Spracovanie argumentu pozicie
        if (pos_num == CODE_NOT_A_NUMBER)
        {
            printf("Error: Argument pozicie vratil: NaN\n");
            return CODE_FUNCTION_ERROR;
        }
        else if (pos_num == CODE_NUMBER_OWF)
        {
            printf("Error: Argument pozicie vratil chybu pretecenia\n");
            return CODE_FUNCTION_ERROR;
        }
        else if (pos_num > 0)
        {
            defined_arguments.position = function_strtoint(argv[counter]);
        }
        else
        {
            function_printf_help();
            printf("Error: Nadbytocny alebo nepodporovany argument! - [%s]\n", argv[counter]);
            return CODE_FUNCTION_ERROR;
        }
      
    }
    
    // Spracovanie vstupu podla argumentov a vypis poctu najdenych
    printf("%d\n",function_process_input(defined_arguments));
    
    // Koniec programu !
    return CODE_FUNCTION_SUCESS;
}