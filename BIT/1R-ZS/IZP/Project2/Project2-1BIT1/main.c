#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>

/* Zakladne definicie pouzite v programe
 * @DEF1: cislo pi pouzivane pri vypoctoch
 * @DEF2: odchylka pouzivana pri iteraciach
 * @DEF3: definicia hranicnej hodnoty pri asin
 * @DEF4: definicia konstanty NaN
 * @DEF5: definicia konstanty INF */
#define PI                 3.14159265358979323846
#define EPS                0.000000000001
#define TRUESHOLD_LIMIT    0.7
#define NOT_A_NUMBER       "nan"
#define INFINITY_NUMBER    "inf"

/* Struktura argumentov
 * @VAR1:   uspesnost funkcie
 * @VAR2:   typ sprac. argumentu
 * @VAR3:   hodnota pre SQRT a ASIN
 * @VAR4-9: hodnoty pre TRIANGLE */
typedef struct
{
    unsigned short int ErrCode;
    unsigned short int TypArgumentu;
    double X, AX, AY, BX, BY, CX, CY;
}   MyArgs;

/* Struktura vysledku trojuholnika
 * @VAR1-3: vysledky uhlov alpha, beta, gama */
typedef struct
{
    double Alpha, Beta, Gamma;
}   TriangleOut;

/* Vymenovanie typov argumentov
 * ENUM1: argument typu odmocnina
 * ENUM2: argument typu arcus sinus
 * ENUM3: argument typu triangle */
enum
{
    SQRT = 1,
    ASIN = 2,
    TRIANGLE = 3,
}   ArgTypes;

/* Vymenovanie navratovych hodnot
 * ENUM1: kod v pripade uspechu
 * ENUM2: kod v pripade chyby
 * ENUM3: kod v pripade ak nie je potreba dalsich operacii */
enum
{
    SUCESS = 0,
    ERROR = 1,
    COMPLETED = 2,
}   ReturnTypes;

/* Funkcia ktora vypise manualove stranky */
static void MyHelpPage()
{
    printf("--------------------- ProjectTwo-3.0 ----------------------\n"
           "| Projekt vytvoril Martin Pavelka 2. listopadu 2013       |\n"
           "| Argumenty:                                              |\n"
           "| --help                = vypise manual a verziu programu |\n"
           "| --sqrt $cislo         = vypocita druhu odmocninu cisla  |\n"
           "| --asin $cislo         = vypocita arcus sinus cisla      |\n"
           "| --triangle $suradnice = vypocita uhly trojuholnika      |\n"
           "| $suradnice            = $AX $AY $BX $BY $CX $CY body    |\n"
           "-----------------------------------------------------------\n\n");
}

/* Funkcia na vypocitanie absolutnej hodnoty cisla
 * @PARAM1: cislo na vypocet absolutnej hodnoty
 * @RETURN: absolutna hodnota cisla z parametru */
static double MyAbs(double x)
{
    return (x < 0) ? -x : x;
}

/* Funkcia na vypocitanie druhej odmocniny cisla
 * @PARAM1: cislo na vypocitanie druhej odmocniny
 * @RETURN: druha odmocnina cisla z parametru */
static double my_sqrt(double x)
{
    /* Lokalne premenne
     * @VAR1: konecny vysledok
     * @VAR2: predchadzajuci vysledok */
    double Vysledok    = x;
    double VysledokTmp = 0.0;
    
    do // Cyklus vykonavany az po dosiahnutie pozadovanej odchylky
    {
        // Ulozenie predosleho vysledku a vypocet noveho
        VysledokTmp = Vysledok;
        Vysledok = 0.5 * (Vysledok + ( x / Vysledok ) );
    }
    while (MyAbs(VysledokTmp - Vysledok) > Vysledok * EPS);
    return  Vysledok;
}

/* Funkcia na vypocitanie arcus sinus (arcsin) cisla
 * @PARAM1: cislo na vypocitanie pomocou arcsin
 * @RETURN: vypocet arcsin cisla v parametre */
static double my_asin(double x)
{
    // Pomocna premenna krajnych hodnot pre + a - vstup
    double y = MyAbs(x);
    
    // Ak krajna hodnota uprav vstup
    if (x > TRUESHOLD_LIMIT || x < -TRUESHOLD_LIMIT) y = my_sqrt(1 - (x * x));
    
    /* Lokalne premenne
     * @VAR1: vypocet prvej casti vzorca
     * @VAR2: vypocet druhej casti vzorca
     * @VAR3: konecny vysledok
     * @VAR4: ulozisko druhej mocniny x
     * @VAR5: ulozisko aktualnej mocniny
     * @VAR6: pocitadlo pre algoritmus */
    double Vypocet1   = 1.0;
    double Vypocet2   = 0.0;
    double Vysledok   = y;
    double XNaX       = y * y;
    double AktMocnina = y;
    int    i          = 1;
    
    do // Cyklus vykonavany az po pozadovanu odchylku
    {
        // Vytvorenie novej mocniny
        AktMocnina *= XNaX;
        
        // Vypocitanie stabilnej a premennej casti vzorca a ulozenie vysledku
        Vypocet1 = Vypocet1 * ( (double)i / ( i + 1) );
        Vypocet2 = Vypocet1 * ( (double)AktMocnina / (i + 2) );
        Vysledok = Vysledok + Vypocet2;
        
        // Nadstavenie pocitadla pre dalsiu iteraciu
        i += 2;
    }
    while (MyAbs(Vypocet2) > (EPS * MyAbs(Vysledok)));
    
    // Upravenie vysledku pre krajnu hodnotu
    if (x > TRUESHOLD_LIMIT || x < -TRUESHOLD_LIMIT)
        Vysledok = (0.5 * PI) - Vysledok;
    
    // Vratenie vypocitanej hodnoty so znamienkom
    return (x < 0) ? -Vysledok : Vysledok;
}

/* Funkcia na vypocitanie dlzky vektora -> usecky -> strany trojuholnika
 * @PARAM1-2: suradnice prveho bodu
 * @PARAM3-4: suradnice druheho bodu
 * @RETURN: dlzka strany trojuholnika */
static double TrojuholnikUsecky(double X1, double Y1, double X2, double Y2)
{
    // Vypocet suradnic vektora
    double VektorX = X2 - X1;
    double VektorY = Y2 - Y1;
    
    // Vratenie dlzky vektora vypoctom
    return my_sqrt((VektorX * VektorX) + (VektorY * VektorY));
}

/* Funkcia na vypocet cosinusu uhlov trojuholnika
 * @PARAM1-3: dlzky vsetkych stran trojuholnika
 * @RETURN: cosinus vysledneho uhla */
static double TrojuholnikCos(double Usecka1, double Usecka2, double Usecka3)
{
    return ((Usecka2 * Usecka2) + (Usecka3 * Usecka3) - (Usecka1 * Usecka1)) / (2 * Usecka2 * Usecka3);
}

/* Hlavna funkcia na vypocet vsetkych uhlov trojuholnika
 * @PARAM1-6: X/Y suradnica bodov A, B, C 
 * @RETURN: struktura vysledku vypoctu trojuholnika */
static TriangleOut TrojuholnikMain(double AX, double AY, double BX, double BY, double CX, double CY)
{
    /* Lokalna premenna
     * @VAR1: definovanie struktury vysledku */
    TriangleOut Output;
    
    // Vypocet dlzok vektorov pre vsetky strany trojuholnika
    double UseckaC = TrojuholnikUsecky(AX, AY, BX, BY);
    double UseckaA = TrojuholnikUsecky(BX, BY, CX, CY);
    double UseckaB = TrojuholnikUsecky(AX, AY, CX, CY);
    
    // Vypocet cos uhlov pre kazdy uhol v trojuholniku
    double CosAlpha = TrojuholnikCos(UseckaA, UseckaB, UseckaC);
    double CosBeta  = TrojuholnikCos(UseckaB, UseckaA, UseckaC);
    double CosGama  = TrojuholnikCos(UseckaC, UseckaB, UseckaA);
    
    // Prepocet cos na vysledny uhol v rad cez arcsin
    Output.Alpha  = MyAbs(my_asin(CosAlpha) - PI/2);
    Output.Beta   = MyAbs(my_asin(CosBeta) - PI/2);
    Output.Gamma  = MyAbs(my_asin(CosGama) - PI/2);
    return Output;
}

/* Funkcia na overnie vypoctu odmocniny
 * @PARAM1: hodnota pre overenie
 * @RETURN: SUCESS - vypocet, ERROR - 0, INF, NAN */
static bool MySqrtChck(double x)
{
    if      (x == 0)      printf("%.10e\n", 0.0);
    else if (x > DBL_MAX) printf(INFINITY_NUMBER "\n");
    else if (x < 0)       printf(NOT_A_NUMBER "\n");
    else if (x > 0)       return SUCESS;
    return  ERROR;
}

/* Funkcia na overnie vypoctu arcus sinus
 * @PARAM1: hodnota pre overenie
 * @RETURN: SUCESS - vypocet, ERROR - NaN, -1, 1 */
static bool MyAsinChck(double x)
{
    if      (x == 1.0)        printf("%.10e\n", PI / 2);
    else if (x == -1.0)       printf("%.10e\n", PI / -2);
    else if (x > -1 && x < 1) return SUCESS;
    else                      printf(NOT_A_NUMBER "\n");
    return ERROR;
}

/* Funkcia na overnie vypoctu trojuholnika
 * @PARAM1: hodnoty useciek pre overenie
 * @RETURN: SUCESS - vypocet, ERROR - nie je trojuholnik */
static bool MyTriangleChck(double AX, double AY, double BX, double BY, double CX, double CY)
{
    // Ziskanie informacii o dlzke useciek
    double Usecka1 = TrojuholnikUsecky(AX, AY, BX, BY);
    double Usecka2 = TrojuholnikUsecky(BX, BY, CX, CY);
    double Usecka3 = TrojuholnikUsecky(AX, AY, CX, CY);
    
    // Overenie vety o trojuholniku
    if (Usecka1 < (Usecka2 + Usecka3) && Usecka2 < (Usecka1 + Usecka3) &&
        Usecka3 < (Usecka1 + Usecka2))
        return SUCESS;
    else
        printf(NOT_A_NUMBER "\n" NOT_A_NUMBER "\n" NOT_A_NUMBER "\n");
    
    return ERROR;
}

/* Funkcia spracujuca vstup vo forme argumentov 
 * @PARAM1: pocet argumentov
 * @PARAM2: pole argumentov 
 * @RETURN: struktura argumentov */
static MyArgs SpracujArgumenty(int argc, char * argv[])
{
    /* Lokalne premenne
     * @VAR1: struktura argumentov
     * @VAR2: ulozisko chyby strtod */
    MyArgs Arguments;
    char *Strtoderr;
    
    // Spracuj HELP jednoparametrove argumenty
    if (argc == 2 && strcmp(argv[1], "--help") == 0)
    {
        MyHelpPage();
        Arguments.ErrCode = COMPLETED;
        return Arguments;
    }
    
    // Spracuj SQRT dvojparametrove argumenty
    if (argc == 3 && (strcmp(argv[1], "--sqrt") == 0))
    {
        // Overenie argumentu
        strtod(argv[2], &Strtoderr);
        if (*Strtoderr != '\0')
        {
            Arguments.ErrCode = ERROR;
            return Arguments;
        }
        
        // Overenie ci je potrebny vypocet
        if (MySqrtChck(strtod(argv[2], NULL)))
        {
            Arguments.ErrCode = COMPLETED;
            return Arguments;
        }
        
        // Ulozenie hodnot do struktury
        Arguments.ErrCode = SUCESS;
        Arguments.TypArgumentu = 1;
        Arguments.X = strtod(argv[2], NULL);
        return Arguments;
    }
    
    // Spracuj ASIN dvojparametrove argumenty
    if (argc == 3 && (strcmp(argv[1], "--asin") == 0))
    {
        // Overenie argumentu
        strtod(argv[2], &Strtoderr);
        if (*Strtoderr != '\0')
        {
            Arguments.ErrCode = ERROR;
            return Arguments;
        }
        
        // Overenie ci je potrebny vypocet
        if (MyAsinChck(strtod(argv[2], NULL)))
        {
            Arguments.ErrCode = COMPLETED;
            return Arguments;
        }
        
        // Ulozenie hodnot do struktury
        Arguments.ErrCode = SUCESS;
        Arguments.TypArgumentu = 2;
        Arguments.X = strtod(argv[2], NULL);
        return Arguments;
    }
    
    // Spracuj TRIANGLE sedemparametrove argumenty
    if (argc == 8 && (strcmp(argv[1], "--triangle") == 0))
    {
        // Overenie argumentov
        for (int i = 2; i < 7; i++)
        {
            strtod(argv[i], &Strtoderr);
            if (*Strtoderr != '\0')
            {
                Arguments.ErrCode = ERROR;
                return Arguments;
            }
        }
        
        // Overenie ci je potrebny vypocet
        if(MyTriangleChck(
        strtod(argv[2],NULL), strtod(argv[3],NULL),
        strtod(argv[4],NULL), strtod(argv[5],NULL),
        strtod(argv[6],NULL), strtod(argv[7],NULL)))
        {
            Arguments.ErrCode = COMPLETED;
            return Arguments;
        }
        
        // Ulozenie hodnot do struktury
        Arguments.AX = strtod(argv[2], NULL);
        Arguments.AY = strtod(argv[3], NULL);
        Arguments.BX = strtod(argv[4], NULL);
        Arguments.BY = strtod(argv[5], NULL);
        Arguments.CX = strtod(argv[6], NULL);
        Arguments.CY = strtod(argv[7], NULL);
        Arguments.ErrCode = SUCESS;
        Arguments.TypArgumentu = 3;
        return Arguments;
    }
    
    // Chyba spracovania
    Arguments.ErrCode = ERROR;
    return Arguments;
}

/* Hlavna funkcia sluziaca na ziskanie argumentov a ich vyhodnotenie
 * @PARAM1 => pocet argumentov aj s cestou k programu
 * @PARAM2 => pole argumentov zadanych programu
 * @RETURN => Chyba - konstanta ERROR, SUCESS ak v poriadku */
int main(int argc, char * argv[])
{
    // Nacitanie argumentov programu
    MyArgs arguments = SpracujArgumenty(argc, argv);
    
    // Ak chyba pri spracovani ukonci
    if (arguments.ErrCode == ERROR)
    {
        MyHelpPage();
        printf("Chyba spracovania vstupu!\n");
        return ERROR;
    }
    
    if (arguments.ErrCode == COMPLETED)
        return SUCESS;
    
    // Ak argument typu SQRT
    if (arguments.TypArgumentu == SQRT)
    {
        printf("%.10e\n", my_sqrt(arguments.X));
        return SUCESS;
    }
    
    // Ak argument typu ASIN
    if (arguments.TypArgumentu == ASIN)
    {
        printf("%.10e\n", my_asin(arguments.X));
        return SUCESS;
    }
    
    // Ak argument typu TRIANGLE
    if (arguments.TypArgumentu == TRIANGLE)
    {
        TriangleOut TriangleOutput = TrojuholnikMain(arguments.AX,arguments.AY,arguments.BX,
                                                     arguments.BY,arguments.CX,arguments.CY);
        printf("%.10e\n", TriangleOutput.Alpha);
        printf("%.10e\n", TriangleOutput.Beta);
        printf("%.10e\n", TriangleOutput.Gamma);
        return SUCESS;
    }
    
    return ERROR;
}
