\select@language {slovak}
\contentsline {section}{\numberline {1}Pr\IeC {\'\i }klad 1, variant G}{1}{section.1}
\contentsline {paragraph}{}{2}{section*.2}
\contentsline {paragraph}{}{2}{section*.3}
\contentsline {paragraph}{}{3}{section*.4}
\contentsline {paragraph}{}{3}{section*.5}
\contentsline {paragraph}{}{3}{section*.6}
\contentsline {paragraph}{}{3}{section*.7}
\contentsline {paragraph}{}{4}{section*.8}
\contentsline {paragraph}{}{4}{section*.9}
\contentsline {paragraph}{}{4}{section*.10}
\contentsline {section}{\numberline {2}Pr\IeC {\'\i }klad 2, variant F}{5}{section.2}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 3. Vypo\IeC {\v c}\IeC {\'\i }tame celkov\IeC {\'y} pr\IeC {\'u}d, ktor\IeC {\'y} pretek\IeC {\'a} cez obvod bez rezistora $R_5$ - $I_{R1}$ }}}{6}{section*.11}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 4. Vypo\IeC {\v c}\IeC {\'\i }tame nap\IeC {\"a}tie $U_{R1}$ odporu $R_1$ a nap\IeC {\"a}tie $U_{R2}$ odporu $R_2$ }}}{6}{section*.12}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 5. Vypo\IeC {\v c}\IeC {\'\i }tame pr\IeC {\'u}d $I_{R2}$ a n\IeC {\'a}sledne dopo\IeC {\v c}\IeC {\'\i }tame pr\IeC {\'u}d $I_{R3}$ }}}{6}{section*.13}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 6. Vypo\IeC {\v c}\IeC {\'\i }tame $U_i$ a $R_{i}$ pri zapojen\IeC {\'\i } bez zdroja a odporu $R_5$\\\\ }}}{6}{section*.14}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 7. Pomocou $U_i$ a $R_i$ vypo\IeC {\v c}\IeC {\'\i }tame $I_{R5}$ a $U_{R5}$ nasledovne }}}{6}{section*.15}
\contentsline {section}{\numberline {3}Pr\IeC {\'\i }klad 3, variant D}{7}{section.3}
\contentsline {paragraph}{}{7}{section*.16}
\contentsline {paragraph}{}{8}{section*.17}
\contentsline {paragraph}{}{8}{section*.18}
\contentsline {paragraph}{}{8}{section*.19}
\contentsline {paragraph}{}{8}{section*.20}
\contentsline {paragraph}{}{9}{section*.21}
\contentsline {paragraph}{}{9}{section*.22}
\contentsline {paragraph}{}{9}{section*.23}
\contentsline {section}{\numberline {4}Pr\IeC {\'\i }klad 4, variant G}{10}{section.4}
\contentsline {paragraph}{Z1 - Predstavuje impedanciu resitoru $R_1$\\ Z2 - Predstavuje impedanciu $R_2$ a kondenz\IeC {\'a}toru $C_1$\\ Z3 - Impedancia $ R_3$ a $L_2$\\ Z4 - Impedancia $C_2$\\ Z5 - Impedancia L1\\}{10}{section*.24}
\contentsline {paragraph}{}{11}{section*.25}
\contentsline {paragraph}{}{11}{section*.26}
\contentsline {paragraph}{}{11}{section*.27}
\contentsline {paragraph}{}{11}{section*.28}
\contentsline {paragraph}{}{11}{section*.29}
\contentsline {paragraph}{}{12}{section*.30}
\contentsline {section}{\numberline {5}Pr\IeC {\'\i }klad 5, variant F}{13}{section.5}
\contentsline {paragraph}{}{13}{section*.31}
\contentsline {paragraph}{}{13}{section*.32}
\contentsline {paragraph}{ \leavevmode {\color {mycol}\textbf { 3. Dosad\IeC {\'\i }me hodnoty zo zadania }}}{14}{section*.33}
\contentsline {paragraph}{}{14}{section*.34}
\contentsline {paragraph}{}{14}{section*.35}
\contentsline {paragraph}{}{14}{section*.36}
\contentsline {paragraph}{}{15}{section*.37}
\contentsline {paragraph}{}{15}{section*.38}
\contentsline {section}{\numberline {6}Pr\IeC {\'\i }klad 6, variant D}{16}{section.6}
\contentsline {section}{\numberline {7}V\IeC {\'y}sledky}{18}{section.7}
