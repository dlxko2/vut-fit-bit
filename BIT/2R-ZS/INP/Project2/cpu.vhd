-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2014 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): xstude22
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (0) / zapis (1)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

	--signaly pre counter_reg
	signal OUTCNT : std_logic_vector(12 downto 0); --velkost sa moze zmenit
	signal PROC_CNTINC : std_logic;
	signal PROC_CNTDEC : std_logic;
	signal PROC_CNTRESET : std_logic;

	--signaly pre prog_counter_reg
	signal OUTPC : std_logic_vector(12 downto 0);
	signal PROC_PCINC : std_logic;
	signal PROC_PCDEC : std_logic;
	
	--signaly pre ins_OUTPTR
	signal OUTPTR : std_logic_vector(12 downto 0);
	signal PROC_PTRINC : std_logic;
	signal PROC_PTRDEC : std_logic;

	signal MX_ADDR_SEL		: std_logic;
	signal MX_READDATA_SEL	: std_logic_vector (1 downto 0) := (others => '0');

	--sada instrukci procesoru 
	type inst_types is (I_INCPTR, I_DECPTR, I_INCVAL, I_DECVAL, I_WHILEB, I_WHILEE, I_WRITE, I_READ, I_HALT, SKIP);
	signal inst : inst_types; 

	type FSM_state is (STATE_FETCH, STATE_DECODE, STATE_INCVAL_1, STATE_INCVAL_START, STATE_DECVAL_1, STATE_DECVAL_START, STATE_WRITE_2, 
		STATE_WRITE_1, STATE_WRITE_START, STATE_WRITE_3, STATE_READ_START, STATE_HALT_START, STATE_WHILEB_1, STATE_WHILEB_START, STATE_WHILEB_2, STATE_WHILELOOP, 
		STATE_WHILEE_START, STATE_WHILEE2, STATE_WHILEE_1, STATE_WHILELOOP2 );
	signal PresState : FSM_state;
	signal NextState : FSM_state;

begin
	
	-- Proces pre CNT
	PROC_CNT : process(RESET, CLK, EN)
	begin
		-- Na zaciatku definujeme adresu ako 0
		if (RESET = '1') then
			OUTCNT <= (others=>'0');
		elsif (CLK = '1') and (CLK'event) then
			-- Ak budeme inkrementovat
			if(PROC_CNTINC = '1') then
	 			OUTCNT <= OUTCNT + 1;
			-- Ak budeme dekrementovat
			elsif(PROC_CNTDEC = '1') then
				OUTCNT <= OUTCNT - 1;
			-- Ak budeme resetovat
			elsif(PROC_CNTRESET = '1') then
				OUTCNT <= (others => '0');
			end if;
		end if;
	end process;

	-- Proces pre PC
	PROC_PC : process(RESET, CLK, EN )
	begin
		-- Ak je reset nadstav adresu na 0
		if (RESET = '1') then
	        OUTPC <= (others=>'0');
		        elsif (CLK = '1') and (CLK'event) then    	
	        	-- Ak budeme inkrementovat
				if(PROC_PCINC = '1') then
	 				OUTPC <= OUTPC + 1;
				-- Ak budeme dekrementovat
				elsif(PROC_PCDEC = '1') then
					OUTPC <= OUTPC - 1;
				end if;
		end if;
	end process;

	-- Proces pre PTR
	PROC_PTR : process(RESET, CLK, EN)
	begin
		-- Ak resetujeme zaciname na adrese 1
		if (RESET = '1') then
			OUTPTR <= "1000000000000";
        elsif (CLK = '1') and (CLK'event)  then
			-- Ak budeme inkrementovat
			if(PROC_PTRINC = '1') then
 				OUTPTR <= OUTPTR + 1;
 			-- Ak budeme dekrementovat
			elsif(PROC_PTRDEC = '1') then
				OUTPTR <= OUTPTR - 1;
			end if;
		end if;
	end process;	
	
	-- Proces na vyberanie typu adresy
	MX : process(CLK)
	begin
		case MX_ADDR_SEL is
			when '1' 	=> DATA_ADDR <= OUTPC;
			when '0' 	=> DATA_ADDR <= OUTPTR;
			when others => DATA_ADDR <= OUTPTR;
		end case;
	end process;

	-- Proces na vyber READ_DATA
	MX2 : process(CLK)
	begin
		case MX_READDATA_SEL is
		when "00" 	=> DATA_WDATA <= IN_DATA;
		when "01" 	=> DATA_WDATA <= DATA_RDATA - 1;
		when "10" 	=> DATA_WDATA <= DATA_RDATA + 1;
		when others	=> DATA_WDATA <= DATA_RDATA;
	end case;

end process;
	-- Proces pre INSTRUKCIE
	PROC_INST : process(DATA_RDATA)
	begin
		case DATA_RDATA is
			when "00111110" => inst <= I_INCPTR;
			when "00111100" => inst <= I_DECPTR; 
			when "00101011" => inst <= I_INCVAL; 
			when "00101101" => inst <= I_DECVAL; 
			when "01011011" => inst <= I_WHILEB; 
			when "01011101" => inst <= I_WHILEE; 
			when "00101110" => inst <= I_WRITE;  
			when "00101100" => inst <= I_READ;    
			when "00000000" => inst <= I_HALT;    
			when others => inst <= SKIP;   
		end case;
	end process;	
	
	-- Proces pre FSM
	PROC_FSM: process (RESET,CLK, EN)
	begin
		if(RESET = '1') then
			PresState <= STATE_FETCH;
		elsif(CLK = '1') and (CLK'event) then 
			if(EN = '1') then 
				PresState <= NextState;
			end if;
		end if;
	end process;

	-- Telo konecneho automatu
	FSM_nstate: process (PresState, inst, OUTPC, OUTPTR, OUTCNT,  DATA_RDATA, OUT_BUSY, IN_VLD, IN_DATA)
		begin

			-- Nulujeme a nadstavime pociatocne hodnoty
			OUT_DATA        <= DATA_RDATA;
			DATA_EN         <= '0';
			
			IN_REQ          <= '0';
			OUT_WE          <= '0';
			DATA_RDWR       <= '0'; 
			
			MX_READDATA_SEL <= "00";
			MX_ADDR_SEL     <= '0';
			PROC_CNTINC     <= '0';
			PROC_CNTDEC     <= '0';
			PROC_CNTRESET   <= '0';
			PROC_PCINC      <= '0';
			PROC_PCDEC      <= '0';
			PROC_PTRINC     <= '0';
		    PROC_PTRDEC     <= '0';	
			NextState 		<= STATE_FETCH;
			
			-- Zistime aky je aktualny stav
			case PresState is

				-- Ak nacitavame novu instrukciu
				when STATE_FETCH =>
					-- Budeme pouzivat PC register
					MX_ADDR_SEL <= '1';
					DATA_EN <= '1';
					NextState <= STATE_DECODE;

				-- Nacitavanie instrukcii a ich dekodovanie (nadstavenie hodnot)					
				when STATE_DECODE => 
					case (inst) is

						-- Ak zvysujeme hodnotu ukazatela
						-- Posleme signal na zvysenie a zvysime PC
						-- Dalej pokracujeme na dalsu instrukciu
						when I_INCPTR =>    
							PROC_PCINC <= '1';
							PROC_PTRINC <= '1'; 
							NextState <= STATE_FETCH;

						-- Ak znizujeme hodnotu ukazatela
						-- Posleme signal na znizenie hodnoty a zvysime PC
						-- Dalej pokracujeme na dalsiu instrukciu
						when I_DECPTR =>   
							PROC_PTRDEC <= '1'; 
							PROC_PCINC <= '1';
							NextState <= STATE_FETCH;

						-- Ak budeme preskakovat instrukciu
						-- Zvysime PC a nacitame dalsiu
						when SKIP => 
							PROC_PCINC <= '1';
							NextState <= STATE_FETCH;
						
						-- Ak zvysujeme hodnotu bunky
						-- Budeme riesit az neskor v dalsom stave
						when I_INCVAL =>    
							NextState <= STATE_INCVAL_START;

						-- Ak budeme znizovat hodnotu bunku
						-- Budeme riesit neskor v dalsich stavoch
						when I_DECVAL =>    
							NextState <= STATE_DECVAL_START;

						-- Ak budeme vypisovat hodnotu bunky
						-- Budeme riesit v dalsich stavoch
						when I_WRITE =>      
							NextState <= STATE_WRITE_START;

						-- Ak budeme nacitavat hodnotu
						-- Budeme riesit v dalsich stavoch
						when I_READ =>        
							NextState <= STATE_READ_START;

						-- Ak budeme koncit 
						-- Budeme riesit v dalsom stave
						when I_HALT =>
							NextState <= STATE_HALT_START;

						-- Ak zaciname cyklus 
						-- Budeme riesit v dalsich stavoch
						when I_WHILEB =>
							NextState <= STATE_WHILEB_START;

						-- Ak koncime cyklus 
						-- Budeme pokracovat v dalsich stavoch
						when I_WHILEE =>
							NextState <= STATE_WHILEE_START;
					end case;

				-- Zaciatok spracovanie konca cyklu
				-- Vynulujeme register CNT, PC-1
				when STATE_WHILEE_START =>
					PROC_CNTRESET <= '1';
					PROC_PCDEC <= '1';
					NextState <= STATE_WHILEE_1;

				--	Spracovanie konca cyklu
				-- Instrukcny register nadstav
				when STATE_WHILEE_1 =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '1';
					NextState <= STATE_WHILEE2;

				-- Spracovanie konca cyklu
				-- Instrukcie nadstavime ako adresu
				-- Ak CNT je prazdne a instrukcia [ - CNTRST a dalsia instrukcia
				-- Inak ak instrukcia ] - CNT+1, PC-1, opakujeme znova
				-- Ak je instrukcia [ - CNT-1, PC-1, opakujeme znova 
				when STATE_WHILEE2 =>		
					DATA_EN <= '1';
					MX_ADDR_SEL <= '1';

					if (OUTCNT = "0000000000000" and DATA_RDATA = "01011011") then 
						PROC_CNTRESET <= '1';
						NextState <= STATE_FETCH;
					else
						if(DATA_RDATA =  "01011101") then 
							PROC_CNTINC <= '1';
						elsif DATA_RDATA =  "01011011" then 	
							PROC_CNTDEC <= '1';
						end if;	

						PROC_PCDEC <= '1';	
						NextState <= STATE_WHILEE_1;
					end if;	

				-- Zaciatok spracovania zaciatku cyklu
				-- Vynulujeme register CNT
				when STATE_WHILEB_START =>
					PROC_CNTRESET <= '1';
					NextState <= STATE_WHILEB_1;

				-- Spracovavanie zaciatku cyklu
				-- Nadtavime na data
				when STATE_WHILEB_1 =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';
					NextState <= STATE_WHILEB_2;
				
				-- Spracovavanie zaciatku cykly
				-- Nadstavime na data, PC+1
				-- Ak nenacitame nulove data - CNTRST a skocime na stav LOOP
				-- Ak nacitame NULL - dalsia instrukcia
				when STATE_WHILEB_2 =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';		
					PROC_PCINC <= '1';

					if (DATA_RDATA = "000000000") then
						PROC_CNTRESET <= '1';
						NextState <= STATE_WHILELOOP; 
					else
						NextState <= STATE_FETCH;  
					end if;	

				-- Stav cyklenia while
				-- Nadstavime na instrukcie
				when STATE_WHILELOOP =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '1';
					NextState <= STATE_WHILELOOP2;

				-- Pokracovanie while cyklenia
				-- Nadstavime na instrukcie, PC+1
				-- Ak CNT je prazne a instrukcia je ] - CNTRST a dalsia instrukcia
				-- Inak ak instrukcia je ] - CNT-1 a skok na stav LOOP
				-- Ak instrukcia je [ - CNT+1 a skok na stav LOOP
				when STATE_WHILELOOP2 => 
					DATA_EN <= '1';
					MX_ADDR_SEL <= '1';
					PROC_PCINC <= '1';

					if (OUTCNT = "0000000000000" and DATA_RDATA = "01011101" ) then 	
						PROC_CNTRESET <= '1';
						NextState <= STATE_FETCH;
					else
						if(DATA_RDATA = "01011011") then 
							PROC_CNTINC <= '1';
						elsif (DATA_RDATA = "01011101") then
							PROC_CNTDEC <= '1';
						end if;
						NextState <= STATE_WHILELOOP;  
					end if;

				-- Zaciatok spracovania WRITE
				-- Nadstavime na citanie a na data
				when STATE_WRITE_START =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';
					DATA_RDWR <= '0';
					NextState <= STATE_WRITE_1;

				-- Spracovanie vypisu hodnoty
				-- Nadstavenie datovej adresy
				when STATE_WRITE_1 =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';
					NextState <= STATE_WRITE_2;
				
				-- Spracovanie vypisu hodnoty
				-- Podla busy naplnime WE (write enable)
				-- Ak nie je busy PC+1 a dalsi stav
				when STATE_WRITE_2 =>
					DATA_EN <= '1';

					if (OUT_BUSY = '0') then
						OUT_WE <= '1';
					else
						OUT_WE <= '0';
					end if;

					if(OUT_BUSY = '0') then 
						PROC_PCINC <= '1'; 
						NextState <= STATE_WRITE_3;
					end if;

				-- Spracovanie vypisu hodnoty
				-- Write enable = 1
				when STATE_WRITE_3 =>
					DATA_EN <= '1';
					OUT_WE <= '1';
					NextState <= STATE_FETCH;		

				-- Zaciatok spracovania dekrementovania bunky
				-- Nadstavime na data, na citanie
				when STATE_DECVAL_START =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';
					DATA_RDWR <= '0'; 
					NextState <= STATE_DECVAL_1;

				-- Spracovanie dekrementacie bunky
				-- Zapis, PC+1
				-- Ulozenie nacitanych dat - 1 potm dalsia instrukcia
				when STATE_DECVAL_1 =>
				    DATA_EN <= '1';
				    DATA_RDWR <= '1';
					PROC_PCINC <= '1';
					MX_READDATA_SEL <= "01";
					NextState <= STATE_FETCH;

				-- Zaciatok spracovania inkrementacie bunky
				-- Nadstavime citanie a na data
				when STATE_INCVAL_START =>
					DATA_EN <= '1';
					MX_ADDR_SEL <= '0';
					DATA_RDWR <= '0';
					NextState <= STATE_INCVAL_1;

				-- Spracovanie inkrementacie bunky
				-- Zapis, PC + 1
				-- Zapis do pamete precitanych dat + 1 potom dalsia instrukcia
				when STATE_INCVAL_1 =>
					DATA_EN <= '1';
					DATA_RDWR <= '1';
					PROC_PCINC <= '1';
					MX_READDATA_SEL <= "10";
					NextState <= STATE_FETCH;

				-- Zaciatok spracovania instrukcie READ
				-- Poziadavok na vstupne data, zapis
				-- Pracujeme s hodnotami, zapisovane data su vstupne
				-- Povolime cinnost podla VLD
				-- Ak su VLD - PC+1, dalsia instrukcia
				-- Ak nie su VLD - opakujeme 
				when STATE_READ_START =>
					IN_REQ <= '1';
					DATA_RDWR <= '1';

					MX_ADDR_SEL <= '0';
					MX_READDATA_SEL <= "00";
					
					if (IN_VLD = '0') then
						DATA_EN <= '0';
					else 
						DATA_EN <= '1';
					end if;

				   	if (IN_VLD = '1') then
						PROC_PCINC <= '1';
					    NextState <= STATE_FETCH;
					else	
						NextState <= STATE_READ_START;
					end if;

				-- Spracovanie instrukcie HALT opakovanim
				when STATE_HALT_START =>
					NextState <= STATE_FETCH;	

			end case;
		end process;	
end behavioral;
 