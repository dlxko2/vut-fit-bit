-- Projekt INP1, Martin Pavelka, xpavel27, 10.10.2014

-- Definovanie pouzitych kniznic
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_unsigned.all;
	use IEEE.std_logic_arith.all;

	-- Vytvorenie entity pre LED modul
	entity ledc8x8 is
		port (
			-- Zahrna signaly SMCLK a RESET
   			SMCLK, RESET: in std_logic;
   			-- Zahrna vektorove signaly ROW a LED
   			ROW, LED: out std_logic_vector(0 to 7)
			);
	end ledc8x8;

	-- Definovanie spravania entity LED modulu
	architecture DisplayName of ledc8x8 is

		-- Definovanie premennych
    	signal ce: std_logic;
    	signal LineNumber,TextRow: std_logic_vector(7 downto 0) := "01111111";
    	signal Counter: std_logic_vector(7 downto 0) := "00000000";
		begin
			-- Definicia procesu pocitadla
     		process (SMCLK,RESET)
     			begin
     				-- Ak signal reset vynuluje pocitadlo
         			if (RESET = '1') then
            			Counter <= "00000000";
            		-- V tomto pripade zvysime pocitadlo
         			elsif (SMCLK'event and SMCLK = '1') then
            			Counter <= Counter + 1;
            		-- Nadstavenie ce
        			if Counter (7 downto 0) = "11111111" then
           				ce <= '1';
        			else
           				ce <= '0';
	    			end if;
         			end if;
      		end process;

      		-- Proces posuvania riadkov
			process (RESET,SMCLK,ce,LineNumber)
    			begin
    				-- Asynchronne nadstavime prvy riadok
      				if (RESET = '1') then
          				LineNumber <= "10000000";
          			-- Ak ce je jedna posuniem riadky
      				elsif (SMCLK'event and SMCLK = '1' and ce = '1') then
          				LineNumber <= LineNumber(0) & LineNumber(7 downto 1);
      				end if;
      				-- Do ROW zasleme LineNumber
      				ROW <= LineNumber;
    		end process;

    		-- Vykreslovanie riadkov
			dec: process (LineNumber) begin
				-- Pre jednotlive riadky pridel text
				case (LineNumber) is
					when "00000001" => TextRow <= "11011101";
    				when "00000010" => TextRow <= "11001001";
					when "00000100" => TextRow <= "11010101";
					when "00001000" => TextRow <= "11011101";
					when "00010000" => TextRow <= "11000011";
					when "00100000" => TextRow <= "11011011";
					when "01000000" => TextRow <= "11010011";
					when "10000000" => TextRow <= "11011111";
					when others     => TextRow <= "11111111";
				end case;
			end process;

			-- Do ROW zasli riadok
			-- Do LED text riadka
			ROW <= LineNumber;
			LED <= TextRow;

	end DisplayName;
