/********************************************************
* Subor: main.c                                         *
* Nazov projektu: Implementacia jazyka IFJ14            *
* Varianta: Tým 003, varianta a/2/I                     *
* Vypracovali:                                          *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz         *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz            *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz           *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz           *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz          *
*********************************************************/

// Includes
#include "main.h"
#include "scanner.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"
#include "lablist.h"
#include "parser.h"
#include "interpret.h"

/* Hlavna funkcia programu volajuca parser a interpret */
int main(int argc, const char * argv[])
{
    // Skontroluj pocet argumentov
    if (argc != 2) return PROCESS_OTHER_ERROR;
    
    // Nacitaj subor z argumentu a over ho
    FILE *TokenFile = fopen(argv[1], "r");
    if (TokenFile == NULL) return PROCESS_OTHER_ERROR;
   
    // Inicializuje list instrukcii
    tListOfInstr iList;
    listInit(&iList);
    
    // Inicializujeme tabulku funkcii
    fList FunctionTable;
    FUNCTIONlistInit(&FunctionTable);
    
    // Inicializujeme LAB tabulku
    tListOfLabs LabList;
    LABlistInit(&LabList);
    
    // Inicializujeme TS Globalnu
    Node *GlobalSymbolTable;
    BSTInit(&GlobalSymbolTable);
 
    // Ulozisko zoznamu tokenov
    tDLList TokenList;
    DLInitList(&TokenList);
    
    // Ulozisko navratovej hodnoty
    int ReturnValue = 0;
    
    // Ak debug vypisanie suboru pre testy
    if (DEBUG_MODE) fprintf(stderr, "FILE:[%s]\n", argv[1]);
    
    // Zaciatok skenera
    if (DEBUG_MODE) printf("-----------SCANNING-----------\n");
    
    // Skus ziskat tokeny inak vrat chybu
    if (!GetTokens(&TokenList, TokenFile))
    {
        ReturnValue = LEXICAL_PROCESS_ERROR;
        if (DEBUG_MODE)
        {
            // Vypiseme navratovu hodnotu
            printf("\n--------------------\n");
            printf("|RETURNED VALUE:[%d]|\n", ReturnValue);
            printf("--------------------\n");
        }
        return ReturnValue;
    }
    
    // Zavolanie parseru a overenie navratovej hodnoty
    if (DEBUG_MODE) printf("-----------PARSING-----------\n");
    ReturnValue = FunctionDoParse(TokenFile, &iList, &LabList, &GlobalSymbolTable, &FunctionTable, &TokenList);

    // Pokracujeme len ak uspel parser
    if (ReturnValue == FUNCTION_SUCCESS)
    {
        // Vypiseme vygenerovane instrukcie
        if (DEBUG_MODE)
        {
            printf("\n-----------INSTRUCTIONS-----------\n");
            printIList(&iList);
        }
    
        // Zavolanie interpret
        if (DEBUG_MODE) printf("\n-----------INTERPRETING-----------\n");
        ReturnValue = intstrukcie(&iList, &LabList, GlobalSymbolTable, &FunctionTable);
    }
    
    // Vypiseme navratovu hodnotu
    if (DEBUG_MODE)
    {
        printf("\n--------------------\n");
        printf("|RETURNED VALUE:[%d]|\n", ReturnValue);
        printf("--------------------\n");
    }
    
    // Dealoc vsetkeho
    listFree(&iList);
    LABlistFree(&LabList);
    BSTDispose(&GlobalSymbolTable);
    
    // Vratenie navratovej hodnoty
    return ReturnValue;
}
