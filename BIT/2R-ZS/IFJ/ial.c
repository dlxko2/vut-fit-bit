/***********************************************************
* Subor: ial.c                                             *
* Nazov projektu: Implementacia algoritmov Heap Sort a KMP *
* Varianta: Tým 003, varianta a/2/I                        *
* Vypracovali:                                             *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz            *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz               *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz              *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz              *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz             *
***********************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include <string.h>

//+++++++++++++++++++++++++++++++ BINARY TREE TS +++++++++++++++++++++++++++++++//


/* Inicializacia stromu */
void BSTInit (Node **RootPtr)
{
	*RootPtr = NULL;
}

/* Vyhladavanie v binarnom strome, rekurzivne */
Node *BSTSearch (Node *RootPtr, string K)
{
    // Overenie pointeru korena, nenaslo kluc K
    if(RootPtr == NULL) return NULL;
    
    // Ak sa kluce rovnaju, naslo kluci K
    if (strCmpString(&RootPtr->Key, &K) == 0)
        return RootPtr;
    
    // Ak je kluc hladany mensi hladame vlavo
	else if (strCmpString(&K, &RootPtr->Key) < 0)
		return BSTSearch(RootPtr->LPtr, K);
	
    // Ak je kluc vecsi tak hladame vpravo
    else return BSTSearch(RootPtr->RPtr, K);
}

/* Vlozenie prvku do stromu, rekurzivne */
int BSTInsertNew (Node **RootPtr, string K, dataT typ)
{
    // Ak je *RootPtr == NULL, vkladam novy prvok
	if(*RootPtr == NULL)
	{
        // Alokacia noveho uzlu
	  	*RootPtr = malloc(sizeof(struct tBSTNode));
	  	if(RootPtr == NULL) return INTERNAL_PROGRAM_ERROR;
        
        // Inicializacia stringov
        strInit(&(*RootPtr)->Key);
        
        // Pridelime novemu prvku hodnoty
        strCopyString(&(*RootPtr)->Key, &K);
		(*RootPtr)->type = typ;
		(*RootPtr)->init = false;
	  	(*RootPtr)->LPtr = NULL;
	  	(*RootPtr)->RPtr = NULL;
		return FUNCTION_SUCCESS;
	}
    
    // Ak koren bol inicializovany a kluc je mensi vlozime vlavo
	else if(strCmpString(&K, &(*RootPtr)->Key) < 0)
		return BSTInsertNew (&(*RootPtr)->LPtr, K, typ);
    
    // Ak je kluc vecsi tak vkladame vpravo
	else if(strCmpString(&K, &(*RootPtr)->Key) > 0)
		return BSTInsertNew (&(*RootPtr)->RPtr, K, typ);
    
    // Kluce sa rovnaju, premenna uz je deklarovana, redeklaracia, chyba 3 !!
	else return SEMANTIC_UNDEF_REDEF_ERROR;
}

/* Vlozenie hodnoty do existujuceho prvku, inicializuje ju */
int BSTInsertInit(Node **RootPtr, string Key, Cont *Content)
{
	// Najprv overime ci dana premenna je deklarovana, ak nieje vrati chybu 3
	// Ulozime uzol kde ju (ne)naslo do pomocnej premennej
	Node *pomVar;
	pomVar = BSTSearch(*RootPtr, Key);
    
	// Nenaslo ju!! inicializacie nedeklarovanej premennej (mozno este je v globalnej TS)
	if(pomVar == NULL) return SEMANTIC_UNDEF_REDEF_ERROR;
	
	// Naslo ju - Prevedie naplnenie obsahu premennej podla toho co je tato premenna
	else
	{
		if(pomVar->type == INTEGER)
			pomVar->content.i = Content->i;
		else if(pomVar->type == REAL)
			pomVar->content.r = Content->r;
		else if(pomVar->type == BOOLEAN)
			pomVar->content.b = Content->b;
		else if(pomVar->type == STRING)
			pomVar->content.s = Content->s;
	}
	pomVar->init = true;
	return FUNCTION_SUCCESS;
}

/* pomocna funkcia na mazanie prvku v TS
 parametry: *PtrReplaced - ukazuje na uzol, kde bude presunuty napravejsi uzol stromu
 **RootPtr - TS
 navratove hodnoty:
 0 - uspech
 1 - neexistuje TS
 */

int ReplaceByMostRight (Node *PtrReplaced, Node **RootPtr) {
	if((*RootPtr) != NULL)
		return 1;
	if(((*RootPtr)->RPtr) != NULL)
		ReplaceByMostRight(PtrReplaced, &(*RootPtr)->RPtr);
	else{
		PtrReplaced->Key = (*RootPtr)->Key;		// presunu sa vsetky data
		PtrReplaced->type = (*RootPtr)->type;
		PtrReplaced->content = (*RootPtr)->content;
		PtrReplaced->init = (*RootPtr)->init;
		PtrReplaced = *RootPtr;
		
		Node *tmp = (*RootPtr);
		(*RootPtr) = (*RootPtr)->LPtr;
		free(tmp);
	}
	return 0;
}



/* Funkcia na vymazanie prvku z TS
 parametry: **RootPtr - TS
 Key - kluc prvku ktory sa ma vymazat
 
 navratove hodnoty:
 0 - uspesne vymazalo prvok s klucom Key
 1 - kluc Key nenaslo v TS!
 */

int BSTDelete(Node **RootPtr, string K){
    
	if((*RootPtr) != NULL){		// skontrolujeme ci vobec TS existuje
		// najdeme prvok ktory mame vymazat
		// hladame ho v TS
        if (strCmpString(&(*RootPtr)->Key, &K) < 0)
			BSTDelete((&(*RootPtr)->RPtr), K);
        if (strCmpString(&(*RootPtr)->Key, &K) > 0)
			BSTDelete((&(*RootPtr)->LPtr), K);
		// prvok sa nasiel a ma 2 podstromy
		else if(((*RootPtr)->LPtr) != NULL  && ((*RootPtr)->RPtr) != NULL)
			ReplaceByMostRight ((*RootPtr), (&(*RootPtr)->LPtr));
		// prvok sa nasiel a ma 1 podstrom
		else{
			Node *tmp = (*RootPtr);
			if((*RootPtr)->RPtr)
				*RootPtr = (*RootPtr)->RPtr;
			else
				*RootPtr = (*RootPtr)->LPtr;
			free(tmp);
			return 0;
		}
	}
	return 1;			// nenasiel sa
}

/* Funkcia na zmazanie binarneho stromu, rekurzivne */
void BSTDispose (Node **RootPtr)
{
    // Binarny strom je prazdny, skonci
	if((*RootPtr) == NULL) return;
    
    // Zmaze pravy podstrom
	BSTDispose(&(*RootPtr)->RPtr);
    
    // Zmaze lavy podstrom
	BSTDispose(&(*RootPtr)->LPtr);
    
    // Uvolnenie korena
	free(*RootPtr);
    
    // Nadstavime ho na NULL
	*RootPtr = NULL;
}

Node *BSTCopy (Node **RootPtr, Node **DestPtr)
{
    // Binarny strom je prazdny, skonci
	if((*RootPtr) == NULL) return NULL;
    
    // Alloc
    (*DestPtr) = malloc(sizeof(struct tBSTNode));
    if (*DestPtr == NULL) exit(INTERNAL_PROGRAM_ERROR);
    strInit(&(*DestPtr)->Key);
    
    (*DestPtr)->RPtr =BSTCopy(&(*RootPtr)->RPtr, &(*DestPtr)->RPtr);
    (*DestPtr)->LPtr = BSTCopy(&(*RootPtr)->LPtr, &(*DestPtr)->LPtr);
    
    // Plnenie
    strCopyString(&(*DestPtr)->Key, &(*RootPtr)->Key);
    (*DestPtr)->content = (*RootPtr)->content;
    (*DestPtr)->init = (*RootPtr)->init;
    (*DestPtr)->type = (*RootPtr)->type;
    
    return (*DestPtr);
}

int LenghtAlgorith (string *ZadanyRetazec)
{
    return ZadanyRetazec->length;
}

bool CopyAlgorith(string *ZadanyRetazec, int ZaciatokPodretazca, int DlzaKopirovania, string *Temp)
{
    // Osetrenie
    if ((ZaciatokPodretazca+DlzaKopirovania-1) > ZadanyRetazec->length)
        return false;
    
    // Nova dlzka bude dlzka kopirovania
    int NewLenght = DlzaKopirovania;
    
    // Alokuj miesto
    if (NewLenght >= Temp->allocSize)
    {
        if ((Temp->str = (char*) realloc(Temp->str, (unsigned int)NewLenght + 1)) == NULL)
            return false;
        Temp->allocSize = NewLenght + 1;
    }
    
    strncpy(Temp->str, ZadanyRetazec->str+ZaciatokPodretazca, DlzaKopirovania);
    Temp->length = NewLenght;

    return true;
}

//------------------------------- BINARY TREE TS -------------------------------//


//+++++++++++++++++++++++++++++++ HEAP SORT +++++++++++++++++++++++++++++++//


void HeapSort(char arr[], unsigned int N) // TODO: char*
{

    int t; /* Docasna hodnota - pomocna premenna. */
    unsigned int n = N, parent = N/2, index, child; /* Heap - hromada indexy. */
    /* Podmienka - Cykli dovtedy pokial nie je pole vysortovane. */
    while (1) { 
        if (parent > 0) { 
            /* Prvy stupen - Sortovanie heapu - hromady. */
            t = arr[--parent];  /* Uloz staru hodnotu do t */
        } else {
            /* Druhy stupen - Vyextrahuje miestne prvky. */
            n--;                /* Zmensi heap - hromadu. */
            if (n == 0) {
                return; /* Pokial je heap - hromada prazdna, skoncili sme. */
            }
            t = arr[n];         /* Uloz pocet prvkov pola do pomocnej premennej t. */
            arr[n] = arr[0];    /* Uloz korenovy vstup za heap - hromadu. */
        }
        /* Triediaca operacia - Posuvame tcko heapom - hromadou kvoli nahradeniu rodica. */
        index = parent; /* Zacni na rodicovskom indexe. */
        child = index * 2 + 1; /* Ziskaj index laveho potomka. */
        while (child < n) {
            /* Vyber najvacsieho potomka. */
            if (child + 1 < n  &&  arr[child + 1] > arr[child]) {
                child++; /* Existuje pravy potomok a je vacsi. */
            }
            /* Podmienka - je najvatsi potomok vacsi ako rodic? */
            if (arr[child] > t) {
                arr[index] = arr[child]; /* Prepis rodicovsky index potomkom. */
                index = child; /* Presun indexu na potomka. */
                child = index * 2 + 1; /* Najdi dalsieho potomka. */
            } else {
                break; /* Nasli sme miesto tcko - docasna premenna. */
            }
        }
        /* Uloz hodnotu z pomocnej premennej na nove miesto do heapu - hromady. */
        arr[index] = (char)t;
    }
    // TODO: return arr; 
} 

//------------------------------- HEAP SORT -------------------------------//


//+++++++++++++++++++++++++++++++ KNUTH-MORIS-PRATT ALGORITHM +++++++++++++++++++++++++++++++//


/*
 * Knuth-Morris-Prattov algoritmus vyhladavania zadaneho vzoru "vzor" s poctom znakov "dlzka_v"
 * v retazci "retazec" s poctom znakov "dlzka_r"
 * funkcia vracia index prveho znaku najdeneho vzoru v retazci 
 * (v pripade neuspechu vyhladavania vracia 0)
 */


//funkcia na vytvorenie pomocneho pola pre KMP algoritmus
void pole_pre_KMP(char* vzor, int dlzka, int* postupnost)
{
    int i = 0;
    int j = postupnost[0] = -1;
    while (i  <  dlzka) {
        while (j > -1 && vzor[i] != vzor[j]) j = postupnost[j];
        i++; j++;
        if (vzor[i] == vzor[j]) postupnost[i] = postupnost[j];
        else postupnost[i] = j;
    }
}

int KMP(char* vzor, int dlzka_v, char* retazec, int dlzka_r)
{
	if (dlzka_v > dlzka_r) return PROCESS_OTHER_ERROR; 
	if (strcmp(vzor,"") == 0) return 1;
	int postupnost[dlzka_v+1];
	pole_pre_KMP(vzor, dlzka_v, &postupnost[0]);

	int i=0;
	int j=0;
	while (j < dlzka_r) {
		if (vzor[i] == retazec[j]) {
			if (i == dlzka_v-1) {
				return ((j-dlzka_v+1)+1);
			}
			else 
			{
				i++; 
				j++;
			}
		}
		else 
			if (postupnost[i] == -1) 
			{
				i=0; 
				j++;
			}
		else i=postupnost[i];
	}
	return 0;
}

//------------------------------- KNUTH-MORIS-PRATT ALGORITHM -------------------------------//
