/*************************************************
* Subor: scanner.h                               *
* Nazov projektu: Hlavickovy subor pre scanner.c *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/
typedef struct tDLElem {                 /* prvek dvousměrně vázaného seznamu */
    LEXType Type;                                            /* užitečná data */
    string attrib;
    struct tDLElem *lptr;          /* ukazatel na předchozí prvek seznamu */
    struct tDLElem *rptr;        /* ukazatel na následující prvek seznamu */
} *tDLElemPtr;

typedef struct {                                  /* dvousměrně vázaný seznam */
    tDLElemPtr First;                      /* ukazatel na první prvek seznamu */
    tDLElemPtr Act;                     /* ukazatel na aktuální prvek seznamu */
    tDLElemPtr Last;                    /* ukazatel na posledni prvek seznamu */
} tDLList;

void DLInitList (tDLList *L);
void DLDisposeList (tDLList *L);
void DLInsertLast(tDLList *L, LEXType Type, string Attribut);
LEXType DLCopy (tDLList *L, string *val);
bool GetTokens(tDLList *L, FILE *TokenFile);
void DLSucc (tDLList *L);
int GetNewToken(tDLList *L, string *ReturnedAttribute, bool DetectEOL);
int UngetGetToken(tDLList *L, string *ReturnedAttribute, bool DetectEOL);

