/****************************************************
* Subor: lablist.c                                  *
* Nazov projektu: Implementacia zoznamu navesti     *
* Varianta: Tým 003, varianta a/2/I                 *
* Vypracovali:                                      *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz     *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz        *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz       *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz       *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz      *
*****************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"
#include "lablist.h"

void LABlistInit(tListOfLabs *L)
// Funkce inicializuje seznam navesti
{
    L->first  = NULL;
    L->ActualID = 0;
}

void LABlistFree(tListOfLabs *L)
// Funkce dealokuje seznam navesti
{
    // Pre kazdy prvok
    tLabItem *ptr;
    while (L->first != NULL)
    {
        // Premosti a dealokuj
        ptr = L->first;
        L->first = L->first->nextItem;
        free(ptr);
    }
}

bool LABlistInsertLast(tListOfLabs *L, tInstr *I, LabType Type)
// Vlozi nove navestie na koniec zoznamu
{
    // Alokujeme novy prvok
  	tLabItem *newItem;
	newItem = malloc(sizeof (tLabItem));
    if (newItem == NULL) return false;
    
    // Naplnime strukturu hodnotami
    L->ActualID++;
    newItem->LabInstPointer = I;
    newItem->Type = Type;
    newItem->ID = L->ActualID;
 	newItem->nextItem = NULL;
    
    // Premostime
    newItem->nextItem = L->first;
    L->first = newItem;
    
    // Vrat uspech
    return true;
}

tInstr *LABlistSearch(int ID, tListOfLabs *L)
// Vrati odkaz na instrukciu v zozname podla ID
{
    // Zacneme prvym prvkom
    tLabItem *TempItem = L->first;
    if (TempItem == NULL) return NULL;
    
    // Kym je nejaky prvok
    while (TempItem != NULL)
    {
        // Porovnaj IDcka a bud skoc na dalsi alebo vrat
        if (TempItem->ID == ID)
            return TempItem->LabInstPointer;
        else
            TempItem = TempItem->nextItem;
    }
    
    // Ak nenajde vrati NULL
    return NULL;
}
