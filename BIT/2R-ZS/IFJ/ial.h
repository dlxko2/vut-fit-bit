/*************************************************
* Subor: ial.h                                   *
* Nazov projektu: Hlavickovy subor k ial.c 		 *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/

#ifndef IAL_H
#define IAL_H

// Vycet datovych typov
typedef enum{
	INTEGER = 1,
	REAL = 2,
	STRING = 3,
	BOOLEAN = 4,
} dataT;

// Unia pre datove typy
typedef union {
	int i;
	double r;
	bool b;
	string s;
} Cont;

// Jeden uzol v strome
typedef struct tBSTNode {
	string Key;	// identifikator v TS
	dataT type;	// datovy typ
	Cont content; // obsah uzlu
	bool init; // je premenna inicializovana?
	struct tBSTNode *LPtr; // lavy podstrom
	struct tBSTNode *RPtr; // pravy podstrom
} Node;

// Funkcie v zdrojaku
Node *BSTCopy (Node **RootPtr, Node **DestPtr);
void BSTInit   (Node **);
Node *BSTSearch  (Node *, string Key);
int BSTInsertNew (Node **, string, dataT);
int BSTInsertInit(Node **, string, Cont *);
int ReplaceByMostRight (Node *PtrReplaced, Node **RootPtr);
int BSTDelete(Node **RootPtr, string Key);
void BSTDispose(Node **);


void HeapSort(char arr[],unsigned int N);
void pole_pre_KMP(char* vzor, int dlzka, int* postupnost);
int KMP(char* vzor, int dlzka_v, char* retazec, int dlzka_r);
bool CopyAlgorith(string *ZadanyRetazec, int ZaciatokPodretazca, int DlzaKopirovania, string *VyslednyRetazec);
int LenghtAlgorith (string *ZadanyRetazec);
#endif

