/********************************************************
* Subor: interpret.h                                    *
* Nazov projektu: Hlavickovy subor pre interpret.h      *
* Varianta: Tým 003, varianta a/2/I                     *
* Vypracovali:                                          *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz         *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz            *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz           *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz           *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz          *
*********************************************************/

#ifndef INTERPRET_H
#define INTERPRET_H 

// prvok zasobniku
typedef struct stackTS{
	Node *LocalSymbolTable;		// ukazatel na Node *
    int ActualFunction;
    tInstr *ActualFunctionRET;
    bool WithPush;
	struct stackTS *next;	// dalsia polozka v zasobniku
    function *ActualFunctionPtr;
} sTS;


int intstrukcie(tListOfInstr *instList, tListOfLabs *LabList, Node *ST, fList *FunctionTable);

typedef struct ItemExp
{
    dataT ContType;
    Cont Content;
}
InterItem;

typedef struct stackExp{
	InterItem *top;
	struct stackExp *next;
} sInter;

#endif
