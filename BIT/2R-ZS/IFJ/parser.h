/*************************************************
* Subor: parser.h                                *
* Nazov projektu: Hlavickovy subor pre parser.c  *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/

typedef struct
{
  int Type;
  string attr;
} TokenStructure;


int FuntionSintxVar(TokenStructure *TokenStruc, Node **FST, bool Function);
int FunctionSintxSlozPrik(TokenStructure *TokenStruc, bool ActiveFunction, bool Repeat);
int FunctionSintxPrikazPriradenie(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxIf(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxWhile(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxRepeat(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxReadln(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxWrite(TokenStructure *TokenStruc, bool ActiveFunction);
int FunctionSintxSort(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction);
int FunctionSintxFind(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction);
int FunctionSintxCopy(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction);
int FunctionSintxLenght(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction);
int FunctionSintxRunFunction(TokenStructure *TokenStruc, bool WithPush);

int FunctionSintxFunction(TokenStructure *TokenStruc);
int FunctionSintxTeloBegin(TokenStructure *TokenStruc);
int FunctionSintxFunction(TokenStructure *TokenStruc);
int FunctionSintxFunctionParams(TokenStructure *TokenStruc, pList *ParamList);
bool FunctionJeTyp(TokenStructure *TokenStruc);
int FunctionDoParse(FILE *TokenFile, tListOfInstr *iList, tListOfLabs *ListLab, Node **ST, fList *FT, tDLList *TokenList);
int FunctionSintxProgram(TokenStructure *TokenStruc);
