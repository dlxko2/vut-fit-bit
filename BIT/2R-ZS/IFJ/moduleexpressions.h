/*********************************************************
* Subor: moduleexpressions.h                             *
* Nazov projektu: Hlavickovy subor k moduleexpressions.c *
* Varianta: Tým 003, varianta a/2/I                      *
* Vypracovali:                                           *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz          *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz             *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz            *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz            *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz           *
**********************************************************/
#ifndef EXP_H
#define EXP_H

// Struktura prvku zasobnika
typedef struct ItemExp
{
    bool IsID; // Ci je identifikator
    char ExpType; // Typ vyrazu
    dataT ContType; // Data typ
    Cont Content; // Data
}ExpItem;

// Struktura zasobnika
typedef struct stackExp
{
	ExpItem *top; // Vrchol zasobnika
	struct stackExp *next; // Dalsi prvok
} sExp;

// Funkcie zo zdrojaku
int FunctionParseExpression(TokenStructure *Token, Node *SymbolTable, Node *GlobSymbolTable, tListOfInstr *InstList, tDLList *TokenList, bool Priradenie, dataT *ResultType);
ExpItem *FindTopTerm(sExp *);
#endif
