/***************************************************************************
* Subor: modulebinaretree.c                                                *
* Nazov projektu: Operacie nad binarnym stromom, search, insert, dispose   *
*				  Binarny strom ako implementacna metoda tabulky symbolov  *	
* Varianta: Tým 003, varianta a/2/I                                        *
* Vypracovali:                                                             *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz                            *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz                               *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz                              *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz                              *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz                             *
***************************************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"

void PARAMlistFree(pList *L);
void FUNCTIONlistFree(fList *L);

//================================OPERACIE NAD TS FUNKCII ==========================
void PARAMlistInit(pList *L)
// Funkce inicializuje seznam navesti
{
    L->first  = NULL;
    L->last = NULL;
}

void PARAMlistFree(pList *L)
// Funkce dealokuje seznam navesti
{
    // Pre kazdy prvok
    param *ptr;
    while (L->first != NULL)
    {
        // Premosti a dealokuj
        ptr = L->first;
        L->first = L->first->next;
        free(ptr);
    }
}

bool PARAMlistSearch(pList *L, string Name)
// Funkce dealokuje seznam navesti
{
    // Pre kazdy prvok
    param *ptr = L->first;
    while (ptr != NULL)
    {
        if (strCmpString(&Name, &ptr->name) == 0)
            return true;
        ptr = ptr->next;
    }
    return false;
}

bool PARAMlistInsertLast(pList *L, string Name, dataT Type)
// Vlozi nove navestie na koniec zoznamu
{
    // Alokujeme novy prvok
  	param *newItem;
	newItem = malloc(sizeof (param));
    if (newItem == NULL) return false;
    
    // Naplnime strukturu hodnotami
    newItem->typ = Type;
    strInit(&newItem->name);
    strCopyString(&newItem->name, &Name);
    
    newItem->prev = L->last;	// doteraz posledny bude predposledny
    newItem->next = NULL; // bude posledny napravo je NULL
    
    if (L->last == NULL)		// posledny prvok je prazdny = zoznam je prazdny, novy prvok bude aj prvym aj poslednym
        L->first = newItem;
    else				// inak vlozime prvok za posledny
        L->last->next = newItem;
    
    L->last = newItem;		// posledny prvok = novy
    
    // Vrat uspech
    return true;
}

void FUNCTIONlistInit(fList *L)
// Funkce inicializuje seznam navesti
{
    L->first  = NULL;
    L->ActualID = 0;
}

void FUNCTIONlistFree(fList *L)
// Funkce dealokuje seznam navesti
{
    // Pre kazdy prvok
    function *ptr;
    while (L->first != NULL)
    {
        // Premosti a dealokuj
        ptr = L->first;
        L->first = L->first->next;
        free(ptr);
    }
}

bool FuncInst(fList *L, bool Declared, bool Defined, string Name, int LABIndex, Node *LocTS, pList *ParList, dataT Return)
// Vlozi nove navestie na koniec zoznamu
{
    // Alokujeme novy prvok
  	function *newItem = NULL;
    newItem = FUNCTIONlistIsFunction(L, &Name);
    
    if (newItem == NULL)
    {
        // Alloc
        newItem = malloc(sizeof (function));
        if (newItem == NULL) return false;
        
        // Naplnime strukturu hodnotami
        L->ActualID++;
        newItem->declared = Declared;
        newItem->defined = Defined;
        strInit(&newItem->Key);
        strCopyString(&newItem->Key, &Name);
        newItem->LabTableIndex = LABIndex;
        newItem->localTS = LocTS;
        newItem->paramList = ParList;
        newItem->returnV = Return;
        newItem->ID = L->ActualID;
        
        // Premostime
        newItem->next = L->first;
        L->first = newItem;
    }
    else
    {
        newItem->declared = Declared;
        newItem->defined = Defined;
        newItem->LabTableIndex = LABIndex;
        newItem->localTS = LocTS;
    }
    
    // Vrat uspech
    return true;
}

function *FUNCTIONlistSearch(int ID, fList *L)
// Vrati odkaz na instrukciu v zozname podla ID
{
    // Zacneme prvym prvkom
    function *TempItem = L->first;
    if (TempItem == NULL) return NULL;
    
    // Kym je nejaky prvok
    while (TempItem != NULL)
    {
        // Porovnaj IDcka a bud skoc na dalsi alebo vrat
        if (TempItem->ID == ID)
            return TempItem;
        else
            TempItem = TempItem->next;
    }
    
    // Ak nenajde vrati NULL
    return NULL;
}

function *FUNCTIONlistIsFunction(fList *L, string *Identifikator)
// Vrati odkaz na instrukciu v zozname podla ID
{
    // Zacneme prvym prvkom
    function *TempItem = L->first;
    if (TempItem == NULL) return NULL;
    
    // Kym je nejaky prvok
    while (TempItem != NULL)
    {
        // Porovnaj IDcka a bud skoc na dalsi alebo vrat
        if (strCmpString(&TempItem->Key, Identifikator) == 0)
            return TempItem;
        else
            TempItem = TempItem->next;
    }
    
    // Ak nenajde vrati NULL
    return NULL;
}
