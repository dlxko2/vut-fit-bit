/***********************************************************
* Subor: lablist.c                                         *
* Nazov projektu: Hlavickovy subor k lablist.c             *
* Varianta: Tým 003, varianta a/2/I                        *
* Vypracovali:                                             *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz            *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz               *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz              *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz              *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz             *
************************************************************/

// Vycet typov navesti
typedef enum
{
	LAB_IF = 1,
	LAB_THEN = 2,
	LAB_ELSE = 3,
    LAB_ENDIF = 4,
    LAB_WHILE = 5,
    LAB_DO = 6,
    LAB_ENDWHILE = 7,
    LAB_FUNCTION = 8,
    LAB_REPEAT = 9,
    LAB_ENDREPEAT = 10,
} LabType;

// Prvok zoznamu navesti
typedef struct labitem
{
    int ID; // ID navestia
    LabType Type; // Typ navestia
    tInstr *LabInstPointer; // Pointer na instrukciu navestia
    struct labitem *nextItem; // Dalsi prvok zoznamu
}  tLabItem;

// Zoznam navesti struktura
typedef struct
{
    int ActualID; // Aktualne ID v zozname
    struct labitem *first;  // Ukazatel na prvy prvok
} tListOfLabs;

// Definicia funkcii v zdrojaku
void LABlistInit(tListOfLabs *L);
void LABlistFree(tListOfLabs *L);
bool LABlistInsertLast(tListOfLabs *L, tInstr *I, LabType Type);
tInstr *LABlistSearch(int ID, tListOfLabs *L);
