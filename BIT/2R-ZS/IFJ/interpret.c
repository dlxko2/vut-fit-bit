/********************************************************
* Subor: interpret.c                                    *
* Nazov projektu: Implementacia interpretu jazyka IFJ14 *
* Varianta: Tým 003, varianta a/2/I                     *
* Vypracovali:                                          *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz         *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz            *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz           *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz           *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz          *
*********************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"
#include "lablist.h"
#include "interpret.h"
#include "scanner.h"
#include "parser.h"

void EndProgram(tListOfInstr *InstList, int ErrorCodeNum);
sTS *pushFS(sTS *head, Node *LocalST, int ActualFunction, tInstr *ActualRET, bool WithPush, function *ActualFunctionPtr);
void initFS(sTS **head);
sTS *popFS(sTS *head);
bool isEmptyFS(sTS *head);
void initSI(sInter **head);
sInter *pushSI(sInter *head, InterItem *a);
sInter *popSI(sInter *head);
bool isEmptyI(sInter *head);



// Podpora dealokovaneho ukoncenia
int ErrorCode;
void EndProgram(tListOfInstr *InstList, int ErrorCodeNum)
{
    listGoto(InstList, findEnd(InstList));
    ErrorCode = ErrorCodeNum;
}

//==================================== ZASOBNIK TABULIEK SYMBOLOV (RAMEC) =====================================
// Init zasobniku
void initFS(sTS **head)
{
	*head = NULL;
}

/* Funkcia pre push a top, vytvorena nova TS, ktora bude na vrchole
   *head = vrchol zasobniku
    Node *TS = TS ktora bude vlozena
	NULL - nepodaril sa malloc chyba 99, skontroluj funkcia co ma volas! :D
    head - uspesne vlozilo novu polozku na vrchol, vracia jeho adresu */
sTS *pushFS(sTS *head, Node *LocalST, int ActualFunction, tInstr *ActualRET, bool WithPush, function *ActualFunctionPtr)
{
    // Malloc
	struct stackTS *tmp;
	tmp = (struct stackTS*)malloc(sizeof(struct stackTS));
	if(tmp == NULL) return NULL;
	
    BSTInit(&tmp->LocalSymbolTable);
    BSTCopy(&LocalST, &tmp->LocalSymbolTable);

    // Naplnenie
    tmp->ActualFunctionPtr = ActualFunctionPtr;

    tmp->ActualFunction = ActualFunction;
    tmp->ActualFunctionRET = ActualRET;
    tmp->WithPush = WithPush;
	tmp->next = head;
	
    // Mostenie
	head = tmp;				
	return head;		
}

// Funkcia pop - vyprazdni TS z ramca a uvolni pamat
sTS *popFS(sTS *head)
{
	sTS *tmp;		
	tmp = head;			
	head = (head)->next;
	free(tmp);			
	return head;			
}

/* Je zasobnik prazdny */
bool isEmptyFS(sTS *head)
{
	if(head == NULL) return true;
	else return false;
}

//=========================================== ZASOBNIK PRE INTERPRET ==========================================
// Globalna premenna zasobnika
sInter *InterpretStack;

// Init zasobnika
void initSI(sInter **head)
{
	*head = NULL;
}

// Push prvka na zasobnik
sInter *pushSI(sInter *head, InterItem *a)
{
    // Malloc
	sInter *tmp;
	tmp = (struct stackExp*)malloc(sizeof(struct stackExp));
	if(tmp == NULL) return NULL;
	
    // Malloc
    InterItem *tmpExp = malloc(sizeof(struct ItemExp));
    
    // Plnenie
    tmpExp->Content = a->Content;
    tmpExp->ContType = a->ContType;
    
    // Plnenie
	tmp->top = tmpExp;
	tmp->next = head;
    
    // Mostenie
	head = tmp;
	return head;
}

// Pop prvka zo zasobnika
sInter *popSI(sInter *head)
{
	sInter *tmp;
	tmp = head;
	head = head->next;
	free(tmp);
	return head;
}

// Ci je prazdny
bool isEmptyI(sInter *head)
{
	if(head == NULL) return true;
	else return false;
}

// =============================================== INTERPRET =================================================
int intstrukcie(tListOfInstr *instList, tListOfLabs *LabList, Node *ST, fList *FunctionTable)
{
    // Inicializacia zasobnika
    sTS *FunctionStack = NULL;
    initFS(&FunctionStack);
    function *TmpFunc = NULL;
    
    // Inicializacia errorcode
    ErrorCode = 0;
    
    // String for readln
    string ReadString;
    strInit(&ReadString);
    int ReadChar = 0;
    
    // String pre short
    string SortString;
    strInit(&SortString);

    // String pre copy
    string CopyString;
    strInit(&CopyString);
    
    // Node
    Node *STItem;
    
    // Nastavi prvy prvok 
	listFirst(instList);
    
    // Inicializuje aktualnu intrukciu
	tInstr *I;
    
    // Definovanie zasobnika
    initSI(&InterpretStack);
    
    // Definovanie prvku
    InterItem InterStackItem;
    InterItem InterStackItem2;
    InterItem InterStackItem3;

    // Priznak pre instrukciu I_COMP
    bool cmp_flag = false;
    bool cmp_flag_set = false;
    
    // Nadstavime START
    listGoto(instList, findSTART(instList));
    
    // Cykly pre vsetky instrukcie
	while (1)
	{
        // Nacita dalsiu instrukciu a overi ju
        I = listGetData(instList);
        if (!I) return FUNCTION_SUCCESS;
        
        // Vyberie pozadovanu instrukciu
		switch (I->instType)
		{
            case I_JMPCONT:
                cmp_flag_set = false;
                // Pushneme ak treba
                if (FunctionStack != NULL)
                {
                    if (FunctionStack->WithPush)
                    {
                        // Najdeme co treba pushnut
                        Node *PushNode = BSTSearch(FunctionStack->LocalSymbolTable, FunctionStack->ActualFunctionPtr->Key);
                    
                        // Naplnime pushitem a pushneme a overime
                        InterStackItem.ContType = PushNode->type;
                        InterStackItem.Content = PushNode->content;
                        InterpretStack = pushSI(InterpretStack, &InterStackItem);
                        if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    }
                
                    listGoto(instList, FunctionStack->ActualFunctionRET);
                    FunctionStack = popFS(FunctionStack);
                }
                else EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
                
            case I_JMPFUNC:
                cmp_flag_set = false;
                // Overime adresu
                if (I->addr1.Type != STRING && I->addr2.Type != BOOLEAN)
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                
                // Najdeme si funkciu podla parametru v adrese
                TmpFunc = FUNCTIONlistIsFunction(FunctionTable, &I->addr1.AdressValue.s);
                
                if (TmpFunc->defined == false)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                // Naplnime zasobnik funkcii
                FunctionStack = pushFS(FunctionStack, TmpFunc->localTS, TmpFunc->ID, I, I->addr2.AdressValue.b, TmpFunc);
                
                // Naplnime parametre
                if (FunctionStack->ActualFunctionPtr->paramList->first != NULL)
                {
                    // Nadstavime prvy prvok
                    param *TempCounter = FunctionStack->ActualFunctionPtr->paramList->last;
                    
                    // Cyklime kym je prvok
                    while (TempCounter != NULL)
                    {
                        // Overime ci nie je prazdny zasobnik
                        if (isEmptyI(InterpretStack))
                        {
                            EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                            break;
                        }
                        
                        // Popneme data
                        InterStackItem = *InterpretStack->top;
                        InterpretStack = popSI(InterpretStack);
                        
                        // Overime typ
                        if (InterStackItem.ContType == TempCounter->typ)
                        {
                            // Vlozime do LTS
                            if (BSTInsertInit(&FunctionStack->LocalSymbolTable, TempCounter->name, &InterStackItem.Content) != FUNCTION_SUCCESS)
                            {
                                EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                                break;
                            }
                        }
                        // Ak sa typy nerovnaju 
                        else
                        {
                            EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                            break;
                        }
                        
                        // Posunieme sa na dalsi parameter
                        TempCounter = TempCounter->prev;
                    }
                }
                // Skocime na funkciu
                listGoto(instList, LABlistSearch(FunctionStack->ActualFunctionPtr->LabTableIndex, LabList));
                
                
                break;
         
                // instrukcia pre RETURN
            case I_RETURN:break;
                
            // instrukcia pre zavolanie funkcie
      		case I_CALL: break;
                
            // Instrukcia nic nerobi
            case I_LAB:
                cmp_flag_set = false;
                break;
                
			// Vypis zaciatku programu
			case I_START:
                cmp_flag_set = false;
                if (DEBUG_MODE) printf("Zaciatok programu:\n");
                break;
            
            // Ak je false skace na indexLAB v adrese
            case I_WJMPFALSE:
                // Ak nebol cmp_flag pozri na zasobnik
                if (!cmp_flag_set)
                {
                    // Over ci je prazdny zasobnik
                    if (isEmptyI(InterpretStack))
                    {
                        EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                        break;
                    }
                    // Nacitaj prvok z top-u, popni a over
                    InterStackItem = *InterpretStack->top;
                    InterpretStack = popSI(InterpretStack);
                    
                    if (InterStackItem.ContType != BOOLEAN)
                    {
                        EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                        break;
                    }
                    cmp_flag = InterStackItem.Content.b;
                }
                if(!cmp_flag) listGoto(instList, LABlistSearch(I->addr1.AdressValue.i, LabList));
                cmp_flag_set = false;
                break;
            
            // Ak je true skace na adresu indexLAB v adrese
            case I_WJMP:
                listGoto(instList, LABlistSearch(I->addr1.AdressValue.i, LabList));
                cmp_flag_set = false;
                break;
                
			// Ukonci program a dealokuje zdroje	
			case I_STOP:
                cmp_flag_set = false;
                if (DEBUG_MODE) printf("Koniec programu:\n");
                strFree(&ReadString);
                strFree(&SortString);
                strFree(&CopyString);
                // TODO INTERPRET STACK
                // TODO INTERPRETSTACKITEM STRING
                return ErrorCode;
                break;
            
            // Pushne z adresy na zasobnik
            case I_PUSH:
                cmp_flag_set = false;
                InterStackItem.ContType = I->addr1.Type;
                InterStackItem.Content = I->addr1.AdressValue;
                InterpretStack = pushSI(InterpretStack, &InterStackItem);
                if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
              
            // Pushne identifikator s menom z adresy na zasobnik
            case I_PUSHID:
                cmp_flag_set = false;
                // Over ci ide o string v adrese kde je ocakavany
                if (I->addr1.Type != STRING)
                {
                    EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                    break;
                }
                // Vyhladaj v TS a over ci nasiel alebo nie
                if (FunctionStack != NULL)
                {
                    STItem = BSTSearch(FunctionStack->LocalSymbolTable, I->addr1.AdressValue.s);
                    if (STItem == NULL)
                        STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                }
                else STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                
                if (STItem == NULL)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                
                if (STItem->init == false)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                // Pushni na zasobnik
                InterStackItem.ContType = STItem->type;
                InterStackItem.Content = STItem->content;
                InterpretStack = pushSI(InterpretStack, &InterStackItem);
                if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;

            // Implementacia readln prikazu
            case I_READ:
                cmp_flag_set = false;
                // Over ci ide o string v adrese kde je ocakavany
                if (I->addr1.Type != STRING)
                {
                    EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                    break;
                }

                // Vyhladaj v TS a over ci nasiel alebo nie
                if (FunctionStack != NULL)
                {
                    STItem = BSTSearch(FunctionStack->LocalSymbolTable, I->addr1.AdressValue.s);
                    if (STItem == NULL)
                        STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                }
                else STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                
                if (STItem == NULL)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                // Vymaze obsah stringu
                strClear(&ReadString);

                // Ak boolean tak je chyba
                if (STItem->type == BOOLEAN)
                    return SEMANTIC_TYPE_NUMPARAM_ERROR;
                
                // Nacitavaj znaky
                while (1)
                {
                    // Nacitame znak
                    ReadChar = getc(stdin);
                    
                    // Ak je koniec ukoncime
                    if (ReadChar == '\n' || ReadChar == EOF) break;
                    
                    // Ak je REAL alebo INTEGER
                    if (STItem->type == REAL || STItem->type == INTEGER)
                        
                        // Ak je prazdne miesto preskoc
                        if (isspace(ReadChar)) continue;
                    
                    // Ak integer len cisla
                    if (STItem->type == INTEGER)
                        if(!isdigit(ReadChar) && (ReadChar != '+') && (ReadChar != '-'))
                            return PROCESS_NUMLOAD_ERROR;
                    
                    if (STItem->type == REAL)
                        if (!isdigit(ReadChar) && (ReadChar != '.') && (ReadChar != '+') && (ReadChar != '-'))
                            return PROCESS_NUMLOAD_ERROR;
                        
                    // Vloz znak
                    strAddChar(&ReadString, (char)ReadChar);
                }
                // Ak ide o string nacitany
                if (STItem->type == STRING)
                {
                    // Alokuj nadstav a skopiruj
                    strInit(&InterStackItem.Content.s);
                    InterStackItem.ContType = STRING;
                    strCopyString(&InterStackItem.Content.s, &ReadString);
                }
                // Ak ide o real
                else if (STItem->type == REAL)
                {
                    // Nadstav typ a skopiruj
                    InterStackItem.ContType = REAL;
                    InterStackItem.Content.r = atof(strGetStr(&ReadString));
                }
                // Ak ide o integer
                else if (STItem->type == INTEGER)
                {
                    // Nadstav typ a skopiruj
                    InterStackItem.ContType = INTEGER;
                    InterStackItem.Content.i = atoi(strGetStr(&ReadString));
                }
                // Pushni na zasobnik
                InterpretStack = pushSI(InterpretStack, &InterStackItem);
                if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
           
            // Funkcia na vypisanie identifikatoru z adresy
            case I_WRITEID:
                cmp_flag_set = false;
                // Over ci ide o string v adrese kde je ocakavany
                if (I->addr1.Type != STRING)
                {
                    EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                    break;
                }
                // Vyhladaj v TS a over ci nasiel alebo nie
                if (FunctionStack != NULL)
                {
                    STItem = BSTSearch(FunctionStack->LocalSymbolTable, I->addr1.AdressValue.s);
                    if (STItem == NULL)
                        STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                }
                else STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                
                if (STItem == NULL)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                
                if (STItem->init == false) return PROCESS_UNINITIALIZED_ERROR;
               
                // Vypis podla typu
                if (STItem->type == INTEGER) printf ("%d", STItem->content.i);
                else if (STItem->type == REAL) printf ("%g", STItem->content.r);
                else if (STItem->type == STRING) printf ("%s", strGetStr(&STItem->content.s));
                else if (STItem->type == BOOLEAN)
                {
                    if (STItem->content.b == true) printf ("TRUE");
                    else printf ("FALSE");
                }
                break;
                
            // Instrukcia pre vypis hodnoty
            case I_WRITE:
                cmp_flag_set = false;
                // Podla tyou vypise z adresy
                if (I->addr1.Type == INTEGER) printf ("%d", I->addr1.AdressValue.i);
                else if (I->addr1.Type == REAL) printf ("%g", I->addr1.AdressValue.r);
                else if (I->addr1.Type == STRING) printf ("%s", strGetStr(&I->addr1.AdressValue.s));
                else if (I->addr1.Type == BOOLEAN)
                {
                    if (I->addr1.AdressValue.b == true) printf ("TRUE");
                    else printf ("FALSE");
                }
                break;

            // Instrukcia pre priradenie
            case I_ASSIGN:
                cmp_flag_set = false;
                // Over ci ide o string v adrese kde je ocakavany
                if (I->addr1.Type != STRING)
                {
                    EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                    break;
                }
                // Vyhladaj v TS a over ci nasiel alebo nie
                if (FunctionStack != NULL)
                {
                    STItem = BSTSearch(FunctionStack->LocalSymbolTable, I->addr1.AdressValue.s);
                    if (STItem == NULL)
                        STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                }
                else STItem = BSTSearch(ST, I->addr1.AdressValue.s);
                
                if (STItem == NULL)
                {
                    EndProgram(instList, PROCESS_UNINITIALIZED_ERROR);
                    break;
                }
                // Over ci je prazdny zasobnik
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                // Nacitaj prvok z top-u, popni a over
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
               
                // Over ci sedia typy
                if (STItem->type != InterStackItem.ContType)
                {
                    EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                    break;
                }
                
                // Nahraj novu hodnotu
                STItem->content = InterStackItem.Content;
                STItem->init = true;
                break;

            // Instrukcia pre scitanie
            case I_SUM:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Ak ide o integer a integer
                if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == INTEGER)
                {
                    // Vysledok je integer ako scitanie integerov, pushni a over
                    InterStackItem3.ContType = INTEGER;
                    InterStackItem3.Content.i = InterStackItem.Content.i + InterStackItem2.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o integer a real
                else if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == REAL)
                {
                    // Vysledok je real ako sucet, pushni a over
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = (double)InterStackItem.Content.i + InterStackItem2.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a integer
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == INTEGER)
                {
                    // Vysledok je real ako sucet, pushni a over
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem.Content.r + (double)InterStackItem2.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a real
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == REAL)
                {
                    // Vysledok je real ako sucet, pushni a over
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem.Content.r + InterStackItem2.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else if (InterStackItem.ContType == STRING && InterStackItem2.ContType == STRING)
                {
                    // Vysledok je real ako sucet, pushni a over
                    InterStackItem3.ContType = STRING;
                    strInit(&InterStackItem3.Content.s);
                    strCopyString(&InterStackItem3.Content.s, &InterStackItem2.Content.s);
                    strConcaterateString(&InterStackItem3.Content.s, &InterStackItem.Content.s);
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Inak chyba
                else return PROCESS_NUMLOAD_ERROR;
                break;

            // Instrukcia pre odcitanie
            case I_SUB:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Ak ide o integer a integer
                if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == INTEGER)
                {
                    InterStackItem3.ContType = INTEGER;
                    InterStackItem3.Content.i = InterStackItem2.Content.i - InterStackItem.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o integer a real
                else if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == REAL)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.r - (double)InterStackItem.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a integer
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == INTEGER)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = (double)InterStackItem2.Content.i - InterStackItem.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a real
                else if (InterStackItem.ContType == REAL && InterStackItem.ContType == REAL)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.r - InterStackItem.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else return PROCESS_NUMLOAD_ERROR;
                
                break;

            // Instrukcia pre nasobenie
            case I_MUL:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Ak ide o integer a integer
                if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == INTEGER)
                {
                    InterStackItem3.ContType = INTEGER;
                    InterStackItem3.Content.i = InterStackItem.Content.i * InterStackItem2.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o integer a real
                else if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == REAL)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = (double)InterStackItem.Content.i * InterStackItem2.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a integer
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == INTEGER)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem.Content.r * (double)InterStackItem2.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a real
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == REAL)
                {
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem.Content.r * InterStackItem2.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else return PROCESS_NUMLOAD_ERROR;
                break;

            // Instrukcia pre delenie
            case I_DIV:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Ak ide o integer a integer
                if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == INTEGER)
                {
                    // Over delenie nulou
                    if (InterStackItem.Content.i == 0)
                    {
                        EndProgram(instList, PROCESS_DIVIDENULL_ERROR);
                        break;
                    }
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.i / (double)InterStackItem.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o integer a real
                else if (InterStackItem.ContType == INTEGER && InterStackItem2.ContType == REAL)
                {
                    // Over delenie nulou
                    if (InterStackItem.Content.i == 0)
                    {
                        EndProgram(instList, PROCESS_DIVIDENULL_ERROR);
                        break;
                    }
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.r / InterStackItem.Content.i;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a integer
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == INTEGER)
                {
                    // Over delenie nulou
                    if (InterStackItem.Content.r == 0)
                    {
                        EndProgram(instList, PROCESS_DIVIDENULL_ERROR);
                        break;
                    }
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.i / InterStackItem.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                // Ak ide o real a real
                else if (InterStackItem.ContType == REAL && InterStackItem2.ContType == REAL)
                {
                    // Over delenie nulou
                    if (InterStackItem.Content.r == 0)
                    {
                        EndProgram(instList, PROCESS_DIVIDENULL_ERROR);
                        break;
                    }
                    InterStackItem3.ContType = REAL;
                    InterStackItem3.Content.r = InterStackItem2.Content.r / InterStackItem.Content.r;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else return PROCESS_NUMLOAD_ERROR;
                break;

            // Instrukcia funkcie lenght
            case I_LENGTH:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci parameter je string
                if (InterStackItem.ContType == STRING)
                {
                    // Vykonaj algoritmus dlzky
                    LenghtAlgorith(&InterStackItem.Content.s);
                }
                else EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                break;

            // Instrukcia pre funkciu lenght s pushom
            case I_LENGHTPUSH:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci parameter je string
                if (InterStackItem.ContType == STRING)
                {
                    // Uloz do premennej vysledok algoritmu ako INT, pushni a over
                    InterStackItem2.ContType = INTEGER;
                    InterStackItem2.Content.i = LenghtAlgorith(&InterStackItem.Content.s);
                    InterpretStack = pushSI(InterpretStack, &InterStackItem2);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else EndProgram(instList, PROCESS_NUMLOAD_ERROR);
                break;

            // Instrukcia funkcie copy
            case I_COPY:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over parametre
                if (InterStackItem.ContType == STRING && I->addr1.Type == INTEGER && I->addr2.Type == INTEGER)
                {
                    if (I->addr1.AdressValue.i < 1 || strGetLength(&InterStackItem.Content.s) < 1)
                    {
                        EndProgram(instList, PROCESS_OTHER_ERROR);
                        break;
                    }
                    if (I->addr2.AdressValue.i < 1) break;
                    
                    // Vykonaj copy algoritmus
                    if(!CopyAlgorith(&InterStackItem.Content.s, I->addr1.AdressValue.i-1, I->addr2.AdressValue.i, &CopyString))
                        EndProgram(instList, PROCESS_OTHER_ERROR);
                }
                break;

            case I_COPYPUSH:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over parametre
                if (InterStackItem.ContType == STRING && I->addr1.Type == INTEGER && I->addr2.Type == INTEGER)
                {
                    if (I->addr1.AdressValue.i < 1 || strGetLength(&InterStackItem.Content.s) < 1)
                    {
                        EndProgram(instList, PROCESS_OTHER_ERROR);
                        break;
                    }
                    if (I->addr2.AdressValue.i < 1)
                        strClear(&CopyString);
                    else
                    {
                        // Vykonaj copy algoritmus
                        if (!CopyAlgorith(&InterStackItem.Content.s, I->addr1.AdressValue.i-1, I->addr2.AdressValue.i, &CopyString))
                        {
                            EndProgram(instList, PROCESS_OTHER_ERROR);
                            break;
                        }
                    }
                }
                
                // Pushni vysledok ako string
                InterStackItem3.ContType = STRING;
                strInit(&InterStackItem3.Content.s);
                strCopyString(&InterStackItem3.Content.s, &CopyString);
                InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;

            // Instrukcia pre funkciu sort
            case I_SORT:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over parametre a vykonaj algoritmus
                if (InterStackItem.ContType == STRING)
                {
                    if (InterStackItem.Content.s.length != 0)
                        HeapSort(InterStackItem.Content.s.str, (unsigned int)InterStackItem.Content.s.length);
                }
                else
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
        
            // Instrukcia pre sort s pushom
            case I_SORTPUSH:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over parametre a vykonaj algoritmus a pushni
                if (InterStackItem.ContType == STRING)
                {
                    if (InterStackItem.Content.s.length == 0)
                    {
                        InterpretStack = pushSI(InterpretStack, &InterStackItem);
                        if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    }
                    else
                    {
                        HeapSort(InterStackItem.Content.s.str, (unsigned int)InterStackItem.Content.s.length);
                        InterpretStack = pushSI(InterpretStack, &InterStackItem);
                        if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    }
                }
                else EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
            
            // Instrukcia pre find 
            case I_FIND:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Overime parametre
                if (InterStackItem.ContType == STRING && InterStackItem2.ContType == STRING)
                {
                    KMP(strGetStr(&InterStackItem.Content.s), strGetLength(&InterStackItem.Content.s), strGetStr(&InterStackItem2.Content.s), strGetLength(&InterStackItem2.Content.s));
                }
                else EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
                
            // Instrukcia pre find s pushom
            case I_FINDPUSH:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;
                InterpretStack = popSI(InterpretStack);
                
                // Over parametre
                if (InterStackItem.ContType == STRING && InterStackItem2.ContType == STRING)
                {
                    InterStackItem3.ContType = INTEGER;
                    InterStackItem3.Content.i = KMP(strGetStr(&InterStackItem.Content.s), strGetLength(&InterStackItem.Content.s), strGetStr(&InterStackItem2.Content.s), strGetLength(&InterStackItem2.Content.s));
                    InterpretStack = pushSI(InterpretStack, &InterStackItem3);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                else EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                break;
                
            // Instrukcia pre compare
            case I_COMP:
                cmp_flag_set = false;
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem = *InterpretStack->top;		
                InterpretStack = popSI(InterpretStack);
                
                // Over ci je prazdny zasobnik inak nacitaj vrch a pop
                if (isEmptyI(InterpretStack))
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                InterStackItem2 = *InterpretStack->top;		
                InterpretStack = popSI(InterpretStack);

                // Overime typy porovnavanych prvkov
                if(InterStackItem.ContType == InterStackItem2.ContType)
                {
                    // porovnanie 2 integerov
                    if(InterStackItem.ContType == INTEGER)
                    {
                        // do tmp ulozi vysledok odcitania
                        int tmp;
                        tmp = InterStackItem2.Content.i - InterStackItem.Content.i;
					
                        // vyhodnoti vyraz na zaklade operatora >/</>=/<=/<>/=
                        // pripad a < b
                        if(I->addr1.AdressValue.i == CMP_LESS)
                        {
                            if(tmp < 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATER)
                        {
                            if(tmp > 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <= b
                        else if(I->addr1.AdressValue.i == CMP_LESSEQ)
                        {
                            if(tmp <= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATEREQ)
                        {
                            if(tmp >= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a = b
                        else if(I->addr1.AdressValue.i == CMP_EQUAL)
                        {
                            if(tmp == 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <> b
                        else if(I->addr1.AdressValue.i == CMP_NOTEQUAL)
                        {
                            if(tmp != 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                    }
                    // porovnanie 2 realov
                    else if(InterStackItem.ContType == REAL)
                    {
                        double tmp2;
                        tmp2 = InterStackItem2.Content.r - InterStackItem.Content.r;

                        // vyhodnoti vyraz na zaklade operatora >/</>=/<=/<>/=
                        // pripad a < b
                        if(I->addr1.AdressValue.i == CMP_LESS)
                        {
                            if(tmp2 < 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATER)
                        {
                            if(tmp2 > 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <= b
                        else if(I->addr1.AdressValue.i == CMP_LESSEQ)
                        {
                            if(tmp2 <= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATEREQ)
                        {
                            if(tmp2 >= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a = b
                        else if(I->addr1.AdressValue.i == CMP_EQUAL)
                        {
                            if(tmp2 == 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <> b
                        else if(I->addr1.AdressValue.i == CMP_NOTEQUAL)
                        {
                            if(tmp2 != 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                    }
                    // porovnanie 2 boolean
                    else if(InterStackItem.ContType == BOOLEAN)
                    {
                        bool tmp3, tmp4;
                        tmp3 = InterStackItem2.Content.b;
                        tmp4 = InterStackItem.Content.b;
				
                        // vyhodnoti vyraz na zaklade operatora >/</>=/<=/<>/=
                        // pripad a < b
                        // vrati true v pripad ak vyraz vypada: false < true
                        if(I->addr1.AdressValue.i == CMP_LESS)
                        {
                            if(tmp3 == false && tmp4 == true)
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }
                        // pripad a > b
                        // vrati true pre vyraz: true > false
                        else if(I->addr1.AdressValue.i == CMP_GREATER)
                        {
                            if(tmp3 == true && tmp4 == false)
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }
                        // pripad a <= b
                        // vrati true pre vyrazy: true <= true, false <= false, false <= true
                        else if(I->addr1.AdressValue.i == CMP_LESSEQ)
                        {
                            if((tmp3 == true && tmp4 == true) || (tmp3 == false && tmp4 == false) || (tmp3 == false && tmp4 == true))
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }
                        // pripad a > b
                        // vrati true pre vyrazy: true >= true, false >= false, true >= false
                        else if(I->addr1.AdressValue.i == CMP_GREATEREQ)
                        {
                            if((tmp3 == true && tmp4 == true) || (tmp3 == false && tmp4 == false) || (tmp3 == true && tmp4 == false))
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }
                        // pripad a = b
                        // vrati true pre vyrazy: true = true, false = false
                        else if(I->addr1.AdressValue.i == CMP_EQUAL)
                        {
                            if((tmp3 == true && tmp4 == true) || (tmp3 == false && tmp4 == false))
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }
                        // pripad a <> b
                        // vrati true pre vyrazy: true <> false, false <> true
                        else if(I->addr1.AdressValue.i == CMP_NOTEQUAL)
                        {
                            if((tmp3 == true && tmp4 == false) || (tmp3 == false && tmp4 == true))
                                cmp_flag = true;
                            else
                                cmp_flag = false;
                        }

                    }
                    // porovnanie 2 stringov, lexikograficky
                    else if(InterStackItem.ContType == STRING)
                    {
                        int tmp5;
                        tmp5 = strCmpString(&InterStackItem2.Content.s, &InterStackItem.Content.s);
				
                        // vyhodnoti vyraz na zaklade operatora >/</>=/<=/<>/=
                        // pripad a < b
                        if(I->addr1.AdressValue.i == CMP_LESS)
                        {
                            if(tmp5 < 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATER)
                        {
                            if(tmp5 > 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <= b
                        else if(I->addr1.AdressValue.i == CMP_LESSEQ)
                        {
                            if(tmp5 <= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a > b
                        else if(I->addr1.AdressValue.i == CMP_GREATEREQ)
                        {
                            if(tmp5 >= 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a = b
                        else if(I->addr1.AdressValue.i == CMP_EQUAL)
                        {
                            if(tmp5 == 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                        // pripad a <> b
                        else if(I->addr1.AdressValue.i == CMP_NOTEQUAL)
                        {
                            if(tmp5 != 0) cmp_flag = true;
                            else cmp_flag = false;
                        }
                    }
                }
                // prvky niesu rovnakeho druhu
                else
                {
                    EndProgram(instList, SEMANTIC_TYPE_NUMPARAM_ERROR);
                    break;
                }
                
                if (I->addr2.Type != BOOLEAN)
                {
                    EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                    break;
                }
                
                if (I->addr2.AdressValue.b == true)
                {
                    InterStackItem.ContType = BOOLEAN;
                    InterStackItem.Content.b = cmp_flag;
                    InterpretStack = pushSI(InterpretStack, &InterStackItem);
                    if (InterpretStack == NULL) EndProgram(instList, INTERNAL_PROGRAM_ERROR);
                }
                
                cmp_flag_set = true;
                break;
            default:break;
        }
        listNext(instList);
	}
return INTERPRET_SUCCESS;
}
