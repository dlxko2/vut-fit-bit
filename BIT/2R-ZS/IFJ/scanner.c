/*************************************************
* Subor: scanner.c                               *
* Nazov projektu: Implementacia scanneru         *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/
#include "main.h"
#include "scanner.h"

int GetNewTokenBody(FILE *TokenFile, string *ReturnedAttribute, bool DetectEOL);

// Funkcia na odovzdanie noveho tokenu a odovzdanie jeho atributov
int GetNewTokenBody(FILE *TokenFile, string *ReturnedAttribute, bool DetectEOL)
{
    /* Pouzite lokalne premenne
     @ScannerState - stadium v ktorom sa nachadza lexikal
     @Character - aktualne nacitany znak
     @DigitHandler - je alphanumericky retazec ciselny literal?
     @FloatingHandler - je alphanumericky retazec desatiny literal? */
    int ScannerState = 0;
    int Character = 0;
    string Escaper;
    strInit(&Escaper);
    
    // Vymaz co je vo vracanom atribute
    strClear(ReturnedAttribute);
    
    // Cyklenie
    while (1)
    {
        // Nacitanie znaku
        Character = getc(TokenFile);
        
        // Rozhodovanie podla stavu
        switch (ScannerState)
        {
                
                // Pociatocny stav
            case 0:
                if (Character == '\n' && DetectEOL)  return LEX_EOL;
                else if (isspace(Character)) ScannerState = 0; // Ignorovanie prazdnych miest
                else if (Character == 39)  ScannerState = 6; // Retazcovy literal '
                else if (Character == '{')   ScannerState = 1; // Stav komentaru
                else if (Character == ':')   ScannerState = 3; // Pokracovanie dvojbodky
                else if (Character == '<')   ScannerState = 4; // Pokracovanie mensieho
                else if (Character == '>')   ScannerState = 5; // Pokracovanie vacsieho
                else if (Character == '(')   return LEX_LAVAZATVORKA;
                else if (Character == ')')   return LEX_PRAVAZATVORKA;
                else if (Character == '=')   return LEX_ROVNATKO;
                else if (Character == '+')   return LEX_SCITANIE;
                else if (Character == '-')
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    ScannerState = 10;
                    break;
                }
                else if (Character == '/')   return LEX_DELENO;
                else if (Character == '.')   return LEX_BODKA;
                else if (Character == '*')   return LEX_KRAT;
                else if (Character == ',')   return LEX_CIARKA;
                else if (Character == ';')   return LEX_BODKOCIARKA;
                else if (Character == EOF)   return END_OF_FILE;
                else if (isalnum(Character) || Character == '_') // Ak ide o alphanumericky retazec
                {
                    // Vlozi znak do odovzdavaneho attributu a prejde na stav 2
                    strAddChar(ReturnedAttribute, (char)Character);
                    
                    if (Character == '_' || isalpha(Character))
                        ScannerState = 8;
                    else ScannerState = 2;
                }
		else					// ine znaky = lex. chyba
			return LEXICAL_PROCESS_ERROR;
                break;
                
                // Stav komentara
            case 1:
                if(Character == '}')  
			 ScannerState = 0; 		// Ak konec komentara -> poc. stav
                else if(Character == EOF)   
			return LEXICAL_PROCESS_ERROR; 	// Ak konec komentaru chyba
                break;
                
            // INT 
            case 2:
                // Overime podmienku floatu
                if (Character == '.')
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    ScannerState = 9;
                    break;
                }
                else if (Character == 'e' || Character == 'E')
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    Character = getc(TokenFile);
                    
                    if (Character == '+' || Character == '-')
                        strAddChar(ReturnedAttribute, (char)Character);
                    else if (isdigit(Character))
                        strAddChar(ReturnedAttribute, (char)Character);
                    else return SYNTACTIC_PROCESS_ERROR;
                    
                    ScannerState = 9;
                    break;
                }
                
                // Overime ci naozaj je INT
                if (!isdigit(Character))
                {
                    ungetc(Character, TokenFile);
                    return LEX_CELOCISLITERAL;
                }
                
                // Ulozime dalsie cislo
                strAddChar(ReturnedAttribute, (char)Character);
                break;
                
                // Spracovanie :
            case 3:
                if (Character == '=') 		// :=
			return LEX_PRIROVNANIE;
                else{				// :
		 	ungetc(Character, TokenFile);
		  	return LEX_DVOJBODKA;
		}
                break;
                
                // Spracovanie <
            case 4:
                if (Character == '=')		// <=
			 return LEX_MENEJROVNATKO;
                else if (Character == '>') 	// <>
			return LEX_MENEJVIAC;
                else{ 				// <
			ungetc(Character, TokenFile);
			return LEX_MENEJ;
		}
                break;
                
                // Spracovanie vecsieho
            case 5:
                if (Character == '=') 
			return LEX_VIACROVNATKO;
                else{
			ungetc(Character, TokenFile); 
			return LEX_VIAC;
		}
                break;
                
                // Literar spracovanie
            case 6:
                // Ak najdeme dalsi apostrof
                if (Character == 39)
                {
                    // Pozrieme na dalsi znak
                    Character = getc(TokenFile);
                    
                    // Ak je escape sekvencia
                    if (Character == '#')
                    {
                        strInit(&Escaper);
                        ScannerState = 7;
                        break;
                    }
                    
                    // Ak je sekvencia apostrofu
                    else if (Character == 39)
                    {
                        strAddChar(ReturnedAttribute, 39);
                        break;
                    }
                    
                    // Inak je konec stringu
                    else
                    {
                        ungetc(Character, TokenFile);
                        ScannerState = 0;
                        return LEX_STRINGRETAZEC;
                    }
                }
                // Pre ostatne znaky
                else if (Character > 31)
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    break;
                }
                else return LEXICAL_PROCESS_ERROR;
                
                // Znamena ze zacala '# sekvencia
            case 7:
                // Ak ' je konec escape
                if (Character == 39)
                {
                    // Ak mame escape
                    if (strGetLength(&Escaper) != 0)
                    {
                        if (atoi(strGetStr(&Escaper)) < 1 || atoi(strGetStr(&Escaper)) > 255)
                            return LEXICAL_PROCESS_ERROR;
                        
                        // Prevedie cely string na jedno cislo a to prevedie na char znova a ulozi
                        strAddChar(ReturnedAttribute, (char)atoi(strGetStr(&Escaper)));
                    }
                    
                    // Uvolni string escapera
                    strClear(&Escaper);
                    strFree(&Escaper);
                    
                    ScannerState = 6;
                    break;
                }
                
                // Ak nie je cislo chyba
                if(!(isdigit(Character)))
                    return FUNCTION_ERROR;
                
                // Ak nie je v intervale
                if (Character > 255 || Character < 1)
                    return FUNCTION_ERROR;
                
                // Ak ok tak uloz znak
                strAddChar(&Escaper, (char)Character);
                break;
                
            // IDENTIFIKATOR
            case 8:
                // Overime podmienku identifikatoru ak sedi pridame dalsi znak
                if(isalpha(Character) || ((strcmp(strGetStr(ReturnedAttribute), "end") == 0) && (Character == '.')) || Character == '_' || isdigit(Character))
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                }
                
                // Koniec alfanumerickeho retazca
                else
                {
                    // Posledny znak mu uz nepatri a musime ho vratit
                    ungetc(Character, TokenFile);
                    
                    // Porovnanie syntaktickych nazvov
                    if (strCmpConstStr(ReturnedAttribute, "begin") == 0) return LEX_BEGIN;
                    else if (strCmpConstStr(ReturnedAttribute, "boolean") == 0) return LEX_BOOLEAN;
                    else if (strCmpConstStr(ReturnedAttribute, "copy") == 0) return LEX_COPY;
                    else if (strCmpConstStr(ReturnedAttribute, "length") == 0) return LEX_LENGHT;
                    else if (strCmpConstStr(ReturnedAttribute, "do") == 0) return LEX_DO;
                    else if (strCmpConstStr(ReturnedAttribute, "else") == 0) return LEX_ELSE;
                    else if (strCmpConstStr(ReturnedAttribute, "end") == 0) return LEX_END;
                    else if (strCmpConstStr(ReturnedAttribute, "false") == 0) return LEX_FALSE;
                    else if (strCmpConstStr(ReturnedAttribute, "find") == 0) return LEX_FIND;
                    else if (strCmpConstStr(ReturnedAttribute, "forward") == 0) return LEX_FORWARD;
                    else if (strCmpConstStr(ReturnedAttribute, "function") == 0) return LEX_FUNCTION;
                    else if (strCmpConstStr(ReturnedAttribute, "if") == 0) return LEX_IF;
                    else if (strCmpConstStr(ReturnedAttribute, "integer") == 0) return LEX_INTEGER;
                    else if (strCmpConstStr(ReturnedAttribute, "readln") == 0) return LEX_READLN;
                    else if (strCmpConstStr(ReturnedAttribute, "real") == 0) return LEX_REAL;
                    else if (strCmpConstStr(ReturnedAttribute, "sort") == 0) return LEX_SORT;
                    else if (strCmpConstStr(ReturnedAttribute, "string") == 0) return LEX_STRING;
                    else if (strCmpConstStr(ReturnedAttribute, "then") == 0) return LEX_THEN;
                    else if (strCmpConstStr(ReturnedAttribute, "true") == 0) return LEX_TRUE;
                    else if (strCmpConstStr(ReturnedAttribute, "var") == 0) return LEX_VAR;
                    else if (strCmpConstStr(ReturnedAttribute, "while") == 0) return LEX_WHILE;
                    else if (strCmpConstStr(ReturnedAttribute, "write") == 0) return LEX_WRITE;
                    else if (strCmpConstStr(ReturnedAttribute, "end.") == 0) return LEX_ENDBODKA;
                    else if (strCmpConstStr(ReturnedAttribute, "until") == 0) return LEX_UNTIL;
                    else if (strCmpConstStr(ReturnedAttribute, "repeat") == 0) return LEX_REPEAT;
                    else return LEX_IDENTIFIKATOR;
                }
                break;
                
                // FLOAT
                case 9:
                    if(Character == 'e' || Character == 'E')
                    {
                        strAddChar(ReturnedAttribute, (char)Character);
                        Character = getc(TokenFile);
                        
                        if (Character == '+' || Character == '-')
                            strAddChar(ReturnedAttribute, (char)Character);
                        else if (isdigit(Character))
                            strAddChar(ReturnedAttribute, (char)Character);
                        else return SYNTACTIC_PROCESS_ERROR;
                        
                    }
                    else if (Character == '+' || Character == '-')
                    {
                        
                    }
                    else if (isdigit(Character))
                    {
                        strAddChar(ReturnedAttribute, (char)Character);
                    }
                    else
                    {
                        ungetc(Character, TokenFile);
                        return LEX_DESATINYLITERAL;
                    }
                break;
                
            // Unarni minus
            case 10:
                if (isdigit(Character))
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    ScannerState = 2;
                }
                else if (Character == '.')
                {
                    strAddChar(ReturnedAttribute, (char)Character);
                    ScannerState = 9;
                }
                else
                {
                    ungetc(Character, TokenFile);
                    return LEX_ODCITANIE;
                }
                break;
        }
    }
    
    // Funkcia skoncila spravne
    return FUNCTION_SUCCESS;
}

int UngetGetToken(tDLList *L, string *ReturnedAttribute, bool DetectEOL)
{
    // Over aktivitu
    if (L->Act == NULL)
        return 0;
    
    // Posun dozadu
    L->Act = L->Act->lptr->lptr;
    
    return GetNewToken(L, ReturnedAttribute, DetectEOL);
    
}
int GetNewToken(tDLList *L, string *ReturnedAttribute, bool DetectEOL)
{
    LEXType ReturnValue = 0;
    ReturnValue = DLCopy(L, ReturnedAttribute);
    DLSucc(L);
    
    if (!DetectEOL && ReturnValue == LEX_EOL)
    {
        while (ReturnValue == LEX_EOL)
        {
            ReturnValue = DLCopy(L, ReturnedAttribute);
            DLSucc(L);
        }
    }
    
    if (DEBUG_MODE) printf("TOKEN:%d STRING:%s\n", ReturnValue, strGetStr(ReturnedAttribute));
    return ReturnValue;
}

bool GetTokens(tDLList *L, FILE *TokenFile)
{
    // Premenne
    string TempString;
    strInit(&TempString);
    int TempType = 0;
    
    // Insert
    while (TempType != END_OF_FILE)
    {
        TempType = GetNewTokenBody(TokenFile, &TempString, true);
        if (TempType == LEXICAL_PROCESS_ERROR) return false;
        DLInsertLast(L, (LEXType)TempType, TempString);
    }
    
    L->Act = L->First;
    strFree(&TempString);
    return true;
}


void DLInitList (tDLList *L)
{
    // Nadstavime pointre na NULL
    L->Act = NULL;
    L->First = NULL;
    L->Last = NULL;
}

void DLDisposeList (tDLList *L)
{
    // Inicializacia premennych
    tDLElemPtr MazanyElement;
    tDLElemPtr LoopElement = L->First;
    
    // Pozrie ci nie je prazdny zoznam
    if (LoopElement != NULL)
    {
        // Kym mazany prvok nie je prazdny
        while (LoopElement != NULL)
        {
            // Mazany prvok napln aktualnym
            // Ukaz na dalsi prvok
            // Zmaz mazany element
            MazanyElement = LoopElement;
            LoopElement = LoopElement->rptr;
            free(MazanyElement);
        }
        
        // Nadstavime pointery na NULL
        L->First = NULL;
        L->Last = NULL;
        L->Act = NULL;
    }
}

void DLInsertLast(tDLList *L, LEXType Type, string Attribut)
{
    // Definujeme novy prvok
    tDLElemPtr NewElement;
    
    // Vytvori pamatovy priestor pre novy prvok v pamati inak error
    if ((NewElement = malloc(sizeof(struct tDLElem))) == NULL)
    {
        return;
    }
    
    // Nadstavime hodnotu elementu
    NewElement->Type = Type;
    strInit(&NewElement->attrib);
    strCopyString(&NewElement->attrib, &Attribut);
    
    // Doprava neukazuje, dolava na minuly
    NewElement->rptr = NULL;
    NewElement->lptr = L->Last;
    
    /* Pridame vynimky
     Ak je prvym je zaroven tvorcom prveho
     Ak nie je prvy pozera aj na nasledujuci */
    if (L->Last == NULL)
        L->First = NewElement;
    else
        L->Last->rptr = NewElement;
    
    // Priradime ako posledny novy prvok
    L->Last = NewElement;
}

LEXType DLCopy (tDLList *L, string *val)
{
    // Ak je nejaky aktivny vrat jeho hodnotu inak error
    if (L->Act != NULL)
    {
        *val = L->Act->attrib;
        return L->Act->Type;
    }
    else return 0;
}

void DLSucc (tDLList *L)
{
    // Posun aktivitu na dalsi
    if (L->Act != NULL)
        L->Act = L->Act->rptr;
}
