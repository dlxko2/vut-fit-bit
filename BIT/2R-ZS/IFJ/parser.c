/*************************************************
* Subor: parser.c                                *
* Nazov projektu: Implementacia parseru          *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"
#include "lablist.h"
#include "scanner.h"
#include "parser.h"
#include "moduleexpressions.h"	// GlobalSymbolTable
#include "modulestring.h"

// Globlne ulozisko 
FILE *ParserTokenFile;
Node *GlobalSymbolTable;
fList *FunctionTable;
tListOfLabs *LabList;
tListOfInstr *list;
tDLList *TokList;
function *ActualFunction;

dataT FunctionAttribToType(int TokenType);
int FunctionSintxWriteTelo(TokenStructure *TokenStruc, bool ActiveFunction);

/* Pridavna funkcia ci token je typ */
bool FunctionJeTyp(TokenStructure *TokenStruc)
{
    // Overi ci spada typ tokenu do jedneho z definovanych typov 
    if (TokenStruc->Type != LEX_INTEGER && TokenStruc->Type != LEX_REAL &&
        TokenStruc->Type != LEX_BOOLEAN && TokenStruc->Type != LEX_STRING)
        return false;
    
    // Ak spada je typ
    return true;
}

/* Zmena typu tokenu na datovy typ */
dataT FunctionAttribToType(int TokenType)
{
    if (TokenType == LEX_INTEGER) return INTEGER;
    else if (TokenType == LEX_BOOLEAN) return BOOLEAN;
    else if (TokenType == LEX_STRING) return STRING;
    else if (TokenType == LEX_REAL) return REAL;
    return 0;
}

/* Main funkcia parsera riadiaca jeho beh */
int FunctionDoParse(FILE *TokenFile, tListOfInstr *iList, tListOfLabs *ListLab, Node **ST, fList *FT, tDLList *TokenList)
{
    // Ulozisko vysledku funkcii ci prebehli spravne
    int Result = 0;
    
    // Nulujeme
    ActualFunction = 0;
    
    // Init InstrList, GTS, LabList, Token, File 
    list = iList;
    GlobalSymbolTable = *ST;
    FunctionTable = FT;
    LabList = ListLab;
    TokList = TokenList;
    TokenStructure FirstToken;
    ParserTokenFile = TokenFile;
    
    // Inicializacia stringu v tokene a naplnenie
    strInit(&FirstToken.attr);
    FirstToken.Type = GetNewToken(TokList, &FirstToken.attr, false);
    if (FirstToken.Type == END_OF_FILE) return SYNTACTIC_PROCESS_ERROR;
    
    // Spustime hlavnu gramatiku <PROG> a overime ci uspela
    Result = FunctionSintxProgram(&FirstToken);
    if (Result != FUNCTION_SUCCESS) return Result;
    
    // Uvolnenie inicializovaneho stringu
    strFree(&FirstToken.attr);
    
    // Ulozime pozmenenu ST a FT
    *ST = GlobalSymbolTable;
    
    // Overime ci su definovane vsetky fcie
    function *TemporaryFunction = FunctionTable->first;
    while (TemporaryFunction != NULL)
    {   if (TemporaryFunction->defined == false)
        return SEMANTIC_UNDEF_REDEF_ERROR;
        TemporaryFunction = TemporaryFunction->next;
    }
    
    
    // V inych pripadoch povazujeme uspech
    return FUNCTION_SUCCESS;
}

/* <PROG> -> var <TELO_VAR>          
   <PROG> -> begin <TELO_BEGIN>       
   <PROG> -> function <TELO_FUNCTION> */
int FunctionSintxProgram(TokenStructure *TokenStruc)
{
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Gramatika <PROG> -> var <TELO_VAR>
    if (TokenStruc->Type == LEX_VAR)
    {
        // Nacitame novy token a zavolame <TELO_VAR> potom overime vysledok
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FuntionSintxVar(TokenStruc, NULL, false);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    
    // Gramatika <PROG> -> var <TELO_FUNCTION>
    if (TokenStruc->Type == LEX_FUNCTION)
    {
        // Nacitame novy token a zavolame <TELO_VAR> potom overime vysledok
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FunctionSintxFunction(TokenStruc);
        if (Result != FUNCTION_SUCCESS) return Result;
    }

    
    // Gramatika <PROG> -> begin <TELO_BEGIN>
    if (TokenStruc->Type == LEX_BEGIN)
    {
        // Zavolame gramatiku <TELO_BEGIN> a potom overime
        Result = FunctionSintxTeloBegin(TokenStruc);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Posledny token musi byt EOF
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != END_OF_FILE) return SYNTACTIC_PROCESS_ERROR;
    
    // Na konci pocitame ze nastal uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_VAR> -> nazov_var : typ; nazov_var 
   <TELO_VAR> -> nazov_var : typ; function   
   <TELO_VAR> -> nazov_var : typ; begin     
   <TELO_VAR> -> begin  */
int FuntionSintxVar(TokenStructure *TokenStruc, Node **FST, bool Function)
{
    // Premenna pre navratove hodnoty
    int Result = 0;
    
    
    // Gramatika <TELO_VAR> -> begin <TELO_BEGIN>
    if (TokenStruc->Type == LEX_BEGIN) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika <TELO_VAR> -> nazov_var : typ;
    if (TokenStruc->Type == LEX_FIND || TokenStruc->Type == LEX_SORT || TokenStruc->Type == LEX_READLN || TokenStruc->Type == LEX_WRITE)
        return SEMANTIC_UNDEF_REDEF_ERROR;
    
    if (TokenStruc->Type == LEX_TRUE || TokenStruc->Type == LEX_FALSE ||  TokenStruc->Type == LEX_DO || TokenStruc->Type == LEX_INTEGER ||
        TokenStruc->Type == LEX_REAL || TokenStruc->Type == LEX_STRING || TokenStruc->Type == LEX_BOOLEAN)
        return SYNTACTIC_PROCESS_ERROR;
    
    if (strcmp(strGetStr(&TokenStruc->attr), "TRUE") == 0 || strcmp(strGetStr(&TokenStruc->attr), "FALSE") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "DO") == 0 || strcmp(strGetStr(&TokenStruc->attr), "INTEGER") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "STRING") == 0 || strcmp(strGetStr(&TokenStruc->attr), "BOOLEAN") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "REAL") == 0)
        return SYNTACTIC_PROCESS_ERROR;

    if (TokenStruc->Type != LEX_IDENTIFIKATOR) return SYNTACTIC_PROCESS_ERROR;

    // Nacitame pomocnu premennu s nazvom identifikatora
    string TempString;
    strInit(&TempString);
    strCopyString(&TempString, &TokenStruc->attr);
    
    // Overime gramatiku ':'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_DVOJBODKA)
    {
        strFree(&TempString);
        return SYNTACTIC_PROCESS_ERROR;
    }
    
    // Overime gramatiku 'typ' ak v poriadku ulozime do stromu
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (!FunctionJeTyp(TokenStruc))
    {
        strFree(&TempString);
        return SYNTACTIC_PROCESS_ERROR;
    }

    // Ak do globalne
    if (Function == false)
    {
        // Ulozime do tabulky symbolov, overime a zmazeme string
        Result = BSTInsertNew(&GlobalSymbolTable, TempString, FunctionAttribToType(TokenStruc->Type));
        if (Result != FUNCTION_SUCCESS)
        {
            strFree(&TempString);
            return Result;
        }
    }
    // Ak do tabulky symbolov funkcie
    else
    {
        Result = BSTInsertNew(FST, TempString, FunctionAttribToType(TokenStruc->Type));
        if (Result != FUNCTION_SUCCESS)
        {
            strFree(&TempString);
            return Result;
        }
    }

    strFree(&TempString);

    // Overime gramatiku ';'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_BODKOCIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    /* Gramatika <TELO_VAR> -> nazov_var : typ; nazov_var
     - Nacitame dalsi token ak je nazov_var rekurzivne spustime <TELO_VAR> */
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        Result = FuntionSintxVar(TokenStruc, FST, Function);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    
    /* Gramatika <TELO_VAR> -> nazov_var : typ; function <TELO_FUNCTION>
     - Nacitame dalsi token ak je slovo "function" ukoncime analyzu <TELO_VAR> */
    else if (TokenStruc->Type == LEX_FUNCTION) return FUNCTION_SUCCESS;

    /* Gramatika <TELO_VAR> -> nazov_var : typ; begin <TELO_BEGIN>
     - Nacitame dalsi token ak je slovo "begin" ukoncime analyzu <TELO_VAR> */
    else if (TokenStruc->Type == LEX_BEGIN) return FUNCTION_SUCCESS;
    
    // V inom pripade neplati gramatika a nastava chyba
    else return SYNTACTIC_PROCESS_ERROR;

    // Inak budeme povazovat uspech
    return FUNCTION_SUCCESS;
}

/* <SLOZENY_PRIKAZ> -> <PRIKAZ_PRIRADENIE>   
   <SLOZENY_PRIKAZ> -> if <TELO_IF>           
   <SLOZENY_PRIKAZ> -> while <TELO_WHILE>       
   <SLOZENY_PRIKAZ> -> readln <TELO_READLN>  
   <SLOZENY_PRIKAZ> -> write <TELO_WRITE>      
   <SLOZENY_PRIKAZ> -> end                      
   <SLOZENY_PRIKAZ> -> sort, find, copy, lenght */
int FunctionSintxSlozPrik(TokenStructure *TokenStruc, bool ActiveFunction, bool Repeat)
{
    // Ulozisko navratovej hodnoty funkcie
    int Result = 0;
    
    // Vykonavanie analyzy ciastocnych prikazov az po chybu
    while (Result == FUNCTION_SUCCESS)
    {
        // PRIKAZ_PRIRADENIE [;,end]
        if (TokenStruc->Type == LEX_IDENTIFIKATOR)
        {
            // Ak ide o funkciu
            if (FUNCTIONlistIsFunction(FunctionTable, &TokenStruc->attr) != NULL)
            {
                // Nacitame dalsi token a overime ci '('
                TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
                if (TokenStruc->Type == LEX_LAVAZATVORKA)
                {
                    UngetGetToken(TokList, &TokenStruc->attr, false);
                    Result = FunctionSintxRunFunction(TokenStruc, false);
                }
                else
                {
                    UngetGetToken(TokList, &TokenStruc->attr, false);
                    Result = FunctionSintxPrikazPriradenie(TokenStruc, ActiveFunction);
                }
            }
            else Result = FunctionSintxPrikazPriradenie(TokenStruc, ActiveFunction);
        }

        // IF <TELO_IF> [;,end]
        else if (TokenStruc->Type == LEX_IF) Result = FunctionSintxIf(TokenStruc, ActiveFunction);

        else if (TokenStruc->Type == LEX_BEGIN)
        {
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
            
            if (TokenStruc->Type != LEX_END)
                Result = FunctionSintxSlozPrik(TokenStruc, ActiveFunction, false);
        
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        }
        
        // WHILE <TELO_WHILE> [;,end]
        else if (TokenStruc->Type == LEX_WHILE) Result = FunctionSintxWhile(TokenStruc, ActiveFunction);
        
        // REPEAT <TELO_REPEAT> [;,end]
        else if (TokenStruc->Type == LEX_REPEAT) Result = FunctionSintxRepeat(TokenStruc, ActiveFunction);
        
        // READLN <TELO_READLN> [;,end]
        else if (TokenStruc->Type == LEX_READLN) Result = FunctionSintxReadln(TokenStruc, ActiveFunction);
        
        // WRITE <TELO_WRITE> [;,end]
        else if (TokenStruc->Type == LEX_WRITE) Result = FunctionSintxWrite(TokenStruc, ActiveFunction);
        
        // SORT <TELO_SORT> [;,end]
        else if (TokenStruc->Type == LEX_SORT)
        {
            Result = FunctionSintxSort(TokenStruc, false, ActiveFunction);
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        }
        
        // COPY <TELO_COPY> [;,end]
        else if (TokenStruc->Type == LEX_COPY)
        {
            Result = FunctionSintxCopy(TokenStruc, false, ActiveFunction);
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        }
        
        // LENGHT <TELO_LENGHT> [;,end]
        else if (TokenStruc->Type == LEX_LENGHT)
        {
            Result = FunctionSintxLenght(TokenStruc, false, ActiveFunction);
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        }
        
        // FIND <TELO_FIND> [;,end]
        else if (TokenStruc->Type == LEX_FIND)
        {
            Result = FunctionSintxFind(TokenStruc, false, ActiveFunction);
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        }
        else return SYNTACTIC_PROCESS_ERROR;
        
        if (Result != FUNCTION_SUCCESS)
            return Result;
        
        if (Repeat)
        {
            if (TokenStruc->Type == LEX_BODKOCIARKA)
            {
                TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
                continue;
            }
            else return FUNCTION_SUCCESS;
        }
        // Ak skonci musi nasledovat [;,end] inak chyba
        if (TokenStruc->Type == LEX_END || TokenStruc->Type == LEX_ENDBODKA)
            return FUNCTION_SUCCESS;
        else if (TokenStruc->Type == LEX_BODKOCIARKA)
        {
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
            continue;
        }
        else return SYNTACTIC_PROCESS_ERROR;
    }
    
    // Normalne chovanie ukonci funkciu s end -> preto chyba
    return Result;
}

/* <TELO_BEGIN> -> <SLOZENY_PRIKAZ>  
   <TELO_BEGIN> -> end.       */
int FunctionSintxTeloBegin(TokenStructure *TokenStruc)
{
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Vygeneruj instrukciu zaciatku
    if (!generateInstruction(list, I_START, NULL, NULL, NULL))
        return PROCESS_OTHER_ERROR;
    
    // Overenie ci EPS alebo <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_ENDBODKA)
    {
        Result = FunctionSintxSlozPrik(TokenStruc, false, false);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    
    // Overenie end. a generacia instrukcie
    if (TokenStruc->Type == LEX_ENDBODKA)
    {
        if (!generateInstruction(list, I_STOP, NULL, NULL, NULL))
            return PROCESS_OTHER_ERROR;
        
        return FUNCTION_SUCCESS;
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Tu povazujeme neuspech
    return FUNCTION_ERROR;
}

/* <PRIKAZ_PRIRADENIE -> id := <VYRAZ> */
int FunctionSintxPrikazPriradenie(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Ulozisko pre navratove hodnoty
    int Result = 0;
    
    // Ulozisko result - expression
    dataT ResultType = 0;
    dataT InputType = 0;
    Node *TempNode = NULL;
    function *TempFunction = NULL;
    
    // Premenne
    AdressStruc Adresa;
    strInit(&Adresa.AdressValue.s);
    Adresa.Type = STRING;
    
    // Overim ci ID je v TS ak ano skopirujeme do adresy
    if (ActiveFunction)
    {
        TempNode = BSTSearch(ActualFunction->localTS, TokenStruc->attr);
        if (TempNode == NULL)
        {
            TempNode = BSTSearch(GlobalSymbolTable, TokenStruc->attr);
            if (TempNode == NULL) return SEMANTIC_UNDEF_REDEF_ERROR;
        }
    }
    else
    {
        TempNode = BSTSearch(GlobalSymbolTable, TokenStruc->attr);
        if (TempNode == NULL) return SEMANTIC_UNDEF_REDEF_ERROR;
    }
    strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
    InputType = TempNode->type;
    
    // Overime ci nasleduje ":=" pre uspesne vyhodnotenie gramatiky
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRIROVNANIE) return SYNTACTIC_PROCESS_ERROR;
    
    // Zvysok nechame na funkcie a vyrazy 
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_SORT)
    {
        if (InputType != STRING) return SEMANTIC_TYPE_NUMPARAM_ERROR;
        Result = FunctionSintxSort(TokenStruc, true, ActiveFunction);
    }
    else if (TokenStruc->Type == LEX_FIND)
    {
        if (InputType != INTEGER) return SEMANTIC_TYPE_NUMPARAM_ERROR;
        Result = FunctionSintxFind(TokenStruc, true, ActiveFunction);
    }
    else if (TokenStruc->Type == LEX_COPY)
    {
        if (InputType != STRING) return SEMANTIC_TYPE_NUMPARAM_ERROR;
        Result = FunctionSintxCopy(TokenStruc, true, ActiveFunction);
    }
    else if (TokenStruc->Type == LEX_LENGHT)
    {
        if (InputType != INTEGER) return SEMANTIC_TYPE_NUMPARAM_ERROR;
        Result = FunctionSintxLenght(TokenStruc, true, ActiveFunction);
    }
    else if (FUNCTIONlistIsFunction(FunctionTable, &TokenStruc->attr) != NULL)
    {
        TempFunction = FUNCTIONlistIsFunction(FunctionTable, &TokenStruc->attr);
        ResultType = TempFunction->returnV;
        
        // Nacitame dalsi token a overime ci '('
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        if (TokenStruc->Type == LEX_LAVAZATVORKA)
        {
            UngetGetToken(TokList, &TokenStruc->attr, false);
            Result = FunctionSintxRunFunction(TokenStruc, true);
        }
        
        // Ak neuspech vrat hodnotu
        if (Result != FUNCTION_SUCCESS) return Result;
        
        // Porovnaj typy
        if (InputType != ResultType) return SEMANTIC_TYPE_NUMPARAM_ERROR;
    }
    else
    {
        if (ActiveFunction) Result = FunctionParseExpression(TokenStruc, ActualFunction->localTS, GlobalSymbolTable, list, TokList, true, &ResultType);
        else Result = FunctionParseExpression(TokenStruc, GlobalSymbolTable, GlobalSymbolTable, list, TokList, true, &ResultType);
        
        // Ak neuspech vrat hodnotu
        if (Result != FUNCTION_SUCCESS) return Result;
        
        // Porovnaj typy
        if (InputType != ResultType) return SEMANTIC_TYPE_NUMPARAM_ERROR;
    }
    
    // Ak neuspech vrat hodnotu
    if (Result != FUNCTION_SUCCESS) return Result;
    
    // Ak EOL new token
    if (TokenStruc->Type == LEX_EOL)
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        
    // Generujeme ASSIGN
    if (!generateInstruction(list, I_ASSIGN, &Adresa, NULL, NULL))
        return PROCESS_OTHER_ERROR;
    
    // Zmazeme string
    strFree(&Adresa.AdressValue.s);
                  
    // Nenacitame dalsi a posleme na overenie [;,end] 
    return FUNCTION_SUCCESS;
}

/* <TELO_IF> -> <VYRAZ> then <SLOZENY_PRIKAZ> else <SLOZENY_PRIKAZ> */
int FunctionSintxIf(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Ulozisko navratovky - expressions
    dataT ReturnType = 0;
    
    // Ifelse
    bool IsELSE = false;
    
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Ulozisko navesti
    int LabELSE = 0;
    int LabENDIF = 0;

    // Premenna pre adresy
    AdressStruc Adresa1;
    Adresa1.Type = INTEGER;
    Adresa1.AdressValue.i = 0;
    
    // Ptrka
    tInstr *PtrToJUMP = NULL;
    tInstr *PtrToJUMPFALSE = NULL;

    // Gramatika <TELO_IF> -> <VYRAZ> then <SLOZENY_PRIKAZ> else <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (ActiveFunction)
        Result = FunctionParseExpression(TokenStruc, ActualFunction->localTS, GlobalSymbolTable, list, TokList, false, &ReturnType);
    else
        Result = FunctionParseExpression(TokenStruc, GlobalSymbolTable, GlobalSymbolTable, list, TokList, false, &ReturnType);
        
    if (Result != FUNCTION_SUCCESS) return Result;
    
    if (TokenStruc->Type == LEX_EOL)
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    if (ReturnType != BOOLEAN) return SEMANTIC_TYPE_NUMPARAM_ERROR;
    
    // Generujeme novu instrukciu skoku zatial s nulou ukazeme na nu pre buducu zmenu
    if (!generateInstruction(list, I_WJMPFALSE, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;
    PtrToJUMPFALSE = &listGetPointerLast(list)->Instruction;
    
    // Overenie gramatiky slova "then"
    if (TokenStruc->Type != LEX_THEN) return FUNCTION_ERROR;
    
    // Overenie gramatiky <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_BEGIN)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        
        if (TokenStruc->Type != LEX_END)
        {
            Result = FunctionSintxSlozPrik(TokenStruc, ActiveFunction, false);
            if (Result != FUNCTION_SUCCESS) return Result;
        }
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Generujeme novu instrukciu skoku zatial s nulou ukazeme na nu pre buducu zmenu
    if(!generateInstruction(list, I_WJMP, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;
    PtrToJUMP = &listGetPointerLast(list)->Instruction;
    
    // Overenie gramatiky slova "else"
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_ELSE)
    {
        IsELSE = true;
        
        // Genetujeme ILAB vlozime ho do listu vytvorime instrukciu a ukazeme skokom na nu
        if(!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
        if(!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_ELSE)) return PROCESS_OTHER_ERROR;
        LabELSE = LabList->ActualID;
        
        // Overenie gramatiky <SLOZENY_PRIKAZ>
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        if (TokenStruc->Type == LEX_BEGIN)
        {
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
            
            if (TokenStruc->Type != LEX_END)
            {
                Result = FunctionSintxSlozPrik(TokenStruc, ActiveFunction, false);
                if (Result != FUNCTION_SUCCESS) return Result;
            }
        }
        else return SYNTACTIC_PROCESS_ERROR;
    }
    
    if (!IsELSE)
    {
        LabENDIF = LabList->ActualID + 1;
        PtrToJUMPFALSE->addr1.AdressValue.i = LabENDIF;
    }
    else
    {
        PtrToJUMPFALSE->addr1.AdressValue.i = LabELSE;
        
        // Nacitame dalsi token [;,end] a posleme ho z5 slozenemu prikazu
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    }
    
    // Genetujeme ILAB vlozime ho do listu vytvorime instrukciu a ukazeme skokom na nu
    if(!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if(!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_ENDIF)) return PROCESS_OTHER_ERROR;
    LabENDIF = LabList->ActualID;
    PtrToJUMP->addr1.AdressValue.i = LabENDIF;
    
    // Ak skonci u konca ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_WHILE> -> <VYRAZ> do <SLOZENY_PRIKAZ>;   [TD] (Gramatika cyklu while) */
int FunctionSintxWhile(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Ulozisko typu
    dataT ExpType = 0;
    
    // Ulozisko navesti
    int LabWHILE = 0;
    int LabENDWHILE = 0;
    
    // Ulozisko navratovych hodnot
    int Result = 0;
    
    // Premenna adresy
    AdressStruc Adresa1;
    Adresa1.Type = INTEGER;
    Adresa1.AdressValue.i = 0;
    
    // Generujeme instrukciu ukazeme na nu ulozime ako navestie a ulozime si jeho index
    if(!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if(!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_WHILE)) return PROCESS_OTHER_ERROR;
    LabWHILE = LabList->ActualID;
    
    // <TELO_WHILE> -> <VYRAZ> do <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (ActiveFunction)
        Result = FunctionParseExpression(TokenStruc, ActualFunction->localTS, GlobalSymbolTable, list, TokList, false, &ExpType);
    else
        Result = FunctionParseExpression(TokenStruc, GlobalSymbolTable, GlobalSymbolTable, list, TokList, false, &ExpType);
    
    if (Result != FUNCTION_SUCCESS) return SYNTACTIC_PROCESS_ERROR;
    
    // Ak zostal z expressions EOL posunieme sa
    if (TokenStruc->Type == LEX_EOL) TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    // Overime navratku expression
    if (ExpType != BOOLEAN) return SEMANTIC_TYPE_NUMPARAM_ERROR;
    
    // Overime DO
    if (TokenStruc->Type != LEX_DO) return SYNTACTIC_PROCESS_ERROR;
    
    // Generujeme novu instrukciu skoku a ulozime si na nu odkaz
    if (!generateInstruction(list, I_WJMPFALSE, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;
    tInstr *TempInstrucPtr = &listGetPointerLast(list)->Instruction;
    
    // Overenie gramatiky <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_BEGIN)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        
        if (TokenStruc->Type != LEX_END)
        {
            Result = FunctionSintxSlozPrik(TokenStruc, ActiveFunction, false);
            if (Result != FUNCTION_SUCCESS) return Result;
        }
    }
    else return SYNTACTIC_PROCESS_ERROR;

    // Nacitame dalsi token [;,end] a posleme ho z5 slozenemu prikazu
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    // Generujeme novu instrukciu s navestim while ktore uz pozname
    Adresa1.AdressValue.i = LabWHILE;
    if(!generateInstruction(list, I_WJMP, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;

    // Vytvorime navestie ulozime do tabulky a zapiseme si jeho hodnotu
    if (!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if (!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_ENDWHILE)) return PROCESS_OTHER_ERROR;
    LabENDWHILE = LabList->ActualID;

    // Prepiseme navestie u skoku
    TempInstrucPtr->addr1.AdressValue.i = LabENDWHILE;
    
    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_READLN> -> (id)  */
int FunctionSintxReadln(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Premenna adresy
    AdressStruc Adresa;
    Adresa.Type = STRING;
    
    // Gramatika <TELO_READLN> -> (id)
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_IDENTIFIKATOR) return SYNTACTIC_PROCESS_ERROR;
   
    // Overim ci ID je v TS ak ano skopirujeme do adresy
    if (ActiveFunction)
    {
        if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
    }
    else
    {
        if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
            return SEMANTIC_UNDEF_REDEF_ERROR;
    }
    
    // Alokujeme string a posleme ho do adresy
    strInit(&Adresa.AdressValue.s);
    strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);

    // Generujeme ASSIGN
    if(!generateInstruction(list, I_READ, &Adresa, NULL, NULL))
    {
        strFree(&Adresa.AdressValue.s);
        return PROCESS_OTHER_ERROR;
    }
    
    // Generujeme ASSIGN
    if(!generateInstruction(list, I_ASSIGN, &Adresa, NULL, NULL))
    {
        strFree(&Adresa.AdressValue.s);
        return PROCESS_OTHER_ERROR;
    }
    
    // Dealokujeme string
    strFree(&Adresa.AdressValue.s);
    
    // Overime gramatiku ")"
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;

    // Nacitame dalsi token [;,end] a posleme ho z5 slozenemu prikazu
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);

    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_SORT> -> (id)  */
int FunctionSintxSort(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction)
{
    // Premenna adresy
    AdressStruc Adresa;
    Adresa.Type = STRING;
    
    // Gramatika (
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika IDENTIFIKATOR
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        
        // Generujeme instrukciu po naplneni stringu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if (!generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    // GRAMATIKA ak nie id ale STRING
    else if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Generujeme instrukciu po naplneni stringu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }

    // Gramatika ")"
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Generujeme instrukcie
    if (WithPush)
        if(!generateInstruction(list, I_SORTPUSH, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if (!WithPush)
        if(!generateInstruction(list, I_SORT, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    
    // Pre [;,end]
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_COPY> -> id, int, int  */
int FunctionSintxCopy(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction)
{
    // Premenna adresy
    AdressStruc Adresa;
    Adresa.Type = STRING;
    AdressStruc Adresa2;
    Adresa2.Type = INTEGER;
    
    // Ulozisko integerov
    int Attrib1 = 0;
    int Attrib2 = 0;
    
    // Gramatika "("
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika IDENTIFIKATOR
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }

        
        // Alokujeme string a nakopirujeme do adresy info
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if (!generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    // Ak nie identifikator ale STRING
    else if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Alokujeme string a nakopirujeme do adresy info
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika CIARKY
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_CIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika INTEGER1
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_CELOCISLITERAL)
        Attrib1 = atoi(strGetStr(&TokenStruc->attr));
    else return SYNTACTIC_PROCESS_ERROR;

    // Gramatika CIARKY
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_CIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika INTEGER 2
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_CELOCISLITERAL)
        Attrib2 = atoi(strGetStr(&TokenStruc->attr));
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika ')'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Naplnime adresy
    Adresa.Type = INTEGER;
    Adresa.AdressValue.i = Attrib1;
    Adresa2.AdressValue.i = Attrib2;
    
    // Generujeme instructions
    if (WithPush)
        if(!generateInstruction(list, I_COPYPUSH, &Adresa, &Adresa2, NULL)) return PROCESS_OTHER_ERROR;
    if (!WithPush)
        if(!generateInstruction(list, I_COPY, &Adresa, &Adresa2, NULL)) return PROCESS_OTHER_ERROR;
    
    // Pre [;,end]
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_FIND> -> (ID/STRING, ID/STRING)  */
int FunctionSintxFind(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction)
{
    // Premenna adresy
    AdressStruc Adresa;
    Adresa.Type = STRING;
    
    // Gramatika '('
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika IDENTIFIKATOR
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        
        // Alokujeme string a nakopirujeme do adresy a vygenerujeme instrukciu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    // Ak nie identifikator tak STRING
    else if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Alokujeme string a nakopirujeme do adresy a vygenerujeme instrukciu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    
    // Gramatika CIARKA
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_CIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    // IDENTIFIKATOR2
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        
        // Alokujeme string a nakopirujeme do adresy a vygenerujeme instrukciu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    // Ak nie identifikator tak STRING
    else if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Alokujeme string a nakopirujeme do adresy a vygenerujeme instrukciu
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }        
    }
    
    // Gramatika ')'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Generujeme instrukcie
    if (WithPush)
        if(!generateInstruction(list, I_FINDPUSH, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if (!WithPush)
        if(!generateInstruction(list, I_FIND, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    
    // Pre [;,end]
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    
    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_LENGHT> -> (ID/STRING)  */
int FunctionSintxLenght(TokenStructure *TokenStruc, bool WithPush, bool ActiveFunction)
{
    // Premenna adresy
    AdressStruc Adresa;
    Adresa.Type = STRING;
    
    // Gramatika '('
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Gramatika IDENTIFIKATOR
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        
        // Init, copy -> instrukcia
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
    }
    // Ak nie identifikator tak STRING
    else if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Init, copy -> instrukcia
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }        
    }
    else return SYNTACTIC_PROCESS_ERROR; 
    
    // Gramatika ')'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    if (WithPush)
        if(!generateInstruction(list, I_LENGHTPUSH, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if(!WithPush)
        if(!generateInstruction(list, I_LENGTH, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    
    // Ak sa dostaneme na koniec ocakavame uspech
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    return FUNCTION_SUCCESS;
}

/* <TELO_TELO_WRITE> -> (ID/STRING) */
int FunctionSintxWriteTelo(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Premenna adresy
    AdressStruc Adresa;
    
    // Gramatika STRING
    if (TokenStruc->Type == LEX_STRINGRETAZEC)
    {
        // Init, copy -> instrukcia
        Adresa.Type = STRING;
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_WRITE, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
        // Dealok string
        strFree(&Adresa.AdressValue.s);
    }
    // Gramatika IDENTIFIKATOR
    else if (TokenStruc->Type == LEX_IDENTIFIKATOR)
    {
        // Overim ci ID je v TS ak ano skopirujeme do adresy
        if (ActiveFunction)
        {
            if (BSTSearch(ActualFunction->localTS, TokenStruc->attr) == NULL)
                if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                    return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        else
        {
            if (BSTSearch(GlobalSymbolTable, TokenStruc->attr) == NULL)
                return SEMANTIC_UNDEF_REDEF_ERROR;
        }
        
        // Init, copy -> instrukcia
        Adresa.Type = STRING;
        strInit(&Adresa.AdressValue.s);
        strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
        if(!generateInstruction(list, I_WRITEID, &Adresa, NULL, NULL))
        {
            // Dealok
            strFree(&Adresa.AdressValue.s);
            return PROCESS_OTHER_ERROR;
        }
        // Dealok string
        strFree(&Adresa.AdressValue.s);
    }
    else if (TokenStruc->Type == LEX_CELOCISLITERAL)
    {
        Adresa.Type = INTEGER;
        Adresa.AdressValue.i = atoi(strGetStr(&TokenStruc->attr));
        if(!generateInstruction(list, I_WRITE, &Adresa, NULL, NULL)) return PROCESS_OTHER_ERROR;
    }
    else if (TokenStruc->Type == LEX_DESATINYLITERAL)
    {
        Adresa.Type = REAL;
        Adresa.AdressValue.r = atof(strGetStr(&TokenStruc->attr));
        if(!generateInstruction(list, I_WRITE, &Adresa, NULL, NULL)) return PROCESS_OTHER_ERROR;
    }
    else if (TokenStruc->Type == LEX_TRUE)
    {
        Adresa.Type = BOOLEAN;
        Adresa.AdressValue.b = true;
        if(!generateInstruction(list, I_WRITE, &Adresa, NULL, NULL)) return PROCESS_OTHER_ERROR;
    }
    else if (TokenStruc->Type == LEX_FALSE)
    {
        Adresa.Type = BOOLEAN;
        Adresa.AdressValue.b = false;
        if(!generateInstruction(list, I_WRITE, &Adresa, NULL, NULL)) return PROCESS_OTHER_ERROR;
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Ak CIARKA tak opakujeme
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_CIARKA)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FunctionSintxWriteTelo(TokenStruc, ActiveFunction);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    
    // Ak sme sa dostali sem uspech
    return FUNCTION_SUCCESS;
}

/* <TELO_WRITE> -> TELO_TELO_WRITE */
int FunctionSintxWrite(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Ulozisko navratovej hodnoty
    int Result = 0;

    // Gramatika <TELO_WRITE> -> TELO_TELO_WRITE
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_PRIROVNANIE) return SEMANTIC_OTHER_ERROR;
    else if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;

    // Overenie gramatiky <TELO_TELO_WRITE>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    Result = FunctionSintxWriteTelo(TokenStruc, ActiveFunction);
    if (Result != FUNCTION_SUCCESS) return Result;
    
    // Overenie pravej zatvorky
    if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
                        
    // Nacitame dalsi token [;,end] a posleme ho z5 slozenemu prikazu
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
   
    // Ak sme sa dostali az sem ocakavame uspech
    return FUNCTION_SUCCESS;
}

/* function identifikator ( <zoznam_parametrov> ) : typ;
var <telo_var>
begin <slozeny_prikaz> end; */
int FunctionSintxFunction(TokenStructure *TokenStruc)
{
    // TempFunction
    function *TempFunctione = NULL;
    
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Premenna adresy
    AdressStruc Adresa1;
    Adresa1.Type = INTEGER;
    Adresa1.AdressValue.i = FunctionTable->ActualID+1;
    int LabFUNCTION = 0;
    
    // Ulozisko nazvu
    string TempName;
    strInit(&TempName);
    
    // Ulozisko navratoveho typu
    dataT TempType = 0;
    
    // Parameter list
    pList *ParameterList;
    ParameterList = malloc(sizeof(pList));
    PARAMlistInit(ParameterList);
    
    // SymbolTable pre Funkciu
    Node *FuncSymbolTable;
    BSTInit(&FuncSymbolTable);
    
    // Generujeme instrukciu ukazeme na nu ulozime ako navestie a ulozime si jeho index
    if(!generateInstruction(list, I_LAB, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if(!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_FUNCTION)) return PROCESS_OTHER_ERROR;
    LabFUNCTION = LabList->ActualID;

    if (strcmp(strGetStr(&TokenStruc->attr), "sort") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "copy") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "length") == 0 ||
        strcmp(strGetStr(&TokenStruc->attr), "find") == 0)
        return SEMANTIC_UNDEF_REDEF_ERROR;
    
    // Nacitame dalsi token a overime gramatiku ci je IDENTIFIKATOR
    if (TokenStruc->Type != LEX_IDENTIFIKATOR) return SYNTACTIC_PROCESS_ERROR;
    
    // Ulozime nazov
    strCopyString(&TempName, &TokenStruc->attr);
    
    // Overime gramatiku '('
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Zavolame <ZOZNAM_PARAMETROV>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA)
    {
        Result = FunctionSintxFunctionParams(TokenStruc, ParameterList);
        if (Result != FUNCTION_SUCCESS) return Result;
    
        // Overime gramatiku ')' a nenacitavame token lebo uz je z paramov
        if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    }
    
    // Overime gramatiku ':'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_DVOJBODKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Overime gramatiku TYP
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (!FunctionJeTyp(TokenStruc)) return SYNTACTIC_PROCESS_ERROR;
    TempType = FunctionAttribToType(TokenStruc->Type);

    // Overime gramatiku BODKOCIARKA
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_BODKOCIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Overime gramatiku VAR
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_VAR)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FuntionSintxVar(TokenStruc, &FuncSymbolTable, true);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    else if (TokenStruc->Type == LEX_FORWARD)
    {
        if (FUNCTIONlistIsFunction(FunctionTable, &TempName) != NULL)
            return SEMANTIC_UNDEF_REDEF_ERROR;
        
        // Naplnime tabulku funkcii
        FuncInst(FunctionTable, true, false, TempName, 0, NULL, ParameterList, TempType);
        
        // Overime gramatiku BODKOCIARKA
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        if (TokenStruc->Type != LEX_BODKOCIARKA) return SYNTACTIC_PROCESS_ERROR;
        
        // Prednacitame dalsi token pre kompatibilitu
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        
        if (TokenStruc->Type == LEX_FUNCTION)
        {
            TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
            Result = FunctionSintxFunction(TokenStruc);
            if (Result != FUNCTION_SUCCESS) return Result;
        }
        return FUNCTION_SUCCESS;
    }
    else if (TokenStruc->Type == LEX_BEGIN) { }
    
    // Skopirujeme parametre do lokalnej tabulky
    if (ParameterList->first != NULL)
    {
        param *TempParam = ParameterList->first;
        while (TempParam != NULL)
        {
            // Ulozime parameter do LTS
            Result = BSTInsertNew(&FuncSymbolTable, TempParam->name, TempParam->typ);
            if(Result != FUNCTION_SUCCESS) return Result;
            TempParam = TempParam->next;
        }
    }
    
    // Skopirujeme do LTS navratovu premennu
    Result = BSTInsertNew(&FuncSymbolTable, TempName, TempType);
    if (Result != FUNCTION_SUCCESS) return Result;
    
    // Ak je definovana
    TempFunctione = FUNCTIONlistIsFunction(FunctionTable, &TempName);
    if (TempFunctione != NULL)
    {
        // Ak deklarovana
        if (TempFunctione->defined == false && TempFunctione->declared == true)
        {
            if (TempFunctione->returnV != TempType) return SEMANTIC_TYPE_NUMPARAM_ERROR;
            
            param *TempParam = TempFunctione->paramList->first;
            param *TempParamNew = ParameterList->first;
            while (TempParam != NULL && TempParamNew != NULL)
            {
                if (TempParam->typ != TempParamNew->typ) return SEMANTIC_TYPE_NUMPARAM_ERROR;
                if (strCmpString(&TempParam->name, &TempParamNew->name) != 0) return SEMANTIC_TYPE_NUMPARAM_ERROR;
                TempParam = TempParam->next;
                TempParamNew = TempParamNew->next;
            }
        }
        else return SEMANTIC_UNDEF_REDEF_ERROR;
    }
   
    // Naplnime tabulku funkcii
    FuncInst(FunctionTable, true, true, TempName, LabFUNCTION, FuncSymbolTable, ParameterList, TempType);
    
    ActualFunction = FUNCTIONlistIsFunction(FunctionTable, &TempName);;
    
    // Overime gramatiku BEGIN
    if (TokenStruc->Type == LEX_BEGIN)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FunctionSintxSlozPrik(TokenStruc, true, false);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    else return SYNTACTIC_PROCESS_ERROR;
    
    // Overime gramatiku END;
    if (TokenStruc->Type != LEX_END) return SYNTACTIC_PROCESS_ERROR;
    
    // Overime gramatiku BODKOCIARKA
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_BODKOCIARKA) return SYNTACTIC_PROCESS_ERROR;
    
    // Zavolame funkciu intrukciou
    if (!generateInstruction(list, I_JMPCONT, NULL, NULL, NULL))
        return INTERNAL_PROGRAM_ERROR;
    
    // Prednacitame dalsi token pre kompatibilitu
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_FUNCTION)
    {
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FunctionSintxFunction(TokenStruc);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    // TODO JUMP START
    return FUNCTION_SUCCESS;
}

int FunctionSintxFunctionParams(TokenStructure *TokenStruc, pList *ParamList)
{
    // Ulozisko navratky
    int Result = 0;
    
    // Ulozisko nazvu
    string TempName;
    strInit(&TempName);
    
    // Overime ci IDENTIFIKATOR
    if (TokenStruc->Type != LEX_IDENTIFIKATOR) return SYNTACTIC_PROCESS_ERROR;
    strCopyString(&TempName, &TokenStruc->attr);
    
    // Overime gramatiku ':'
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_DVOJBODKA) return SYNTACTIC_PROCESS_ERROR;

    // Overime gramatiku TYP
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (!FunctionJeTyp(TokenStruc)) return SYNTACTIC_PROCESS_ERROR;
    
    // Overime ci uz neni
    if (PARAMlistSearch(ParamList, TempName))
        return SEMANTIC_UNDEF_REDEF_ERROR;
    
    // Ulozime typ a vygenerujeme param
    PARAMlistInsertLast(ParamList, TempName, FunctionAttribToType(TokenStruc->Type));
    
    // Overime gramatiku CIARKA
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type == LEX_BODKOCIARKA)
    {
        // Ak ciarka nacitame dalsi a rekurzivne zavolame
        TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
        Result = FunctionSintxFunctionParams(TokenStruc, ParamList);
        if (Result != FUNCTION_SUCCESS) return Result;
    }
    // Ak je ')' tak vrat z5
    else return FUNCTION_SUCCESS;

    return FUNCTION_SUCCESS;
}
                
int FunctionSintxRunFunction(TokenStructure *TokenStruc, bool WithPush)
{
    // Pomocne premenne
    param *TempParam;
    function *TempFunction;
    AdressStruc Adresa;
    strInit(&Adresa.AdressValue.s);
    
    // Najdeme funkciu
    TempFunction = FUNCTIONlistIsFunction(FunctionTable, &TokenStruc->attr);
    if (TempFunction == NULL) return SEMANTIC_UNDEF_REDEF_ERROR;
    
    // Overime ci (
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_LAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;

    // Ziskame prvy parameter alebo )
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (TokenStruc->Type != LEX_PRAVAZATVORKA)
    {
        // Overujeme a plnime attributy
        TempParam = TempFunction->paramList->first;
    
        // Ak je nejaky parameter
        if (TempParam != NULL)
        {
            // Kym mame parametre
            while (TempParam != NULL)
            {
                // Konvertujeme ho na pozadovanu hodnotu ktoru naplnime
                if (TokenStruc->Type == LEX_CELOCISLITERAL && TempParam->typ == INTEGER)
                {
                    Adresa.Type = INTEGER;
                    Adresa.AdressValue.i = atoi(strGetStr(&TokenStruc->attr));
                }
                else if (TokenStruc->Type == LEX_DESATINYLITERAL && TempParam->typ == REAL)
                {
                    Adresa.Type = REAL;
                    Adresa.AdressValue.r = atof(strGetStr(&TokenStruc->attr));
                }
                else if (TokenStruc->Type == LEX_STRINGRETAZEC && TempParam->typ == STRING)
                {
                    Adresa.Type = STRING;
                    strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
                }
                else if (TokenStruc->Type == LEX_TRUE && TempParam->typ == BOOLEAN)
                {
                    Adresa.Type = BOOLEAN;
                    Adresa.AdressValue.b = true;
                }
                else if (TokenStruc->Type == LEX_FALSE && TempParam->typ == BOOLEAN)
                {
                    Adresa.Type = BOOLEAN;
                    Adresa.AdressValue.b = false;
                }
                else if (TokenStruc->Type == LEX_IDENTIFIKATOR)
                {
                    Adresa.Type = STRING;
                    strInit(&Adresa.AdressValue.s);
                    strCopyString(&Adresa.AdressValue.s, &TokenStruc->attr);
                }
                else return SEMANTIC_TYPE_NUMPARAM_ERROR;
                
                // Pushneme
                if (TokenStruc->Type != LEX_IDENTIFIKATOR)
                {
                    if (!generateInstruction(list, I_PUSH, &Adresa, NULL, NULL))
                        return INTERNAL_PROGRAM_ERROR;
                }
                else if (TokenStruc->Type == LEX_IDENTIFIKATOR)
                {
                    if (!generateInstruction(list, I_PUSHID, &Adresa, NULL, NULL))
                        return INTERNAL_PROGRAM_ERROR;
                }
            
                // Skocime na dalsi parameter
                TempParam = TempParam->next;
                TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
                if (TokenStruc->Type == LEX_CIARKA)
                {
                    // Ziskame prvy parameter
                    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
                    continue;
                }
                else if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
                else break;
            }
            
        
            // Overime ci zobral vsetky parametre
            if (TempParam != NULL) return SEMANTIC_TYPE_NUMPARAM_ERROR;
        }
        
        // Overime ci )
        if (TokenStruc->Type != LEX_PRAVAZATVORKA) return SYNTACTIC_PROCESS_ERROR;
    }
    
    // Zavolame funkciu intrukciou
    Adresa.Type = STRING;
    Adresa.AdressValue.s = TempFunction->Key;
    
    AdressStruc Adresa2;
    Adresa2.Type = BOOLEAN;
    
    if (!WithPush)
        Adresa2.AdressValue.b = false;
    else
        Adresa2.AdressValue.b = true;
    
    if (!generateInstruction(list, I_JMPFUNC, &Adresa, &Adresa2, NULL))
        return INTERNAL_PROGRAM_ERROR;
    
    // Uspech
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    return FUNCTION_SUCCESS;
    
}

/* <TELO_REPEAT> -> <SLOZENY_PRIKAZ> until <VYRAZ>;   [TD] (Gramatika cyklu while) */
int FunctionSintxRepeat(TokenStructure *TokenStruc, bool ActiveFunction)
{
    // Exp Type
    dataT ExpType = 0;
    
    // Ulozisko navesti
    int LabREPEAT = 0;
    
    // Ulozisko navratovych hodnot
    int Result = 0;
    
    // Premenna adresy
    AdressStruc Adresa1;
    Adresa1.Type = INTEGER;
    Adresa1.AdressValue.i = 0;
    
    // Generujeme instrukciu ukazeme na nu ulozime ako navestie a ulozime si jeho index
    if(!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if(!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_REPEAT)) return PROCESS_OTHER_ERROR;
    LabREPEAT = LabList->ActualID;
    
    // Overenie gramatiky <SLOZENY_PRIKAZ>
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    Result = FunctionSintxSlozPrik(TokenStruc, ActiveFunction,true);
    if (Result != FUNCTION_SUCCESS) return Result;
    
    // Overenie UNTIL
    if (TokenStruc->Type != LEX_UNTIL) return SYNTACTIC_PROCESS_ERROR;
    
    // Overenie VYRAZ
    TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);
    if (ActiveFunction)
        Result = FunctionParseExpression(TokenStruc, ActualFunction->localTS, GlobalSymbolTable, list, TokList, false, &ExpType);
    else
        Result = FunctionParseExpression(TokenStruc, GlobalSymbolTable, GlobalSymbolTable, list, TokList, false, &ExpType);
    
    if (Result != FUNCTION_SUCCESS) return SYNTACTIC_PROCESS_ERROR;
    
    // Porovname vysledku expression
    if (ExpType != BOOLEAN) return SEMANTIC_TYPE_NUMPARAM_ERROR;
    
    // Ak zostal z expressions EOL posunieme sa
    if (TokenStruc->Type == LEX_EOL) TokenStruc->Type = GetNewToken(TokList, &TokenStruc->attr, false);

    Adresa1.AdressValue.i = LabREPEAT;
    if(!generateInstruction(list, I_WJMPFALSE, &Adresa1, NULL, NULL)) return PROCESS_OTHER_ERROR;
    
    // Vytvorime navestie ulozime do tabulky a zapiseme si jeho hodnotu
    if (!generateInstruction(list, I_LAB, NULL, NULL, NULL)) return PROCESS_OTHER_ERROR;
    if (!LABlistInsertLast(LabList, &listGetPointerLast(list)->Instruction, LAB_ENDWHILE)) return PROCESS_OTHER_ERROR;
    
    // Ak sa dostaneme na koniec ocakavame uspech
    return FUNCTION_SUCCESS;
}
