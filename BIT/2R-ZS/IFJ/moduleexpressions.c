/**************************************************
* Subor: moduleexpressions.c                      *
* Nazov projektu: Implementacia vyrazov           *
* Varianta: Tým 003, varianta a/2/I               *
* Vypracovali:                                    *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz   *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz      *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz     *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz     *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz    *
***************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"
#include "lablist.h"
#include "scanner.h"
#include "parser.h"
#include "moduleexpressions.h"

void initSE(sExp **head);
sExp *pushSE(sExp *head, ExpItem *a);
sExp *popSE(sExp *head);
bool isEmptyE(sExp *head);
dataT FindResultType(sExp *Stack);
char TokenToChar(TokenStructure *Token);
int CharToIndex (char znak);
int FunctionReduce(Node *SymbolTable, Node *GlobalSymbolTable, tListOfInstr *InstList, bool Priradenie);

// Precedencna tabulka
char rule[14][14] = {
    /*               *    /    +    -    <    >    <=   >=   =    <>   i    (    )    $     */
    /* *  */	   {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* /  */	   {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* +  */	   {'<', '<', '>', '>', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* -  */	   {'<', '<', '>', '>', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* <  */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* >  */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* <= */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* >= */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* =  */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* <> */	   {'<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '<', '>', '>', } ,
    /* i  */	   {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'x', 'x', '>', '>', } ,
    /* (  */	   {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '=', 'x', } ,
    /* )  */	   {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'x', 'x', '>', '>', } ,
    /* $  */	   {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', 'x', 'F', }
};

// Globalny zasobnik
sExp *Stack;

// Funkcia na inicializaciu zasobnika
void initSE(sExp **head)
{
	*head = NULL;
}

// Funkcia na vlozenie prvku na zasobnik
sExp *pushSE(sExp *head, ExpItem *a)
{
    // Alloc
	sExp *tmp;
	tmp = (struct stackExp*)malloc(sizeof(struct stackExp));
	if(tmp == NULL) return NULL;
	
    // Another alloc
    ExpItem *tmpExp = malloc(sizeof(struct ItemExp));
    
    // Priradenia 1
    tmpExp->Content = a->Content;
    tmpExp->ContType = a->ContType;
    tmpExp->ExpType = a->ExpType;
    tmpExp->IsID = a->IsID;
    
    // Priradenia 2
	tmp->top = tmpExp;
	tmp->next = head;
    
    // Premostenia
	head = tmp;
	return head;
}

// Funkcia na vybranie prvku zo zasobniku
sExp *popSE(sExp *head)
{
	sExp *tmp;
	tmp = head;
	head = head->next;
	free(tmp);
	return head;
}

// Funkcia ci je zasobnik prazdny
bool isEmptyE(sExp *head)
{
	if(head == NULL) return true;
	else return false;
}

// Funkcia na prehladavanie zasobniku sChar, hlada char ExpType
ExpItem *FindTopTerm(sExp *StackFunc)
{
    // Temp item
    sExp *Temp = StackFunc;
    
	// Cykli, kym neprejde cely zasobnik
	while(Stack != NULL)
    {
		// Vrati polozku ExpItem, ktora neobsahuje znaky 'E' a 'P'
		if(Temp->top->ExpType != 'E' && Temp->top->ExpType != 'P')
			return Temp->top;		
		
        // Ukaz na dalsi prvok
        Temp = Temp->next;
	}
    
    // Ak nic nenajde tak NULL
	return NULL;
}

// Funkcia na prehladavanie zasobniku sChar, hlada char ExpType
dataT FindResultType(sExp *StackFunc)
{
    // Temp item
    sExp *Temp = StackFunc;
    
	// Cykli, kym neprejde cely zasobnik
	while(Stack != NULL)
    {
		// Vrati polozku ExpItem, ktora neobsahuje znaky 'E' a 'P'
		if(Temp->top->ExpType == 'E' || Temp->top->ExpType == 'P')
			return Temp->top->ContType;
		
        // Ukaz na dalsi prvok
        Temp = Temp->next;
	}
    
    // Ak nic nenajde tak NULL
	return 0;
}


/* Funkcia na prevod tokenu na char */
char TokenToChar(TokenStructure *Token)
{
    switch (Token->Type)
    {
		case LEX_KRAT: return '*';
		case LEX_DELENO: return '/';
		case LEX_SCITANIE: return '+';
		case LEX_ODCITANIE: return '-';
		case LEX_MENEJ: return '<';
		case LEX_VIAC: return '>';
		case LEX_MENEJROVNATKO: return '{';
		case LEX_VIACROVNATKO: return '}';
		case LEX_ROVNATKO: return '=';
		case LEX_MENEJVIAC: return '!';
		case LEX_CELOCISLITERAL: return 'i';
        case LEX_DESATINYLITERAL: return 'i';
        case LEX_IDENTIFIKATOR: return 'i';
        case LEX_TRUE: return 'i';
        case LEX_FALSE: return 'i';
        case LEX_STRINGRETAZEC: return 'i';
		case LEX_LAVAZATVORKA: return '(';
		case LEX_PRAVAZATVORKA: return ')';
		default: return '$';
	}
	return '$';
}

/* Funkcia prevodu Charu na Index precedencnej */
int CharToIndex (char znak)
{
	switch (znak)
    {
		case '*': return 0;
		case '/': return 1;
		case '+': return 2;
		case '-': return 3;
		case '<': return 4;
		case '>': return 5;
		case '{': return 6;
		case '}': return 7;
		case '=': return 8;
		case '!': return 9;
		case 'i': return 10;
		case '(': return 11;
		case ')': return 12;
		case '$': return 13;
		default: return 13;
	}
	return 13;
}

/* Funkcia na redukciu vyrazu */
int FunctionReduce(Node *SymbolTable, Node *GlobalSymbolTable, tListOfInstr *InstList, bool Priradenie)
{
    // Temp Types
    dataT Type1 = 0;
    dataT Type2 = 0;
    
    // Pomocna premenna pre typy
    char TempExpType;
    
    // Pripravime si ukladanie E
    ExpItem TempExprItem;
    TempExprItem.ExpType = 'E';
    TempExprItem.ContType = 0;

    // Inicializacia adresi
    AdressStruc Adresa;
    AdressStruc Adresa2;
 
    // Nacitame vrchol zasobniku a uvolnime
    if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
    ExpItem TempChar = *Stack->top;
    Stack = popSE(Stack);
    
    // Podla typu expression vyberieme pravidlo
    switch (TempChar.ExpType)
    {
            // E->i
            case 'i':
        
                // Do adresi ulozime hodnotu a vygenerujeme PUSH
                Adresa.AdressValue = TempChar.Content;
                Adresa.Type = TempChar.ContType;
            
                // Vyberieme ci ide o ID
                if (!TempChar.IsID)
                {   TempExprItem.ContType = TempChar.ContType;
                    if(!generateInstruction(InstList, I_PUSH, &Adresa, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                }
                if (TempChar.IsID)
                {   if (BSTSearch(SymbolTable, TempChar.Content.s) != NULL)
                        TempExprItem.ContType = BSTSearch(SymbolTable, TempChar.Content.s)->type;
                    else if (BSTSearch(GlobalSymbolTable, TempChar.Content.s) != NULL)
                        TempExprItem.ContType = BSTSearch(GlobalSymbolTable, TempChar.Content.s)->type;
                    
                    if(!generateInstruction(InstList, I_PUSHID, &Adresa, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                }
            
                
                // Dalsi nacitany znak musi byt 's'
                if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                TempChar = *Stack->top;
                Stack = popSE(Stack);
                if (TempChar.ExpType != 's') return SYNTACTIC_PROCESS_ERROR;
            
                // Ulozime 'E'
                Stack = pushSE(Stack, &TempExprItem);
                if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                return FUNCTION_SUCCESS;
            
            // E-> (E)
            case ')':
            
                // Nacitame dalsi znak a musi byt 'E' alebo 'P'
                if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                TempChar = *Stack->top;
                Stack = popSE(Stack);
                if (TempChar.ExpType != 'E' && TempChar.ExpType != 'P') return SYNTACTIC_PROCESS_ERROR;
                TempExprItem.ContType = TempChar.ContType;
            
                // Nacitame dalsi znak a musi byt '('
                if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                TempChar = *Stack->top;
                Stack = popSE(Stack);
                if (TempChar.ExpType != '(') return SYNTACTIC_PROCESS_ERROR;
            
                // Nacitame dalsi znak a musi byt 's'
                if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                TempChar = *Stack->top;
                Stack = popSE(Stack);
                if (TempChar.ExpType != 's') return SYNTACTIC_PROCESS_ERROR;
            
                // Ulozime 'E'
                Stack = pushSE(Stack, &TempExprItem);
                if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                return FUNCTION_SUCCESS;
           
            /* E-> E*E, E-> E/E, E-> E+E, E-> E-E */
            case 'E':
                Type1 = TempChar.ContType;
            
                // Nacitame dalsi znak a mal by byt znamienko
                if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                TempChar = *Stack->top;
                Stack = popSE(Stack);
            
                // Vyberieme podla znamienka
                if (TempChar.ExpType == '*' || TempChar.ExpType == '/' || TempChar.ExpType == '+' || TempChar.ExpType == '-')
                {
                    // Ulozime si znamienko
                    TempExpType = TempChar.ExpType;
                    
                    // Nacitame si dalsi znak a mal by byt 's'
                    if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                    TempChar = *Stack->top;
                    Stack = popSE(Stack);
                    if (TempChar.ExpType != 's') return SYNTACTIC_PROCESS_ERROR;
                    
                    // Nacitame dalsi znak a mal by byt 'E'
                    if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                    TempChar = *Stack->top;
                    Stack = popSE(Stack);
                    if (TempChar.ExpType != 'E') return SYNTACTIC_PROCESS_ERROR;
                    Type2 = TempChar.ContType;
                    
                    // Generujeme instrukcie podla znamienka
                    if (TempExpType == '+')
                    {
                        if (Type1 == INTEGER && Type2 == INTEGER) TempExprItem.ContType = INTEGER;
                        else if (Type1 == INTEGER && Type2 == REAL) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == INTEGER) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == REAL) TempExprItem.ContType = REAL;
                        else if (Type1 == STRING && Type2 == STRING) TempExprItem.ContType = STRING;
                        else return SEMANTIC_TYPE_NUMPARAM_ERROR;
                        if(!generateInstruction(InstList, I_SUM, NULL, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                    }
                    if (TempExpType == '/')
                    {
                        if (Type1 == INTEGER && Type2 == INTEGER) TempExprItem.ContType = REAL;
                        else if (Type1 == INTEGER && Type2 == REAL) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == INTEGER) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == REAL) TempExprItem.ContType = REAL;
                        else return SEMANTIC_TYPE_NUMPARAM_ERROR;
                        if(!generateInstruction(InstList, I_DIV, NULL, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                    }
                    if (TempExpType == '*')
                    {
                        if (Type1 == INTEGER && Type2 == INTEGER) TempExprItem.ContType = INTEGER;
                        else if (Type1 == INTEGER && Type2 == REAL) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == INTEGER) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == REAL) TempExprItem.ContType = REAL;
                        else return SEMANTIC_TYPE_NUMPARAM_ERROR;
                        if(!generateInstruction(InstList, I_MUL, NULL, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                    }
                    if (TempExpType == '-')
                    {
                        if (Type1 == INTEGER && Type2 == INTEGER) TempExprItem.ContType = INTEGER;
                        else if (Type1 == INTEGER && Type2 == REAL) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == INTEGER) TempExprItem.ContType = REAL;
                        else if (Type1 == REAL && Type2 == REAL) TempExprItem.ContType = REAL;
                        else return SEMANTIC_TYPE_NUMPARAM_ERROR;
                        if(!generateInstruction(InstList, I_SUB, NULL, NULL, NULL)) return INTERNAL_PROGRAM_ERROR;
                    }
                    
                    // Ulozime 'E'
                    Stack = pushSE(Stack, &TempExprItem);
                    if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                    
                    return FUNCTION_SUCCESS;
                }
            
                /* E-> E<E, E-> E>E, E-> E{E, E-> E}E, E-> E=E, E-> E!E */
                else if (TempChar.ExpType =='<' || TempChar.ExpType =='>' || TempChar.ExpType =='{'
                         || TempChar.ExpType=='}' || TempChar.ExpType=='=' || TempChar.ExpType=='!')
                {
                    // Ulozime si rovnitko
                    TempExpType = TempChar.ExpType;
                    
                    // Nacitame dalsi znak a mal by byt 's'
                    if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                    TempChar = *Stack->top;
                    Stack = popSE(Stack);
                    if (TempChar.ExpType != 's') return SYNTACTIC_PROCESS_ERROR;
                    
                    // Nacitame dalsi znak a mal by byt 'E'
                    if(isEmptyE(Stack) == true) return INTERNAL_PROGRAM_ERROR;
                    TempChar = *Stack->top;
                    Stack = popSE(Stack);
                    if (TempChar.ExpType != 'E') return SYNTACTIC_PROCESS_ERROR;
                    Type2 = TempChar.ContType;
                    
                    // Generujeme instrukciu podla znamienka
                    Adresa.Type = INTEGER; 
                    if (TempExpType == '<') Adresa.AdressValue.i = CMP_LESS;
                    if (TempExpType == '>') Adresa.AdressValue.i = CMP_GREATER;
                    if (TempExpType == '{') Adresa.AdressValue.i = CMP_LESSEQ;
                    if (TempExpType == '}') Adresa.AdressValue.i = CMP_GREATEREQ;
                    if (TempExpType == '=') Adresa.AdressValue.i = CMP_EQUAL;
                    if (TempExpType == '!') Adresa.AdressValue.i = CMP_NOTEQUAL;
                    
                    Adresa2.Type = BOOLEAN;
                    Adresa2.AdressValue.b = Priradenie;
                    if (!generateInstruction(InstList, I_COMP, &Adresa, &Adresa2, NULL)) return INTERNAL_PROGRAM_ERROR;
                    
                    // Ulozime 'E'
                    TempExprItem.ContType = BOOLEAN;
                    Stack = pushSE(Stack, &TempExprItem);
                    if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                    
                    return FUNCTION_SUCCESS;                    
                }
                else return SYNTACTIC_PROCESS_ERROR;
            default: return SYNTACTIC_PROCESS_ERROR;
    }
    return SYNTACTIC_PROCESS_ERROR;
}

/* Funkcia ktora podla algoritmu spracovava vyrazy */
int FunctionParseExpression(TokenStructure *Token, Node *SymbolTable, Node *GlobSymbolTable, tListOfInstr *InstList, tDLList *TokenList, bool Priradenie, dataT *ResultType)
{
    // Empty handler
    bool EmptyExp = false;
    
    // Ulozisko navratovej hodnoty
    int Result = 0;
    
    // Definovanie zasobnika
    initSE(&Stack);
    
    // Definujeme potrebne premenne
    ExpItem ASymbolTop;
    ExpItem BSymbolInput;
    ExpItem TempExprItem;
    char TempChar;
    Cont TempCont;
    
    // Vlozime zaciatocne '$' na zasobnik
    TempExprItem.ExpType = '$';
    TempExprItem.ContType = 0;
    TempExprItem.IsID = false;
    Stack = pushSE(Stack, &TempExprItem);
    if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;

    // Repeat
    do
    {
        // Nechť a = top
        ASymbolTop = *FindTopTerm(Stack);
        
        // b = aktuální znak na vstupu
        TempChar = TokenToChar(Token);
        
        // Ak sa jedna o ID urobime potrebne operacie
        if (TempChar == 'i')
        {
            // Ak je celociselny nadstav hodnoty s atoi
            if (Token->Type == LEX_CELOCISLITERAL)
            {
                TempCont.i = atoi(strGetStr(&Token->attr));
                BSymbolInput.ContType = INTEGER;
                BSymbolInput.IsID = false;
            }
            // Ak je desatiny nadstav hodnoty s atof
            else if (Token->Type == LEX_DESATINYLITERAL)
            {
                TempCont.r = atof(strGetStr(&Token->attr));
                BSymbolInput.ContType = REAL;
                BSymbolInput.IsID = false;
            }
            // Ak je boolean true nadstav
            else if (Token->Type == LEX_TRUE)
            {
                TempCont.b = true;
                BSymbolInput.ContType = BOOLEAN;
                BSymbolInput.IsID = false;
            }
            // Ak je boolean false nadstav
            else if (Token->Type == LEX_FALSE)
            {
                TempCont.b = false;
                BSymbolInput.ContType = BOOLEAN;
                BSymbolInput.IsID = false;
            }
            // Ak je identifikator nadstav zo ST
            else if (Token->Type == LEX_IDENTIFIKATOR)
            {
                if(BSTSearch(SymbolTable, Token->attr) == NULL)
                    if(BSTSearch(GlobSymbolTable, Token->attr) == NULL)
                        return SEMANTIC_UNDEF_REDEF_ERROR;
                
                strInit(&TempCont.s);
                strCopyString(&TempCont.s, &Token->attr);
                BSymbolInput.ContType = STRING;
                BSymbolInput.IsID = true;
            }
            // Ak je string nadstav
            else if (Token->Type == LEX_STRINGRETAZEC)
            {
                strInit(&TempCont.s);
                strCopyString(&TempCont.s, &Token->attr);
                BSymbolInput.ContType = STRING;
                BSymbolInput.IsID = false;
            }
            // Dopln informacie pre expressions
            BSymbolInput.Content = TempCont;
            BSymbolInput.ExpType = 'i';
        }
        else
        {
            BSymbolInput.ExpType = TempChar;
            BSymbolInput.ContType = 0;
            BSymbolInput.IsID = false;
        }
        
        // Case Tabulka [a, b] 
        switch(rule[CharToIndex(ASymbolTop.ExpType)][CharToIndex(BSymbolInput.ExpType)])
        {
             // Pripad '='
             case '=':
                EmptyExp = true;
                // Push(b)
                Stack = pushSE(Stack, &BSymbolInput);
                if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                
                // Přečti další symbol b ze vstupu
                Token->Type = GetNewToken(TokenList, &Token->attr, true);
                break;
              
            // Pripad '<'
            case '<':
                EmptyExp = true;
                // Zaměň a za a< na zásobníku
                // Vlozime startovaciu znacku
                TempExprItem.ExpType = 's';
                TempExprItem.ContType = 0;
                TempExprItem.IsID = false;
                Stack = pushSE(Stack, &TempExprItem);
                if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                
                // Push(b)
                Stack = pushSE(Stack, &BSymbolInput);
                if (Stack == NULL) return INTERNAL_PROGRAM_ERROR;
                
                // Přečti další symbol b ze vstupu
                Token->Type = GetNewToken(TokenList, &Token->attr, true);
                break;
              
             // Pripad '>'
             case '>':
                EmptyExp = true;
                // Vykoname redukciu vyrazu
                Result = FunctionReduce(SymbolTable, GlobSymbolTable, InstList, Priradenie);
                if (Result != FUNCTION_SUCCESS) return Result;
                break;
                
            // Pripad 'F' finish
            case 'F': break;
            default:return SYNTACTIC_PROCESS_ERROR;
        }
    }
    // Until a = $ and top = $
    while ((ASymbolTop.ExpType != '$') || (FindTopTerm(Stack)->ExpType != '$'));
    
    if (!EmptyExp) return SYNTACTIC_PROCESS_ERROR;
    
    *ResultType = FindResultType(Stack);
    
    // Return success
    return FUNCTION_SUCCESS;
}

