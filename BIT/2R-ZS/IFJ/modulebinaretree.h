/********************************************************
* Subor: modulebinaretree.h                             *
* Nazov projektu: Hlavickovy subor k modulebinaretree.h *
* Varianta: Tým 003, varianta a/2/I                     *
* Vypracovali:                                          *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz         *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz            *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz           *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz           *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz          *
*********************************************************/
#ifndef BS_H
#define BS_H

// Jeden parameter
typedef struct parameter
{
    string name;
	dataT typ;	// datovy typ parametru
	struct parameter *next;	// dalsi prvok
    struct parameter *prev; // predchadzajuci prvok
} param;

// Zoznam parametrov
typedef struct parameterList{
	param *first;
	param *last;
} pList;

// TS pre funkcie
typedef struct functionItem
{
    int ID; // ID funkcie index
	string Key;	// meno funkcie
	dataT returnV; // navratovy typ
	pList *paramList; // zoznam parametrov
	Node *localTS; // lokalna TS, iba pre funkciu 
	bool defined; // bola uz definovana? musi byt prave raz
	bool declared; // maximalne moze byt 1 deklaracia
	int LabTableIndex;	// zoznam instrukcii pre funkciu
	struct functionItem *next;
} function;

// Zoznam funkcii
typedef struct functionsList{
	function *first;
    int ActualID;
} fList;

bool PARAMlistInsertLast(pList *L, string Name, dataT Type);
void PARAMlistInit(pList *L);
bool PARAMlistSearch(pList *L, string Name);

function *FUNCTIONlistSearch(int ID, fList *L);
void FUNCTIONlistInit(fList *L);
bool FuncInst(fList *L, bool Declared, bool Defined, string Name, int LABIndex, Node *LocTS, pList *ParList, dataT Return);
function *FUNCTIONlistIsFunction(fList *L, string *Identifikator);
#endif
