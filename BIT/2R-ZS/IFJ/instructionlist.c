/****************************************************
* Subor: instrukctionlist.c                         *
* Nazov projektu: Implementacia zoznamu instrukcii  *
* Varianta: Tým 003, varianta a/2/I                 *
* Vypracovali:                                      *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz     *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz        *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz       *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz       *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz      *
*****************************************************/
#include "main.h"
#include "ial.h"
#include "modulebinaretree.h"
#include "instructionlist.h"

// Globalna premenna na generovanie jedninecneho kodu
int counter;

void listInit(tListOfInstr *L)
// Funkcia inicializuje seznam instrukci
{
    L->first  = NULL;
    L->last   = NULL;
    L->active = NULL;
}
  
void listFree(tListOfInstr *L)
// Funkcia dealokuje seznam instrukci
{
    // Kym je nejaky prvok
    tListItem *ptr;
    while (L->first != NULL)
    {
        // Premostenie
        ptr = L->first;
        L->first = L->first->nextItem;
        
        // Uvolni pointer
        free(ptr);
    }
}

bool listInsertLast(tListOfInstr *L, tInstr I)
// Vlozi novu instruciu na koniec zoznamu
{
    // Alokuje novy prvok
  	tListItem *newItem;
	newItem = malloc(sizeof (tListItem));
    if (newItem == NULL) return false;
    
    // Naplni strukturu
 	newItem->Instruction = I;
    
    // Napln stringy
    if (I.addr1.Type == STRING)
    {
        strInit(&newItem->Instruction.addr1.AdressValue.s);
        strCopyString(&newItem->Instruction.addr1.AdressValue.s, &I.addr1.AdressValue.s);
    }
    if (I.addr2.Type == STRING)
    {
        strInit(&newItem->Instruction.addr2.AdressValue.s);
        strCopyString(&newItem->Instruction.addr2.AdressValue.s, &I.addr2.AdressValue.s);
    }
    if (I.addr3.Type == STRING)
    {
        strInit(&newItem->Instruction.addr3.AdressValue.s);
        strCopyString(&newItem->Instruction.addr3.AdressValue.s, &I.addr3.AdressValue.s);
    }
    
    // Premostenie
    newItem->nextItem = NULL;
    if (L->first == NULL)
 		L->first = newItem;
  	else
        L->last->nextItem=newItem;
  	L->last=newItem;
    
    // Vratime uspech
    return true;
}

void listFirst(tListOfInstr *L)
// Zaktivuje prvu instrukciu
{
  L->active = L->first;
}

void listNext(tListOfInstr *L)
// Aktivnou instrukciou se stane nasledujuca instrukcia
{
 	if (L->active != NULL)
 		L->active = L->active->nextItem;
}

void listGoto(tListOfInstr *L, void *gotoInstr)
// Nastavi aktivnu instrukciu podla zadaneho ukazatela
{
  L->active = (tListItem*) gotoInstr;
}

tListItem *listGetPointerLast(tListOfInstr *L)
// Vrati ukazatel na poslednu instrukciu
{
	return L->last;
}

tInstr *listGetData(tListOfInstr *L)
// Vrati aktivnu instrukciu
{
    // Ak nie je vrati NULL
    if (L->active == NULL)
        return NULL;
    else return &(L->active->Instruction);
}

// Generuje identifikator pre ulozenie do tabulky symbolov
void generateVariable(string *var)
{
    // Vlozi '$'
	strClear(var);
	strAddChar(var, '$');
    
    // Pocitadlo
	int i = counter;
  	while (i != 0)
    {
        // Algoritmus
        strAddChar(var, (char)(i % 10 + '0'));
        i = i / 10;
  	}
  	counter++;
}

// Ulozi instrukciu do zoznamu
bool generateInstruction(tListOfInstr *list, InsT iType, AdressStruc *Key1, AdressStruc *Key2, AdressStruc *Key3)
{
    // Nova instrukcia
	tInstr I;
    I.addr1.Type = 0;
    I.addr2.Type = 0;
    I.addr3.Type = 0;
    
    // Naplnime ju hodnotami
	I.instType = iType;
    if (Key1 != NULL) I.addr1 = *Key1;
	if (Key2 != NULL) I.addr2 = *Key2;
	if (Key3 != NULL) I.addr3 = *Key3;
    
    // Ulozime na koniec zoznamu a overime uspech
	if (listInsertLast(list, I))
        return true;
    else
        return false;
}

// Ulozisko nazvov instrukcii
const char *nameI[] =
{"I_START", "I_STOP", "I_READ", "I_WRITE", "I_WRITEID","I_ASSIGN", "I_SUM", "I_SUB", "I_MUL", "I_DIV",
 "I_RETURN", "I_CALL", "I_LENGTH", "I_COPY", "I_FIND",  "I_PUSH", "I_POP", "I_COMP", "I_PUSHID", "I_SORT",
 "I_SORTPUSH", "I_FINDPUSH", "I_COPYPUSH", "I_LENGHTPUSH", "I_WJMP", "I_WJMPFALSE", "I_LAB", "I_JMPFUNC", "I_JMPCONT"};

// Vypisanie zoznamu instrukcii
void printIList(tListOfInstr *list)
{
    // Zacneme prvou
    tListItem *Temp = list->first;
    
    // Pocitame kym je prvok
	int COunt = 1;
	while(Temp != NULL)
    {
        // Vypisujeme instrukcie
		if (DEBUG_MODE) printf("%d.Instrukcia: [%s]\n", COunt ,nameI[Temp->Instruction.instType]);
		Temp = Temp->nextItem;
		COunt++;
	}	
}

// Vratenie instrukcie pred koncom
tListItem *findEnd(tListOfInstr *list)
{
    // Zacneme prvou
    tListItem *Temp = list->first;
    tListItem *Prev = NULL;
    
    // Pocitame kym je prvok
	while(Temp != NULL)
    {
        if (Temp->Instruction.instType == I_STOP)
            return Prev;
        Prev = Temp;
		Temp = Temp->nextItem;
	}
    return NULL;
}

// Vratenie instrukcie pred koncom
tListItem *findSTART(tListOfInstr *list)
{
    // Zacneme prvou
    tListItem *Temp = list->first;
    
    // Pocitame kym je prvok
	while(Temp != NULL)
    {
        if (Temp->Instruction.instType == I_START)
            return Temp;
		Temp = Temp->nextItem;
	}
    return NULL;
}
