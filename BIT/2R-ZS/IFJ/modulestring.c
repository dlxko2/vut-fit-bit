/**************************************************************
* Subor: modulestring.c                                       *
* Nazov projektu: Implementacia funkcii pre pracu s retazcami *
* Varianta: Tým 003, varianta a/2/I                           *
* Vypracovali:                                                *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz               *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz                  *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz                 *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz                 *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz                *
***************************************************************/

//jednoducha knihovna pro praci s nekonecne dlouhymi retezci
#include "main.h"

#define STR_LEN_INC 8
#define STR_ERROR   1
#define STR_SUCCESS 0

int strInit(string *s)
// funkce vytvori novy retezec
{
   if ((s->str = (char*) malloc(STR_LEN_INC)) == NULL)
      return STR_ERROR;
   s->str[0] = '\0';
   s->length = 0;
   s->allocSize = STR_LEN_INC;
   return STR_SUCCESS;
}

void strFree(string *s)
// funkce uvolni retezec z pameti
{
    if (s->str != NULL) free(s->str);
}

void strClear(string *s)
// funkce vymaze obsah retezce
{
   s->str[0] = '\0';
   s->length = 0;
}

int strAddChar(string *s1, char c)
// prida na konec retezce jeden znak
{
   if (s1->length + 1 >= s1->allocSize)
   {
      // pamet nestaci, je potreba provest realokaci
      if ((s1->str = (char*) realloc(s1->str, (unsigned int)s1->length + STR_LEN_INC)) == NULL)
         return STR_ERROR;
      s1->allocSize = s1->length + STR_LEN_INC;
   }
   s1->str[s1->length] = c;
   s1->length++;
   s1->str[s1->length] = '\0';
   return STR_SUCCESS;
}

int strCopyString(string *s1, string *s2)
// prekopiruje retezec s2 do s1
{
   int newLength = s2->length;
   if (newLength >= s1->allocSize)
   {
      // pamet nestaci, je potreba provest realokaci
      if ((s1->str = (char*) realloc(s1->str, (unsigned int)newLength + 1)) == NULL)
         return STR_ERROR;
      s1->allocSize = newLength + 1;
   }
   strcpy(s1->str, s2->str);
   s1->length = newLength;
   return STR_SUCCESS;
}

int strConcaterateString(string *s1, string *s2)
// prekopiruje retezec s2 do s1
{
    int newLength = s1->length + s2->length;
    if (newLength >= s1->allocSize)
    {
        // pamet nestaci, je potreba provest realokaci
        if ((s1->str = (char*) realloc(s1->str, (unsigned int)newLength + 1)) == NULL)
            return STR_ERROR;
        s1->allocSize = newLength + 1;
    }
    
    strcat(s1->str, s2->str);
    s1->length = newLength;
    return STR_SUCCESS;
}

int strCmpString(string *s1, string *s2)
// porovna oba retezce a vrati vysledek
{
   return strcmp(s1->str, s2->str);
}

int strCmpConstStr(string *s1, char* s2)
// porovna nas retezec s konstantnim retezcem
{
   return strcmp(s1->str, s2);
}

char *strGetStr(string *s)
// vrati textovou cast retezce
{
   return s->str;
}

int strGetLength(string *s)
// vrati delku daneho retezce
{
   return s->length;
}
