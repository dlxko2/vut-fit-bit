/*************************************************
* Subor: main.h		                             *
* Nazov projektu: Hlavickovy subor pre main.c 	 *
* Varianta: Tým 003, varianta a/2/I              *
* Vypracovali:                                   *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz  *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz     *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz    *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz    *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz   *
**************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "modulestring.h"

#define DEBUG_MODE 0

// Navratovky funkcii
typedef enum
{
    FUNCTION_SUCCESS = 0,
    FUNCTION_ERROR = 1,
} FunctionReturns;

// Ukoncovacie kody
typedef enum
{
    INTERPRET_SUCCESS = 0,
    LEXICAL_PROCESS_ERROR = 1,
    SYNTACTIC_PROCESS_ERROR = 2,
    SEMANTIC_UNDEF_REDEF_ERROR = 3,
    SEMANTIC_TYPE_NUMPARAM_ERROR = 4,
    SEMANTIC_OTHER_ERROR = 5,
    PROCESS_NUMLOAD_ERROR = 6,
    PROCESS_UNINITIALIZED_ERROR = 7,
    PROCESS_DIVIDENULL_ERROR = 8,
    PROCESS_OTHER_ERROR = 9,
    INTERNAL_PROGRAM_ERROR = 99,
}GlobalExitCodes;

// Lexikalne tokeny
typedef enum
{
    END_OF_FILE = 2,
    // Datove typy premennych
    LEX_BOOLEAN,	//2
    LEX_INTEGER,	//3
    LEX_STRING,		//4
    LEX_REAL,		//5
    
    // Datove typy
    LEX_IDENTIFIKATOR,	//6
    LEX_CELOCISLITERAL,	//7
    LEX_DESATINYLITERAL,//8
    LEX_STRINGRETAZEC,	//9
    LEX_TRUE,		//10
    LEX_FALSE,		//11
    
    // Vlastne funkcie
    LEX_COPY,		//12
    LEX_LENGHT,		//13
    LEX_FIND,		//14
    LEX_SORT,		//15
    LEX_READLN,		//16
    LEX_WRITE,		//17
    
    // Syntaktika
    LEX_LAVAZATVORKA,	//18
    LEX_PRAVAZATVORKA,	//19
    LEX_DVOJBODKA,	//20
    LEX_BODKA,		//21
    LEX_BODKOCIARKA,	//22
    LEX_CIARKA,		//23
    
    // Aritmetika
    LEX_ROVNATKO,	//24
    LEX_MENEJ,		//25
    LEX_VIAC,		//26
    LEX_MENEJROVNATKO,	//27
    LEX_VIACROVNATKO,	//28
    LEX_SCITANIE,	//29
    LEX_ODCITANIE,	//30
    LEX_KRAT,		//31
    LEX_DELENO,		//32
    LEX_MENEJVIAC,	//33
    LEX_PRIROVNANIE,	//34
    LEX_EOL,		//35
    	
    // Klucove slova syntaktiky
    LEX_REPEAT,		//36
    LEX_UNTIL,		//37
    LEX_FUNCTION,	//38
    LEX_WHILE,		//39
    LEX_FORWARD,	//40
    LEX_VAR,		//41
    LEX_BEGIN,		//42
    LEX_DO,		//43
    LEX_IF,		//44
    LEX_ELSE,		//45
    LEX_THEN,		//46
    LEX_END,		//47
    LEX_ENDBODKA,	//48
} LEXType;	
