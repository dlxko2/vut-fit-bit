/***********************************************************
* Subor: instructuionlist.h                                *
* Nazov projektu: Hlavickovy subor k instructionlist.c     *
* Varianta: Tým 003, varianta a/2/I                        *
* Vypracovali:                                             *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz            *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz               *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz              *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz              *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz             *
************************************************************/
#ifndef IN_H
#define IN_H

// Vycet typov instrukcii
typedef enum{
    I_START,      // [NULL, NULL, NULL] start programu
	I_STOP,		  // [NULL, NULL, NULL] skoncil program end.
	I_READ,		  // [ADR1=ID_NAME, NULL, NULL] readln(id)
	I_WRITE,	  // [ADR1=STRING, NULL, NULL] write('')
    I_WRITEID,    // [ADR1=ID_NAME, NULL, NULL] write(id)
	I_ASSIGN,	  // [ADR1=ID_NAME, NULL, NULL] a := b
	I_SUM,		  // [NULL, NULL, NULL] a = b + c
	I_SUB,		  // [NULL, NULL, NULL] a = b - c
	I_MUL,		  // [NULL, NULL, NULL] a = b * c
	I_DIV,		  // [NULL, NULL, NULL] a = b / c
	I_RETURN,	  // ! NOT IMPLEMENTED return
	I_CALL,		  // ! NOT IMPLEMENTED vola funkciu
	I_LENGTH,	  // [NULL, NULL, NULL] length(s: string):integer
	I_COPY,		  // [I, N, NULL] copy(s: string; i:integer; n: integer):string
	I_FIND,	   	  // [NULL, NULL, NULL] find(s: string; search: string):integer
    I_PUSH,       // [ADR1=VALUE, NULL, NULL] pushne hodnotu na zasobnik interpreta
    I_POP,        // ! NOT_IMPLEMENTED vyberie zo zasobnika interpreta
    I_COMP,       // [ADR1=COMPARETYPE, NULL, NULL] porovna hodnoty zo zasobnika
    I_PUSHID,     // [ADR1=ID_NAME, NULL, NULL] pushne z tabulky symbolov
    I_SORT,       // [NULL, NULL, NULL] sort(s : string) : string
    I_SORTPUSH,   // [NULL, NULL, NULL] sort pri assign
    I_FINDPUSH,   // [NULL, NULL, NULL] find pri assign
    I_COPYPUSH,   // [I, N, NULL] copy pri assign
    I_LENGHTPUSH, // [NULL, NULL, NULL] lenght pri assign
    I_WJMP,       // [LABTABLE_ID, NULL, NULL] novy skok 
    I_WJMPFALSE,  // [LABTABLE_ID, NULL, NULL] skok pri false
    I_LAB,        // [NULL, NULL, NULL] vytvori prazdnu instrukciu ako navestie
    I_JMPFUNC,
    I_JMPCONT,
}InsT;

// Vycet moznych porovnani
typedef enum{
	CMP_LESS = 1,
	CMP_GREATER = 2,
	CMP_LESSEQ = 3,
	CMP_GREATEREQ = 4,
    CMP_EQUAL = 5,
    CMP_NOTEQUAL = 6,
} CompType;

// Struktura adresy
typedef struct
{
  Cont AdressValue; // Hodnota adresy
  dataT Type; // Typ datoveho typu
} AdressStruc;

// Struktura instrukcie
typedef struct
{
  InsT instType; // Typ instrukcie
  AdressStruc addr1; // Adresa 1
  AdressStruc addr2; // Adresa 2
  AdressStruc addr3; // Adresa 3
} tInstr;

// Prvok zoznamu instrukcii
typedef struct listItem
{
  tInstr Instruction; // Instrukcia
  struct listItem *nextItem; // Dalsi prvok
} tListItem;

// Zoznam instrukcii
typedef struct
{
  struct listItem *first;  // Ukazatel na prvy prvok
  struct listItem *last;   // Ukazatel na posledny prvok
  struct listItem *active; // Ukazatel na aktivy prvok
} tListOfInstr;

// Funkcie zo zdrojaku
bool generateInstruction(tListOfInstr *list, InsT iType, AdressStruc *Key1, AdressStruc *Key2, AdressStruc *Key3);
bool listInsertLast(tListOfInstr *L, tInstr I);
void listGoto(tListOfInstr *L, void *gotoInstr);
tListItem *listGetPointerLast(tListOfInstr *L);
tInstr *listGetData(tListOfInstr *L);
void listInit(tListOfInstr *L);
void listFree(tListOfInstr *L);
void listFirst(tListOfInstr *L);
void listNext(tListOfInstr *L);
void generateVariable(string *var);
void printIList(tListOfInstr *);
tListItem *findEnd(tListOfInstr *list);
tListItem *findSTART(tListOfInstr *list);
#endif
