/********************************************************
* Subor: modulestring.h                                 *
* Nazov projektu: Hlavickovy subor pre modulestring.c   *
* Varianta: Tým 003, varianta a/2/I                     *
* Vypracovali:                                          *
* 1. Martin Pavelka, xpavel27@stud.fit.vutbr.cz         *
* 2. Ján Stehlík, xstehl15@stud.fit.vutbr.cz            *
* 3. Šimon Lipták, xlipta02@stud.fit.vutbr.cz           *
* 4. Matej Lupták, xlupta02@stud.fit.vutbr.cz           *
* 5. Vojtech Pícha, xpicha06@stud.fit.vutbr.cz          *
*********************************************************/
#ifndef MOD_H
#define MOD_H

// Struktura stringu
typedef struct
{
    char* str; // misto pro dany retezec ukonceny znakem '\0'
  int length; // skutecna delka retezce
  int allocSize; // velikost alokovane pameti
} string;

// Funkcie v zdrojaku
int strInit(string *s);
void strFree(string *s);
void strClear(string *s);
int strAddChar(string *s1, char c);
int strCopyString(string *s1, string *s2);
int strCmpString(string *s1, string *s2);
int strCmpConstStr(string *s1, char *s2);
char *strGetStr(string *s);
int strGetLength(string *s);
int strConcaterateString(string *s1, string *s2);
#endif
