
/* c016.c: **********************************************************}
{* T�ma:  Tabulka s Rozpt�len�mi Polo�kami
**                      Prvn� implementace: Petr P�ikryl, prosinec 1994
**                      Do jazyka C prepsal a upravil: Vaclav Topinka, 2005
**                      �pravy: Karel Masa��k, ��jen 2014
**                      �pravy: Radek Hranick�, ��jen 2014
**
** Vytvo�ete abstraktn� datov� typ
** TRP (Tabulka s Rozpt�len�mi Polo�kami = Hash table)
** s explicitn� �et�zen�mi synonymy. Tabulka je implementov�na polem
** line�rn�ch seznam� synonym.
**
** Implementujte n�sleduj�c� procedury a funkce.
**
**  HTInit ....... inicializuje tabulku p�ed prvn�m pou�it�m
**  HTInsert ..... vlo�en� prvku
**  HTSearch ..... zji�t�n� p��tomnosti prvku v tabulce
**  HTDelete ..... zru�en� prvku
**  HTRead ....... p�e�ten� hodnoty prvku
**  HTClearAll ... zru�en� obsahu cel� tabulky (inicializace tabulky
**                 pot�, co ji� byla pou�ita)
**
** Definici typ� naleznete v souboru c016.h.
**
** Tabulka je reprezentov�na datovou strukturou typu tHTable,
** kter� se skl�d� z ukazatel� na polo�ky, je� obsahuj� slo�ky
** kl��e 'key', obsahu 'data' (pro jednoduchost typu float), a
** ukazatele na dal�� synonymum 'ptrnext'. P�i implementaci funkc�
** uva�ujte maxim�ln� rozm�r pole HTSIZE.
**
** U v�ech procedur vyu��vejte rozptylovou funkci hashCode.  Pov�imn�te si
** zp�sobu p�ed�v�n� parametr� a zamyslete se nad t�m, zda je mo�n� parametry
** p�ed�vat jin�m zp�sobem (hodnotou/odkazem) a v p��pad�, �e jsou ob�
** mo�nosti funk�n� p��pustn�, jak� jsou v�hody �i nev�hody toho �i onoho
** zp�sobu.
**
** V p��kladech jsou pou�ity polo�ky, kde kl��em je �et�zec, ke kter�mu
** je p�id�n obsah - re�ln� ��slo.
*/

#include "c016.h"

int HTSIZE = MAX_HTSIZE;
int solved;

/* Rozptylovacia funkcia */
int hashCode ( tKey key ) {
	int retval = 1;
	int keylen = strlen(key);
	for ( int i=0; i<keylen; i++ )
		retval += key[i];
	return ( retval % HTSIZE );
}

/* Inicializacia tabulky */
void htInit ( tHTable* ptrht )
{
    // Overime pointer
 	if(!ptrht) return;
    
    // Vynulujeme pamet na zaciatok
	memset(*ptrht,0,sizeof(struct tHTItem*)*HTSIZE);

}

/* Vyhladanie v tabulke */
tHTItem* htSearch ( tHTable* ptrht, tKey key )
{
    // Ak jeden z pointerov je NULL
 	if (!(*ptrht)) return NULL;
    if (!ptrht) return NULL;
    
    // Naplnime prvy prvok zaciatkom synonym
	tHTItem* Tmp = (*ptrht)[hashCode(key)];
	
    // Pokial neprideme na koniec
    while(Tmp != NULL)
	{
        // Porovnavaj kluc, pripadne vrat vysledok
        if(strcmp(Tmp->key, key) == 0) return Tmp;
        
        // Posun sa na dalsie synonymum
        Tmp = Tmp->ptrnext;
	}
	return NULL;
}

/* Vlozenie prvku do tabulky */
void htInsert ( tHTable* ptrht, tKey key, tData data )
{
    // Overime ci pointer nie je NULL
 	if(!ptrht) return;
    
    // Definujeme pomocnu premennu a overime ci uz existuje nejaky s rov. klucom
	tHTItem* Tmp = htSearch(ptrht,key);
    
    // Ak nebol este v tabulke
    if(Tmp == NULL)
    {
        // Prvok na zaciatok zoznamu
	  	Tmp = (*ptrht)[hashCode(key)];
        
        // Alokovanie noveho prvku
	  	tHTItem* Item = malloc(sizeof(struct tHTItem));
        if (!Item) exit (-1);
       
        // Alokujeme miesto v pameti pre jeho kluc
        Item->key = malloc(strlen(key)+1);
        if (!Item->key) exit (-1);
        
        // Naplnime novy prvok
        memcpy(Item->key,key,strlen(key)+1);
        Item->data = data;
        Item->ptrnext = Tmp;
                
        // Ukazeme na novu polozku
        (*ptrht)[hashCode(key)] = Item;
    }
    
    // Ak ano tak data aktualizujeme
    else Tmp->data = data;
    return;
}

/* Precitanie datovej casti polozky */
tData* htRead ( tHTable* ptrht, tKey key )
{
    // Overime pointer ci nie je NULL
	if(!ptrht) return NULL;
    
    //  Definujeme pomocnu premennu a vyhladame polozku
	tHTItem* Temp = htSearch(ptrht,key);
    
    // Ak je najdena vratime jej data
	if(Temp) return &(Temp->data);
    
    // Ak nenajde vrati NULL
	else return NULL;
}

/* Zmazanie prvku v tabulke */
void htDelete ( tHTable* ptrht, tKey key )
{
    // Over pointre ci nie su NULL
 	if(!ptrht) return;
    if (!(*ptrht)) return;
	
    // Definujeme pomocnu premennu na zaciatku synonym a overime
	tHTItem* Temp = (*ptrht)[hashCode(key)];
	if(!Temp) return;
    
    // Definujeme pomocnu premennu na predchadzajuci prvok a overime
    tHTItem* PreTemp = (*ptrht)[hashCode(key)];
    if(!PreTemp) return;
    
    // Ak nenajde prvok hned nacitame dalsi ak ano zmazeme
	if(strcmp(Temp->key, key) != 0)
        Temp = Temp->ptrnext;
    else
    {
        // Ukazeme na dalsi
	  	(*ptrht)[hashCode(key)] = Temp->ptrnext;
        
        // Uvolnime kluc a prvok
	  	free(Temp->key);
	  	free(Temp);
        
        // Nanulujeme
	  	Temp = NULL;
	}
	
    // Kym nebude koniec
	while(Temp != NULL)
	{
        // Ak najdeme prvok s rovnakym klucom
	  	if(strcmp(Temp->key, key) == 0)
	  	{
            // Ukazeme na dalsi
            PreTemp->ptrnext = Temp->ptrnext;
            
            // Dealokujeme
            free(Temp->key);
            free(Temp);
            return;
	  	}
        
        // Premostime
	  	PreTemp = Temp;
        
        // Ukazeme na dalsi
	  	Temp = Temp->ptrnext;
	}
	return;
}

/* Zrusenie vsetkych poloziek tabulky */
void htClearAll ( tHTable* ptrht )
{
    // Pre kazdu polozku tabulky
 	for(int i = 0; i < HTSIZE; i++)
	{
        // Pre kazdu polozku 2D az kym nepride na prazdny prvok
	  	for(tHTItem* Item = (*ptrht)[i]; Item != NULL; )
		{
            // Ulozime item do docasnej premennej
            tHTItem* Temp = Item;
            
            // Ukazeme na dalsi prvok
            Item = Item->ptrnext;
            
            // Zmazeme docasny
            free(Temp->key);
            free(Temp);
	  	}
	}
    
    // Vynulujeme pamet
	memset(*ptrht,0,sizeof(struct tHTItem*)*HTSIZE);
}
