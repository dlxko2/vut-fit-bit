
/* c402.c: ********************************************************************}
{* Téma: Nerekurzivní implementace operací nad BVS 
**                                     Implementace: Petr Přikryl, prosinec 1994
**                                           Úpravy: Petr Přikryl, listopad 1997
**                                                     Petr Přikryl, květen 1998
**			  	                        Převod do jazyka C: Martin Tuček, srpen 2005
**                                         Úpravy: Bohuslav Křena, listopad 2009
**                                         Úpravy: Karel Masařík, říjen 2013
**                                         Úpravy: Radek Hranický, říjen 2014
**
** S využitím dynamického přidělování paměti, implementujte NEREKURZIVNĚ
** následující operace nad binárním vyhledávacím stromem (předpona BT znamená
** Binary Tree a je u identifikátorů uvedena kvůli možné kolizi s ostatními
** příklady):
**
**     BTInit .......... inicializace stromu
**     BTInsert ........ nerekurzivní vložení nového uzlu do stromu
**     BTPreorder ...... nerekurzivní průchod typu pre-order
**     BTInorder ....... nerekurzivní průchod typu in-order
**     BTPostorder ..... nerekurzivní průchod typu post-order
**     BTDisposeTree ... zruš všechny uzly stromu
**
** U všech funkcí, které využívají některý z průchodů stromem, implementujte
** pomocnou funkci pro nalezení nejlevějšího uzlu v podstromu.
**
** Přesné definice typů naleznete v souboru c402.h. Uzel stromu je typu tBTNode,
** ukazatel na něj je typu tBTNodePtr. Jeden uzel obsahuje položku int Cont,
** která současně slouží jako užitečný obsah i jako vyhledávací klíč 
** a ukazatele na levý a pravý podstrom (LPtr a RPtr).
**
** Příklad slouží zejména k procvičení nerekurzivních zápisů algoritmů
** nad stromy. Než začnete tento příklad řešit, prostudujte si důkladně
** principy převodu rekurzivních algoritmů na nerekurzivní. Programování
** je především inženýrská disciplína, kde opětné objevování Ameriky nemá
** místo. Pokud se Vám zdá, že by něco šlo zapsat optimálněji, promyslete
** si všechny detaily Vašeho řešení. Povšimněte si typického umístění akcí
** pro různé typy průchodů. Zamyslete se nad modifikací řešených algoritmů
** například pro výpočet počtu uzlů stromu, počtu listů stromu, výšky stromu
** nebo pro vytvoření zrcadlového obrazu stromu (pouze popřehazování ukazatelů
** bez vytváření nových uzlů a rušení starých).
**
** Při průchodech stromem použijte ke zpracování uzlu funkci BTWorkOut().
** Pro zjednodušení práce máte předem připraveny zásobníky pro hodnoty typu
** bool a tBTNodePtr. Pomocnou funkci BTWorkOut ani funkce pro práci
** s pomocnými zásobníky neupravujte 
** Pozor! Je třeba správně rozlišovat, kdy použít dereferenční operátor *
** (typicky při modifikaci) a kdy budeme pracovat pouze se samotným ukazatelem 
** (např. při vyhledávání). V tomto příkladu vám napoví prototypy funkcí.
** Pokud pracujeme s ukazatelem na ukazatel, použijeme dereferenci.
**/

#include "c402.h"
int solved;

void BTWorkOut (tBTNodePtr Ptr)		{
/*   ---------
** Pomocná funkce, kterou budete volat při průchodech stromem pro zpracování
** uzlu určeného ukazatelem Ptr. Tuto funkci neupravujte.
**/
			
	if (Ptr==NULL) 
    printf("Chyba: Funkce BTWorkOut byla volána s NULL argumentem!\n");
  else 
    printf("Výpis hodnoty daného uzlu> %d\n",Ptr->Cont);
}
	
/* -------------------------------------------------------------------------- */
/*
** Funkce pro zásobník hotnot typu tBTNodePtr. Tyto funkce neupravujte.
**/

void SInitP (tStackP *S)  
/*   ------
** Inicializace zásobníku.
**/
{
	S->top = 0;  
}	

void SPushP (tStackP *S, tBTNodePtr ptr)
/*   ------
** Vloží hodnotu na vrchol zásobníku.
**/
{ 
                 /* Při implementaci v poli může dojít k přetečení zásobníku. */
  if (S->top==MAXSTACK) 
    printf("Chyba: Došlo k přetečení zásobníku s ukazateli!\n");
  else {  
		S->top++;  
		S->a[S->top]=ptr;
	}
}	

tBTNodePtr STopPopP (tStackP *S)
/*         --------
** Odstraní prvek z vrcholu zásobníku a současně vrátí jeho hodnotu.
**/
{
                            /* Operace nad prázdným zásobníkem způsobí chybu. */
	if (S->top==0)  {
		printf("Chyba: Došlo k podtečení zásobníku s ukazateli!\n");
		return(NULL);	
	}	
	else {
		return (S->a[S->top--]);
	}	
}

bool SEmptyP (tStackP *S)
/*   -------
** Je-li zásobník prázdný, vrátí hodnotu true.
**/
{
  return(S->top==0);
}	

/* -------------------------------------------------------------------------- */
/*
** Funkce pro zásobník hotnot typu bool. Tyto funkce neupravujte.
*/

void SInitB (tStackB *S) {
/*   ------
** Inicializace zásobníku.
**/

	S->top = 0;  
}	

void SPushB (tStackB *S,bool val) {
/*   ------
** Vloží hodnotu na vrchol zásobníku.
**/
                 /* Při implementaci v poli může dojít k přetečení zásobníku. */
	if (S->top==MAXSTACK) 
		printf("Chyba: Došlo k přetečení zásobníku pro boolean!\n");
	else {
		S->top++;  
		S->a[S->top]=val;
	}	
}

bool STopPopB (tStackB *S) {
/*   --------
** Odstraní prvek z vrcholu zásobníku a současně vrátí jeho hodnotu.
**/
                            /* Operace nad prázdným zásobníkem způsobí chybu. */
	if (S->top==0) {
		printf("Chyba: Došlo k podtečení zásobníku pro boolean!\n");
		return(NULL);	
	}	
	else {  
		return(S->a[S->top--]); 
	}	
}

bool SEmptyB (tStackB *S) {
/*   -------
** Je-li zásobník prázdný, vrátí hodnotu true.
**/
  return(S->top==0);
}

/* -------------------------------------------------------------------------- */
/*
** Následuje jádro domácí úlohy - funkce, které máte implementovat. 
*/

/* Funkcia na inicializaciu stromu */
void BTInit (tBTNodePtr *RootPtr)
{
	// Nadstavime koren na NULL
	 *RootPtr = NULL;	
}

/* Funkcia vkladania noveho prvku do stromu */
void BTInsert (tBTNodePtr *RootPtr, int Content)
{
	// Pomocne premenne
    tBTNodePtr PreTemp;
	tBTNodePtr Temp;
    int FoundHandler = FALSE;
    
    // Nacitame si koren stromu
	Temp  = (*RootPtr);
	
    // Ak existuje koren
	if(Temp != NULL)
	{
        // Cykly kym nenajde alebo temp nebude NULL
	  	while((FoundHandler == FALSE) && (Temp != NULL))
		{
            // Ulozime aktualny Temp do druhej pomocnej premennej
            PreTemp = Temp;
            
            // Ak hodnota je mensia potom pokracuj vlavo
            if(Temp->Cont > Content)  Temp = Temp->LPtr;

            // Ak hodnota je vecsia potom pokracuj vpravo
			else if(Temp->Cont < Content) Temp = Temp->RPtr;
            
            // Ak hodnota sa rovna tak prvok uz existuje
            else if(Temp->Cont == Content) FoundHandler = TRUE;
	  	}
	}
    
    // Ak neexistuje nadstavime prepinace
	else
	{
        PreTemp = NULL;
	  	FoundHandler = FALSE;
	}
    
    // Ak nebol najdeny potom vytvorime
	if(FoundHandler != TRUE)
	{
        // Novy prvok alokujeme a overime ci s uspechom
	  	tBTNodePtr item = malloc(sizeof(struct tBTNode));
	  	if(item == NULL) exit(-1);
        
        // Naplnime ho hodnotami
        item->LPtr = NULL;
	  	item->RPtr = NULL;
	  	item->Cont = Content;
        
        // Ak sa ma stat novy prvok korenom
	  	if(PreTemp == NULL) (*RootPtr) = item;
        
        // Ak je hodnota vecsia a koren je tak vytvoryme pravy podstrom
	  	else if(PreTemp->Cont < Content) PreTemp->RPtr = item;
        
        // Ak je hodnota mensia a koren je tak vytvorime lavy podstrom
	 	else PreTemp->LPtr = item;
	}
}

/* Funkcia iduca po najlavejsich uzloch */
void Leftmost_Preorder (tBTNodePtr ptr, tStackP *Stack)
{
	// Kym uzol existuje
	while(ptr)
	{
        // Uloz ukazatel na vrchol zasobnika
	  	SPushP(Stack,ptr);
        
        // Vypis uzol
	  	BTWorkOut(ptr);
        
        // Pokracuj na dalsi lavy poduzol
	  	ptr = ptr->LPtr;
	}
}

/* Funkcia preorder binarneho stromu */
void BTPreorder (tBTNodePtr RootPtr)
{
    // Definujeme pomocnu premennu
	tBTNodePtr Temp;

	// Over ci existuje koren
	if(RootPtr == NULL) return;
    
    // Vytvorime a inicializujeme pomocny zasobnik
	tStackP TempStack;
	SInitP(&TempStack);
    
    // Spustime leftmost preoder a ulozime vysledok do zasobnika
	Leftmost_Preorder(RootPtr, &TempStack);
        
    // Kym nie je prazdny zasobnik
	while(!SEmptyP(&TempStack))
	{
        // Ulozime zo zasobniku
	  	Temp = STopPopP(&TempStack);
        
        // Najdeme opet najlavejsiu cast
	  	Leftmost_Preorder(Temp->RPtr, &TempStack);
	}
}

/* Funkcia leftmost pre inorder */
void Leftmost_Inorder(tBTNodePtr ptr, tStackP *Stack)
{
	// Kym existuje uzol
	while(ptr)
	{
        // Ulozime na vrchol zasobnika
	  	SPushP(Stack,ptr);
        
        // Ukazeme na dalsi lavy uzol
	  	ptr = ptr->LPtr;
	}	
}

/* Funkcia inorder */
void BTInorder (tBTNodePtr RootPtr)
{
    // Definujeme pomocnu premennu
	tBTNodePtr Temp;

	// Overime ci existuje
	if(RootPtr == NULL) return;
    
    // Pomocny zasobnik
	tStackP TempStack;
	SInitP(&TempStack);
    
    // Najdeme najlavejsi uzol a prechod ulozime na zasobnik
	Leftmost_Inorder (RootPtr, &TempStack);
        
    // Kym nie je prazdny zasobnik
	while(!SEmptyP(&TempStack))
	{
        // Ulozime prvok zo zasobniku
	  	Temp = STopPopP(&TempStack);
        
        // Vypiseme ho
	  	BTWorkOut(Temp);
        
        // Ulozime prechod k najlavejsiemu
	  	Leftmost_Inorder (Temp->RPtr, &TempStack);
	}
}

/* Funkcia leftmost pre postorder */
void Leftmost_Postorder (tBTNodePtr ptr, tStackP *StackP, tStackB *StackB)
{
	// Kym existuje uzol
	while(ptr)
	{
        // Do zasobniku ulozime true
		SPushB(StackB,TRUE);
        
        // Do dalsieho zasobniku ulozime ukazatel
	  	SPushP(StackP,ptr);
        
        // Pokracujeme na dalsi lavy
	  	ptr = ptr->LPtr;
	}
}

/* Funkcia postorder */
void BTPostorder (tBTNodePtr RootPtr)
{	
    // Overime koren
	if(RootPtr == NULL) return;
	
    // Ulozisko boolovej hodnoty uzla
    int TempBoolHandler;

    // Pomocna premenna pre uzle
	tBTNodePtr Temp  = RootPtr;

    // Definujeme pomocne zasobniky
    tStackP FirstStack;
	tStackB SecondStack;
    
    // Inicializujeme pomocne zasobniky
	SInitP(&FirstStack);
	SInitB(&SecondStack);
        
    // Vykoname leftmost a vysledky ulozime do zasobnikov
	Leftmost_Postorder (Temp, &FirstStack, &SecondStack);
    
    // Kym nie je prazdny hlavny zasobnik
	while(!SEmptyP(&FirstStack))
	{
        // Vyberieme prvok zo zasobniku
	  	Temp = STopPopP(&FirstStack);
        
        // Do zasobniku ulozime nacitany prvok
	  	SPushP(&FirstStack,Temp);
        
        // Nacitame hodnotu boolu
	  	TempBoolHandler = STopPopB(&SecondStack);
        
        // Ak handler je FALSE mozeme nacitat
	  	if(TempBoolHandler == FALSE)
	  	{
            // Vyberieme zo zasobnika
            STopPopP(&FirstStack);
            
            // Vypiseme
            BTWorkOut(Temp);
	  	}
        // Ak handler je true
	  	else
	  	{ 
            // Ulozime zmenenu hodnotu na FALSE
            SPushB(&SecondStack,FALSE);
            
            // Najdeme najlavejsi prvok a ulozime hodnoty na zasobniky
            Leftmost_Postorder(Temp->RPtr, &FirstStack, &SecondStack);
	  	}
	}
}

/* Funkcia na zrusenie stromu */
void BTDisposeTree (tBTNodePtr *RootPtr)
{
    // Definujeme pomocny zasobnik a inicializujeme ho
	tStackP TempStack;
	SInitP(&TempStack);
    
    // Vlozime do zasobnika koren
	SPushP(&TempStack,*RootPtr);
    
    // Kym nie je prazdny zasobnik
	while(!SEmptyP(&TempStack))
	{
        // Ulozime prvok z vrchu zasobnika a overime ho
        tBTNodePtr Temp = STopPopP(&TempStack);
        if(Temp == FALSE) continue;
        
        // Ulozime na zasobnik jeho podstromy
        SPushP(&TempStack,Temp->RPtr);
        SPushP(&TempStack,Temp->LPtr);
        
        // Uvolnime aktualny uzol
        free(Temp);
	}
    
    // Pointer na koren bude NULL
	*RootPtr=NULL;
}
