
/* c401.c: **********************************************************}
{* T�ma: Rekurzivn� implementace operac� nad BVS
**                                         Vytvo�il: Petr P�ikryl, listopad 1994
**                                         �pravy: Andrea N�mcov�, prosinec 1995
**                                                      Petr P�ikryl, duben 1996
**                                                   Petr P�ikryl, listopad 1997
**                                  P�evod do jazyka C: Martin Tu�ek, ��jen 2005
**                                         �pravy: Bohuslav K�ena, listopad 2009
**                                         �pravy: Karel Masa��k, ��jen 2013
**                                         �pravy: Radek Hranick�, ��jen 2014
**
** Implementujte rekurzivn�m zp�sobem operace nad bin�rn�m vyhled�vac�m
** stromem (BVS; v angli�tin� BST - Binary Search Tree).
**
** Kl��em uzlu stromu je jeden znak (obecn� j�m m��e b�t cokoliv, podle
** �eho se vyhled�v�). U�ite�n�m (vyhled�van�m) obsahem je zde integer.
** Uzly s men��m kl��em le�� vlevo, uzly s v�t��m kl��em le�� ve stromu
** vpravo. Vyu�ijte dynamick�ho p�id�lov�n� pam�ti.
** Rekurzivn�m zp�sobem implementujte n�sleduj�c� funkce:
**
**   BSTInit ...... inicializace vyhled�vac�ho stromu
**   BSTSearch .... vyhled�v�n� hodnoty uzlu zadan�ho kl��em
**   BSTInsert .... vkl�d�n� nov� hodnoty
**   BSTDelete .... zru�en� uzlu se zadan�m kl��em
**   BSTDispose ... zru�en� cel�ho stromu
**
** ADT BVS je reprezentov�n ko�enov�m ukazatelem stromu (typ tBSTNodePtr).
** Uzel stromu (struktura typu tBSTNode) obsahuje kl�� (typu char), podle
** kter�ho se ve stromu vyhled�v�, vlastn� obsah uzlu (pro jednoduchost
** typu int) a ukazatel na lev� a prav� podstrom (LPtr a RPtr). P�esnou definici typ� 
** naleznete v souboru c401.h.
**
** Pozor! Je t�eba spr�vn� rozli�ovat, kdy pou��t dereferen�n� oper�tor *
** (typicky p�i modifikaci) a kdy budeme pracovat pouze se samotn�m ukazatelem 
** (nap�. p�i vyhled�v�n�). V tomto p��kladu v�m napov� prototypy funkc�.
** Pokud pracujeme s ukazatelem na ukazatel, pou�ijeme dereferenci.
**/

#include "c401.h"
int solved;

/* Inicializacia stromu */
void BSTInit (tBSTNodePtr *RootPtr)
{
    // Koren nastavime na NULL
	*RootPtr = NULL;
}	

/* Vyhladavanie v binarnom strome */
int BSTSearch (tBSTNodePtr RootPtr, char K, int *Content)
{
    // Overenie pointeru korena
    if(RootPtr == NULL) return FALSE;
    
    // Ak sa kluce rovnaju
	if(RootPtr->Key == K)
	{
        // Ulozenie obsahu ako navratovu hodnotu
	    *Content = RootPtr->BSTNodeCont;
	    return TRUE;
	}
    // Ak je kluc hladany mensi hladame vlavo
	else if(K < RootPtr->Key) return BSTSearch(RootPtr->LPtr, K, Content);
	
    // Ak je kluc vecsi tak hladame vpravo
    else return BSTSearch(RootPtr->RPtr, K, Content);    
} 

/* Vlozenie prvku do stromu */
void BSTInsert (tBSTNodePtr* RootPtr, char K, int Content)
{
    // Overime korenovy pointer
	if((*RootPtr) == NULL)
	{
        // Ak nebol koren tak ho alokujeme
	  	*RootPtr = malloc(sizeof(struct tBSTNode));
        
        // Ak sa nepodarila alokacia ukonci program
	  	if((*RootPtr) == NULL) exit(-1);
        
        // Pridelime novemu prvku hodnoty
	  	(*RootPtr)->BSTNodeCont = Content;
	  	(*RootPtr)->LPtr = NULL;
	  	(*RootPtr)->RPtr = NULL;
	  	(*RootPtr)->Key = K;
	}
    // Ak koren bol inicializovany a kluc je mensi vlozime vlavo
	else if(K < (*RootPtr)->Key) BSTInsert (&(*RootPtr)->LPtr, K, Content);
    
    // Ak je kluc vecsi tak vkladame vpravo
	else if(K > (*RootPtr)->Key) BSTInsert (&(*RootPtr)->RPtr, K, Content);
	
    // Inak aktualizujeme
    else (*RootPtr)->BSTNodeCont = Content;
}

/* Funkcia na presunutie hodnoty najpravejsieho uzla */
void ReplaceByRightmost (tBSTNodePtr PtrReplaced, tBSTNodePtr *RootPtr)
{
    // Overime pointre
	if (RootPtr == NULL) return;
    if ((*RootPtr) == NULL) return;
	
    // Ak uz je najpravejsi
	if((*RootPtr)->RPtr == NULL)
    {
        // Presuneme kluc a obsah
        PtrReplaced->BSTNodeCont = (*RootPtr)->BSTNodeCont;
	  	PtrReplaced->Key = (*RootPtr)->Key;
        
        // Docasne ulozime koren
	  	tBSTNodePtr Temp  = (*RootPtr);
        
        // Ukazeme na dalsi
	  	(*RootPtr) = (*RootPtr)->LPtr;
        
        // Dealokujeme
	  	free(Temp);
    }
    // Ak je definovany ukazatel vpravo zavolame rekurzivne znovu
	else ReplaceByRightmost(PtrReplaced, &(*RootPtr)->RPtr);
}

/* Funkcia na zmazanie prvku */
void BSTDelete (tBSTNodePtr *RootPtr, char K)
{
    // Overime pointre
    if (RootPtr == NULL) return;
    if ((*RootPtr) == NULL) return;
    
    // Ak kluc je mensi mazeme vlavo
    if((*RootPtr)->Key > K) BSTDelete((&(*RootPtr)->LPtr), K);
	
    // Ak kluc je vecsi mazeme vpravo
    else if((*RootPtr)->Key < K) BSTDelete((&(*RootPtr)->RPtr), K);
	
    // Ak ma oba podstromy nahradime najpravejsim
    else if((*RootPtr)->LPtr && (*RootPtr)->RPtr)
        ReplaceByRightmost ((*RootPtr), (&(*RootPtr)->LPtr));
    
    // Ak ma jeden podstrom
	else
	{
        // Docasne ulozime koren
        tBSTNodePtr Temp = (*RootPtr);
        
        // Ak je pravy uzol tak dedime zprava
		if((*RootPtr)->RPtr) *RootPtr = (*RootPtr)->RPtr;
        
        // Ak je lavy uzol tak dedime zlava
        else *RootPtr = (*RootPtr)->LPtr;
        
        // Uvolnime pozadovany prvok
        free(Temp);
	}
} 

/* Funkcia na zmazanie birnaneho stromu */
void BSTDispose (tBSTNodePtr *RootPtr)
{
    // Overime pointer korena
	if((*RootPtr) == NULL) return;
    
    // Zmazeme pravy podstrom
	BSTDispose(&(*RootPtr)->RPtr);

    // Zmazeme lavy podstrom
	BSTDispose(&(*RootPtr)->LPtr);
    
    // Uvolnime koren
	free(*RootPtr);
    
    // Nadstavime ho na NULL
	*RootPtr = NULL;
}
