#include "c201.h"

int solved;
int errflg;

void Error()
{
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;
}

void InitList (tList *L)
{
    // Vytvori nove nulove pointery
    L->Act = NULL;
    L->First = NULL;
}

void DisposeList (tList *L)
{
    // Lokalne pomocne premenne
    tElemPtr MazanyElement;
    tElemPtr LoopElement = L->First;
    
    // Pozrie ci nie je prazdny zoznam
    if (L->First != NULL)
    {
        // Kym nepride na prazdny pointer
        while (LoopElement != NULL)
        {
            // Mazany element je aktualny
            // Posun aktualny na dalsi
            // Zmaz prvok
            MazanyElement = LoopElement;
            LoopElement = LoopElement->ptr;
            free(MazanyElement);
        }
        
        // Nadstavime pointery na NULL
        L->First = NULL;
        L->Act = NULL;
    }
}

void InsertFirst (tList *L, int val)
{
    // Definujeme novy prvok
    tElemPtr NewElement;
    
    // Vytvori pamatovy priestor pre novy prvok v pamati inak error
    if ((NewElement = malloc(sizeof(struct tElem)))== NULL)
    {
        Error();
        return;
    }
    
    // Naplnime ho hodnotou
    NewElement->data = val;
    
    // Ukazeme na dalsi a oznacime ako prvy
    NewElement->ptr = L->First;
    L->First = NewElement;
}

void First (tList *L)
{
    // Nadstavime aktivny na prvy
    L->Act = L->First;
}

void CopyFirst (tList *L, int *val)
{
    // Ak je nejaky prvok v zozname vytiahni data inak error
    if (L->First != NULL)
        *val = L->First->data;
    else
        Error();
}

void DeleteFirst (tList *L)
{
    // Ak prvy nie je prazdny
    if (L->First != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tElemPtr NasledZaFirst;
        NasledZaFirst = L->First->ptr;
        
        // Ak aktivny bol prvy
        if (L->Act == L->First)
            L->Act = NULL;
        
        // Uvolnenie pamate
        free (L->First);
        
        // Premostenie
        L->First = NasledZaFirst;
    }
}
// Zrusi prvok za aktivnym prvkom
void PostDelete (tList *L)
{
    // Ak je nejaky aktivny a dalsi po nom
    if (L->Act != NULL && L->Act->ptr != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tElemPtr NasledZaRus;
        NasledZaRus = L->Act->ptr->ptr;
        
        // Uvolnenie pamate
        free(L->Act->ptr);
        
        // Premostenie
        L->Act->ptr = NasledZaRus;
    }
}
// Vlozi prvok za aktivny prvok
void PostInsert (tList *L, int val)
{
    // Ak je nejaky aktivny
    if (L->Act != NULL)
    {
        // Pomocne premenne a inicializacia
        tElemPtr NewElement;
        
        // Alokacia noveho prvku a inicializacia inak error
        if ((NewElement = malloc(sizeof(struct tElem)))== NULL)
        {
            Error();
            return;
        }
        
        // Napln novy prvok
        NewElement->ptr = L->Act->ptr;
        NewElement->data = val;
        
        // Premostenie
        L->Act->ptr = NewElement;
    }
}

void Copy (tList *L, int *val)
{
    // Ak je nejaky aktivny vrat jeho hodnotu inak error
	if (L->Act != NULL)
        *val = L->Act->data;
    else
        Error();
}

void Actualize (tList *L, int val)
{
    // Zmen hodnotu aktualneho prvku ak existuje
    if (L->Act != NULL)
        L->Act->data = val;
}

void Succ (tList *L)
{
    // Posun aktivitu na dalsi prvok
    if (L->Act != NULL)
        L->Act = L->Act->ptr;
}

int Active (tList *L)
{
    // Over ci je aktivny
    return (L->Act == NULL) ? 0 : 1;
}
