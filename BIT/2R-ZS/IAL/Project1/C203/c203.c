#include "c203.h"

void queueError (int error_code)
{
	static const char* QERR_STRINGS[MAX_QERR+1] = {"Unknown error","Queue error: UP","Queue error: FRONT","Queue error: REMOVE","Queue error: GET","Queue error: INIT"};
	if ( error_code <= 0 || error_code > MAX_QERR )
		error_code = 0;
	printf ( "%s\n", QERR_STRINGS[error_code] );
	err_flag = 1;
}

void queueInit (tQueue* q)
{
    // Skontroluj ci je alokovane pole
    if (q != NULL)
    {
        // Pre kazdy prvok vloz *
        for (int i = 0; i < QUEUE_SIZE; i++)
            q->arr[i] = '*';
    
        // Nadstav indexi na 0
        q->b_index = 0;
        q->f_index = 0;
    }
    else queueError(QERR_INIT);
}

int nextIndex (int index)
{
    // Vrati zvysok po deleni
    return (index+1) % (QUEUE_SIZE);
}

int queueEmpty (const tQueue* q)
{
    // Prazdny ak sa rovnaju oba indexi
    return (q->b_index == q->f_index) ? 1 : 0;
}

int queueFull (const tQueue* q)
{
    return (nextIndex(q->b_index) == q->f_index);
}

void queueFront (const tQueue* q, char* c)
{
    // Skontroluj ci je prazne
    if (!queueEmpty(q))
        // Vrat zaciatocny znak
        *c = q->arr[q->f_index];
    else
        queueError(QERR_FRONT);
}

void queueRemove (tQueue* q)
{
    // Skontroluj ci je prazne
    if (!queueEmpty(q))
    {
        // Index zaciatku posun o jedna
        q->f_index = nextIndex(q->f_index);
    }
    else queueError(QERR_REMOVE);
}

void queueGet (tQueue* q, char* c)
{
    // Skontroluj ci je prazdne
    if (!queueEmpty(q))
    {
        // Vyber prvy znak a zmaz ho
        queueFront(q, c);
        queueRemove(q);
    }
    else queueError(QERR_GET);
}

void queueUp (tQueue* q, char c)
{
    // Skontroluj ci je plne
    if(!queueFull(q))
    {
        // Vloz znak na posledne miesto
        q->arr[q->b_index] = c;
        
        // Osetrenie kruhovosti
        q->b_index = nextIndex(q->b_index);
    }
    else queueError(QERR_UP);
}

