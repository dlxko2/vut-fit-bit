#include "c206.h"

int solved;
int errflg;

void DLError()
{
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;             /* glob�ln� prom�nn� -- p��znak o�et�en� chyby */
    return;
}

void DLInitList (tDLList *L)
{
    // Nadstavime pointre na NULL
    L->Act = NULL;
    L->First = NULL;
    L->Last = NULL;
}

void DLDisposeList (tDLList *L)
{
    // Inicializacia premennych
    tDLElemPtr MazanyElement;
    tDLElemPtr LoopElement = L->First;
    
    // Pozrie ci nie je prazdny zoznam
    if (LoopElement != NULL)
    {
        // Kym mazany prvok nie je prazdny
        while (LoopElement != NULL)
        {
            // Mazany prvok napln aktualnym
            // Ukaz na dalsi prvok
            // Zmaz mazany element
            MazanyElement = LoopElement;
            LoopElement = LoopElement->rptr;
            free(MazanyElement);
        }
        
        // Nadstavime pointery na NULL
        L->First = NULL;
        L->Last = NULL;
        L->Act = NULL;
    }
}

void DLInsertFirst (tDLList *L, int val)
{
    // Definujeme novy prvok
    tDLElemPtr NewElement;
    
    // Vytvori pamatovy priestor pre novy prvok v pamati inak error
    if ((NewElement = malloc(sizeof(struct tDLElem))) == NULL)
    {
        DLError();
        return;
    }
    
    // Nadstavime hodnotu noveho elementu
    NewElement->data = val;
    
    // Dolava neukazuje, doprava na minuly
    NewElement->lptr = NULL;
    NewElement->rptr = L->First;
    
    /* Pridame vynimky
       Ak je prvy je zaroven tvorcom posledneho
       Ak nie je prvy pozera aj na predchadzajuci */
    if (L->First == NULL)
        L->Last = NewElement;
    else
        L->First->lptr = NewElement;
    
    // Priradime ako prvy novy prvok
    L->First = NewElement;
}

void DLInsertLast(tDLList *L, int val)
{
    // Definujeme novy prvok
    tDLElemPtr NewElement;
    
    // Vytvori pamatovy priestor pre novy prvok v pamati inak error
    if ((NewElement = malloc(sizeof(struct tDLElem))) == NULL)
    {
        DLError();
        return;
    }
  
    // Nadstavime hodnotu elementu
    NewElement->data = val;
    
    // Doprava neukazuje, dolava na minuly
    NewElement->rptr = NULL;
    NewElement->lptr = L->Last;
    
    /* Pridame vynimky
     Ak je prvym je zaroven tvorcom prveho
     Ak nie je prvy pozera aj na nasledujuci */
    if (L->Last == NULL)
        L->First = NewElement;
    else
        L->Last->rptr = NewElement;
    
    // Priradime ako posledny novy prvok
    L->Last = NewElement;
}

void DLFirst (tDLList *L)
{
    // Aktivnym je prvy
    L->Act = L->First;
}

void DLLast (tDLList *L)
{
    // Aktivnym je posledny
    L->Act = L->Last;
}

void DLCopyFirst (tDLList *L, int *val)
{
    // Ak je prvy ziska jeho hodnotu inak error
    if (L->First != NULL)
        *val = L->First->data;
    else
        DLError();
}

void DLCopyLast (tDLList *L, int *val)
{
    // Al je posledny ziska jeho hodnotu inak error
    if (L->Last != NULL)
        *val = L->Last->data;
    else
        DLError();
}

void DLDeleteFirst (tDLList *L)
{
    // Ak je nejaky prvy
    if (L->First != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tDLElemPtr NasledZaFirst;
        NasledZaFirst = L->First->rptr;
        
        // Ak aktivny bol prvy
        if (L->Act == L->First)
            L->Act = NULL;
        
        // Ak je jedinym prvkom
        if (L->Last == L->First)
            L->Last = NULL;
        
        // Uvolnenie pamate
        free (L->First);
        
        // Premostenie
        L->First = NasledZaFirst;
        
        // Ak je prvy nadstav lavy na NULL
        if (L->First)
            L->First->lptr = NULL;
    }
}	

void DLDeleteLast (tDLList *L)
{
    // Ak je nejaky posledny
    if (L->Last != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tDLElemPtr PredZaLast;
        PredZaLast = L->Last->lptr;
        
        // Ak aktivny bol prvy
        if (L->Act == L->Last)
            L->Act = NULL;
        
        // Ak je jeden prvok v zozname
        if (L->Last == L->First)
            L->First = NULL;
        
        // Uvolnenie pamate
        free (L->Last);
        
        // Premostenie
        L->Last = PredZaLast;
        
        // Ak je posledny nadstav pravy na NULL
        if (L->Last != NULL)
            L->Last->rptr = NULL;
    }
}

void DLPostDelete (tDLList *L)
{
    // Ak je nejaky aktivny a dalsi po nom
    if (L->Act != NULL && L->Act->rptr != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tDLElemPtr NasledZaRus;
        NasledZaRus = L->Act->rptr->rptr;
        
        // Ak je mazany posledny urob novy posledny
        if (L->Last == L->Act->rptr)
            L->Last = L->Act;
        
        // Uvolnenie pamate
        free(L->Act->rptr);
        
        // Premostenie z oboch stran
        if (NasledZaRus != NULL)
            NasledZaRus->lptr = L->Act;
        L->Act->rptr = NasledZaRus;
    }
}

void DLPreDelete (tDLList *L)
{
    // Ak je nejaky aktivny a dalsi po nom
    if (L->Act != NULL && L->Act->lptr != NULL)
    {
        // Pomocna premenna a jej inicializacia
        tDLElemPtr PredZaRus;
        PredZaRus = L->Act->lptr->lptr;
        
        // Ak je mazany prvy urob novy prvy
        if (L->First == L->Act->lptr)
            L->First = L->Act;
        
        // Uvolnenie pamate
        free(L->Act->lptr);
        
        // Premostenie z oboch stran
        L->Act->lptr = PredZaRus;
        if (PredZaRus != NULL)
            PredZaRus->rptr = L->Act;
    }
}
    
void DLPostInsert (tDLList *L, int val)
{
    // Ak je nejaky aktivny
    if (L->Act != NULL)
    {
        // Pomocne premenne a inicializacia
        tDLElemPtr NewElement;
        
        // Alokacia noveho prvku a inicializacia inak error
        if ((NewElement = malloc(sizeof(struct tDLElem)))== NULL)
        {
            DLError();
            return;
        }
        
        // Zachovaj poslednost
        if (L->Act == L->Last)
            L->Last = NewElement;
        
        // Napln novy prvok hodnotami
        NewElement->rptr = L->Act->rptr;
        NewElement->data = val;
        NewElement->lptr = L->Act;
        
        // Premostenie z oboch stran
        if (L->Act->rptr != NULL)
            L->Act->rptr->lptr = NewElement;
        L->Act->rptr = NewElement;
    }
}

void DLPreInsert (tDLList *L, int val)
{
    // Ak je nejaky aktivny
    if (L->Act != NULL)
    {
        // Pomocne premenne a inicializacia
        tDLElemPtr NewElement;
        
        // Alokacia noveho prvku a inicializacia inak error
        if ((NewElement = malloc(sizeof(struct tDLElem)))== NULL)
        {
            DLError();
            return;
        }
        
        // Napln novy prvok hodnotami
        NewElement->lptr = L->Act->lptr;
        NewElement->rptr = L->Act;
        NewElement->data = val;
        
        // Zachovaj prvost
        if (L->Act == L->First)
            L->First = NewElement;
        
        // Premostenie z oboch stran
        if (L->Act->lptr != NULL)
            L->Act->lptr->rptr = NewElement;
        L->Act->lptr = NewElement;
    }
}

void DLCopy (tDLList *L, int *val)
{
    // Ak je nejaky aktivny vrat jeho hodnotu inak error
    if (L->Act != NULL)
        *val = L->Act->data;
    else
        DLError();
}

void DLActualize (tDLList *L, int val)
{
    // Zmen hodnotu aktualneho prvku
    if (L->Act != NULL)
        L->Act->data = val;
}

void DLSucc (tDLList *L)
{
    // Posun aktivitu na dalsi
    if (L->Act != NULL)
        L->Act = L->Act->rptr;
}


void DLPred (tDLList *L)
{
    // Posun aktivitu dozadu
    if (L->Act != NULL)
        L->Act = L->Act->lptr;
    
}

int DLActive (tDLList *L)
{
    // Vrat ci je aktivny
    return (L->Act == NULL) ? 0 : 1;
}
