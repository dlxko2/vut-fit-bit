% Martin Pavelka (xpavel27)
% Project ISS; FIT 2BIA
% 2014/2015

% Uloha 1 - Zaostrenie obrazu
I = imread('xpavel27.bmp');
h = [-0.5 -0.5 -0.5; -0.5 5.0 -0.5; -0.5 -0.5 -0.5];
I2 = imfilter(I,h);
imwrite(I2,'step1.bmp');

% Uloha 2 - Otocenie obrazu
I3 = fliplr(I2);
imwrite(I3,'step2.bmp');

% Uloha 3 - Medianovy filter
I4 = medfilt2(I3, [5,5]);
imwrite(I4,'step3.bmp');

% Uloha 4 - Rozmazanie obrazu
h2 = [1 1 1 1 1; 1 3 3 3 1; 1 3 9 3 1; 1 3 3 3 1; 1 1 1 1 1];
h3 = h2 / 49;
I5 = imfilter(I4, h3);
imwrite(I5,'step4.bmp');

% Uloha 5 - Chyba v obraze
I6 = imread('xpavel27.bmp');
I6 = fliplr(I6);
noise=0;
I5 = double(I5);
I6 = double(I6);
for (x=1:512)
  for (y=1:512)
    noise=noise+double(abs(I5(x,y)-I6(x,y)));
    end; 
end;
noise=noise/(512*512);
chyba = noise;

% Uloha 6 - Roztazenie histogramu
I7 = imread('step4.bmp');
I7 = im2double(I7);
minimum = min(min(I7));
maximum = max(max(I7));
I7 = imadjust(I7,[minimum; maximum],[0.0; 1.0]);
imwrite(I7,'step5.bmp');

% Uloha 7 - Stredna hodnota a smerodatna odchylka
I8 = double(imread('step4.bmp'));
I9 = double(imread('step5.bmp'));
mean_no_hist = mean2(I8);
mean_hist = mean2(I9);
std_no_hist = std2(I8);
std_hist = std2(I9);

% Uloha 8 - Kvantizace obrazu
I10 = double(imread('step5.bmp'))
x = size(I10);
N = 2;
IQ = zeros(x(1),x(2));

for k=1:x(1)
  for l=1:x(2)
    IQ(k,l) = round(((2^N)-1)*(double(I10(k,l))-0)/(255-0))*(255-0)/((2^N)-1) + 0;
  end
end

IQ=uint8(IQ);
imwrite(IQ,'step6.bmp');

