var platno, obsah, tool;
var canX = [];
var canY = [];
var len = 0;

// definovana zakladna farba
var curColor = "#000000";

// definovana sirka stetca
var curWidth = 2;

// inicializacia pri spusteni skriptu
function init () {

	// definovane platno a obsah platna
	platno = document.getElementById('platno');
	platno.width = 500;
	platno.height = 500;
	obsah = platno.getContext('2d');

	// vytvorenie pera
	tool = new tool_pencil();

	// eventlistenery pre pohyby mysou a dotyky prsta
	platno.addEventListener('mousedown', kresli, false);
	platno.addEventListener('mousemove', kresli, false);
	platno.addEventListener('mouseup',   kresli, false);
	platno.addEventListener('touchstart', kresli, false);
	platno.addEventListener('touchmove', kresli, false);
	platno.addEventListener('touchend',   kresli, false);	
}

// nastroj na kreslenie 
function tool_pencil () {
	var tool = this;
	this.started = false;

	// ak sa drzi stlacene tlacidlo mysi, drzi prst na displayi,
	// vykona sa funkcia a zacne sa kreslit ciara
	this.tap = this.touchstart = this.mousedown = function (event) {
		event.preventDefault(); // zabrazni zmene kurzoru na textovy
		obsah.beginPath(); // otvori cestu
		tool.started = true;
		obsah.arc(event._x, event._y, curWidth/30, 0, 2*Math.PI, true); // nakresli 'bod'
	};
	
	// funkcia sa vola ked sa pohybuje mysou, prstom
	this.touchmove = this.mousemove = function (event) {
		if (tool.started) {
			obsah.strokeStyle = curColor; // nastavi sa farba
			obsah.lineWidth = curWidth; // nastavi sa hrubka
			obsah.lineJoin  = "round"; // zaoblenie hran
			obsah.lineCap   = "round"; // zaoblenie hran
			obsah.lineTo(event._x, event._y); // nakresli ciaru v danom bode
			obsah.stroke(); // vykresli
		}
	};

	// sa zavola ak sa pusti mys alebo odtiahne prst
	this.touchend = this.mouseup = function (event) {		
		if (tool.started) {
			tool.mousemove(event);
			tool.started = false;
		}
	};	
}

// Iba event handler. Zistuje sa relativna pozicia prstu/mysi 
function kresli (event) {

	// Zistuje, ci zariadenie podporuje dotyk, vracia 0/1
	function je_dotyk() {
		return !!('ontouchstart' in window) ? 1 : 0;
	}

	// Ak je dotykove zariadenie, suradnice sa zistia podla
	// aktualnej polohy prsta, inak poloha kurzoru
	if (je_dotyk() == 1) {
		event._x = event.targetTouches[0].pageX-this.offsetLeft;
		event._y = event.targetTouches[0].pageY-this.offsetTop;
	}
	else if (event.offsetX || event.offsetX == 0) { 
		event._x = event.offsetX;
		event._y = event.offsetY;
	} else {
		event._x = event.layerX-this.offsetLeft;
		event._y = event.layerY-15;
	}

	// Zavola sa event handler tool, cize nastroj(ceruzka)
	var func = tool[event.type];
	if (func) { event._x
		func(event);
	}
}

//inicializacia platna a event handlerov
init();

// zabrani scroolovaniu na platne, aby sa pri kresleni web nehybal
document.getElementById('platno').addEventListener('touchmove',function(event){
	len = event.touches.length;
	if (len == 1) event.preventDefault();	
},false);

// vyber farieb
$('#red').click(function(){
	curColor = "#CD0000";
});
$('#blue').click(function(){
	curColor = "#6666FF";
});
$('#yellow').click(function(){
	curColor = "#FFCF33";
});
$('#white').click(function(){
	curColor = "#FFFFFF";
});
$('#black').click(function(){
	curColor = "#000000";
});
$('#brown').click(function(){
	curColor = "#986928";
});
$('#green').click(function(){
	curColor = "#659B41";
});
$('#orange').click(function(){
	curColor = "#FF6600";
});
$('#purple').click(function(){
	curColor = "#CB3594";
});

// pri zmene slideru sa zmeni hrubka stetca
$('#w_slider').change(function(){
    curWidth = $('#width_slider').val();
})