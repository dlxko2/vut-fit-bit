#########################
# vytvorenie user tabulky
#########################
CREATE TABLE IF NOT EXISTS `users` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `skupina` TINYINT UNSIGNED NOT NULL DEFAULT '3',
  `email` VARCHAR( 40 ) NOT NULL ,  
  `heslo` CHAR( 40 ) NOT NULL ,
  `meno` VARCHAR( 20 ) NOT NULL ,
  `priezvisko` VARCHAR( 40 ) NOT NULL ,  
  `avatar` VARCHAR( 100 ) NOT NULL ,
  `web` VARCHAR( 100 ) NOT NULL ,
  `blocked` TINYINT UNSIGNED NOT NULL DEFAULT '0', UNIQUE ( `email` )
) ENGINE = MYISAM DEFAULT CHARSET=utf8;

###############################
# vytvorenie databazy pristupov
###############################
CREATE TABLE IF NOT EXISTS `users_login_log` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `u_id` SMALLINT UNSIGNED NOT NULL,
  `ip` varchar(39) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `browser` varchar(100) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

##############################
# tabulka - trvale prihlasenie
##############################
CREATE TABLE IF NOT EXISTS `permanent_login` (
  `u_id` SMALLINT UNSIGNED NOT NULL,
  `token` VARCHAR(50) NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8;

####################
# tabulka - slovicka
####################
CREATE TABLE IF NOT EXISTS `words` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `okruh` SMALLINT UNSIGNED NOT NULL ,
  `en_slovko` CHAR( 40 ) NOT NULL ,
  `sk_slovko` CHAR( 40 ) NOT NULL ,
  `vyslovnost` CHAR( 40 ) NOT NULL ,
  `popis` VARCHAR( 200 ) NOT NULL ,
  `obrazok` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8;

##########################
# tabulka - zoznam okruhov
##########################
CREATE TABLE IF NOT EXISTS `theme` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `u_id` SMALLINT UNSIGNED NOT NULL ,  
  `okruh` VARCHAR( 40 ) NOT NULL ,
  UNIQUE ( `okruh` )
) ENGINE = MYISAM DEFAULT CHARSET=utf8;