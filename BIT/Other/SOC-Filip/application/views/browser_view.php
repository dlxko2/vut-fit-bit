<!DOCTYPE html>
<html>
<head>
	<title>Social Word</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>
	<style>
	* {
		font-family: 'Cabin', Helvetica, Arial, sans-serif;
		line-height: 150%;
	}
	body {
		margin: 0 auto;
	}
	h1 {
		font-family: 'Holtwood One SC', serif;
		font-size: 150%;
	}
	</style>
</head>
<body>
<center>
	<h1>Browser nie je podporovaný</h1>
	<p>Prosím stiahnite si Google Chrome =)</p>
	<small>...alebo testujte na iPade...</small>
</center>
</body>
</html>