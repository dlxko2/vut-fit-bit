<!DOCTYPE html>
<html>
<head>
	<title>Social Word</title>
	<meta charset="utf-8">
	<!-- Prispôsobovanie sa UI JQM k rozlíšeniu prístroja -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css" />
	<!-- Link ku CSS súboru jQuery -->
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.css" />
	<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script> <!-- Klasické jQuery -->
	<script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script> <!-- jQuery Mobile -->
	<script src="<?=base_url()?>assets/js/main.js"></script> <!-- hlavne js -->
	<link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>
</head>
<body> <? if ( empty($nadpis) ) $nadpis = 'Social Word'; if ( empty($header) ) $header = 'includes/header_main'; ?>
<div data-role="page" data-ajax='false'>
<?php $this->load->view($header); ?>

	<div data-role="content">
<?php $this->load->view($hlavny_obsah); ?>	
	</div> <!-- /content -->

	<div class="posun"></div> <!-- footer hack -->

<?php if ( !@is_logged() ) { ?>
	<div data-role="footer" data-mini="true" data-theme="c">
		<h4>@Niorko</h4>
	</div><!-- /footer -->
<?php } else { ?>
	<div data-role="footer" data-mini="true" data-theme="c">
		<h4>@Niorko</h4>
		<a href="<?=base_url()?>logout" data-role="button" data-icon="gear" class="ui-btn-right">Logout</a>
	</div><!-- /footer -->
<?php } ?>

</div><!-- /page -->
</body>
</html>