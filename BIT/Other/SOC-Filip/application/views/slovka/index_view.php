<h1>Okruh slovnej zásoby:</h1>

<fieldset class="ui-grid-a">
	<div class="ui-block-a">
		<ul data-role="listview" data-autodividers="true" data-theme="e" data-dividertheme="a" data-inset="true" id="left_column">
		<?php foreach ($themes as $theme) : ?>
			<li><a href="<?=base_url()?>slovka/okruh/<?= $theme->id?>"><?= $theme->okruh?></a></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<div class="ui-block-b">
		<div id="right_column">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis pharetra eros. Fusce velit felis, venenatis vitae sodales eget, accumsan eget sem. Curabitur aliquam lectus vitae purus feugiat mattis. Maecenas gravida mi vel tortor consectetur vitae auctor diam faucibus. Donec malesuada mi quis libero aliquam ornare. Cras vehicula urna et magna tempus tempor. Integer sodales tortor vitae dui faucibus id molestie augue interdum.</p>
		</div>
	</div>
</div
