<h1>Slovíčka</h1>
	<div style="margin-top: 60px;">
		<ul data-role="listview" data-inset="true">
		<?php foreach ($words as $word) : ?>
			<li><img src="<?= $word->obrazok?>" title="<?= ucfirst($word->sk_slovko);?>" />
			<?= ucfirst($word->en_slovko);?> - <?= my_mb_ucfirst($word->sk_slovko);?> <small>{<?= ucfirst($word->vyslovnost);?>}</small>
			<p style="white-space:normal"><br/><?= ucfirst($word->popis);?></p></li>
		<?php endforeach; ?>
		<ul>
	</div>
