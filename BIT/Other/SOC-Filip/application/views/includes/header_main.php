	<? $url_segment = $this->uri->rsegment(1); ?>	
	<div data-role="header" data-position="fixed" data-theme="c">
		<div data-role="navbar" data-iconpos="top">
			<ul>
				<li><a href="<?=base_url()?>" data-icon="domov"<?php echo $url_segment == 'domov' ? 'class="ui-btn-active ui-state-persist"' : ''; ?>>Domov</a></li>
				<li><a href="<?=base_url()?>skic" data-icon="skic"<?php echo $url_segment == 'skic' ? 'class="ui-btn-active ui-state-persist"' : ''; ?>>Skic</a></li>
				<li><a href="<?=base_url()?>slovka" data-icon="slovka"<?php echo $url_segment == 'slovka' ? 'class="ui-btn-active ui-state-persist"' : ''; ?>>Slovka</a></li>
				<li><a href="#" data-icon="social"<?php echo $url_segment == 'social' ? 'class="ui-btn-active ui-state-persist"' : ''; ?>>Social</a></li>
				<li><a href="#" data-icon="profil"<?php echo $url_segment == 'profil' ? 'class="ui-btn-active ui-state-persist"' : ''; ?>>Profil</a></li>
			</ul>
		</div><!-- /navbar -->				
	</div><!-- /header -->
	