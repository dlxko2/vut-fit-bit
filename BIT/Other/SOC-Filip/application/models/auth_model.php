<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model{

	function __construct()
	{
		parent::__construct();
		//konstruktor
	}

	function register()
	{
		$data = array(
			'meno' => $_POST['meno'],
			'priezvisko' => $_POST['priezvisko'],
			'email' => $_POST['email'],
			'heslo' => sha1($_POST['heslo']),
			);

		$this->db->insert('users',$data);		
	}

	function check()
	{
		$result = $this->db->where('email',$_POST['email'])
						   ->where('heslo',sha1($_POST['heslo']))
						   ->get('users');

		return $result->num_rows();
	}

	function getUserData($email)
	{
		$select = $this->db->select('id','meno','priezvisko','email')
						   ->where('email', $email)
						   ->limit(1)
						   ->get('users');

		if ($select->num_rows() > 0) return $select->row_array();
		else return false; 
	}

	function isEmailFree($email)
	{
		$select = $this->db->select('email')
						   ->where('email', $email)
						   ->limit(1)
						   ->get('users');

		return $select->num_rows();
	}

	function userLoginLog()
	{
		$data = $this->session->all_userdata();

		$data['ip_adress'] = $this->input->ip_address();
		// osetrenie IP
		if ($data['ip_adress']==0) $data['ip_adress']=1;

		if($data['id'] != 0) {
			$query = array(
				'u_id' => $data['id'], 
				'ip' => $data['ip_adress'],
				'browser' => $data['user_agent']
				);

			$fromCookie = array(
				'u_id' => $data['id'],
				'token' => $data['session_id']
				);
		} else {
			$this->load->helper('url');
			redirect('');
		}

		$this->db->insert('users_login_log',$query);
		$this->db->insert('permanent_login',$fromCookie);
	}

	function getToken($u_id,$token)
	{
		$select = $this->db->select('u_id','token')
						   ->where('u_id', $u_id)
						   ->where('token', $token)
						   ->limit(1)
						   ->get('permanent_login');

		return $select->num_rows();
	}

	function clear_log()
	{
		$this->db->where( 'token', $this->session->userdata('session_id') )
										->delete('permanent_login');
	}	

}

?>