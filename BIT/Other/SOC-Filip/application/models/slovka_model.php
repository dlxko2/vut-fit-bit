<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slovka_model extends CI_Model{

	function __construct()
	{
		parent::__construct();
		//konstruktor
	}

	function getThemes()
	{
		$this->db->order_by('okruh');
		$q = $this->db->get('theme');					  

		return $q->result();
	}

	function getWords($id)
	{
		$q = $this->db->where('okruh',$id)
					  ->get('words');				  

		return $q->result();	
	}

}