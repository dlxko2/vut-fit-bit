<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slovka extends CI_Controller {

	//konstruktor
	function __construct()
	{
		parent::__construct();
		if ( !@is_logged() ) @stranka('domov_view','Social Word','header_home');
		$this->load->model('slovka_model');
	}

	function index()
	{
		$data['hlavny_obsah'] = 'slovka/index_view';
		$data['themes'] = $this->slovka_model->getThemes();
		
		$this->load->view('template',$data);
	}

	function okruh()
	{
		$slovka_id = $this->uri->rsegment(3);	

		$data['hlavny_obsah'] = 'slovka/okruh_view';
		$data['words'] = $this->slovka_model->getWords( $slovka_id );
		
		$this->load->view('template',$data);	
	}

}

?>