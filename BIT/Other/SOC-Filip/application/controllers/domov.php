<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Domov extends CI_Controller {

	//konstruktor
	function __construct()
	{
		parent::__construct();
		if ( !@is_logged() ) @stranka('domov_view','Social Word','header_home');
	}

	function index()
	{
		@stranka('domov_view');			
	}

}

?>