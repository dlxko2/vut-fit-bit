<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Auth extends CI_Controller{

		//konštruktory - t.j. stuffs, ktore sa nacitaju pri pouziti class
		function __construct()
		{
			parent::__construct();
			$this->load->helper('form');
			$this->load->model('auth_model');
			$this->load->helper('url');
			$this->load->library('form_validation');			
		}

		//defaultna metoda pri zavolani /auth
		function index()
		{
			redirect('');
		}

		//registracia
		function register()
		{
			//kontrola prihlasenia
			if ( $this->session->userdata('logged_in') ) redirect('');

			$this->form_validation->set_rules('meno', 'Meno', 'required');
			$this->form_validation->set_rules('priezvisko', 'Priezvisko', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
			$this->form_validation->set_rules('heslo', 'Heslo', 'required|min_length[4]');
			$this->form_validation->set_error_delimiters('','<br/>');

			if( $this->form_validation->run() )
			{
				if ( !$this->auth_model->isEmailFree($_POST['email']) )
				{
					if ( !$this->auth_model->register() )
					{
						@zapis_session_db();
						redirect('');
					}
					else
					{
						@set_flash_message('Registrácia sa nepodarila');
						redirect('register');
					}
				}
				else
				{
					@set_flash_message('Uživateľ s týmto emailom už existuje!');
					redirect('register');
				}							
			}
			else
			{
				@stranka('auth/register_view','Registrácia','header_auth');
			}

		}

		function login()
		{
			//kontrola prihlasenia
			if ( $this->session->userdata('logged_in') ) redirect('');

			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|');
			$this->form_validation->set_rules('heslo', 'Heslo', 'required|min_length[4]');
			$this->form_validation->set_error_delimiters('','<br/>');

			if( $this->form_validation->run())
			{	
				if($this->auth_model->check())
				{
					//zapis do session a nastavenie cookie
					@zapis_session_db();
					redirect('');
				}
				else
				{
					@set_flash_message('Email alebo heslo nie je správne!');
					redirect('login');
				}
	
			}
			else
			{
				@stranka('auth/login_view','Prihlásenie','header_auth');
			}

		}

		// odhlasenie
		function logout()
		{
			$this->auth_model->clear_log();
			$this->session->sess_destroy();
			redirect('');
		}	

	}

?>