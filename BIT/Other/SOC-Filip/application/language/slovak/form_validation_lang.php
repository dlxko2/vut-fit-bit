<?php

$lang['required']			= "Pole %s je povinné.";
$lang['isset']				= "Pole %s musí mať hodnotu.";
$lang['valid_email']		= "Zadajte prosím správny email.";
$lang['valid_emails']		= "%s musí obsahovať spravne emaily.";
$lang['valid_url']			= "%s musí obsahovať spravnu adresu.";
$lang['valid_ip']			= "%s musí obsahovať spravnu IP adresu.";
$lang['min_length']			= "%s musí mať dĺžku minimalne %s znakov.";
$lang['max_length']			= "%s musí mať dĺžku maximalne %s znakov.";
$lang['exact_length']		= "%s musí obsahovať presne %s znakov.";
$lang['alpha']				= "%s musí obsahovať iba písmená.";
$lang['alpha_numeric']		= "%s môže obsahovať iba písmená a čísla.";
$lang['alpha_dash']			= "%s môže obsahovať iba písmená a čísla, podtržník a bodky.";
$lang['numeric']			= "%s musí obsahovať iba čísla.";
$lang['is_numeric']			= "%s musí obsahovať iba číselné znaky.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "%s neobsahuje to iste ako %s.";
$lang['is_unique'] 			= "The %s field must contain a unique value.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */