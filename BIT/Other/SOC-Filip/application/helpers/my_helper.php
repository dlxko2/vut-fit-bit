<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// zobrazi flaash spravu
function flash_message()
{
	// ziska flash spravu z CI instancie
	$ci =& get_instance();
	$sprava = $ci->session->flashdata('flash_sprava');

	$html = '';
	if (is_array($sprava))
	{
		$html = $sprava['obsah'];
	}
	return $html;
}

// nastavi flash spravu
function set_flash_message($sprava)
{
	// ziska flash spravu z CI instancie
	$ci =& get_instance();
	
	// nastavenie spravy
	$ci->session->set_flashdata( 'flash_sprava', array('obsah' => $sprava));		
}

// nacita obsah stranky do template
function stranka($stranka,$nadpis = 'Social Word',$header = 'header_main')
{
	// ziska flash spravu z CI instancie
	$ci =& get_instance();

	$data = array(
		'hlavny_obsah'=>$stranka,
		'nadpis'=>$nadpis,
		'header'=>'includes/'.$header
		);
	$ci->load->view('template',$data);
}

// zapis do session + db helper
function zapis_session_db()
{
	// ziska flash spravu z CI instancie
	$ci =& get_instance();

	// nastavenie dat
	$data = $ci->auth_model->getUserData($_POST['email']);
	$data['logged_in'] = TRUE;

	//zapis do session
	$ci->session->set_userdata($data);

	// zapis do db
	$ci->auth_model->userLoginLog();

}

// vrati 1 pri prihlaseni
function is_logged()
{
	// ziska flash spravu z CI instancie
	$ci =& get_instance();

	$ci->load->model('auth_model');
	
	
	if ( $ci->session->userdata('logged_in') ) return 1;
	elseif ( $ci->input->cookie('user_cookie', TRUE)) 
	{
		// vyberie cely retazec z cookie
		$cookie = $ci->input->cookie('user_cookie', TRUE);
		// rozbitie retazca na casti
		$retazec = explode(':',$cookie);

		// najdenie id a tokenu
		if ( isset($retazec[25]) && isset($retazec[6]) )
		{
			$u_id = explode(';',$retazec[25]);
			$u_id = trim($u_id[0],'\"');
			$token = explode(';',$retazec[6]);
			$token = trim($token[0],'\"');
		}
		else
		{
			// osetrenie chyby
			$u_id = 1;
			$token = 1;
		}
		

		// zistenie obsahu cookie... ak obsahuje udaje z db
		// vytvori sa session a uzivatel sa moze prihlasit
		if ( $ci->auth_model->getToken($u_id, $token) )
		{
			$data['logged_in'] = TRUE;

			//data sa zapisu do session
			$ci->session->set_userdata( $data );
			
			// zapis do db
			$ci->auth_model->userLoginLog();

			return 1; 
		}	

	}
	else return 0;	
	
}

function my_ucfirst($words)
{
    $string = mb_strtoupper(mb_substr($words, 0, 1));
    return $string.mb_substr($words, 1);
}

?>
