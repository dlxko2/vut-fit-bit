-- Authors: xbajan01, xgavry00
-- Description: Script for creating database scheme and filling sample data
-- Date: 29.03.2015

-- Zhod tabulky aktualneho uzivatela
DROP MATERIALIZED VIEW AKTUALNE_ZISKY_OBJEDNAVOK;
BEGIN
    FOR rec IN (
        SELECT 'DROP ' || object_type || ' ' || object_name || DECODE ( object_type, 'TABLE', ' CASCADE CONSTRAINTS PURGE' ) AS v_sql
        FROM user_objects
        WHERE object_type IN ( 'TABLE', 'VIEW', 'PACKAGE', 'TYPE', 'PROCEDURE', 'FUNCTION', 'TRIGGER', 'SEQUENCE' )
        ORDER BY object_type, object_name
    ) LOOP
        EXECUTE IMMEDIATE rec.v_sql;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Some non-important warning!');
END;
/


CREATE SEQUENCE pecivo_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE pecivo (
    id              INTEGER         CONSTRAINT PK_pecivo_id PRIMARY KEY,
    nazev           VARCHAR2(50)    CONSTRAINT NN_pecivo_nazev NOT NULL,
    datum_expirace  DATE            CONSTRAINT NN_pecivo_expirace NOT NULL,
    cena            NUMBER(12,2)    CONSTRAINT NN_pecivo_cena NOT NULL,
    alergeny        VARCHAR2(150)   CONSTRAINT NN_pecivo_alerg NOT NULL,
    mnozstvi        INTEGER         CONSTRAINT NN_pecivo_mnozstvi CHECK(mnozstvi >= 0)
);

CREATE SEQUENCE surovina_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE surovina (
    id              INTEGER         CONSTRAINT PK_surovina_id PRIMARY KEY,
    nazev           VARCHAR2(50)    CONSTRAINT NN_surovina_nazev NOT NULL,
    mnozstvi        INTEGER         CONSTRAINT NN_surovina_mnozstvi NOT NULL,
    datum_expirace  DATE            CONSTRAINT NN_surovina_expirace NOT NULL,
    nakupni_cena    NUMBER(12,2)    CONSTRAINT NN_surovina_cena NOT NULL
);

CREATE TABLE potreba (
    id_peciva       INTEGER         CONSTRAINT FK_pecivo REFERENCES pecivo(id),
    id_suroviny     INTEGER         CONSTRAINT FK_surovina REFERENCES surovina(id),
    mnozstvi        NUMBER(12,3)    CONSTRAINT NN_potreba_mnozstvi NOT NULL
);

ALTER TABLE potreba ADD (
    CONSTRAINT PK_potreba_id PRIMARY KEY (id_peciva, id_suroviny)
);

CREATE SEQUENCE osoba_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE osoba (
    id              INTEGER         CONSTRAINT PK_osoba_id PRIMARY KEY,
    jmeno           VARCHAR2(30)    CONSTRAINT NN_osoba_jmeno NOT NULL,
    prijmeni        VARCHAR2(30)    CONSTRAINT NN_osoba_prijmeni NOT NULL,
    datum_narozeni  DATE            CONSTRAINT NN_osoba_narozeni NOT NULL,
    telefon         VARCHAR2(20)    CONSTRAINT NN_osoba_telefon NOT NULL,
    adresa          VARCHAR2(140)   CONSTRAINT NN_osoba_adresa NOT NULL,
    cislo_uctu      VARCHAR2(34)    CONSTRAINT NN_osoba_cislo_uctu NOT NULL
);

CREATE TABLE manager (
    osoba_id        INTEGER         CONSTRAINT FK_manager_osoba REFERENCES osoba (id),
                                    CONSTRAINT PK_manager_osoba PRIMARY KEY(osoba_id),
    vzdelani        VARCHAR2(100)   CONSTRAINT NN_manager_vzdelani NOT NULL,
    datum_nastupu   DATE            CONSTRAINT NN_manager_datum_nastupu NOT NULL
);

CREATE TABLE ridic (
    osoba_id        INTEGER         CONSTRAINT FK_ridic_osoba REFERENCES osoba (id),
                                    CONSTRAINT PK_ridic_osoba PRIMARY KEY(osoba_id),
    kategorie_ridickeho_prukazu VARCHAR2(10) CONSTRAINT NN_ridic_kategorie_prukazu NOT NULL,
    datum_nastupu   DATE            CONSTRAINT NN_ridic_datum_nastupu NOT NULL
);

CREATE TABLE zakaznik (
    osoba_id        INTEGER         CONSTRAINT FK_zakaznik_osoba REFERENCES osoba (id),
                                    CONSTRAINT PK_zakaznik_osoba PRIMARY KEY(osoba_id),
    adresa_sidla    VARCHAR2(140)   CONSTRAINT NN_zakaznik_adresa_sidla NOT NULL
);

CREATE SEQUENCE oblast_rozvozu_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE oblast_rozvozu (
    id              INTEGER         CONSTRAINT PK_oblast_rozvozu_id PRIMARY KEY,
    ridic_id        INTEGER         NOT NULL CONSTRAINT FK_ridic_id REFERENCES ridic(osoba_id),
    nazev           VARCHAR2(30)    CONSTRAINT NN_oblast_rozvozu_nazev NOT NULL,
    cas_rozvozu     TIMESTAMP       CONSTRAINT NN_oblast_rozvozu_cas_rozvozu NOT NULL
);

CREATE SEQUENCE adresa_doruceni_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE adresa_doruceni (
    id              INTEGER         CONSTRAINT PK_adresa_id PRIMARY KEY,
    zakaznik_id     INTEGER         NOT NULL CONSTRAINT FK_zakaznik_id       REFERENCES zakaznik(osoba_id),
    oblast_rozvozu_id INTEGER       NOT NULL CONSTRAINT FK_oblast_rozvozu_id REFERENCES oblast_rozvozu(id),
    cislo_pobocky   SMALLINT        CONSTRAINT NN_adresa_cislo_pobocky NOT NULL,
    mesto           VARCHAR2(50)    CONSTRAINT NN_adresa_mesto NOT NULL,
    ulice           VARCHAR2(50)    CONSTRAINT NN_adresa_ulice NOT NULL,
    cislo_domu      SMALLINT        CONSTRAINT NN_adresa_cislo_domu NOT NULL
);

CREATE SEQUENCE objednavka_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE objednavka (
    id              INTEGER         CONSTRAINT PK_objednavka_id PRIMARY KEY,
    datum_dodani    DATE            CONSTRAINT NN_objednavka_datum_dodani NOT NULL,
    spusob_dodani   VARCHAR2(10)    CONSTRAINT CHOICES_dodani CHECK(spusob_dodani IN ('osobne', 'na pobocku')),
    spusob_platby   VARCHAR2(10)    CONSTRAINT CHOICES_platby CHECK(spusob_platby IN ('hotovost', 'kartou', 'na ucet')),
    datum_objednani DATE            CONSTRAINT NN_objednavka_datum_objednani NOT NULL,
    celkova_cena    NUMBER(12,2)    CONSTRAINT NN_objednavka_celkova_cena NOT NULL,
    adresa_doruceni_id INTEGER      CONSTRAINT FK_objecnavka_id REFERENCES adresa_doruceni(id),
    zakaznik_id     INTEGER         CONSTRAINT FK_obj_zakaznik_id REFERENCES zakaznik(osoba_id),
    vybavena        CHAR(1)         DEFAULT('N') CONSTRAINT ENUM_vybavena_bool CHECK (vybavena in ('Y', 'N'))
);

CREATE SEQUENCE polozka_objednavky_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE polozka_objednavky (
    poradove_id     INTEGER         CONSTRAINT PK_polozka_obj_id PRIMARY KEY,
    pecivo_id       INTEGER         NOT NULL CONSTRAINT FK_polozka_obj_pecivo REFERENCES pecivo (id),
    objednavka_id   INTEGER         NOT NULL CONSTRAINT FK_polozka_obj_objednavka REFERENCES objednavka(id),
    pocet           INTEGER         CONSTRAINT NN_polozka_obj_pocet CHECK(pocet >= 0)
);

CREATE TABLE auto (
    spz             VARCHAR2(10)    CONSTRAINT PK_auto_spz PRIMARY KEY,
    ridic_id        INTEGER         CONSTRAINT FK_auto_ridic_id REFERENCES ridic (osoba_id),
    pojizdnost      CHAR(1)         CONSTRAINT ENUM_pojizdnost_bool CHECK (pojizdnost in ('Y', 'N')),
    nosnost         SMALLINT        CONSTRAINT NN_auto_nosnost NOT NULL,
    datum_tech_kontroly DATE        CONSTRAINT NN_auto_datum_kontroly NOT NULL
);
------------------------------------------------------------------------------------------------------------------------
-- naplneni tabulky pecivo
INSERT INTO pecivo (id, nazev, datum_expirace, cena, alergeny, mnozstvi)
    VALUES (pecivo_seq.NEXTVAL, 'Kralovsky rohlik', TO_DATE('04.04.2015', 'DD.MM.YYYY'), 2.00, 'lepek, soja', 2000);

INSERT INTO pecivo (id, nazev, datum_expirace, cena, alergeny, mnozstvi)
    VALUES (pecivo_seq.NEXTVAL, 'Cerealni chleb', TO_DATE('01.04.2015', 'DD.MM.YYYY'), 25.00, 'lepek', 1000);
                  
INSERT INTO pecivo (id, nazev, datum_expirace, cena, alergeny, mnozstvi)
    VALUES (pecivo_seq.NEXTVAL, 'Buchty tvarohove', TO_DATE('02.04.2015', 'DD.MM.YYYY'), 15.00, 'lepek, soja, mleko, vejce', 800);

INSERT INTO pecivo (id, nazev, datum_expirace, cena, alergeny, mnozstvi)
    VALUES (pecivo_seq.NEXTVAL, 'Kobliha coko', TO_DATE('03.04.2015', 'DD.MM.YYYY'), 10.00, 'lepek, soja, mleko, vejce', 500);

INSERT INTO pecivo (id, nazev, datum_expirace, cena, alergeny, mnozstvi)
    VALUES (pecivo_seq.NEXTVAL, 'Zavin orechovy', TO_DATE('04.04.2015', 'DD.MM.YYYY'), 11.90, 'lepek, soja, mleko, vejce, kakao, vlasska jadra', 600);


------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky surovina
INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mouka hruba psenicna', 200, TO_DATE('10.06.2015', 'DD.MM.YYYY'), 18.80);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mouka polohruba psenicna', 250, TO_DATE('20.05.2015', 'DD.MM.YYYY'), 19.80);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mouka hladka psenicna', 400, TO_DATE('10.05.2015', 'DD.MM.YYYY'), 19.90);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mouka polohruba zitna', 300, TO_DATE('07.07.2015', 'DD.MM.YYYY'), 24.90);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mouka zitna celozrnna', 200, TO_DATE('10.06.2015', 'DD.MM.YYYY'), 26.90);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Jemna krupicka', 250, TO_DATE('10.08.2015', 'DD.MM.YYYY'), 41.80);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Sul jedla kamenna s jodem', 300, TO_DATE('05.10.2015', 'DD.MM.YYYY'), 7.50);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Pekarske drozdi', 200, TO_DATE('10.04.2015', 'DD.MM.YYYY'), 83.33);
                  
INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Rostlinny tuk na peceni', 350, TO_DATE('15.05.2015', 'DD.MM.YYYY'), 55.60);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mleko trvanlive polotucne', 350, TO_DATE('12.06.2015', 'DD.MM.YYYY'), 25.90);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Vejce M', 450, TO_DATE('20.05.2015', 'DD.MM.YYYY'), 2.00);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Pomleta soja', 300, TO_DATE('10.07.2015', 'DD.MM.YYYY'), 36.33);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Voda pitna', 2000, TO_DATE('03.07.2015', 'DD.MM.YYYY'), 2.60);
                  
INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Cukr krystal', 300, TO_DATE('10.08.2015', 'DD.MM.YYYY'), 22.90);
                  
INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Susene drozdi', 230, TO_DATE('10.06.2015', 'DD.MM.YYYY'), 928.57);
                  
INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Ovesne vlocky', 150, TO_DATE('11.07.2015', 'DD.MM.YYYY'), 41.80);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Soda jedla', 200, TO_DATE('11.08.2015', 'DD.MM.YYYY'), 186.67);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Podmasli', 200, TO_DATE('11.05.2015', 'DD.MM.YYYY'), 20.80);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Tvaroh', 250, TO_DATE('23.04.2015', 'DD.MM.YYYY'), 103.60);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Mlete vlasske orechy', 200, TO_DATE('30.12.2015', 'DD.MM.YYYY'), 200.20);

INSERT INTO surovina (id, nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES (surovina_seq.NEXTVAL, 'Cokolada na vareni', 150, TO_DATE('24.06.2015', 'DD.MM.YYYY'), 199.00);

------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky potreba
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 12 , 0.006);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 15, 0.014);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 7, 0.024); 
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 13, 0.5); 
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 3, 0.940); 
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (1, 9, 0.150);
    -------------------------------------------------              
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 1, 0.4); 
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 5, 0.4); 
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 16, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 7, 0.024);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 17, 0.024);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (2, 18, 1);
    -------------------------------------------------
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 3, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 10, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 9, 0.2);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 11, 4);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 8, 0.050);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 19, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (3, 14, 0.3);
    -------------------------------------------------
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 7, 0.01);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 10, 0.6);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 8, 0.08);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 14, 0.14);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 11, 4);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 1, 1.2);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 9, 0.16);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (4, 21, 0.200);
    -------------------------------------------------
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 2, 2.00);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 3, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 10, 1.250);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 9, 0.5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 8, 0.025);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 14, 0.250);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 11, 5);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 7, 0.015);
INSERT INTO potreba (id_peciva, id_suroviny, mnozstvi) VALUES (5, 20, 1.250);

------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky osoba
INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Vladimir', 'Vesely', TO_DATE('15.02.1986', 'DD.MM.YYYY'),
            '+420777256255', 'UIFS FIT VUT Bozetechova 2 Brno', '6000538414/3500');
INSERT INTO manager (osoba_id, vzdelani,  datum_nastupu)
    VALUES (osoba_seq.CURRVAL, 'MENDELU AF - Mgr', TO_DATE('11.01.2011', 'DD.MM.YYYY'));

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Martin', 'Zluty', TO_DATE('19.05.1979', 'DD.MM.YYYY'),
            '+420777252345', 'Palackeho tr. 177 Brno-Kralovo Pole', '5360538414/3500');
INSERT INTO zakaznik (osoba_id, adresa_sidla)
    VALUES (osoba_seq.CURRVAL, 'Slovanske nam. 1804/7 Brno');

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Filip', 'Novy', TO_DATE('20.05.1986', 'DD.MM.YYYY'),
            '+420235252345', 'Svatopluka Cecha 81 Brno-Kralovo Pole', '5344448414/3500');
INSERT INTO zakaznik (osoba_id, adresa_sidla)
    VALUES (osoba_seq.CURRVAL, 'Svatopluka Cecha 81 Brno-Kralovo Pole');

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Jana', 'Lavrinova', TO_DATE('23.05.1976', 'DD.MM.YYYY'),
            '+420776252345', 'Slovanske nam. 2237/1 Brno-Kralovo Pole', '5360538414/3500');
INSERT INTO ridic (osoba_id, kategorie_ridickeho_prukazu, datum_nastupu)
    VALUES (osoba_seq.CURRVAL, 'A, B, C', TO_DATE('15.03.2011', 'DD.MM.YYYY'));

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Matej', 'Lehky', TO_DATE('24.05.1986', 'DD.MM.YYYY'),
            '+420776268845', 'Pionyrska 249/15 Brno', '5360589614/3500');
INSERT INTO ridic (osoba_id, kategorie_ridickeho_prukazu, datum_nastupu)
    VALUES (osoba_seq.CURRVAL, 'A, B, C, D', TO_DATE('15.03.2012', 'DD.MM.YYYY'));

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Hanna', 'Pelicova', TO_DATE('14.04.1963', 'DD.MM.YYYY'),
            '+420777452212', 'Piskova 584/2635 Brno-Bystrc', '5344448414/3600');
INSERT INTO zakaznik (osoba_id, adresa_sidla)
    VALUES (osoba_seq.CURRVAL, 'Hrazni 166 Brno');

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Lenka', 'Ryznarova', TO_DATE('12.04.1986', 'DD.MM.YYYY'),
            '+420255168166', 'Lipova 18 Brno-stred', '6000559968/3100');
INSERT INTO manager (osoba_id, vzdelani,  datum_nastupu)
    VALUES (osoba_seq.CURRVAL, 'VUT FIT - Bc', TO_DATE('08.10.2014', 'DD.MM.YYYY'));

INSERT INTO osoba (id, jmeno, prijmeni, datum_narozeni, telefon, adresa, cislo_uctu)
    VALUES (osoba_seq.NEXTVAL, 'Pavel', 'Havlickuv', TO_DATE('11.07.1976', 'DD.MM.YYYY'),
            '+420676268588', 'Smejkalova 1584/108 Brno', '5360589664/3100');
INSERT INTO ridic (osoba_id, kategorie_ridickeho_prukazu, datum_nastupu)
    VALUES (osoba_seq.CURRVAL, 'A, B, C, D', TO_DATE('10.04.2015', 'DD.MM.YYYY'));


------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky oblast_rozvozu
INSERT INTO oblast_rozvozu (id, ridic_id, nazev, cas_rozvozu)
    VALUES (oblast_rozvozu_seq.NEXTVAL, 4, 'Jih',  TO_DATE('29.03.2015 05:30', 'DD.MM.YYYY HH:MI'));

INSERT INTO oblast_rozvozu (id, ridic_id, nazev, cas_rozvozu)
    VALUES (oblast_rozvozu_seq.NEXTVAL, 5, 'Sever',  TO_DATE('29.03.2015 05:30', 'DD.MM.YYYY HH:MI'));

INSERT INTO oblast_rozvozu (id, ridic_id, nazev, cas_rozvozu)
    VALUES (oblast_rozvozu_seq.NEXTVAL, 5, 'Zapad',  TO_DATE('30.03.2015 05:00', 'DD.MM.YYYY HH:MI'));

INSERT INTO oblast_rozvozu (id, ridic_id, nazev, cas_rozvozu)
    VALUES (oblast_rozvozu_seq.NEXTVAL, 5, 'Vychod',  TO_DATE('30.03.2015 06:00', 'DD.MM.YYYY HH:MI'));
------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky adresa_doruceni
INSERT INTO adresa_doruceni (id, zakaznik_id, oblast_rozvozu_id, cislo_pobocky, mesto, ulice, cislo_domu)
    VALUES (adresa_doruceni_seq.NEXTVAL, 2, 1, 1, 'Brno', 'Valerinova', 86);

INSERT INTO adresa_doruceni (id, zakaznik_id, oblast_rozvozu_id, cislo_pobocky, mesto, ulice, cislo_domu)
    VALUES (adresa_doruceni_seq.NEXTVAL, 2, 2, 2, 'Brno', 'Polni', 103);

INSERT INTO adresa_doruceni (id, zakaznik_id, oblast_rozvozu_id, cislo_pobocky, mesto, ulice, cislo_domu)
    VALUES (adresa_doruceni_seq.NEXTVAL, 3, 3, 1, 'Brno', 'Javornicka', 45);

INSERT INTO adresa_doruceni (id, zakaznik_id, oblast_rozvozu_id, cislo_pobocky, mesto, ulice, cislo_domu)
    VALUES (adresa_doruceni_seq.NEXTVAL, 3, 4, 2, 'Brno', 'Slunecna', 205);

INSERT INTO adresa_doruceni (id, zakaznik_id, oblast_rozvozu_id, cislo_pobocky, mesto, ulice, cislo_domu)
    VALUES (adresa_doruceni_seq.NEXTVAL, 6, 4, 1, 'Brno', 'Piskova', 584);

------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky objednavka
INSERT INTO objednavka (id, datum_dodani, spusob_dodani, spusob_platby, datum_objednani, celkova_cena, adresa_doruceni_id, zakaznik_id)
    VALUES (objednavka_seq.NEXTVAL, TO_DATE('30.03.2015', 'DD.MM.YYYY'), 'na pobocku', 'na ucet', TO_DATE('25.03.2015', 'DD.MM.YYYY'), 1200.50, 1, 2);

INSERT INTO objednavka (id, datum_dodani, spusob_dodani, spusob_platby, datum_objednani, celkova_cena, adresa_doruceni_id, zakaznik_id)
    VALUES (objednavka_seq.NEXTVAL, TO_DATE('29.03.2015', 'DD.MM.YYYY'), 'na pobocku', 'kartou', TO_DATE('22.03.2015', 'DD.MM.YYYY'), 8000, 2, 2);

INSERT INTO objednavka (id, datum_dodani, spusob_dodani, spusob_platby, datum_objednani, celkova_cena, adresa_doruceni_id, zakaznik_id)
    VALUES (objednavka_seq.NEXTVAL, TO_DATE('30.03.2015', 'DD.MM.YYYY'), 'na pobocku', 'hotovost', TO_DATE('26.03.2015', 'DD.MM.YYYY'), 30000, 3, 3);

INSERT INTO objednavka (id, datum_dodani, spusob_dodani, spusob_platby, datum_objednani, celkova_cena, adresa_doruceni_id, zakaznik_id)
    VALUES (objednavka_seq.NEXTVAL, TO_DATE('29.03.2015', 'DD.MM.YYYY'), 'osobne', 'na ucet', TO_DATE('24.03.2015', 'DD.MM.YYYY'), 5500, NULL, 3);

------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky polozka_objednavky
INSERT INTO polozka_objednavky (poradove_id, pecivo_id, objednavka_id, pocet)
    VALUES (polozka_objednavky_seq.NEXTVAL, 1, 1, 500);

INSERT INTO polozka_objednavky (poradove_id, pecivo_id, objednavka_id, pocet)
    VALUES (polozka_objednavky_seq.NEXTVAL, 2, 3, 650);

INSERT INTO polozka_objednavky (poradove_id, pecivo_id, objednavka_id, pocet)
    VALUES (polozka_objednavky_seq.NEXTVAL, 3, 3, 650);

INSERT INTO polozka_objednavky (poradove_id, pecivo_id, objednavka_id, pocet)
    VALUES (polozka_objednavky_seq.NEXTVAL, 2, 4, 200);

INSERT INTO polozka_objednavky (poradove_id, pecivo_id, objednavka_id, pocet)
    VALUES (polozka_objednavky_seq.NEXTVAL, 3, 2, 400);

------------------------------------------------------------------------------------------------------------------------
--naplneni tabulky auto
INSERT INTO auto (spz, ridic_id, pojizdnost, nosnost, datum_tech_kontroly)
    VALUES ('AB3279', 4, 'Y', 1500, TO_DATE('07.08.2014', 'DD.MM.YYYY'));

INSERT INTO auto (spz, ridic_id, pojizdnost, nosnost, datum_tech_kontroly)
    VALUES ('MY6120', 5, 'Y', 2000, TO_DATE('25.08.2014', 'DD.MM.YYYY'));

INSERT INTO auto (spz, ridic_id, pojizdnost, nosnost, datum_tech_kontroly)
    VALUES ('OD3288', 4, 'Y', 1800, TO_DATE('10.07.2014', 'DD.MM.YYYY'));


------------------------------------------------------------------------------------------------------------------------
--triggery

COMMIT;

-- Zapnutie vyspupov na konzolu
SET serveroutput ON;

-- V pripade vynechania primary key sa doplni zo sekvencie
CREATE OR REPLACE TRIGGER SUROVINA_AUTO_INCREMENTER
    BEFORE INSERT ON surovina
    FOR EACH ROW
BEGIN
    IF :new.id IS NULL THEN 
        :new.id := surovina_seq.nextval;
    END IF;
END;
/

-- Demostracia triggeru na vzacnej surovine
INSERT INTO surovina (nazev, mnozstvi, datum_expirace, nakupni_cena)
    VALUES ('Plutonium', 500, TO_DATE('10.06.2599', 'DD.MM.YYYY'), 1000.80);

SELECT * FROM (SELECT r.osoba_id FROM ridic r LEFT JOIN auto a ON (r.osoba_id = a.ridic_id)
    GROUP BY r.osoba_id ORDER BY count(a.spz) ASC) WHERE ROWNUM = 1;

-- Trigger, ktory prideli nove auto ridicovi, ktory ich ma najmenej
CREATE OR REPLACE TRIGGER PRIDELENIE_AUTA
    BEFORE INSERT ON auto
    FOR EACH ROW
DECLARE
    osoba INTEGER;
BEGIN
    IF :new.ridic_id IS NULL THEN
        SELECT *
        INTO osoba
        FROM (SELECT r.osoba_id
            FROM ridic r LEFT JOIN auto a on (r.osoba_id = a.ridic_id)
            GROUP BY r.osoba_id ORDER BY count(a.spz), r.osoba_id ASC)
        WHERE ROWNUM = 1;
            :new.ridic_id := osoba;
    END IF;
END;
/

-- Demonstracia funkcnosti triggeru
INSERT INTO auto (spz, pojizdnost, nosnost, datum_tech_kontroly)
    VALUES ('XYZ1232', 'Y', 2000, TO_DATE('07.08.2015', 'DD.MM.YYYY'));

------------------------------------------------------------------------------------------------------------------------
--procedury

-- Procedura na znizenie ceny peciva
CREATE OR REPLACE PROCEDURE zniz_cenu_peciva
    (ID_PECIVA IN NUMBER, POCET_PERCENT IN NUMBER)
IS
    wrong_percentage EXCEPTION;
BEGIN
    IF POCET_PERCENT < 1 OR POCET_PERCENT > 100 THEN
        RAISE wrong_percentage;
    END IF;
    UPDATE pecivo SET cena = cena * (100 - POCET_PERCENT) / 100 WHERE id = ID_PECIVA;
    IF SQL%ROWCOUNT = 0 THEN
        DBMS_OUTPUT.PUT_LINE('Warning - no data updated!');
    END IF;
    COMMIT;
EXCEPTION
    WHEN wrong_percentage THEN
        DBMS_OUTPUT.PUT_LINE('Error - wrong percentage!');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error - generic error!');
END;
/

-- Znizenie ceny suroviny o 50%
SELECT nazev, cena FROM pecivo WHERE id = 20;
EXECUTE zniz_cenu_peciva(2, 50);
SELECT nazev, cena FROM pecivo WHERE id = 20;

-- Vybavenie objednavky, odpocitanie tovaru zo skladu a oznacenie objednavky ako odbavenej
CREATE OR REPLACE PROCEDURE vybav_objednavku 
    (obj_id IN NUMBER)
AS 
    CURSOR cursor_polozka IS SELECT * FROM polozka_objednavky
        WHERE objednavka_id = obj_id;
    polozka             cursor_polozka%ROWTYPE;
    vybavena_o          objednavka.vybavena%TYPE;
    zly_pocet_kusov     EXCEPTION;
    odbavena_objednavka EXCEPTION;
BEGIN
    -- Zistime, ci objednavka je uz vybavena, ak ano, vyvolame chybu
    SELECT vybavena INTO vybavena_o
        FROM objednavka WHERE id = obj_id;
    IF vybavena_o != 'N' THEN
        raise odbavena_objednavka;
    END IF;

    -- Pre kazdu polozku sa snazime odpocitat potrebny pocet peciva
    OPEN cursor_polozka;
    LOOP
        FETCH cursor_polozka INTO polozka;
        EXIT WHEN cursor_polozka%NOTFOUND;

        UPDATE pecivo p SET mnozstvi = mnozstvi - polozka.pocet WHERE id = polozka.pecivo_id;
        DBMS_OUTPUT.put_line('Expandovano ' || TO_CHAR(polozka.pocet) || ' peciva s id: ' || TO_CHAR(polozka.pecivo_id));
    END LOOP;
    CLOSE cursor_polozka;

    -- Ak sa nam podari, oznacime objednavku ako vybavenu a commitneme
    UPDATE objednavka SET vybavena = 'Y' WHERE id = obj_id;
    DBMS_OUTPUT.PUT_LINE('Objednavka odbavena!');
    COMMIT;

EXCEPTION
    WHEN odbavena_objednavka THEN
        DBMS_OUTPUT.PUT_LINE('Error - objednavka uz bola odbavena!');
        ROLLBACK;
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Error - data nenajdene! Predchadzajuce akcie zrusene!');
        ROLLBACK;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(
            'Error - pozadovany pocet kusov peciva s id ' ||
            TO_CHAR(polozka.pecivo_id) ||
            ' nie je na sklade! Predchadzajuce akcie zrusene!'
        );
        ROLLBACK;
END;
/

-- Ukazka odbavenia procedury
-- Neexistujuca objednavka
EXECUTE vybav_objednavku(10);
-- Prvy krat uspesne vybavime
EXECUTE vybav_objednavku(3);
-- Dalsi krat nam uz vyabvit nedovoli
EXECUTE vybav_objednavku(3);

------------------------------------------------------------------------------------------------------------------------
--explain plan

-- Explain plan
EXPLAIN PLAN FOR 
SELECT s.nazev, s.datum_expirace, COUNT(pe.nazev) 
FROM surovina s 
LEFT JOIN potreba po ON (s.id=po.id_suroviny) 
INNER JOIN pecivo pe ON (pe.id=po.id_peciva) 
WHERE s.nazev = 'Rostlinny tuk na peceni' 
GROUP BY s.nazev, s.datum_expirace;
SELECT PLAN_TABLE_OUTPUT FROM TABLE (dbms_xplan.display());

-- PRIDANIE INDEXU
CREATE INDEX SUROVINA_NAME ON surovina (nazev);
CREATE INDEX SUROVINA_INDEX ON potreba (id_suroviny);

-- Zavolanie explain planu znovu
EXPLAIN PLAN FOR 
SELECT s.nazev, s.datum_expirace, COUNT(pe.nazev) 
FROM surovina s 
LEFT JOIN potreba po ON (s.id=po.id_suroviny) 
INNER JOIN pecivo pe ON (pe.id=po.id_peciva) 
WHERE s.nazev = 'Rostlinny tuk na peceni' 
GROUP BY s.nazev, s.datum_expirace;
SELECT PLAN_TABLE_OUTPUT FROM TABLE (dbms_xplan.display());

------------------------------------------------------------------------------------------------------------------------
-- Materializovany pohlad 
CREATE MATERIALIZED VIEW aktualne_zisky_objednavok
NOLOGGING
CACHE
BUILD IMMEDIATE
ENABLE QUERY REWRITE
AS
    SELECT o.id as id_objednavky, os.jmeno, os.prijmeni,
           z.adresa_sidla, o.celkova_cena,
           SUM(p.cena*po.pocet) AS odporucana_cena,
           (ROUND((o.celkova_cena/SUM(p.cena*po.pocet))*100-100, 2)) || '%' as zisk
    FROM objednavka o
    JOIN zakaznik z on (z.osoba_id=o.zakaznik_id)
    JOIN osoba os on (os.id=o.zakaznik_id)
    LEFT JOIN polozka_objednavky po on(po.objednavka_id=o.id)
    JOIN pecivo p ON (p.id=po.pecivo_id)
    GROUP BY o.id, o.celkova_cena, os.jmeno, os.prijmeni, z.adresa_sidla
    ORDER BY o.id ASC;

-- Otestovanie pohladu
SELECT * FROM AKTUALNE_ZISKY_OBJEDNAVOK;

-- Refreshnem material view, mohla by byt procedura, on demand kvoli vykonnosti
EXEC DBMS_MVIEW.REFRESH('AKTUALNE_ZISKY_OBJEDNAVOK');

------------------------------------------------------------------------------------------------------------------------

-- Granty pre druheho clena tymu
GRANT ALL ON pecivo TO xgavry00;
GRANT ALL ON surovina TO xgavry00;
GRANT ALL ON potreba TO xgavry00;
GRANT ALL ON osoba TO xgavry00;
GRANT ALL ON manager TO xgavry00;
GRANT ALL ON ridic TO xgavry00;
GRANT ALL ON zakaznik TO xgavry00;
GRANT ALL ON oblast_rozvozu TO xgavry00;
GRANT ALL ON adresa_doruceni TO xgavry00;
GRANT ALL ON objednavka TO xgavry00;
GRANT ALL ON polozka_objednavky TO xgavry00;
GRANT ALL ON auto TO xgavry00;
GRANT ALL ON aktualne_zisky_objednavok TO xgavry00;
COMMIT;
