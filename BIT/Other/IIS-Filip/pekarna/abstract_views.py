from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.core.exceptions import FieldError
from .base_forms import SearchForm
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
import operator


def is_popup(request):
    return "popup" in request.GET


class AbstractView():
    DEFAULT_ORDER = "-pk"

    def __init__(self, request):
        self.request = request
        self.order = self.get_order()
        self.objs = self.get_queryset()
        self.form = None
        self.params = self.__get_params()
        self.search_value = self.__search_data()
        self.__sort_queryset()

    def get_queryset(self):
        """ Need override """
        pass

    def get_order(self):
        return self.DEFAULT_ORDER

    def get_search_list(self):
        """ Need override """
        pass

    def get_template(self):
        """ Need override """
        pass

    def get_popup_templates(self):
        return self.get_template().rstrip(".html") + "_popup.html"

    def __get_params(self):
        return self.request.GET.copy()

    def __sort_queryset(self):
        order_key = self.request.GET.get("order_by", self.order)

        try:
            self.objs = self.objs.order_by(order_key)
            len(self.objs)  # Fake execution of query for error check
        except FieldError:
            self.objs = self.objs.order_by(self.DEFAULT_ORDER)

    def __get_search_query(self, search_value):
        """ Generate query for the search """
        search_list = self.get_search_list()
        raw_queries = list()
        for o in search_list:
            attr = o + "__contains"
            raw_queries.append(Q(**{attr: search_value}))

        return reduce(operator.or_, raw_queries)

    def __search_data(self):
        self.form = SearchForm(self.request.POST)

        if self.request.method == "POST":
            if self.form.is_valid():
                search_value = self.form.cleaned_data.get("search")
                query = self.__get_search_query(search_value)

                self.objs = self.objs.filter(query)
                self.params["search_value"] = search_value
            else:
                if 'search_value' in self.params:
                    del self.params["search_value"]

        search_value = self.params.get("search_value", None)
        if search_value is not None:
            query = self.__get_search_query(search_value)
            self.objs = self.objs.filter(query)

        return search_value

    def render(self):
        data_length = len(self.objs)

        if "page" in self.params.keys():
            del self.params["page"]

        paginator = Paginator(self.objs, 25)
        page = self.request.GET.get("page")
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page
            cars = paginator.page(1)
        except EmptyPage:
            # If page is out of range deliver last page of results
            cars = paginator.page(paginator.num_pages)

        context_dict = {
            "params": self.params,
            "queryset": cars,
            "form": self.form,
            "search_value": self.search_value,
            "data_length": data_length
        }

        template = self.get_template()
        popup_templates = self.get_popup_templates()

        if "popup" in self.params:
            if isinstance(popup_templates, dict):
                return render(self.request, popup_templates[self.params["popup"]], context_dict)
            else:
                return render(self.request, popup_templates, context_dict)
        else:
            return render(self.request, template, context_dict)


class ManageAbstractView():

    def __init__(self, request, instance=None):
        self.request = request
        self.instance = instance
        self.Form = self.get_form()
        self.ChildForm = self.get_child_form()
        self.Formset = self.get_formset()
        self.reverse_url = self.get_reverse_url()
        self.template = self.get_template()

    def get_form(self):
        """ Need override """
        pass

    def get_template(self):
        """ Need override """
        pass

    def get_reverse_url(self):
        """ Need override """
        pass

    def get_formset(self):
        """ Need override """
        pass

    def get_child_form(self):
        """ Need override """
        pass

    def handle_valid_form(self, form):
        return form.save()

    def handle_valid_formset(self, formset, instance):
        formset.instance = instance
        return formset.save()

    def get_child_instance(self):
        """ Need override """
        pass

    def get_created_msg(self):
        """ Can be overriden """
        return str(_("Object has been created")) + " - " + str(self.instance)

    def get_changed_msg(self):
        """ Can be overriden """
        return str(_("Object has been changed")) + " - " + str(self.instance)

    def render(self):
        formset = None
        child_form = None

        if self.request.method == "POST":
            form = self.Form(self.request.POST, instance=self.instance)

            if self.Formset is not None:
                formset = self.Formset(self.request.POST, instance=self.instance)

            if self.ChildForm is not None:
                child_form = self.ChildForm(self.request.POST, instance=self.get_child_instance())

            if (formset is None or formset.is_valid()) and \
                    (child_form is None or child_form.is_valid()) and \
                    form.is_valid():

                self.instance = self.handle_valid_form(form)

                if formset is not None:
                    self.handle_valid_formset(formset, self.instance)

                if child_form is not None:
                    child_form.save()

                # If object was just created show created msg
                if self.instance:
                    msg = self.get_changed_msg()
                else:
                    msg = self.get_created_msg()
                messages.add_message(self.request, messages.INFO, msg)

                return HttpResponseRedirect(reverse(self.reverse_url))
        else:
            form = self.Form(instance=self.instance)

            if self.Formset is not None:
                formset = self.Formset(instance=self.instance)

            if self.ChildForm is not None:
                child_form = self.ChildForm(instance=self.get_child_instance())

        data = {
            "form": form,
            "formset": formset,
            "child_form": child_form
        }

        return render(self.request, self.template, data)
