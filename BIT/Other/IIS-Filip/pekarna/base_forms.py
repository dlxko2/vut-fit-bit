from django import forms


class SearchForm(forms.Form):

    search = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={
            "class": "mdl-textfield__input",
            "pattern": ".{0,50}",
            "type": "text"
        }),)
