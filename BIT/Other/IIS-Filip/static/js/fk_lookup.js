var targetInputFk;
var targetInputPrice;
var targetInputContent;
initialize();

function initialize() {
  if (window.opener != null && !window.opener.closed) {
    targetInputFk = $(window.opener.clickedFkEl).siblings("input.fk");
    targetInputPrice = $(window.opener.clickedFkEl).siblings("input.price")
    targetInputContent = $(window.opener.clickedFkEl).siblings("input.fk_content");
    $(".fk_link").click(function(event) {
      event.preventDefault();

      if (targetInputPrice.length > 0) {
        console.log($(this).attr("price"))
        targetInputPrice.val($(this).attr("price"))
      }

      targetInputFk.val($(this).attr("href"));
      targetInputContent.val($(this).text());

      if (typeof window.opener.price_sum == 'function') {
        // Check if function exist
        window.opener.price_sum(); // Notify change
      }

      window.close();  // Close the window
    });
  }
}
