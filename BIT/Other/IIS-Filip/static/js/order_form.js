var sum_price = $("#id_sum_price")

$("table").change(function(event) {
  price_sum();
});

function price_sum() {
  var trs = $('form tbody tr.dynamic-form');
  var sum = 0;

  $.each(trs, function(index, value) {
    var price = parseFloat($(value).find(".price").val().replace(",", "."));
    var amount = parseFloat($(value).find(".amount").val().replace(",", "."));

    sum += (price * amount)
  });

  sum_price.val(sum.toFixed(2));
}

price_sum();
