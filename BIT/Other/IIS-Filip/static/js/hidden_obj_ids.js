var hidden_input = $("#hidden");

$("#delete").click(function(e) {
    fill_hidden_input();
    return confirm("Do you really want to delete?");
})

function fill_hidden_input()
{
    var id_list = [];
    var check_boxes = $(".mdl-checkbox.mdl-js-checkbox:not(:first)[class*='is-checked']")
    var array_len = check_boxes.length

    for (var i = array_len - 1; i >= 0; i--) {
        var obj_id = $(check_boxes[i]).parent().siblings().first().children().attr("href")
        id_list.push(obj_id);
    };

    hidden_input.val(id_list.join());
}

$(".mdl-checkbox.mdl-js-checkbox:not(:first)").change(function(event) {
    console.log(event);
    console.log("event");
});
