var popup;
var clickedFkEl;

function showFkWindow(url, window_name, width, height) {
    var left = (screen.width/2)-(width/2);
    var top = (screen.height/2)-(height/2);
    popup = window.open(url, window_name, "width="+width+",height="+height+",top="+top+",left="+left+",toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no");
    popup.focus();
}

$(".alert").click(function(ev) {
    var type_of_action = $(this).text();
    var msg = "Do you really want to " + type_of_action.toLowerCase() + " order?";
    return confirm(msg);
});

$("a.fk_lookup").click(function(event) {
    event.preventDefault();
    showFkWindow(this.href, null, 800, 600);
    clickedFkEl = this;
});

if ($("#messages").length) {
    var message_div = $("#messages");

    if (!message_div.is(":empty")) {
        message_div.fadeIn(400);
        setTimeout(function() {
            message_div.fadeOut(500);
        }, 1700);
    }
}
