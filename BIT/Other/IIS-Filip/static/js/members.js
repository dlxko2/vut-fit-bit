var customer_div = $("#customer_div");
var driver_div = $("#driver_div");
var manager_div = $("#manager_div");

function show_form(time) {
  var time = typeof time !== 'undefined' ? time : 0;

  var type = member_type.val()
  selected_type = $("#id_selected_type");
  selected_type.val(type);

  customer_div.hide(time);
  driver_div.hide(time);
  manager_div.hide(time);

  if (type === "customer") {
    customer_div.show(time);
  } else if (type === "driver") {
    driver_div.show(time);
  } else if (type === "manager") {
    manager_div.show(time);
  }
}

member_type = $("#id_member_type");
member_type.change(function(event) {
  show_form("slow");
});

show_form();
