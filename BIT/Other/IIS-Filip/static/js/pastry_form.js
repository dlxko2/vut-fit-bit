var sum_price = $("#id_sum_price")
var units_per_batch_field = $('#id_units_per_batch');

units_per_batch_field.change(function(event) {
  price_sum();
});

$("table").change(function(event) {
  price_sum();
});

function price_sum() {
  var trs = $('form tbody tr.dynamic-form');
  var sum = 0;
  var units_per_batch = parseInt(units_per_batch_field.val());

  $.each(trs, function(index, value) {
    var price = parseFloat($(value).find(".price").val().replace(",", "."));
    var amount = parseFloat($(value).find(".amount").val().replace(",", "."));
    var single_price = (price * amount) / units_per_batch;

    sum += single_price
  });

  sum_price.val(sum.toFixed(2));
}

price_sum();
