# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
        ('shipping', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name', db_index=True)),
                ('expiration_date', models.DateTimeField(verbose_name='Expiration date')),
                ('purchase_price', models.FloatField(verbose_name='Purchase price', validators=[django.core.validators.MinValueValidator(0)])),
                ('amount', models.PositiveSmallIntegerField(verbose_name='Amount')),
            ],
            options={
                'verbose_name': 'Ingredient',
                'verbose_name_plural': 'Ingredients',
            },
        ),
        migrations.CreateModel(
            name='IngredientNeedRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.FloatField(verbose_name='Amount in grams', validators=[django.core.validators.MinValueValidator(1)])),
                ('ingredient', models.ForeignKey(to='shopping.Ingredient')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arrival_date', models.DateTimeField(verbose_name='Arrival date')),
                ('delivery_method', models.CharField(default=b'P', max_length=1, verbose_name='Delivery method', choices=[(b'P', 'Personally'), (b'S', 'In the store')])),
                ('payment_method', models.CharField(default=b'D', max_length=1, verbose_name='Payment method', choices=[(b'D', 'Cash on delivery'), (b'C', 'Credit card'), (b'T', 'Bank transfer')])),
                ('order_date', models.DateTimeField(verbose_name='Order date')),
                ('paid', models.BooleanField(default=False, verbose_name='Is paid')),
                ('completed', models.BooleanField(default=False, verbose_name='Is completed')),
                ('price', models.FloatField(verbose_name='Order price', validators=[django.core.validators.MinValueValidator(0)])),
                ('customer', models.ForeignKey(to='members.Customer')),
                ('delivery_address', models.ForeignKey(to='shipping.DeliveryAddress', blank=True)),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='OrderItemRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveSmallIntegerField(verbose_name='Amount', validators=[django.core.validators.MinValueValidator(1)])),
                ('order', models.ForeignKey(to='shopping.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Pastry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name', db_index=True)),
                ('expiration_date', models.DateTimeField(verbose_name='Expiration date')),
                ('price', models.FloatField(verbose_name='Price', validators=[django.core.validators.MinValueValidator(0)])),
                ('alergens', models.CharField(max_length=150, verbose_name='Alergens', blank=True)),
                ('amount', models.PositiveSmallIntegerField(verbose_name='Amount')),
                ('ingredients', models.ManyToManyField(to='shopping.Ingredient', through='shopping.IngredientNeedRelation')),
            ],
            options={
                'verbose_name': 'Ingredient',
                'verbose_name_plural': 'Ingredients',
            },
        ),
        migrations.AddField(
            model_name='orderitemrelation',
            name='pastry',
            field=models.ForeignKey(to='shopping.Pastry'),
        ),
        migrations.AddField(
            model_name='order',
            name='items',
            field=models.ManyToManyField(to='shopping.Pastry', through='shopping.OrderItemRelation'),
        ),
        migrations.AddField(
            model_name='ingredientneedrelation',
            name='pastry',
            field=models.ForeignKey(to='shopping.Pastry'),
        ),
        migrations.AlterUniqueTogether(
            name='ingredientneedrelation',
            unique_together=set([('pastry', 'ingredient')]),
        ),
    ]
