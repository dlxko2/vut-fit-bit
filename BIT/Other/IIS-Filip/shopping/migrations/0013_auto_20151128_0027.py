# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0012_auto_20151128_0024'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pastry',
            old_name='alergens',
            new_name='allergens',
        ),
    ]
