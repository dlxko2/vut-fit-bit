# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0011_order_completed_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pastry',
            name='alergens',
            field=models.CharField(max_length=150, verbose_name='Allergens', blank=True),
        )
    ]
