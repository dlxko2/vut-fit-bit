# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredient',
            name='name',
            field=models.CharField(unique=True, max_length=50, verbose_name='Name', db_index=True),
        ),
        migrations.AlterField(
            model_name='pastry',
            name='name',
            field=models.CharField(unique=True, max_length=50, verbose_name='Name', db_index=True),
        ),
    ]
