# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0003_auto_20151013_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredient',
            name='amount',
            field=models.FloatField(verbose_name='Amount in kg'),
        ),
        migrations.AlterField(
            model_name='ingredientneedrelation',
            name='amount',
            field=models.FloatField(verbose_name='Amount in kg', validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
