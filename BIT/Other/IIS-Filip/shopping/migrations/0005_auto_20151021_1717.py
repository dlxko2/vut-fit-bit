# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0004_auto_20151017_1402'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ingredient',
            name='expiration_date',
        ),
        migrations.RemoveField(
            model_name='pastry',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='pastry',
            name='expiration_date',
        ),
    ]
