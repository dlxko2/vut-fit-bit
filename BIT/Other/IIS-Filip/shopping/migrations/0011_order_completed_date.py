# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0010_auto_20151124_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='completed_date',
            field=models.DateField(null=True, verbose_name='Date completed', blank=True),
        ),
    ]
