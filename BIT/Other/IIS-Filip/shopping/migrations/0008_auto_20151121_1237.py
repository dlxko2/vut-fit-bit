# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0007_pastry_units_per_batch'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='arrival_date',
            field=models.DateField(verbose_name='Arrival date', blank=True),
        ),
    ]
