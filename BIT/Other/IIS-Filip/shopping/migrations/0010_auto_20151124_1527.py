# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0009_auto_20151121_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredientneedrelation',
            name='ingredient',
            field=models.ForeignKey(to='shopping.Ingredient', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(to='members.Customer', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='order',
            name='delivery_address',
            field=models.ForeignKey(to='shipping.DeliveryAddress', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='orderitemrelation',
            name='pastry',
            field=models.ForeignKey(to='shopping.Pastry', on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
