# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0002_auto_20151012_2130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredient',
            name='expiration_date',
            field=models.DateField(verbose_name='Expiration date'),
        ),
        migrations.AlterField(
            model_name='order',
            name='arrival_date',
            field=models.DateField(verbose_name='Arrival date'),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_date',
            field=models.DateField(verbose_name='Order date'),
        ),
        migrations.AlterField(
            model_name='pastry',
            name='expiration_date',
            field=models.DateField(verbose_name='Expiration date'),
        ),
    ]
