# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0008_auto_20151121_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='arrival_date',
            field=models.DateField(null=True, verbose_name='Arrival date', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='delivery_address',
            field=models.ForeignKey(to='shipping.DeliveryAddress'),
        ),
    ]
