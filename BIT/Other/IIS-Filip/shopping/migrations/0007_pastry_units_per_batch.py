# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0006_auto_20151021_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='pastry',
            name='units_per_batch',
            field=models.PositiveIntegerField(default=100, verbose_name='Units per batch', validators=[django.core.validators.MinValueValidator(1)]),
            preserve_default=False,
        ),
    ]
