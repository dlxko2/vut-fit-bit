from django.shortcuts import get_object_or_404
from .models import Ingredient, Pastry, IngredientNeedRelation, Order, OrderItemRelation
from .forms import IngredientForm, PastryForm, OrderForm, OrderArrivalDateForm
from pekarna.abstract_views import AbstractView, ManageAbstractView, is_popup
from django.forms.models import inlineformset_factory
from django.db.models import Count, Sum
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse
from django import forms
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.db import transaction
from collections import defaultdict
from datetime import datetime
from shopping.models import DeliveryAddress


@login_required
def ingredients(request):
    if request.user.is_manager() or request.user.is_admin() or is_popup(request):
        ingredientView = IngredientView(request)
        return ingredientView.render()
    return HttpResponseForbidden()


@login_required
def add_ingredient(request):
    if request.user.is_manager() or request.user.is_admin():
        addIngredientView = AddIngredientView(request)
        return addIngredientView.render()
    return HttpResponseForbidden()


@login_required
def change_ingredient(request, id):
    if request.user.is_manager() or request.user.is_admin():
        ingredient = get_object_or_404(Ingredient, id=id)

        changeIngredientView = ChangeIngredientView(request, ingredient)
        return changeIngredientView.render()
    return HttpResponseForbidden()


class IngredientView(AbstractView):

    def get_queryset(self):
        return Ingredient.objects.all()

    def get_search_list(self):
        return ["name"]

    def get_template(self):
        return "shopping/ingredients.html"


class AddIngredientView(ManageAbstractView):

    def get_form(self):
        return IngredientForm

    def get_template(self):
        return "shopping/add_ingredient.html"

    def get_reverse_url(self):
        return "ingredients"


class ChangeIngredientView(AddIngredientView):
    def get_template(self):
        return "shopping/change_ingredient.html"


@login_required
def pastries(request):
    ingredientView = PastryView(request)
    return ingredientView.render()


@login_required
def add_pastry(request):
    if request.user.is_admin() or request.user.is_manager():
        addPastryView = AddPastryView(request)
        return addPastryView.render()
    return HttpResponseForbidden()


@login_required
def change_pastry(request, id):
    pastry = get_object_or_404(Pastry, id=id)

    changePastryView = ChangePastryView(request, pastry)
    return changePastryView.render()


class PastryView(AbstractView):

    def get_queryset(self):
        return Pastry.objects.annotate(Count('ingredients')).all()

    def get_search_list(self):
        return ["name", "allergens"]

    def get_template(self):
        return "shopping/pastries.html"


class AddPastryView(ManageAbstractView):

    def get_form(self):
        return PastryForm

    def get_template(self):
        return "shopping/add_pastry.html"

    def get_reverse_url(self):
        return "pastries"

    def get_formset(self):
        return inlineformset_factory(
            Pastry, Pastry.ingredients.through,
            fields=("ingredient", "amount",),
            widgets={
                "ingredient": forms.TextInput(attrs={
                    "class": "mdl-textfield__input fk"
                }),
                "amount": forms.TextInput(attrs={
                    "class": "mdl-textfield__input amount",
                }),
            },
            extra=0,
            min_num=1,
            validate_min=True,
            can_delete=True)


class ChangePastryView(AddPastryView):
    def get_template(self):
        return "shopping/change_pastry.html"


@login_required
def orders(request):
    ingredientView = OrderView(request)
    return ingredientView.render()


@login_required
def add_order(request):
    if not request.user.is_driver():
        addOrderView = AddOrderGeneralView(request)
        return addOrderView.render()
    return HttpResponseForbidden()


@login_required
def change_order(request, id):
    order = get_object_or_404(Order, id=id)

    if request.user.is_customer() and order.customer != request.user.customer_set:
        return HttpResponseForbidden()

    if request.user.is_customer():
        allowed_orders = Order.objects.filter(customer=request.user.customer_set)

        if order not in allowed_orders:
            return HttpResponseForbidden()

    elif request.user.is_driver():
        driver = request.user.driver_set
        distribution_areas = driver.distributionarea_set.all()
        delivery_addresses = DeliveryAddress.objects.filter(distribution_area__in=distribution_areas)
        allowed_orders = Order.objects.filter(delivery_address=delivery_addresses, completed=False, paid=True)

        if order not in allowed_orders:
            return HttpResponseForbidden()

    changeOrderView = ChangeOrderView(request, order)
    return changeOrderView.render()


@login_required
def delete_orders(request):
    if request.user.is_manager() or request.user.is_admin() or request.user.is_customer():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shopping/orders/")

        obj_ids = obj_ids.split(",")
        objects = Order.objects.filter(id__in=obj_ids)

        try:
            if objects.filter(paid=True).exists():
                raise BaseException()
            objects.delete()
            msg = _("Selected orders were deleted!")
        except:
            msg = _("Some orders could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shopping/orders/")
    return HttpResponseForbidden()


@login_required
def delete_pastries(request):
    if request.user.is_admin() or request.user.is_manager():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shopping/pastries/")

        obj_ids = obj_ids.split(",")
        objects = Pastry.objects.filter(id__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected pastries were deleted!")
        except:
            msg = _("Some pastries could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shopping/pastries/")
    return HttpResponseForbidden()


@login_required
def delete_ingredients(request):
    if request.user.is_admin() or request.user.is_manager():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shopping/ingredients/")

        obj_ids = obj_ids.split(",")
        objects = Ingredient.objects.filter(id__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected ingredients were deleted!")
        except:
            msg = _("Some ingredients could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shopping/ingredients/")
    return HttpResponseForbidden()


class OrderView(AbstractView):

    def delete(self):
        return True

    def get_queryset(self):
        if self.request.user.is_customer():
            return Order.objects.filter(customer=self.request.user.customer_set)
        elif self.request.user.is_driver():
            driver = self.request.user.driver_set
            distribution_areas = driver.distributionarea_set.all()
            delivery_addresses = DeliveryAddress.objects.filter(distribution_area__in=distribution_areas)

            return Order.objects.filter(delivery_address=delivery_addresses, completed=False, paid=True)
        else:
            return Order.objects.all()

    def get_search_list(self):
        return ["customer__person__last_name", "customer__person__first_name",
                "delivery_address__city", "delivery_address__street",
                "delivery_address__house_number", "id"]

    def get_template(self):
        return "shopping/orders.html"


class AddOrderGeneralView(ManageAbstractView):

    def get_form(self):
        return OrderForm

    def get_template(self):
        return "shopping/add_order.html"

    def get_reverse_url(self):
        return "orders"

    def get_formset(self):
        return inlineformset_factory(
            Order, Order.items.through,
            fields=("pastry", "amount",),
            widgets={
                "pastry": forms.TextInput(attrs={
                    "class": "mdl-textfield__input fk"
                }),
                "amount": forms.TextInput(attrs={
                    "class": "mdl-textfield__input amount",
                }),
            },
            extra=0,
            min_num=1,
            validate_min=True,
            can_delete=True)

    def calculate_pastry_sum(self, cleaned_data):
        sum_price = 0

        for obj in cleaned_data:
            if any(obj) is False or obj["DELETE"] == True:
                # Check if dictionary is empty
                continue

            pastry_price = int(obj["pastry"].price)
            amount = int(obj["amount"])
            sum_price += pastry_price * amount

        return sum_price

    def render(self):
        form = None
        formset = None

        if self.request.method == "POST":
            updated_data = self.request.POST.copy()
            updated_data.update({"price": 0})

            # If customer is creating order, fill the customer field for him
            if self.request.user.is_customer():
                customer = self.request.user.customer_set.person.username
                # print customer.person.username
                updated_data.update({"customer": customer})

            form = self.Form(updated_data, instance=self.instance)
            formset = self.Formset(self.request.POST, instance=self.instance)

            if formset.is_valid() and form.is_valid():
                # Calculate complete price of the order
                sum_price = self.calculate_pastry_sum(formset.cleaned_data)

                # Fill the form with the price
                updated_data.update({"price": sum_price})
                form = self.Form(updated_data, instance=self.instance)
                form.is_valid()

                instance = form.save()
                formset.instance = instance
                formset.save()
                return HttpResponseRedirect(reverse(self.reverse_url))

        else:
            form = self.Form(instance=self.instance)
            formset = self.Formset(instance=self.instance)

        data = {
            "form": form,
            "formset": formset,
        }

        return render(self.request, self.template, data)


class ChangeOrderView(AddOrderGeneralView):
    def get_template(self):
        return "shopping/change_order.html"

    def get_formset(self):
        return inlineformset_factory(
            Order, Order.items.through,
            fields=("pastry", "amount",),
            widgets={
                "pastry": forms.TextInput(attrs={
                    "class": "mdl-textfield__input fk"
                }),
                "amount": forms.TextInput(attrs={
                    "class": "mdl-textfield__input amount",
                }),
            },
            extra=0,
            min_num=1,
            validate_min=True,
            can_delete=False)


@login_required
def change_arrival_date(request, id):
    if not request.user.is_manager() and not request.user.is_driver():
        order_url = "/shopping/orders/%s" % id
        form = None
        try:
            order = Order.objects.get(id=id)
        except Order.DoesNotExist:
            return HttpResponseRedirect(order_url)

        # If the request is a HTTP POST, try to pull out the relevant information.
        if request.method == 'POST':
            form = OrderArrivalDateForm(request.POST, instance=order)

            if form.is_valid():
                form.save()
                msg = _("Arrival date was changed")
                messages.add_message(request, messages.INFO, msg)
                return HttpResponseRedirect(order_url)

        form = OrderArrivalDateForm(instance=order)
        return render(request, 'shopping/change_arrival_date.html', {"form": form})
    return HttpResponseForbidden()


@login_required
def complete_order(request, id):
    if not request.user.is_customer():
        order_url = "/shopping/orders/%s" % id

        order = Order.objects.get(id=id)
        order_relation = OrderItemRelation.objects.filter(order=order).select_related("pastry")

        # If user is driver, complete only if it's order related to driver
        if request.user.is_driver():
            driver = request.user.driver_set
            distribution_areas = driver.distributionarea_set.all()
            delivery_addresses = DeliveryAddress.objects.filter(distribution_area__in=distribution_areas)
            allowed_orders = Order.objects.filter(delivery_address=delivery_addresses)

            if order not in allowed_orders:
                return HttpResponseForbidden()

        if not order.paid:
            msg = _("Order is not paid, please pay order before completion.")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect(order_url)

        # Make summary of ingredients and their amounts
        summary = defaultdict(int)
        for o_rel in order_relation:
            ingredient_rel = IngredientNeedRelation.objects \
                .filter(pastry=o_rel.pastry).select_related("ingredient")

            for i_rel in ingredient_rel:
                summary[i_rel.ingredient] += float(o_rel.amount) / o_rel.pastry.units_per_batch * i_rel.amount

        # Determine if order could be completed
        can_complete_order = True
        for i in summary:
            if i.amount < summary[i]:
                can_complete_order = False

        if request.method != "POST":
            data = {
                "summary": dict(summary),
                "can_complete_order": can_complete_order,
                "order_id": order.id
            }
            return render(request, "shopping/complete_order.html", data)
        else:
            if can_complete_order:
                # Complete order as transaction
                with transaction.atomic():
                    for ingredient in summary:
                        ingredient.amount -= summary[ingredient]
                        ingredient.save()

                    order.completed = True
                    order.completed_date = datetime.today()
                    order.save()

                msg = _("Order was completed successfully")
                messages.add_message(request, messages.INFO, msg)
            else:
                msg = _("Not enough supply to complete order!")
                messages.add_message(request, messages.INFO, msg)

            return HttpResponseRedirect(order_url)
    return HttpResponseForbidden()


@login_required
def pay_order(request, id):
    if request.user.is_manager() or request.user.is_admin():
        url = "/shopping/orders/%s" % id
        order = Order.objects.get(id=id)

        if order.paid:
            msg = _("Order is already paid!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect(url)

        order.paid = True
        order.save()

        msg = _("Order was paid successfully")
        messages.add_message(request, messages.INFO, msg)
        return HttpResponseRedirect(url)
    return HttpResponseForbidden()
