from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from shipping.models import DeliveryAddress
from members.models import Customer


class Ingredient(models.Model):
    name = models.CharField(_("Name"), max_length=50, db_index=True, unique=True)
    purchase_price = models.FloatField(_("Purchase price"), validators=[MinValueValidator(0)])
    amount = models.FloatField(_("Amount in kg"))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Ingredient")
        verbose_name_plural = _("Ingredients")


class Pastry(models.Model):
    name = models.CharField(_("Name"), max_length=50, db_index=True, unique=True)
    price = models.FloatField(_("Price"), validators=[MinValueValidator(0)])
    allergens = models.CharField(_("Allergens"), max_length=150, blank=True)
    units_per_batch = models.PositiveIntegerField(_("Units per batch"), validators=[MinValueValidator(1)])
    ingredients = models.ManyToManyField(Ingredient, through='IngredientNeedRelation')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Ingredient")
        verbose_name_plural = _("Ingredients")


class IngredientNeedRelation(models.Model):
    pastry = models.ForeignKey(Pastry)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    amount = models.FloatField(_("Amount in kg"), validators=[MinValueValidator(1)])

    class Meta:
        unique_together = ['pastry', 'ingredient']


class Order(models.Model):
    IN_PERSON = "P"
    IN_STORE = "S"
    DELIVERY_METHOD = (
        (IN_PERSON, _("Personally")),
        (IN_STORE, _("In the store"))
    )

    DELIVERY_CASH = "D"
    CREDIT_CARD = "C"
    BANK_TRANSFER = "T"
    PAYMENT_METHOD = (
        (DELIVERY_CASH, _("Cash on delivery")),
        (CREDIT_CARD, _("Credit card")),
        (BANK_TRANSFER, _("Bank transfer"))
    )

    arrival_date = models.DateField(_("Arrival date"), null=True, blank=True)
    completed_date = models.DateField(_("Date completed"), null=True, blank=True)
    order_date = models.DateField(_("Order date"), auto_now_add=True)
    delivery_method = models.CharField(_("Delivery method"), max_length=1, choices=DELIVERY_METHOD, default=IN_PERSON)
    payment_method = models.CharField(_("Payment method"), max_length=1, choices=PAYMENT_METHOD, default=DELIVERY_CASH)
    delivery_address = models.ForeignKey(DeliveryAddress, verbose_name=_("Delivery address"), on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, verbose_name=_("Customer"), on_delete=models.PROTECT)
    paid = models.BooleanField(_("Is paid"), default=False)
    completed = models.BooleanField(_("Is completed"), default=False)
    price = models.FloatField(_("Order price"), validators=[MinValueValidator(0)])
    items = models.ManyToManyField(Pastry, through='OrderItemRelation')

    def __unicode__(self):
        return unicode(_("Order n.")) + str(self.id)

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")

    def get_delivery_method(self):
        return dict(self.DELIVERY_METHOD)[self.delivery_method]

    def get_payment_method(self):
        return dict(self.PAYMENT_METHOD)[self.payment_method]


class OrderItemRelation(models.Model):
    order = models.ForeignKey(Order)
    pastry = models.ForeignKey(Pastry, on_delete=models.PROTECT)
    amount = models.PositiveSmallIntegerField(_("Amount"), validators=[MinValueValidator(1)])
