from django.conf.urls import patterns, url
from shopping import views

urlpatterns = patterns(
    '',
    url(r'^ingredients/$', views.ingredients, name='ingredients'),
    url(r'^ingredients/(?P<id>\d+)/$', views.change_ingredient, name='change_ingredient'),
    url(r'^add_ingredient/$', views.add_ingredient, name='add_ingredient'),
    url(r'^pastries/$', views.pastries, name='pastries'),
    url(r'^pastries/(?P<id>\d+)/$', views.change_pastry, name='change_pastry'),
    url(r'^add_pastry/$', views.add_pastry, name='add_pastry'),
    url(r'^orders/$', views.orders, name='orders'),
    url(r'^orders/(?P<id>\d+)/$', views.change_order, name='change_order'),
    url(r'^complete_order/(?P<id>\d+)/$', views.complete_order, name='complete_order'),
    url(r'^change_arrival_date/(?P<id>\d+)/$', views.change_arrival_date, name='change_arrival_date'),
    url(r'^pay_order/(?P<id>\d+)/$', views.pay_order, name='pay_order'),
    url(r'^add_order/$', views.add_order, name='add_order'),
    url(r'^delete_orders/$', views.delete_orders, name='delete_orders'),
    url(r'^delete_pastries/$', views.delete_pastries, name='delete_pastries'),
    url(r'^delete_ingredients/$', views.delete_ingredients, name='delete_ingredients'),
)
