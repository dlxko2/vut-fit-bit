from django import forms
from .models import Ingredient, Pastry, Order


class IngredientForm(forms.ModelForm):

    class Meta:
        model = Ingredient
        fields = ("name", "purchase_price", "amount")
        widgets = {
            "name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "purchase_price": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "\+?\d+(\.\d{0,2})?",
                "required": "True"
            }),
            "amount": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "number",
                "required": "True"
            })
        }


class PastryForm(forms.ModelForm):

    class Meta:
        model = Pastry
        fields = ("name", "price", "allergens", "units_per_batch", "ingredients")
        widgets = {
            "name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "price": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "\+?\d+(\.\d{0,2})?",
                "required": "True"
            }),
            "allergens": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "units_per_batch": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "[1-9]\d{0,10}",
                "type": "number",
                "required": "True"
            }),
        }
        exclude = ['ingredients']


class OrderForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance:
            self.fields['paid'].widget.attrs['readonly'] = True
            self.fields['completed'].widget.attrs['readonly'] = True

    class Meta:
        model = Order
        fields = ("customer", "delivery_address", "arrival_date",
                  "completed_date", "price", "delivery_method",
                  "payment_method", "paid", "completed")
        widgets = {
            "completed_date": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date"
            }),
            "arrival_date": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date"
            }),
            "delivery_address": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "customer": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "paid": forms.TextInput(attrs={
                "type": "checkbox",
                "id": "id_paid",
                "class": "mdl-checkbox__input",
                "readonly": True
            }),
            "completed": forms.TextInput(attrs={
                "type": "checkbox",
                "id": "id_completed",
                "class": "mdl-checkbox__input",
                "readonly": True
            }),
            "delivery_method": forms.Select(attrs={
                "class": "mdl-checkbox__input",
                "type": "select"
            }),
            "price": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "\+?\d+(\.\d{0,2})?",
                "readonly": True,
            })
        }
        exclude = ['items']


class OrderArrivalDateForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ("arrival_date", )
        widgets = {
            "arrival_date": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date"
            })
        }
