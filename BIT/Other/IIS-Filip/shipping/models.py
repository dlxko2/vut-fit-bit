from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator, RegexValidator
from members.models import Driver
from members.models import Customer


class Car(models.Model):
    sps_regex = RegexValidator(
        regex="^\w{1,10}$",
        message=_("Length has to be between 1 and 10"))

    spz = models.CharField(_("SPZ"), max_length=10, validators=[sps_regex], primary_key=True)
    driver = models.ForeignKey(Driver, verbose_name=_("Driver"), blank=True, null=True, on_delete=models.PROTECT)
    working = models.BooleanField(_("Is working"), default=True)
    load = models.PositiveIntegerField(_("Load in kilos"), validators=[MinValueValidator(1)])
    inspection_date = models.DateField(_("Technical inspection date"))

    def __unicode__(self):
        return self.spz

    class Meta:
        verbose_name = _("Car")
        verbose_name_plural = _("Cars")


class DistributionArea(models.Model):
    name = models.CharField(_("Name"), max_length=30, db_index=True)
    driver = models.ForeignKey(
        Driver, verbose_name=_("Driver"), on_delete=models.PROTECT,
        related_name="distributionarea_set")
    distribution_time = models.TimeField(_("Distribution time"))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Distribution area")
        verbose_name_plural = _("Distribution areas")


class DeliveryAddress(models.Model):
    customer = models.ForeignKey(Customer, verbose_name=_("Customer"), on_delete=models.PROTECT)
    distribution_area = models.ForeignKey(
        DistributionArea, on_delete=models.PROTECT,
        related_name="distributionarea_set",
        verbose_name=_("Distribution area"))
    office_number = models.PositiveSmallIntegerField(_("Branch office number"))
    city = models.CharField(_("City"), max_length=50)
    street = models.CharField(_("Street"), max_length=50)
    house_number = models.PositiveSmallIntegerField(_("House number"))

    def __unicode__(self):
        return "%s - %s %s" % (self.city, self.street, self.house_number)

    class Meta:
        verbose_name = _("Delivery address")
        verbose_name_plural = _("Delivery addresses")
