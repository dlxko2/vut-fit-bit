# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0003_auto_20151013_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='spz',
            field=models.CharField(max_length=10, serialize=False, verbose_name='SPZ', primary_key=True, validators=[django.core.validators.RegexValidator(regex=b'^\\w{1,10}$', message='Length has to be between 1 and 10')]),
        ),
        migrations.AlterField(
            model_name='distributionarea',
            name='distribution_time',
            field=models.TimeField(verbose_name='Distribution time'),
        ),
    ]
