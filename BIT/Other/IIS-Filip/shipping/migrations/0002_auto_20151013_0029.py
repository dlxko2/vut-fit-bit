# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='inspection_date',
            field=models.DateField(verbose_name='Technical inspection date'),
        ),
    ]
