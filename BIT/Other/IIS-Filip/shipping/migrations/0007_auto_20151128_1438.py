# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0006_auto_20151128_0024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, verbose_name='Driver', blank=True, to='members.Driver', null=True),
        ),
        migrations.AlterField(
            model_name='deliveryaddress',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, verbose_name='Customer', to='members.Customer'),
        ),
        migrations.AlterField(
            model_name='deliveryaddress',
            name='distribution_area',
            field=models.ForeignKey(related_name='distributionarea_set', on_delete=django.db.models.deletion.PROTECT, verbose_name='Distribution area', to='shipping.DistributionArea'),
        ),
        migrations.AlterField(
            model_name='distributionarea',
            name='driver',
            field=models.ForeignKey(related_name='distributionarea_set', on_delete=django.db.models.deletion.PROTECT, verbose_name='Driver', to='members.Driver'),
        ),
    ]
