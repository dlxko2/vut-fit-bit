# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('spz', models.CharField(max_length=10, serialize=False, verbose_name='SPZ', primary_key=True, validators=[django.core.validators.RegexValidator(regex=b'^\\w{1,10}$', message='Length has to be bigger than 0')])),
                ('working', models.BooleanField(default=True, verbose_name='Is working')),
                ('load', models.PositiveIntegerField(verbose_name='Load in kilos', validators=[django.core.validators.MinValueValidator(1)])),
                ('inspection_date', models.DateTimeField(verbose_name='Technical inspection date')),
                ('driver', models.ForeignKey(to='members.Driver', blank=True)),
            ],
            options={
                'verbose_name': 'Car',
                'verbose_name_plural': 'Cars',
            },
        ),
        migrations.CreateModel(
            name='DeliveryAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('office_number', models.PositiveSmallIntegerField(verbose_name='Branch office number')),
                ('city', models.CharField(max_length=50, verbose_name='City')),
                ('street', models.CharField(max_length=50, verbose_name='Street')),
                ('house_number', models.PositiveSmallIntegerField(verbose_name='House number')),
                ('customer', models.ForeignKey(to='members.Customer')),
            ],
            options={
                'verbose_name': 'Delivery address',
                'verbose_name_plural': 'Delivery addresses',
            },
        ),
        migrations.CreateModel(
            name='DistributionArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='Name', db_index=True)),
                ('distribution_time', models.TimeField(verbose_name='Distribution date')),
                ('driver', models.ForeignKey(to='members.Driver')),
            ],
            options={
                'verbose_name': 'Distribution area',
                'verbose_name_plural': 'Distribution areas',
            },
        ),
        migrations.AddField(
            model_name='deliveryaddress',
            name='distribution_area',
            field=models.ForeignKey(to='shipping.DistributionArea'),
        ),
    ]
