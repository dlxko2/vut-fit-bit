# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0005_auto_20151124_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryaddress',
            name='distribution_area',
            field=models.ForeignKey(related_name='distributionarea_set', on_delete=django.db.models.deletion.PROTECT, to='shipping.DistributionArea'),
        ),
        migrations.AlterField(
            model_name='distributionarea',
            name='driver',
            field=models.ForeignKey(related_name='distributionarea_set', on_delete=django.db.models.deletion.PROTECT, to='members.Driver'),
        ),
    ]
