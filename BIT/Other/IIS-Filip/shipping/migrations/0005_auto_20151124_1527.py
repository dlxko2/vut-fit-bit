# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0004_auto_20151017_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='members.Driver', null=True),
        ),
        migrations.AlterField(
            model_name='deliveryaddress',
            name='customer',
            field=models.ForeignKey(to='members.Customer', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='deliveryaddress',
            name='distribution_area',
            field=models.ForeignKey(to='shipping.DistributionArea', on_delete=django.db.models.deletion.PROTECT),
        ),
        migrations.AlterField(
            model_name='distributionarea',
            name='driver',
            field=models.ForeignKey(to='members.Driver', on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
