from django import forms
from .models import Car, DistributionArea, DeliveryAddress


class CarForm(forms.ModelForm):

    class Meta:
        model = Car
        fields = ("spz", "driver", "load", "inspection_date", "working")
        widgets = {
            "spz": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "^\w{1,10}$",
                "required": "True"
            }),
            "driver": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "load": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "[1-9]\d{0,10}",
                "required": "True"
            }),
            "inspection_date": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date",
                "required": "True"
            }),
            "working": forms.TextInput(attrs={
                "type": "checkbox",
                "id": "id_working",
                "class": "mdl-checkbox__input"
            })
        }


class DistributionAreaForm(forms.ModelForm):

    class Meta:
        model = DistributionArea
        fields = ("name", "driver", "distribution_time")
        widgets = {
            "name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "driver": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "distribution_time": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "time",
                "required": "True"
            })
        }


class DeliveryAddressForm(forms.ModelForm):

    class Meta:
        model = DeliveryAddress
        fields = ("customer", "distribution_area", "office_number", "city", "street", "house_number")
        widgets = {
            "customer": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "distribution_area": forms.TextInput(attrs={
                "class": "mdl-textfield__input fk"
            }),
            "office_number": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "number",
                "required": "True"
            }),
            "city": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "street": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "house_number": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "number",
                "required": "True"
            }),
        }
