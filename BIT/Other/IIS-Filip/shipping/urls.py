from django.conf.urls import patterns, url
from shipping import views

urlpatterns = patterns(
    '',
    url(r'^cars/$', views.cars, name='cars'),
    url(r'^cars/(?P<spz>\w{1,10})/$', views.change_car, name='change_car'),
    url(r'^add_car/$', views.add_car, name='add_car'),
    url(r'^distribution_areas/$', views.distribution_areas, name='distribution_areas'),
    url(r'^distribution_areas/(?P<id>\d+)/$', views.change_distribution_area, name='change_distribution_area'),
    url(r'^add_distribution_area/$', views.add_distribution_area, name='add_distribution_area'),
    url(r'^delivery_addresses/$', views.delivery_addresses, name='delivery_addresses'),
    url(r'^delivery_addresses/(?P<id>\d+)/$', views.change_delivery_address, name='change_delivery_address'),
    url(r'^add_delivery_address/$', views.add_delivery_address, name='add_delivery_address'),
    url(r'^delete_cars/$', views.delete_cars, name='delete_cars'),
    url(r'^delete_distribution_areas/$', views.delete_distribution_areas, name='delete_distribution_areas'),
    url(r'^delete_delivery_addresses/$', views.delete_delivery_addresses, name='delete_delivery_addresses'),
)
