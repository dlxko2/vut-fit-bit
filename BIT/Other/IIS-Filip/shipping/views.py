from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseForbidden
from .models import Car, DistributionArea, DeliveryAddress
from .forms import CarForm, DistributionAreaForm, DeliveryAddressForm
from pekarna.abstract_views import AbstractView, ManageAbstractView, is_popup
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _


@login_required
def cars(request):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver():
        carView = CarView(request)
        return carView.render()
    return HttpResponseForbidden()


@login_required
def add_car(request):
    if request.user.is_admin() or request.user.is_manager():
        addCarView = AddCarView(request)
        return addCarView.render()
    return HttpResponseForbidden()


@login_required
def change_car(request, spz):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver():
        car = get_object_or_404(Car, spz=spz)

        if request.user.is_driver():
            allowed_cars = Car.objects.filter(driver=request.user.driver_set)
            if car not in allowed_cars:
                return HttpResponseForbidden()

        changeCarView = ChangeCarView(request, car)
        return changeCarView.render()
    return HttpResponseForbidden()


@login_required
def delete_cars(request):
    if request.user.is_admin() or request.user.is_manager():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shipping/cars/")

        obj_ids = obj_ids.split(",")
        objects = Car.objects.filter(spz__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected cars were deleted!")
        except:
            msg = _("Some cars could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shipping/cars/")
    return HttpResponseForbidden()


class CarView(AbstractView):

    def get_queryset(self):
        if self.request.user.is_driver():
            return Car.objects.filter(driver=self.request.user.driver_set)
        else:
            return Car.objects.all()

    def get_search_list(self):
        return ["spz"]

    def get_template(self):
        return "shipping/cars.html"


class AddCarView(ManageAbstractView):

    def get_form(self):
        return CarForm

    def get_template(self):
        return "shipping/add_car.html"

    def get_reverse_url(self):
        return "cars"


class ChangeCarView(AddCarView):

    def get_template(self):
        return "shipping/change_car.html"


@login_required
def distribution_areas(request):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver():
        distributionAreaView = DistributionAreaView(request)
        return distributionAreaView.render()
    return HttpResponseForbidden()


@login_required
def add_distribution_area(request):
    if request.user.is_admin() or request.user.is_manager():
        addDistributionAreaView = AddDistributionAreaView(request)
        return addDistributionAreaView.render()
    return HttpResponseForbidden()


@login_required
def change_distribution_area(request, id):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver():
        distribution_area = get_object_or_404(DistributionArea, id=id)

        if request.user.is_driver():
            allowed_areas = DistributionArea.objects.filter(driver=request.user.driver_set)
            if distribution_area not in allowed_areas:
                return HttpResponseForbidden()

        changeDistributionAreaView = ChangeDistributionAreaView(request, distribution_area)
        return changeDistributionAreaView.render()
    return HttpResponseForbidden()


@login_required
def delete_distribution_areas(request):
    if request.user.is_admin() or request.user.is_manager():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shipping/distribution_areas/")

        obj_ids = obj_ids.split(",")
        objects = DistributionArea.objects.filter(id__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected areas were deleted!")
        except:
            msg = _("Some areas could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shipping/distribution_areas/")
    return HttpResponseForbidden()


class DistributionAreaView(AbstractView):

    def get_queryset(self):
        if self.request.user.is_driver():
            return DistributionArea.objects.filter(driver=self.request.user.driver_set)
        else:
            return DistributionArea.objects.all()

    def get_search_list(self):
        return ["name", "driver__person__last_name", "driver__person__first_name"]

    def get_template(self):
        return "shipping/distribution_areas.html"


class AddDistributionAreaView(ManageAbstractView):

    def get_form(self):
        return DistributionAreaForm

    def get_template(self):
        return "shipping/add_distribution_area.html"

    def get_reverse_url(self):
        return "distribution_areas"


class ChangeDistributionAreaView(AddDistributionAreaView):

    def get_template(self):
        return "shipping/change_distribution_area.html"


@login_required
def delivery_addresses(request):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver() or is_popup(request):
        deliveryAddressView = DeliveryAddressView(request)
        return deliveryAddressView.render()
    return HttpResponseForbidden()


@login_required
def add_delivery_address(request):
    if request.user.is_admin() or request.user.is_manager():
        addDeliveryAddressView = AddDeliveryAddressView(request)
        return addDeliveryAddressView.render()
    return HttpResponseForbidden()


@login_required
def change_delivery_address(request, id):
    if request.user.is_admin() or request.user.is_manager() or request.user.is_driver():
        delivery_address = get_object_or_404(DeliveryAddress, id=id)

        if request.user.is_driver():
            distribution_areas = request.user.driver_set.distributionarea_set.all()
            allowed_addresses = DeliveryAddress.objects.filter(distribution_area__in=distribution_areas)

            if delivery_address not in allowed_addresses:
                return HttpResponseForbidden()

        changeDeliveryAddressView = ChangeDeliveryAddressView(request, delivery_address)
        return changeDeliveryAddressView.render()
    return HttpResponseForbidden()


@login_required
def delete_delivery_addresses(request):
    if request.user.is_admin() or request.user.is_manager():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/shipping/delivery_addresses/")

        obj_ids = obj_ids.split(",")
        objects = DeliveryAddress.objects.filter(id__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected addresses were deleted!")
        except:
            msg = _("Some addresses could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/shipping/delivery_addresses/")
    return HttpResponseForbidden()


class DeliveryAddressView(AbstractView):

    def get_queryset(self):
        if self.request.user.is_customer():
            return DeliveryAddress.objects.filter(customer=self.request.user.customer_set)
        elif self.request.user.is_driver():
            distribution_areas = self.request.user.driver_set.distributionarea_set.all()
            return DeliveryAddress.objects.filter(distribution_area__in=distribution_areas)
        else:
            return DeliveryAddress.objects.all()

    def get_search_list(self):
        return ["customer__person__last_name", "customer__person__first_name",
                "distribution_area__name", "office_number", "city", "street",
                "house_number"]

    def get_template(self):
        return "shipping/delivery_addresses.html"


class AddDeliveryAddressView(ManageAbstractView):

    def get_form(self):
        return DeliveryAddressForm

    def get_template(self):
        return "shipping/add_delivery_address.html"

    def get_reverse_url(self):
        return "delivery_addresses"


class ChangeDeliveryAddressView(AddDeliveryAddressView):

    def get_template(self):
        return "shipping/change_delivery_address.html"
