from django.conf.urls import patterns, url
from members import views

urlpatterns = patterns(
    '',
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^change_password/$', views.user_change_password, name='change_password'),
    url(r'^members/$', views.members, name='members'),
    url(r'^members/(?P<username>\w+)/$', views.change_member, name='change_member'),
    url(r'^reset_password/(?P<username>\w+)/$', views.reset_password, name='reset_password'),
    url(r'^add_member/$', views.add_member, name='add_member'),
    url(r'^delete_members/$', views.delete_members, name='delete_members'),
)
