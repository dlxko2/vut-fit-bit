from django import forms
from .models import Person, Driver, Manager, Customer
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = ("username", "first_name", "last_name", "birthday",
                  "phone_number", "address", "bank_account")
        widgets = {
            "username": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "\w+",
                "required": "True"
            }),
            "first_name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "last_name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "birthday": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date",
                "required": "True"
            }),
            "phone_number": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "address": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "bank_account": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
        }


class PersonCreationForm(PersonForm):
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(
            attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }))
    password2 = forms.CharField(
        label=_("Password again"),
        widget=forms.PasswordInput(
            attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }))
    CHOICES = (
        ('admin', 'Admin'),
        ('manager', 'Manager'),
        ('driver', 'Driver'),
        ('customer', 'Customer'),
    )
    member_type = forms.ChoiceField(label=_("Member type"), choices=CHOICES, required=True)
    selected_type = forms.CharField(widget=forms.HiddenInput(), initial="admin")

    def clean(self):
        if (self.cleaned_data.get('password') != self.cleaned_data.get('password2')):
            raise ValidationError("Passwords do not match.")

        return super(PersonCreationForm, self).clean()

    class Meta:
        model = Person
        fields = (
            "member_type", "username", "password", "password2",
            "first_name", "last_name", "birthday",
            "phone_number", "address", "bank_account")
        widgets = {
            "username": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "pattern": "\w+",
                "required": "True"
            }),
            "first_name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "last_name": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "birthday": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "type": "date",
                "required": "True"
            }),
            "phone_number": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "address": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
            "bank_account": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
                "required": "True"
            }),
        }


class CustomerForm(forms.ModelForm):

    class Meta:
        model = Customer
        fields = ["head_office_address"]
        widgets = {
            "head_office_address": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
            })
        }


class ManagerForm(forms.ModelForm):

    class Meta:
        model = Manager
        fields = ["education"]
        widgets = {
            "education": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
            })
        }


class DriverForm(forms.ModelForm):

    class Meta:
        model = Driver
        fields = ["driving_licence"]
        widgets = {
            "driving_licence": forms.TextInput(attrs={
                "class": "mdl-textfield__input",
            })
        }
