from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from .models import Person
from django.core.urlresolvers import reverse
from .forms import PersonForm, CustomerForm, ManagerForm, DriverForm, PersonCreationForm
from pekarna.abstract_views import AbstractView, ManageAbstractView, is_popup
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.http import Http404


def user_login(request):
    next_page = request.GET.get("next", "/")

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        if user:
            # If the account is valid and active, we can log the user in.
            # We'll send the user back to the homepage.
            login(request, user)
            return HttpResponseRedirect(next_page)
        else:
            data = {
                "username": username,
                "password": password,
                "error": _("Wrong username or password!")
            }
            return render(request, 'members/login.html', data)

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request, 'members/login.html')


@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/')


@login_required
def user_change_password(request):
    next_page = request.GET.get("next", "/")

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        old_password = request.POST.get('old_password')
        new_password1 = request.POST.get('new_password1')
        new_password2 = request.POST.get('new_password2')

        if not check_password(old_password, request.user.password):
            data = {"error": _("Old password does not match!")}
            return render(request, 'members/change_password.html', data)

        if new_password1 != new_password2:
            data = {"error": _("New passwords do not match!")}
            return render(request, 'members/change_password.html', data)

        if check_password(new_password1, request.user.password):
            data = {"error": _("Old and new passwords are the same!")}
            return render(request, 'members/change_password.html', data)

        request.user.set_password(new_password1)
        request.user.save()

        # Update session with changed password
        update_session_auth_hash(request, request.user)

        return HttpResponseRedirect(next_page)

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        return render(request, 'members/change_password.html', {})


@login_required
def reset_password(request, username):
    if request.user.is_admin():
        url = "members/reset_password.html"
        next_page = request.GET.get("next", "/")

        member = get_object_or_404(Person, username=username)

        if request.method == 'POST':
            new_password1 = request.POST.get('new_password1')
            new_password2 = request.POST.get('new_password2')

            if new_password1 != new_password2:
                data = {"error": _("New passwords do not match!")}
                return render(request, url, data)

            member.set_password(new_password1)
            member.save()

            msg = _("Password has been changed successfully")
            messages.add_message(request, messages.INFO, msg)

            return HttpResponseRedirect(next_page)
        else:
            return render(request, url, {})
    return HttpResponseForbidden()


@login_required
def delete_members(request):
    if request.user.is_admin():
        obj_ids = request.POST.get("obj_ids")

        if obj_ids == '' or obj_ids is None:
            msg = _("No object was selected!")
            messages.add_message(request, messages.INFO, msg)
            return HttpResponseRedirect("/members/members/")

        obj_ids = obj_ids.split(",")
        objects = Person.objects.filter(username__in=obj_ids)

        try:
            objects.delete()
            msg = _("Selected members were deleted!")
        except:
            msg = _("Some members could not be deleted!")
        messages.add_message(request, messages.INFO, msg)

        return HttpResponseRedirect("/members/members/")
    return HttpResponseForbidden()


@login_required
def members(request):
    if request.user.is_admin() or (is_popup(request) and request.user.is_manager()):
        memberView = None

        if "popup" in request.GET:
            member_type = request.GET.get("popup", "")

            if member_type == "customers":
                memberView = CustomerPopupView(request)
            elif member_type == "drivers":
                memberView = DriverPopupView(request)
            else:
                raise Http404()

        if memberView is None:
            memberView = MemberView(request)

        return memberView.render()
    return HttpResponseForbidden()


@login_required
def add_member(request):
    if request.user.is_admin():
        addMemberView = AddMemberView(request)
        return addMemberView.render()
    return HttpResponseForbidden()


@login_required
def change_member(request, username):
    if request.user.is_admin():
        member = get_object_or_404(Person, username=username)

        changeMemberView = ChangeMemberView(request, member)
        return changeMemberView.render()
    return HttpResponseForbidden()


class MemberView(AbstractView):

    def get_queryset(self):
        return Person.objects.all()

    def get_search_list(self):
        return ["username", "first_name", "last_name", "phone_number", "address"]

    def get_template(self):
        return "members/members.html"

    def get_popup_templates(self):
        return {
            "customers": "members/customers_popup.html",
            "drivers": "members/drivers_popup.html"
        }


class CustomerPopupView(MemberView):

    def get_queryset(self):
        return Person.objects.filter(customer_set__isnull=False)


class DriverPopupView(MemberView):

    def get_queryset(self):
        return Person.objects.filter(driver_set__isnull=False)


class ManagerPopupView(MemberView):

    def get_queryset(self):
        return Person.objects.filter(manager_set__isnull=False)


class AddMemberView():

    def __init__(self, request, instance=None):
        self.request = request
        self.instance = instance
        self.Form = self.get_form()
        self.reverse_url = self.get_reverse_url()
        self.template = self.get_template()

    def get_form(self):
        return PersonCreationForm

    def get_template(self):
        return "members/add_member.html"

    def get_reverse_url(self):
        return "members"

    def render(self):

        if self.request.method == "POST":
            form = self.Form(self.request.POST, instance=self.instance)
            customer_form = CustomerForm(self.request.POST, instance=self.instance)
            manager_form = ManagerForm(self.request.POST, instance=self.instance)
            driver_form = DriverForm(self.request.POST, instance=self.instance)

            if form.is_valid():
                forms_valid = True

                selected_type = self.request.POST.get('selected_type', '')
                if selected_type == "customer":
                    if not customer_form.is_valid():
                        forms_valid = False
                elif selected_type == "manager":
                    if not manager_form.is_valid():
                        forms_valid = False
                elif selected_type == "driver":
                    if not driver_form.is_valid():
                        forms_valid = False

                if forms_valid:
                    person = form.save()
                    person.set_password(person.password)
                    person.save()

                    if selected_type == "customer":
                        customer = customer_form.save(commit=False)
                        customer.person = person
                        customer.save()
                    if selected_type == "manager":
                        manager = manager_form.save(commit=False)
                        manager.person = person
                        manager.save()
                    if selected_type == "driver":
                        driver = driver_form.save(commit=False)
                        driver.person = person
                        driver.save()

                    return HttpResponseRedirect(reverse(self.reverse_url))
        else:
            form = self.Form(instance=self.instance)
            customer_form = CustomerForm(instance=self.instance)
            manager_form = ManagerForm(instance=self.instance)
            driver_form = DriverForm(instance=self.instance)

        data = {
            "form": form,
            "customer_form": customer_form,
            "manager_form": manager_form,
            "driver_form": driver_form
        }

        return render(self.request, self.template, data)


class ChangeMemberView(ManageAbstractView):

    def get_form(self):
        return PersonForm

    def get_template(self):
        return "members/change_member.html"

    def get_reverse_url(self):
        return "members"

    def get_child_instance(self):
        if hasattr(self.instance, "customer_set"):
            return self.instance.customer_set
        elif hasattr(self.instance, "manager_set"):
            return self.instance.manager_set
        elif hasattr(self.instance, "driver_set"):
            return self.instance.driver_set

    def get_child_form(self):
        if hasattr(self.instance, "customer_set"):
            return CustomerForm
        if hasattr(self.instance, "manager_set"):
            return ManagerForm
        if hasattr(self.instance, "driver_set"):
            return DriverForm
