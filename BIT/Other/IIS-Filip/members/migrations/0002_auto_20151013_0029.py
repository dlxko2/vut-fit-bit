# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='start_date',
            field=models.DateField(auto_now_add=True, verbose_name='Start date'),
        ),
        migrations.AlterField(
            model_name='manager',
            name='start_date',
            field=models.DateField(auto_now_add=True, verbose_name='Start date'),
        ),
        migrations.AlterField(
            model_name='person',
            name='birthday',
            field=models.DateField(verbose_name='Birthday'),
        ),
    ]
