# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30, verbose_name='First name', db_index=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='Last name', db_index=True)),
                ('birthday', models.DateTimeField(verbose_name='Birthday')),
                ('phone_number', models.CharField(max_length=20, verbose_name='Phone number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,20}$', message="Phone number format: '+999999999'. Up to 15 digits allowed.")])),
                ('address', models.CharField(max_length=140, verbose_name='Address')),
                ('bank_account', models.CharField(max_length=34, verbose_name='Bank account')),
            ],
            options={
                'verbose_name': 'Person',
                'verbose_name_plural': 'People',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('person', models.OneToOneField(related_name='customer_set', primary_key=True, serialize=False, to='members.Person')),
                ('head_office_address', models.CharField(max_length=140, verbose_name='Head office address')),
            ],
            options={
                'verbose_name': 'Customer',
                'verbose_name_plural': 'Customers',
            },
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('person', models.OneToOneField(related_name='driver_set', primary_key=True, serialize=False, to='members.Person')),
                ('driving_licence', models.CharField(max_length=10, verbose_name='Driving licence')),
                ('start_date', models.DateTimeField(auto_now_add=True, verbose_name='Start date')),
            ],
            options={
                'verbose_name': 'Driver',
                'verbose_name_plural': 'Drivers',
            },
        ),
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('person', models.OneToOneField(related_name='manager_set', primary_key=True, serialize=False, to='members.Person')),
                ('education', models.CharField(max_length=100, verbose_name='Education')),
                ('start_date', models.DateTimeField(auto_now_add=True, verbose_name='Start date')),
            ],
            options={
                'verbose_name': 'Manager',
                'verbose_name_plural': 'Managers',
            },
        ),
    ]
