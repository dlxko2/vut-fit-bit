# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0005_auto_20151105_2031'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='person',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
