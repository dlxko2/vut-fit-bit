from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.auth.models import UserManager, AbstractBaseUser


class Person(AbstractBaseUser):
    phone_regex = RegexValidator(
        regex=r'^\w+$',
        message=_("Username can contains only alphanums."))
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,20}$',
        message=_("Phone number format: '+999999999'. Up to 15 digits allowed."))

    username = models.CharField(max_length=40, unique=True, primary_key=True)
    first_name = models.CharField(_("First name"), max_length=30, db_index=True)
    last_name = models.CharField(_("Last name"), max_length=30, db_index=True)
    birthday = models.DateField(_("Birthday"))
    phone_number = models.CharField(_("Phone number"), validators=[phone_regex], max_length=20)
    address = models.CharField(_("Address"), max_length=140)
    bank_account = models.CharField(_("Bank account"), max_length=34)

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["first_name", "last_name", "birthday", "phone_number", "address", "bank_account"]

    def __unicode__(self):
        return "%s %s" % (self.last_name, self.first_name)

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("People")

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def role(self):
        if hasattr(self, "driver_set"):
            return _("Driver")
        if hasattr(self, "manager_set"):
            return _("Manager")
        if hasattr(self, "customer_set"):
            return _("Customer")
        return _("Admin")

    def is_admin(self):
        # If person is not customer, manager or driver -> admin
        if hasattr(self, "driver_set") or hasattr(self, "manager_set") or hasattr(self, "customer_set"):
            return False
        return True

    def is_manager(self):
        return hasattr(self, "manager_set")

    def is_driver(self):
        return hasattr(self, "driver_set")

    def is_customer(self):
        return hasattr(self, "customer_set")


class Manager(models.Model):
    person = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="manager_set",
        primary_key=True, on_delete=models.CASCADE)
    education = models.CharField(_("Education"), max_length=100)
    start_date = models.DateField(_("Start date"), auto_now_add=True)

    def __unicode__(self):
        return str(self.person)

    class Meta:
        verbose_name = _("Manager")
        verbose_name_plural = _("Managers")


class Driver(models.Model):
    person = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="driver_set",
        primary_key=True, on_delete=models.CASCADE)
    driving_licence = models.CharField(_("Driving licence"), max_length=10)
    start_date = models.DateField(_("Start date"), auto_now_add=True)

    def car_count(self):
        return self.car_set.count()

    def __unicode__(self):
        return str(self.person)

    class Meta:
        verbose_name = _("Driver")
        verbose_name_plural = _("Drivers")


class Customer(models.Model):
    person = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="customer_set",
        primary_key=True, on_delete=models.CASCADE)
    head_office_address = models.CharField(_("Head office address"), max_length=140)

    def __unicode__(self):
        return str(self.person)

    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")
