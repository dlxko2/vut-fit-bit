<?php

#CHA:xpavel27

    // Define regular expressions in one place 
    define("REMOVE_IFDEFS", "/#ifdef[\S\s]*?#endif/");
	define("SPLIT_BY_WHITESPACE", "/[\s]+/");
	define("REMOVE_RIGHT_WHITESPACE", "/(\w+)*\s*(\*)/");
	define("REMOVE_LEFT_WHITESPACE", "/(\*)\s*(\w+)*/");
	define("REMOVE_PARAMETER_NAME", "/\w+\s*$/");
    define("REMOVE_COMMENTS", "/\/\/.*/");
    define("REMOVE_MULTILINE_COMMENTS", "/\/\*.*?\*\//s");
    define("REMOVE_DEFINES", "/#.*/");
    define("REMOVE_DEFINITIONS", "/\{.*?\}/s");
    define("MATCH_FUNCTION", "/((?:\b[*]*[\s]*[\w\*]+[\s]+[*\s]*)+?)(\w+)\s*\((.*?)\)/s");
    define("CHECK_VARIABLES", "/[^.]+\.{3}[^.]+/");
    define("REMOVE_INLINES", "/\s*inline\s+/");

    // Function to write help page
    function WriteHelpPage()
    {
        $HelpPageText = "\n------------------------------------------------------------------------------------------\n"
                      . "                               Copyright Martin Pavelka \n"
                      . "------------------------------------------------------------------------------------------\n"
                      . "*[--help]              prints full help page for all the arguments of this program\n"
                      . "*[--input=fileordir]   sets input file or directory to search for header files to process\n"
                      . "*[--output=filename]   sets output file to write output informations there instead of std\n"
                      . "*[--pretty-xml=*k]     write k whitespaces before next child element in the XML output text\n"
                      . "                       header will be divided by one new row if no k then will be set to 4\n"
                      . "*[--no-inline]         script will ignore functions defined by keyword inline\n"
                      . "*[--max-par=n]         script will accept only functions with n or less arguments \n"
                      . "*[--no-duplicates]     ignore more functions with the same name\n"
                      . "*[--remove-whitespace] delete whitespaces in type and rettype parameters\n"
                      . "! Parameters with * are optional to use\n\n";
        echo $HelpPageText;
        exit(0);
    }
    
    //  Arguments structure
    class ArgStruct
    {
        public $ParamInput;
        public $ParamInputType;
        public $ParamOutput;
        public $ParamPrettyxml;
        public $ParamPrettyxmlValue;
        public $ParamNoinline;
        public $ParamMaxpar;
        public $ParamMaxparValue;
        public $ParamNoduplicates;
        public $ParamRemovewhitespace;
    }
   
    // Function to get list of files to search
    function GetFiles($FileDir, $ActPath)
    {
        // Define local variables
        $DirList = array();
        $FileList = array();
        
        // Look for files in directory
        $LookUp = new FilesystemIterator($FileDir, FilesystemIterator::SKIP_DOTS);
        
        // Process every file/directory
        foreach($LookUp as $File)
        {
            // If local file is a directory do recursion
            if($File->isDir()) $DirList[$File->getFilename()] = GetFiles($File->getPathname(), $ActPath . $File->getFilename(). "/");
            
            // Else save the file with parameters
            else
            {
            	// Check permissions
            	if (!is_readable($File))
            	{
            		fwrite(STDERR, "File $File is not readalble!\n");
            		exit(2);
            	}

            	// Check the extension
            	$FileInfo = pathinfo($File);
                if (isset($FileInfo['extension']))
                {
            	    if ($FileInfo['extension'] == "h")
                	$FileList[$File->getPathname()] = $ActPath;
        		}
            }
        }
        
        // Return list
        return $DirList + $FileList;
    }
    
    // Function to parse arguments
    function ParseArguments($Options)
    {
        // Define structure
        $MyArgStruc = new ArgStruct();
        
        // For every single option process parse
        foreach($Options as $Option=>$OptionValue)
        {
            // Switch type of option
            switch ($Option)
            {
                // Spracovanie parametru help
                case "help":
                    if (count($Options) != 1)
                    {
                    	fwrite(STDERR, "Can't combine arguments with help one!\n");
                        exit(1);
                    }
                    WriteHelpPage();
                    break;
                // Spracovanie parametru no-inline
                case "no-inline":
                    $MyArgStruc->ParamNoinline = true;
                    break;
                // Spracovanie parametru no-duplicates
                case "no-duplicates":
                    $MyArgStruc->ParamNoduplicates = true;
                    break;
                // Spracovanie parametru remove-whitespace
                case "remove-whitespace":
                    $MyArgStruc->ParamRemovewhitespace = true;
                    break;
                // Spracovanie parametru input
                case "input":
                    $MyArgStruc->ParamInput = $OptionValue;
                    (is_dir($OptionValue)) ? $MyArgStruc->ParamInputType = 1 : $MyArgStruc->ParamInputType = 2;
                    if(!is_readable($OptionValue)) 
                    {
                    	fwrite(STDERR, "Can't open input file!\n");
                    	exit(2);
                    }
                    break;
                // Spracovanie parametru output
                case "output":
                    $MyArgStruc->ParamOutput = $OptionValue;
                    if (is_dir($OptionValue))
                    {
                    	fwrite(STDERR, "Can't define output as dir!\n");
                        exit (1);
                    }
                    if (file_exists($OptionValue) && !is_writeable($OptionValue))
                    {
                    		fwrite(STDERR, "Can't write to output file!\n");
                    		exit(3);
                    }
                    break;
                // Spracovanie parametru pretty-xml
                case "pretty-xml":
                    $MyArgStruc->ParamPrettyxml = true;
                    
                    if(ctype_digit($OptionValue))
                        $MyArgStruc->ParamPrettyxmlValue = $OptionValue;
                    
                    else if ($OptionValue == false)
                        $MyArgStruc->ParamPrettyxmlValue = 4;
                    
                    else
                    {
                    	fwrite(STDERR, "Wrong pretty-xml argument type!\n");
                        exit(1);
                    }
                    break;
                // Spracovanie parametra max-par
                case "max-par":
                    $MyArgStruc->ParamMaxpar = true;

                    if(ctype_digit($OptionValue))
                        $MyArgStruc->ParamMaxparValue = $OptionValue;

                    else
                    {
                        fwrite(STDERR, "Wrong max-par argument type!\n");
                        exit(1);
                    }
                    break;
                // Default error
                default:
                	fwrite(STDERR, "Error while getting arguments!\n");
                	exit(1);
                	break;
            }
        }
        return $MyArgStruc;
    }

    // Function to cleanup file
    function CleanFileContent($InputFile)
    {
    	// Get file contents
        $FileContent = trim(file_get_contents($InputFile));
                
        // Remove junk
        $FileContent = preg_replace(REMOVE_COMMENTS, "", $FileContent);
        $FileContent = preg_replace(REMOVE_MULTILINE_COMMENTS, "", $FileContent);
        $FileContent = preg_replace(REMOVE_DEFINES, "", $FileContent);
        $FileContent = preg_replace(REMOVE_IFDEFS, "", $FileContent);
        $FileContent = preg_replace(REMOVE_DEFINITIONS, ";", $FileContent);
                
        // Explode by ";"
        $FileContentExpl = explode(';',$FileContent);

        // Return array
        return $FileContentExpl;
    }
    
    // Function to parse one file
    function ParseSelectedFile($InputFile, $Options, $Path)
    {
    	// Stack for remove-duplicates
    	$FunctionStack = array();

    	// Clean file before parsing
        $FileContentsNext = CleanFileContent($InputFile);
                
        // For each function run parsing
        foreach($FileContentsNext as $FunctionLine)
         {          
            // Check if correct function format found else skip it
            // Do the segmentation to 1:RETTYPE 2:NAME 3:PARAMS
            if (preg_match(MATCH_FUNCTION, $FunctionLine, $FuncParsed) !== 1)
                continue;

            // Check variable parameters function
            (preg_match(CHECK_VARIABLES, trim($FunctionLine), $NullVar)) ? $FcVarArgs = "yes" : $FcVarArgs = "no";
                 
            // Check for no-inline argument condition
            if ($Options->ParamNoinline && preg_match(REMOVE_INLINES, trim($FuncParsed[1]), $NullVar))
            	continue;
            
            // Segment parameters with devide by ","
        	$FcAllParams = explode(',',trim($FuncParsed[3]));

            // Chceck for max-par argument option
            $FcParamNums = count($FcAllParams);
            if ($FcVarArgs == "yes") $FcParamNums = $FcParamNums - 1;
		    if ($Options->ParamMaxpar && ($FcParamNums > $Options->ParamMaxparValue)) 
		    	continue;
                    
            // Parse definition as name and returntype
            $FcName = $FuncParsed[2];
            $FcReturn = trim($FuncParsed[1]);
                    
             // Remove new lines in output
            //$FcReturn = preg_replace("/[\n]/", " ", $FcReturn);

            // Proces argument no-whitespaces
            if ($Options->ParamRemovewhitespace)
            {
                $FcDefExplTrimmedS = preg_split(SPLIT_BY_WHITESPACE, $FcReturn);
				$FcReturn = implode(" ", $FcDefExplTrimmedS);
				$FcReturn = preg_replace(REMOVE_RIGHT_WHITESPACE, "$1$2", trim($FcReturn));
                $FcReturn = preg_replace(REMOVE_LEFT_WHITESPACE, "$1$2", trim($FcReturn));
            }
                    
            // Create file name
            ($Options->ParamInputType == 2) ? $FcFile = $Options->ParamInput : $FcFile = $Path . basename($InputFile);
                    
            // Process no-duplicates argument
            if ($Options->ParamNoduplicates)
            {
                if (!array_search($FcName, $FunctionStack)) 
                    $FunctionStack[$FcName] = $FcName; 
                else continue;
    	    }
            
            // Process pretty-xml argument and write output
            for ($WhiteSpaceNum = 1; $WhiteSpaceNum <= $Options->ParamPrettyxmlValue; $WhiteSpaceNum++) echo " ";
            printf ("<function file=\"%s\" name=\"%s\" varargs=\"%s\" rettype=\"%s\">", $FcFile, $FcName, $FcVarArgs, $FcReturn);
            if ($Options->ParamPrettyxml) echo "\n";

            // Parameter parser function
            for ($ParamNum = 0; $ParamNum < $FcParamNums; $ParamNum++)
            {
                // Skip empty parameters 
                if ($FcParamNums == 0) 
                	continue;
                
                // Get the actual parameter
                $ParamType = $FcAllParams[$ParamNum];

                // Empty param type skil
                if (strlen($ParamType) < 1) 
                	continue; 

                // Void param skip
                if ($ParamType == "void") 
                	continue;

                // Remove parameter name
                $ParamType = preg_replace(REMOVE_PARAMETER_NAME, "", $ParamType);

                // Remove new lines in output
                //$ParamType = preg_replace("/[\n]/", " ", $ParamType);

                // Proces argument no-whitespaces
                if ($Options->ParamRemovewhitespace)
                {
                	$ParamType = preg_split(SPLIT_BY_WHITESPACE, $ParamType);
					$ParamType = implode(" ", $ParamType);
					$ParamType = preg_replace(REMOVE_RIGHT_WHITESPACE, "$1$2", trim($ParamType));
               		$ParamType = preg_replace(REMOVE_LEFT_WHITESPACE, "$1$2", trim($ParamType));
                }
        	
                // Process pretty-xml argument and write XML output
                for ($WhiteSpaceNum = 1; $WhiteSpaceNum <= $Options->ParamPrettyxmlValue*2; $WhiteSpaceNum++) echo " ";
                printf("<param number=\"%d\" type=\"%s\" />", $ParamNum+1, trim($ParamType));
                if ($Options->ParamPrettyxml) echo "\n";
            }
                    
            // Process XML output end
            for ($WhiteSpaceNum = 1; $WhiteSpaceNum <= $Options->ParamPrettyxmlValue; $WhiteSpaceNum++) echo " ";
            printf ("</function>");
            if ($Options->ParamPrettyxml) echo "\n";
        }
    }
    
    // Function to parse files
    function ParseFiles($InputFiles, $Options)
    {   
        // Look for every file to parse it
        foreach($InputFiles as $InputFile=>$InputFileValue)
        {
            // If directory structure recursion
            if (is_array($InputFileValue))
                ParseFiles($InputFileValue, $Options);
            
            // If file then we have to parse it
            else ParseSelectedFile($InputFile, $Options, $InputFileValue);
       	}
    }
    
    // Get arguments array with getopt
    $longopts  = array("help", "input:", "output:", "pretty-xml::", "no-inline", "max-par:", "no-duplicates", "remove-whitespace");
    $Options = getopt("", $longopts);
    
    // Check number of precessed
    if ($argc != (count($Options)+1))
    {
    	fwrite(STDERR, "Error while getting arguments!\n");
        exit(1);
    }
    
    // Call function to parse arguments
    $FileArgStruc = ParseArguments($Options);

    // Modify if no input argument given
    if ($FileArgStruc->ParamInputType == NULL)
    {
    	$FileArgStruc->ParamInputType = 1;
    	$FileArgStruc->ParamInput = "./";
    }
   
    // Process ouput file argument
    if ($FileArgStruc->ParamOutput)
    {
        fclose(STDOUT);
        $OutputError = false;
        $STDOUT = @fopen($FileArgStruc->ParamOutput, 'wb') or $OutputError = true;
	
	if ($OutputError == true)
	{
	    fwrite(STDERR, "Can't create output file\n");
	    exit(3);
	}
    }

    // Process no / input argument given
    $AdditionalPath = "";
    if (substr($FileArgStruc->ParamInput, -1) != "/") $AdditionalPath = "/";

    // Get input file list
    if ($FileArgStruc->ParamInputType == 1)
        $InputFiles = GetFiles($FileArgStruc->ParamInput, $AdditionalPath);
    else
    {
        $InputFiles = array();
        $InputFiles[$FileArgStruc->ParamInput] = $AdditionalPath;
    }
    
    // Create XML header and check pretty argument for line
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    if ($FileArgStruc->ParamPrettyxml) echo "\n";

    // Create FUNCTIONS XML element
    if ($FileArgStruc->ParamInputType == 1) printf ("<functions dir=\"%s\">", $FileArgStruc->ParamInput);
    else if ($FileArgStruc->ParamInputType == 0) printf ("<functions dir=\"%s\">", "./");
    else if ($FileArgStruc->ParamInputType == 2) printf ("<functions dir=\"%s\">", "");
    if ($FileArgStruc->ParamPrettyxml) echo "\n";

    // Call function to parse files
    ParseFiles($InputFiles, $FileArgStruc);

    // End FUNCTIONS element of XML
    printf ("</functions>\n");
    exit(0);
?>