import sys, getopt, os.path, re

#JMP:xpavel27

#Define lists
InputSpaces = False
NoDuplicates = False
MacroDefList = {}
OutputList = ""
DEBUG = 1

# Define return structure
ArgumentStruc = {'RET': True, 'IsHelp': False, 'IsInput': False, 'IsOutput': False, 'IsCmd': False, 'IsR': False, 'Input': '', 'Output': '', 'Cmd': ''}
FunctionStruc = {'Position': 0, 'Output': ''}

# Function to get last char from input
def ReadChar(List):
	if len(List) > 0 :
		ch = List.pop(0)
		return ch
	else :
		return ""

# Function to print help
def PrintHelp():
	print ("----------------------------------------------")
	print ("Created by Martin Pavelka on 7.4.2015 for IPP2")
	print ("--help     : prints program help page")
	print ("--input=$  : input file in ASCI to parse")
	print ("--output=$ : output file in ASCI to save in")
	print ("--cmd=$    : text to be inserted to input")
	print ("-r         : redefinition of macro as error")
	print ("----------------------------------------------")

# Expansion mode for actual macro
def ExpansionMode(MacroName, Input):
	
	# InputSpaces is global variable
	global InputSpaces

	# Show expanding macro report
	if DEBUG == 1 : print ("Expanding macro:", MacroName)

	# Check if macro in dictionary	
	if (MacroName not in MacroDefList):
		print ("Undefined macro '@", MacroName, "'.", sep='', file=sys.stderr)
		sys.exit(56)

	# If macro is set (default one) or synonym of set 
	if (MacroName == "set" and MacroDefList[MacroName] == None) or MacroName == "__set__" :
		
		# Get block
		DefBlock = ParseInput(Input, True)

		# Get block value and process it
		if DefBlock == "-INPUT_SPACES" : InputSpaces = True
		elif DefBlock == "+INPUT_SPACES" : InputSpaces = False
		else : 
			print ("Wrong parameter for set macro!", file=sys.stderr)
			sys.exit(56)

		# Set returns nothing
		return ""

	# If macro is undef (actual one) or synonym of undef
	elif (MacroName == "undef" and  MacroDefList[MacroName] == None) or MacroName == "__undef__" :

		# Get macro name 
		UndefName = ParseInput(Input, True)

		# Chceck if not preddefined macro
		if UndefName == "__undef__" or UndefName == "__set__" or UndefName == "__def__" :
			print("'@", UndefName, "'cannot be deleted.", sep='')
			sys.exit(57)

		# Delete macro name
		del MacroDefList[UndefName]

		# Undef returns nothing
		return ""

	# If expanding def macro 
	elif (MacroName == "def" and MacroDefList[MacroName] == None) or MacroName == "__def__":

		# Init def macro structures
		DefMacroLocal = {}
		DefMacroArgs = {}
		Counter = 1

		# Get macro name 		
		DefName = ParseInput(Input, True)
		
		# Check for duplicates
		if NoDuplicates == True and (DefName in MacroDefList) :
			print ("Cant redef macro '@", DefName, "'!.", sep='', file=sys.stderr)
			sys.exit(57)

		# Get arguments and block
		DefArguments = ParseInput(Input, True)
		DefBlock = ParseInput(Input, True)

		# If any arguments found
		if len(DefArguments) > 0 :

			# Init check variables
			OutputArgsCheck = ""
			InputArgsCheck = DefArguments.replace(" ", "")

			# Parse every argument
			for m in re.finditer(r"\$[a-zA-Z_][a-zA-Z0-9_]*", DefArguments) :
				DefMacroArgs[str(Counter)] = ''
				DefBlock = DefBlock.replace(m.group(0), '{' + str(Counter) + '}')
				OutputArgsCheck += m.group(0)
				Counter = Counter + 1

			# Check if every argument parsed
			if len(OutputArgsCheck) != len(InputArgsCheck) :
				print("@def@", DefName, "{", DefArguments, "}", "{...}: Wrong specification of macro parameters. Missing '$'?.", sep='' , file=sys.stderr)
				sys.exit(55)

			# Check for unbound parameters
			MatchBlockCheck = re.search(r"\$[a-zA-Z_][a-zA-Z0-9_]*", DefBlock)
			if (MatchBlockCheck) : 
				print ("@def@f{...}{...} : Unbounded parameter '", MatchBlockCheck.group(), "'.", sep='', file=sys.stderr)
				sys.exit(55)		

		# Append block to declaration and save to macro table
		DefMacroLocal['ARGS'] = DefMacroArgs
		DefMacroLocal['BLOCK'] = DefBlock
		MacroDefList[DefName] = DefMacroLocal

		# Def returns nothing
		return ""

	# If another macro
	else :

		# Get datas
		MacroArguments = MacroDefList[MacroName].get('ARGS')
		MacroBlock = MacroDefList[MacroName].get('BLOCK')
		MacroArgumentsCount = len(MacroArguments)
		Counter = 1

		# If any arguments
		if (len(MacroArguments) > 0) :

			# Get arguments from input
			while (Counter <= MacroArgumentsCount) :
				InputData = ParseInput(Input, True)
				MacroArguments[str(Counter)] = InputData
				Counter = Counter + 1

			# Modify block for arguments
			Counter = 1
			while (Counter <= MacroArgumentsCount) :
				What = '{' + str(Counter) + '}'
				MacroBlock = MacroBlock.replace(What, MacroArguments.get(str(Counter)))
				Counter = Counter + 1
		

		# Do recursive expansion
		if MacroBlock != None :

			# Write message if debug on
			if DEBUG == 1 : print("Recursive expansion of:", MacroBlock)

			# Save result into input
			MacroBlockL = list(MacroBlock) 
			MacroBlockLR = reversed(MacroBlockL)
			for x in MacroBlockLR : Input.insert(0, x)

			# Already in input
			return ""

# Function to get arguments
def GetArguments():

	# Define local variables
	ArgStruct = ArgumentStruc
	Input = Output = Cmd = ''

	# Get arguments from STDIN
	options, remainder = getopt.getopt(sys.argv[1:], 'r', ['help','input=','output=','cmd=',])

	# Parse arguments into structure
	for opt, arg in options:
		if opt == '--help':
			ArgStruct['IsHelp'] = True;
		elif opt == '--input':
			ArgStruct['IsInput'] = True
			ArgStruct['Input'] = arg
		elif opt == '--output':
			ArgStruct['IsOutput'] = True
			ArgStruct['Output'] = arg
		elif opt == '--cmd':
			ArgStruct['IsCmd'] = True
			ArgStruct['Cmd'] = arg
		elif opt == '-r':
			ArgStruct['IsR'] = True

	# Check if only help argument passed
	if ArgStruct['IsHelp'] == True and len(options) > 1 :
		ArgStruct['RET'] = False

	# Check if input and output argument entered
	elif (ArgStruct['IsInput'] == False or ArgStruct['IsOutput'] == False) and ArgStruct['IsHelp'] == False :
		ArgStruct['RET'] = False 

	# Check input file
	elif (os.path.isfile(ArgStruct['Input']) == False or os.path.exists(ArgStruct['Input']) == False) and  ArgStruct['IsHelp'] == False :
		ArgStruct['RET'] = False

	# Return filled structure
	return ArgStruct

# Function to parse macro name
def ParseMacroName(Input):

	# Init macro name storage 
	CurrentMacro = ""

	# While name continue
	while (True):
		
		# Read one character 
		ch = ReadChar(Input)

		# If alfa or digit add character to name and if debug also print it
		if ch.isalpha() == True or ch.isdigit() == True or ch == '_':
			if DEBUG == 1: print ("<m>", ch)
			CurrentMacro += ch
			
		# Else return macro name
		else : 
			Input.insert(0, ch)
			return CurrentMacro

# Function to parse at signs
def ParseAtSign(Input, IfExpansion, IfColum):

	# Init structure
	ParseAtSignStruct = FunctionStruc
	
	# For standartization check first @
	ch = ReadChar(Input)
	if ch != '@' : sys.exit(55)

	# Get next character
	ch = ReadChar(Input)

	# If alpha then this is a macro name
	if ch.isalpha() or  ch == '_' :
		
		# Get macro name
		Input.insert(0, ch)
		if IfColum == True : Name = '@' + ParseMacroName(Input)
		else : Name = ParseMacroName(Input)
		
		# If not in expansion enter it
		if IfExpansion == False : Expanded = ExpansionMode(Name, Input)

	# If it is just escape section create output
	elif ch == "@" or ch == "{" or ch == "}" or ch == "$" :
		if DEBUG == 1 : print ("<e>", ch)
		return ch

	# Everything else is error
	else: 
		print ("Invalid escape sequence '@", ch, "'.", sep='', file=sys.stderr)
		sys.exit(55)

	# Return name or expanded
	if IfExpansion == True : return Name
	else : return Expanded

# Function to parse blocks
def ParseColumSign(Input, IfExpansion):

	# Init return structure and block storage
	ParseColumStruct = FunctionStruc
	CurrentBlock = ""

	# We need column counters
	LeftBraceCounter = 0
	RightBraceCounter = 0

	# Loop while } found
	while (True) :

		# Load one character
		ch = ReadChar(Input)

		# If left bracer increase counter 
		# Also add this to the output of block
		if ch == '{' :
			LeftBraceCounter = LeftBraceCounter + 1
			if LeftBraceCounter != 1 : 
				if DEBUG == 1 : print ("<c>", ch)
				CurrentBlock += ch
				
		# If right bracer found and is last finish loop
		elif ch == '}' and LeftBraceCounter == 1 : break

		# If right bracer found and not the last 
		# Decrease counter and add this to the output of block
		elif ch == '}' and LeftBraceCounter > 1 :
			if DEBUG == 1 : print ("<c>", ch)
			LeftBraceCounter = LeftBraceCounter - 1
			CurrentBlock += ch
			
		# If none right bracer syntax error
		elif ch == "" : 
			print("Expected '}', but end of input found.", file=sys.stderr)
			sys.exit(55)
			
		# If at sign call the function for it
		elif ch == '@' : 
			Input.insert(0, ch)
			CurrentBlock += ParseAtSign(Input, IfExpansion, True)

		# If another characrer add to the block
		else : 
			if DEBUG == 1 : print ("<c>", ch)
			CurrentBlock += ch

	# If just the block return as output or if not save block
	return CurrentBlock

# Function to parse input file
def ParseInput(Input, IfExpansion):
	
	# Init output storage
	CurrentOutput = ""
	
	# While end of file
	while (True):

		# Read one character
		ch = ReadChar(Input)
		
		# If at sign parse it
		if ch == '@' : 
			Input.insert(0, ch)
			CurrentOutput += ParseAtSign(Input, IfExpansion, False)
			if IfExpansion == True : break

		# If block parse it
		elif ch == '{' : 
			Input.insert(0, ch)
			CurrentOutput += ParseColumSign(Input, IfExpansion)
			if IfExpansion == True : break

		# If ilegal character found syntax error
		elif ch == '$' : 
			print("Invalid input character", ch, '.', file=sys.stderr)
			sys.exit(55)

		# If end of block but not block return error
		elif ch == '}' :
			print("Unexpected '}'. Forgotten '{'?", file=sys.stderr)
			sys.exit(55)

		# If end of file found break
		elif ch == "" : break	

		# Input spaces implementation
		elif (ch == " "  or ch == "\n" or ch == "\t") and InputSpaces == True : continue	

		# If another character add to output
		else : 
			if DEBUG == 1 : print ("<n>", ch)
			CurrentOutput += ch
			if IfExpansion == True : break

	# Return actual output
	return CurrentOutput
	
# Get argument and process them
Output = ""
Input = ""
Arguments = GetArguments()

# Save pre-defined macros
MacroDefList["undef"] = None
MacroDefList["__undef__"] = None
MacroDefList["def"] = None
MacroDefList["__def__"] = None
MacroDefList["set"] = None
MacroDefList["__set__"] = None

# If return code false return error
if Arguments['RET'] == False :
	sys.exit(1)

# If help found print it
if Arguments['IsHelp'] == True :
	PrintHelp()
	sys.exit(0)

# If r argument set it 
if Arguments['IsR'] == True :
	NoDuplicates = True

# If cmd TODO ADD TO INPUT FIRST
if Arguments['IsCmd'] == True :
	Input = Arguments['Cmd']

# Open input file and parse it
File = open(Arguments['Input'], "r")
Input += File.read()
File.close();

# Create list and process it
InputL = list(Input)
if len(InputL) > 0 : Output = ParseInput(InputL, False)

# Open output file and write output
FileOut = open(Arguments['Output'], "w")
FileOut.write(Output)
FileOut.close();

# If debug print macro table
if DEBUG == 1 : print ("MacroList:", MacroDefList)

