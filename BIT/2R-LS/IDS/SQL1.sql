BEGIN
    FOR rec IN (
        SELECT 'DROP ' || object_type || ' ' || object_name || DECODE ( object_type, 'TABLE', ' CASCADE CONSTRAINTS PURGE' ) AS v_sql
        FROM user_objects
        WHERE object_type IN ( 'TABLE', 'VIEW', 'PACKAGE', 'TYPE', 'PROCEDURE', 'FUNCTION', 'TRIGGER', 'SEQUENCE' )
        ORDER BY object_type, object_name
    ) LOOP
        EXECUTE IMMEDIATE rec.v_sql;
    END LOOP;
END;
/

CREATE SEQUENCE osoba_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE osoba 
(
    id          INTEGER         CONSTRAINT PK_osoba_id         PRIMARY KEY,
    meno        VARCHAR2(20)    CONSTRAINT NN_osoba_meno       NOT NULL,
    priezvisko  VARCHAR2(20)    CONSTRAINT NN_osoba_priezvisko NOT NULL,
    adresa      VARCHAR2(50)    CONSTRAINT NN_osoba_adresa     NOT NULL,
    telefon     INTEGER         CONSTRAINT NN_osoba_telefon    NOT NULL,
    mail        VARCHAR2(20)    CONSTRAINT NN_osoba_mail       NOT NULL
);

CREATE SEQUENCE opravnenia_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE opravnenia 
(
    id            INTEGER         CONSTRAINT PK_opravnenia_id         PRIMARY KEY,
    meno          VARCHAR2(20)    CONSTRAINT NN_opravnenia_meno       NOT NULL
);

CREATE SEQUENCE uzivatel_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE uzivatel 
( 
    id               INTEGER         CONSTRAINT PK_uzivatel_id           PRIMARY KEY,
    osoba_id         INTEGER         CONSTRAINT FK_uzivatel_osoba        REFERENCES osoba(id), 
    opravnenia_id    INTEGER         CONSTRAINT FK_uzivatel_opravnenia   REFERENCES opravnenia(id),  
    login            VARCHAR2(20)    CONSTRAINT NN_uzivatel_login        NOT NULL,
    heslo            VARCHAR2(20)    CONSTRAINT NN_uzivatel_heslo        NOT NULL

);

CREATE SEQUENCE kategoria_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kategoria 
(
    id            INTEGER         CONSTRAINT PK_kategoria_id          PRIMARY KEY,
    nazov         VARCHAR2(20)    CONSTRAINT NN_kategoria_nazov       NOT NULL
);

CREATE SEQUENCE software_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE software 
(
    id            INTEGER         CONSTRAINT PK_software_id            PRIMARY KEY,
    typ_licencie  VARCHAR2(20)    CONSTRAINT NN_software_typ_licencie  NOT NULL,
    konfiguracia  VARCHAR2(100)   CONSTRAINT NN_software_konfiguracia  NOT NULL
);

CREATE SEQUENCE hardware_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hardware 
(
    id            INTEGER         CONSTRAINT PK_hardware_id            PRIMARY KEY,
    gramaz        NUMBER(10,2)    CONSTRAINT NN_hardware_gramaz        NOT NULL,
    rozmery       VARCHAR2(20)    CONSTRAINT NN_hardware_rozmery       NOT NULL
);

CREATE SEQUENCE tovar_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE tovar 
(
    id            INTEGER         CONSTRAINT PK_tovar_id              PRIMARY KEY,
    nazov         VARCHAR2(50)    CONSTRAINT NN_tovar_nazov           NOT NULL,
    popis         VARCHAR2(50)	  CONSTRAINT NN_tovar_popis           NOT NULL,
    pocet         INTEGER         CONSTRAINT NN_tovar_pocet           NOT NULL,
    znacka        VARCHAR2(20)    CONSTRAINT NN_tovar_znacka		  NOT NULL,
    cena		  NUMBER(10,2)    CONSTRAINT NN_tovar_cena			  NOT NULL,
    dph 		  NUMBER(10,2)    CONSTRAINT NN_tovar_dph			  NOT NULL,
    skladom       CHAR(1)		  CONSTRAINT NN_tovar_skladom         CHECK (skladom in ('A', 'N')),
    software_id   INTEGER		  CONSTRAINT FK_tovar_software        REFERENCES software(id),
    hardware_id   INTEGER		  CONSTRAINT FK_tovar_hardware        REFERENCES hardware(id),
    kategoria_id  INTEGER  		  CONSTRAINT FK_tovar_kategoria		  REFERENCES kategoria(id)
);

CREATE SEQUENCE hodnotenie_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE hodnotenie 
(
    id            INTEGER         CONSTRAINT PK_hodnotenie_id         PRIMARY KEY,
    uzivatel_id   INTEGER  		  CONSTRAINT FK_hodnotenie_uzivatel   REFERENCES uzivatel(id),
    tovar_id      INTEGER 		  CONSTRAINT FK_hodnotenie_tovar      REFERENCES tovar(id),
    znamka        INTEGER	 	  CONSTRAINT NN_hodnotenie_znamka     CHECK (znamka BETWEEN 1 AND 5)
);

CREATE SEQUENCE kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE kosik 
(
    id            INTEGER         CONSTRAINT PK_kosik_id             PRIMARY KEY,
    uzivatel_id   INTEGER         CONSTRAINT FK_kosik_tovar          REFERENCES uzivatel(id)
);

CREATE SEQUENCE pol_kosik_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE pol_kosik 
(
    id            INTEGER         CONSTRAINT PK_pol_kosik_id         PRIMARY KEY,
    kosik_id      INTEGER  		  CONSTRAINT FK_pol_kosik_uzivatel   REFERENCES kosik(id),
    tovar_id      INTEGER         CONSTRAINT FK_pol_kosik_tovar      REFERENCES tovar(id)
);

CREATE SEQUENCE objednavka_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE;
CREATE TABLE objednavka 
(
    id               INTEGER          CONSTRAINT PK_objednavka_id            PRIMARY KEY,
    zaplatene        CHAR(1)		  CONSTRAINT NN_objednavka_zaplatene     CHECK (zaplatene in ('A', 'N')),
    expedovane       CHAR(1)		  CONSTRAINT NN_objednavka_expedovane    CHECK (expedovane in ('A', 'N')),
    uzivatel_id      INTEGER		  CONSTRAINT FK_objednavka_uzivatel_id   REFERENCES uzivatel(id),
    vybavil_id       INTEGER          CONSTRAINT FK_objednavka_vybavil_id	 REFERENCES uzivatel(id),
    kosik_id         INTEGER          CONSTRAINT FK_objednavka_kosik_id	     REFERENCES kosik(id),
    cena             NUMBER(10,2)      CONSTRAINT NN_objednavka_cena         NOT NULL
);


INSERT INTO osoba (ID, MENO, PRIEZVISKO, ADRESA, TELEFON, MAIL) VALUES (osoba_seq.nextval, 'Martin', 'Pavelka', 'Hlboka 18', '0948304516', 'dlxko@icloud.com');
INSERT INTO opravnenia (ID, MENO) VALUES (opravnenia_seq.nextval, 'admin');
INSERT INTO uzivatel (ID, OSOBA_ID, OPRAVNENIA_ID, LOGIN, HESLO) VALUES (uzivatel_seq.nextval, osoba_seq.currval, opravnenia_seq.currval, 'xpavel27', 'heslo');
INSERT INTO kategoria (ID, NAZOV) VALUES (kategoria_seq.nextval, 'notebook');
INSERT INTO kategoria (ID, NAZOV) VALUES (kategoria_seq.nextval, 'os');
INSERT INTO software (ID, TYP_LICENCIE, KONFIGURACIA) VALUES (software_seq.nextval, 'Freeware', 'Mac OSX');
INSERT INTO hardware (ID, GRAMAZ, ROZMERY) VALUES (hardware_seq.nextval, '3500,5', '30x30x30');
INSERT INTO tovar (ID, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, HARDWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'MacBook Pro', 'MacBook Pro Retina 2014', '1', 'Apple', '1000', '200', 'A', hardware_seq.currval, kategoria_seq.currval-1);
INSERT INTO tovar (ID, NAZOV, POPIS, POCET, ZNACKA, CENA, DPH, SKLADOM, SOFTWARE_ID, KATEGORIA_ID) VALUES (tovar_seq.nextval, 'Windows 7', 'Windows 7 Home Premium', '1', 'Microsoft', '500', '100', 'N', software_seq.currval, kategoria_seq.currval);
INSERT INTO hodnotenie (ID, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval, tovar_seq.currval-1, '4');
INSERT INTO hodnotenie (ID, UZIVATEL_ID, TOVAR_ID, ZNAMKA) VALUES (hodnotenie_seq.nextval, uzivatel_seq.currval, tovar_seq.currval, '3');
INSERT INTO kosik (ID, UZIVATEL_ID) VALUES (kosik_seq.nextval, uzivatel_seq.currval);
INSERT INTO pol_kosik (ID, KOSIK_ID, TOVAR_ID) VALUES (pol_kosik_seq.nextval, kosik_seq.currval, tovar_seq.currval-1);
INSERT INTO pol_kosik (ID, KOSIK_ID, TOVAR_ID) VALUES (pol_kosik_seq.nextval, kosik_seq.currval, tovar_seq.currval);
INSERT INTO objednavka (ID, ZAPLATENE, EXPEDOVANE, UZIVATEL_ID, VYBAVIL_ID, KOSIK_ID, CENA) VALUES (objednavka_seq.nextval, 'A', 'N', uzivatel_seq.currval, uzivatel_seq.currval, kosik_seq.currval, '3000');
