/* Required headers */
#include "c_global.h"
#include "c_list.h"
#include "c_opcodes.h"
#include "c_arguments.h"

/* Function to get arguments */
ArgStruc GetArgs(int argc, char * argv[])
{
    // Used local variables
    ArgStruc Args;
    memset(&Args, 0, sizeof(Args));
    int OptChar = 0;
    int Counter = 0;
    InitList(&Args.List);
    
    // Look for every command and also create sequence
    while ((OptChar = getopt(argc, argv, "h:p:l:u:LUGNHS")) != -1)
    {
        // Switch one option to process
        switch (OptChar)
        {
            case 'h':
                Args.HostName = optarg;
                break;
            case 'p':
                Args.Port = atoi(optarg);
                break;
            // Here look for every parameter argument
            case 'l':
                Args.LFLogin = true;
                Insert(&Args.List, optarg);
                while(optind < argc && argv[optind][0] != '-')
                    Insert(&Args.List, argv[optind++]);
                break;
            case 'u':
                Args.LFUID = true;
                Args.UUID = optarg;
                break;
            case 'L':
                Counter = Counter + 1;
                Args.Logg = true;
                Args.SeqLoggin = Counter;
                break;
            case 'U':
                Counter = Counter + 1;
                Args.UID = true;
                Args.SeqUID = Counter;
                break;
            case 'G':
                Counter = Counter + 1;
                Args.GID = true;
                Args.SeqGID = Counter;
                break;
            case 'N':
                Counter = Counter + 1;
                Args.Name = true;
                Args.SeqName = Counter;
                break;
            case 'H':
                Counter = Counter + 1;
                Args.Home = true;
                Args.SeqHome = Counter;
                break;
            case 'S':
                Counter = Counter + 1;
                Args.Shell = true;
                Args.SeqShell = Counter;
                break;
            default: break;
        }
    }
    Args.Count = Counter;
    
    if (Args.Port == 0 || Args.HostName == NULL)
        Args.ReturnCode = RET_FUNCTION_ERROR;
    
    if (!Args.LFUID && !Args.LFLogin)
        Args.ReturnCode = RET_FUNCTION_ERROR;
    
    return Args;
}
