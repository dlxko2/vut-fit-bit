/* Required includes */
#include "c_global.h"
#include "c_opcodes.h"
#include "c_list.h"

/* Function to insert into list */
bool Insert(TList *list, char *login)
{
    // New element and allocation
    TListElem *NewElement;
    if ((NewElement = malloc(sizeof(TListElem)))== NULL) return RET_FUNCTION_ERROR;
    
    // Fill it
    NewElement->login = login;
    NewElement->next = NULL;
    // Point on it
    if (list->First == NULL)
    {
        list->Last = NewElement;
        list->First = NewElement;
    }
    else
    {
        list->Last->next = NewElement;
        list->Last = NewElement;
    }
  
    return RET_FUNCTION_SUCCESS;
}

/* Function to init list */
void InitList (TList *L)
{
    L->First = NULL;
}

/* Function to dispose list */
void DisposeList (TList *L)
{
    // Create variables
    TListElem *MazanyElement;
    TListElem *LoopElement = L->First;
    
    // Only if not empty
    if (L->First != NULL)
    {
        // While any element
        while (LoopElement != NULL)
        {
            // Get next and remove
            MazanyElement = LoopElement;
            LoopElement = LoopElement->next;
            free(MazanyElement);
        }
        
        // Make it empty
        L->First = NULL;
    }
}

