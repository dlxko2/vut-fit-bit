/* Defines for local return values */
#ifndef MyClient_opcodes_h
#define MyClient_opcodes_h

#define RET_SUCCESS          0
#define RET_ARGUMENT_ERROR   1
#define RET_CONNECTION_ERROR 2
#define RET_SEND_ERROR       3
#define RET_RECV_ERROR       4
#define RET_SOCCLOSE_ERROR   5

#define RET_FUNCTION_SUCCESS 0
#define RET_FUNCTION_ERROR   1
#define RET_FUNCTION_EXTRA   10
#endif
