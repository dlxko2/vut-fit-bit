/* Header of global deifnitions */
#ifndef MyClient_global_h
#define MyClient_global_h

/* Include requested headers */
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Defines */
#define DEBUG_MODE 0
#define MAX_BUFFER 80
#define PACKET_SIZE_DEFAULT 7
#define PACKET_SIZE_LOOKFOR 20
#define PACKET_SIZE_END 28

/* Define structure for list */
typedef struct ListElem
{
    char *login;
    struct ListElem *next;
}   TListElem;

/* Define structure for element of list */
typedef struct List
{
    struct ListElem *First;
    struct ListElem *Last;
}   TList;

/* Structure for arguments */
typedef struct
{
    // Look information type and data
    bool LFLogin, LFUID;
    TList List;
    char *UUID;
    
    // Connection informations
    char *HostName;
    int Port;
    
    // Requested data
    bool Logg, UID, GID, Name, Home, Shell;
    
    // Sequence storage
    int SeqLoggin, SeqUID, SeqGID, SeqName, SeqHome, SeqShell, Count;
    
    // Return code
    int ReturnCode;
}   ArgStruc;

/* Structure for client socket */
typedef struct
{
    struct sockaddr_in CSAddr;
    int CS;
    int Succ;
}   ClientSocket;

/* Data extracted structure */
typedef struct
{
    char *Loggin, *UID, *GID, *Name, *Home, *Shell;
}   ResultStruc;

#endif
