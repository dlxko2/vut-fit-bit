/* Requested includes */
#include "c_global.h"
#include "c_receive.h"
#include "c_opcodes.h"

/* Function to receive answer */
int ReceiveAnswer(int CS, ArgStruc *Args, char* Login)
{
    // Define variable for size information
    char SizeBuffer[MAX_BUFFER];
    memset(SizeBuffer, 0, MAX_BUFFER);
    
    // Read size information
    ssize_t RecvLen = read(CS, SizeBuffer, 10);
    if(RecvLen <= 0) return RET_FUNCTION_ERROR;
    
    // Check for unkown
    if (strcmp(SizeBuffer, "end") == 0)
        return RET_FUNCTION_EXTRA;
    
    // Check for unkown
    if (strcmp(SizeBuffer, "unkown") == 0)
    {
        if (Args->LFLogin) fprintf(stderr, "Chyba: neznamy login %s\n", Login);
        else fprintf(stderr, "Chyba: nezname uid %s\n", Args->UUID);
        return RET_FUNCTION_SUCCESS;
    }
    
    // Get number of size to use
    int SizeOfMsg = atoi(SizeBuffer);
   
    // Buffer for answer
    char Buffer[MAX_BUFFER];
    memset(Buffer, 0, MAX_BUFFER);
    
    // Receive and check
    RecvLen = read(CS, Buffer, (size_t)SizeOfMsg);
    if(RecvLen <= 0) return RET_FUNCTION_ERROR;
    
    // Create debug info
    if (DEBUG_MODE)
    {
        printf("<---");
        int i = 0;
        for (i = 0; i < SizeOfMsg; i++)
            printf("[%c]", Buffer[i]);
        printf("\n");
    }
    
    // Define output variables
    char *popo = NULL;
    ResultStruc Result;
    memset(&Result, 0, sizeof(Result));
    
    // Extract datas from answer
    popo = strtok (Buffer,":");
    if (Args->Logg)  Result.Loggin = popo;
    popo = strtok(NULL, ":");
    if (Args->UID)   Result.UID    = popo;
    popo = strtok(NULL, ":");
    if (Args->GID)   Result.GID    = popo;
    popo = strtok(NULL, ":");
    if (Args->Name)  Result.Name   = popo;
    popo = strtok(NULL, ":");
    if (Args->Home)  Result.Home   = popo;
    popo = strtok(NULL, ":");
    if (Args->Shell) Result.Shell  = popo;
    
    // Create output sequence
    int Counter;
    for (Counter = 1; Counter <= Args->Count; Counter++)
    {
        if (Args->SeqGID == Counter) printf("%s", Result.GID);
        else if (Args->SeqHome == Counter) printf("%s", Result.Home);
        else if (Args->SeqLoggin == Counter) printf("%s", Result.Loggin);
        else if (Args->SeqName == Counter) printf("%s", Result.Name);
        else if (Args->SeqShell == Counter) printf("%s", Result.Shell);
        else if (Args->SeqUID == Counter) printf("%s", Result.UID);
        if (Counter < Args->Count) printf(" ");
    }
    printf("\n");
    
    // Return result
    return RET_FUNCTION_SUCCESS;
}
