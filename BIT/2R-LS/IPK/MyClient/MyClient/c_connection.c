/* Required includes */
#include "c_global.h"
#include "c_opcodes.h"
#include "c_connection.h"

/* Function to set up client BSD sockets */
ClientSocket SetUpClient(ArgStruc Args)
{
    // Local variable
    ClientSocket ClieSock;
    ClieSock.Succ = RET_FUNCTION_ERROR;
    
    // Get ip from hostname
    struct hostent *Host =  gethostbyname(Args.HostName);
    if (Host == NULL)
    {
        ClieSock.Succ = RET_FUNCTION_ERROR;
        return ClieSock;
    }
    
    // Set adress parameters
    memset(&ClieSock.CSAddr, 0, sizeof(ClieSock.CSAddr));
    ClieSock.CSAddr.sin_family = PF_INET;
    ClieSock.CSAddr.sin_port = htons(Args.Port);
    ClieSock.CSAddr.sin_addr.s_addr = INADDR_ANY;
    memcpy(&ClieSock.CSAddr.sin_addr, Host->h_addr, Host->h_length);
    
    // Create socket
    if ((ClieSock.CS = socket(PF_INET, SOCK_STREAM, 0 )) < 0)
    {
        ClieSock.Succ = RET_FUNCTION_ERROR;
        return ClieSock;
    }
    
    // Connect to server
    if(connect(ClieSock.CS, (struct sockaddr *)&ClieSock.CSAddr, sizeof(ClieSock.CSAddr) ) < 0)
    {
        ClieSock.Succ = RET_FUNCTION_ERROR;
        return ClieSock;
    }
    
    // Return configuration
    ClieSock.Succ = RET_FUNCTION_SUCCESS;
    return ClieSock;
}
