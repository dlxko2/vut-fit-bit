/* Includes requested */
#include "c_global.h"
#include "c_opcodes.h"
#include "c_send.h"

/* Function to send request */
bool SendQuestion(int CS, ArgStruc Args, char* Login)
{
    // Size return value
    ssize_t SndLen = 0;
    
    // Inicialize clean message
    char SendText[MAX_BUFFER];
    memset(SendText, 0, MAX_BUFFER);
    memcpy(SendText, "xxxxxxx", PACKET_SIZE_DEFAULT);
    
    // Create OPCODE for reqest
    if (Args.Logg) SendText[0] = 'l';
    if (Args.UID) SendText[1] = 'u';
    if (Args.GID) SendText[2] = 'g';
    if (Args.Name) SendText[3] = 'n';
    if (Args.Home) SendText[4] = 'h';
    if (Args.Shell) SendText[5] = 's';
    
    // Create OPCODE for Lookup
    if (Args.LFUID) SendText[6] = 'U';
    else if (Args.LFLogin) SendText[6] = 'L';
    
    // Add separator
    SendText[7] = ':';
    
    // Add lookfor informations
    if (Args.LFLogin)
    {
        memcpy(&SendText[PACKET_SIZE_DEFAULT+1], Login, strlen(Login));
        SndLen = write(CS, SendText, PACKET_SIZE_DEFAULT+PACKET_SIZE_LOOKFOR+1);
    }
    else
    {
        memcpy(&SendText[PACKET_SIZE_DEFAULT+1], Args.UUID, strlen(Args.UUID));
        SndLen = write(CS, SendText, PACKET_SIZE_DEFAULT+PACKET_SIZE_LOOKFOR+1);
    }
    
    // Create debug info
    if (DEBUG_MODE)
    {
        printf("---->");
        int i = 0;
        for (i = 0; i < PACKET_SIZE_DEFAULT+PACKET_SIZE_LOOKFOR+1; i++)
            printf("[%c]", SendText[i]);
        printf("\n");
    }
    
    // Send message
    if (SndLen <= 0) return RET_FUNCTION_ERROR;
    
    // Return success
    return RET_FUNCTION_SUCCESS;
}
