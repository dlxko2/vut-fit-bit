/* Incluce all headers */
#include "c_global.h"
#include "c_send.h"
#include "c_receive.h"
#include "c_connection.h"
#include "c_list.h"
#include "c_arguments.h"
#include "c_opcodes.h"

/* Main function of client program */
int main(int argc, char * argv[])
{
    // Recv code
    int RecvOpcode = 0;
    
    // Get arguments and check for succ
    ArgStruc Args  = GetArgs(argc,argv);
    if (Args.ReturnCode != RET_SUCCESS)
    {
        fprintf(stderr, "Error getting arguments!\n");
        return RET_ARGUMENT_ERROR;
    }
    
    
    // Get network connection and check
    ClientSocket ClieSock = SetUpClient(Args);
    if (ClieSock.Succ != RET_SUCCESS)
    {
        fprintf(stderr, "Error creating connection!\n");
        return RET_CONNECTION_ERROR;
    }
    
    // If look for login
    if (Args.LFLogin)
    {
        TListElem *Elem;
        
        // Browse all logins entered
        for (Elem = Args.List.First; Elem != NULL; Elem = Elem->next)
        {
            // Send question about login
            if (SendQuestion(ClieSock.CS, Args, Elem->login) != RET_SUCCESS)
            {
                fprintf(stderr, "Error while sending message!\n");
                close(ClieSock.CS);
                return RET_SEND_ERROR;
            }
            // Receive answer from server
            if (ReceiveAnswer(ClieSock.CS, &Args, Elem->login) != RET_FUNCTION_SUCCESS)
            {
                fprintf(stderr, "Error while receiving answer!\n");
                close(ClieSock.CS);
                return RET_RECV_ERROR;
            }
        }
    }
    
    // Ask with UID argument
    else if (Args.LFUID)
    {
        // Send question about login
        if (SendQuestion(ClieSock.CS, Args, NULL) != RET_SUCCESS)
        {
            fprintf(stderr, "Error while sending message!\n");
            close(ClieSock.CS);
            return RET_SEND_ERROR;
        }
        
        // Receive answer
        while (RecvOpcode != RET_FUNCTION_EXTRA)
        {
            RecvOpcode = ReceiveAnswer(ClieSock.CS, &Args, NULL);
            if (RecvOpcode != RET_FUNCTION_SUCCESS && RecvOpcode != RET_FUNCTION_EXTRA)
            {
                fprintf(stderr, "Error while receiving answer!\n");
                close(ClieSock.CS);
                return RET_RECV_ERROR;
            }
        }
    }
    
    // Send end-socket
    if (write(ClieSock.CS, "end", PACKET_SIZE_END) == -1)
        fprintf(stderr, "Error while sending end message!\n");
    
    // Close socket
    if (close(ClieSock.CS) < 0)
        return RET_SOCCLOSE_ERROR;
   
    // Return sucess
    return RET_SUCCESS;
}
