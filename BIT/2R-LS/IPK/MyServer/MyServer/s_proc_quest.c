/* Required headers */
#include "s_global.h"
#include "s_send_answer.h"
#include "s_opcodes.h"
#include "s_proc_quest.h"

/* Function to process requested data */
bool ProcessQuest (QuestStruc Quests, int CS)
{
    // Local variable
    QuestFeedback QuestFeed;
    memset(&QuestFeed, 0, sizeof(QuestFeed));
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    bool Found = false;
    
    // Try to open passwd file
    FILE * PasswdFile = NULL;
    PasswdFile = fopen(PASSWD_FILE, "r");
    if (!PasswdFile) return RET_FUNCTION_ERROR;
    
    // Create regular expression
    char RegExp[MAX_REGEXP_BUFFER];
    memset(RegExp, 0, MAX_REGEXP_BUFFER);
    if (Quests.LFLogin) sprintf(RegExp, "^%s:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+:[^:]+", Quests.LookUp);
    else if (Quests.LFUID) sprintf(RegExp, "^[^:]+:[^:]+:%s:[^:]+:[^:]+:[^:]+:[^:]+", Quests.LookUp);
    
    // Compile it
    regex_t regex;
    int reti = regcomp(&regex, RegExp, REG_EXTENDED | REG_ICASE);
    if (reti) return RET_FUNCTION_ERROR;
    
    // Get each line of file
    while ((read = getline(&line, &len, PasswdFile)) != -1)
    {
        // Execute regular expression
        reti = regexec(&regex, line, 0, NULL, 0);
        if (reti == REG_NOMATCH) continue;

        // Remove new-line character
        if (line[strlen(line) - 1] == '\n' || (line)[len - 1] == '\r')
            line[strlen(line) - 1] = '\0';
        
        // Extract datas from line
        char *popo = strtok (line,":");
        if (Quests.Loggin) QuestFeed.Loggin = popo;
        
        // Parse data
        popo = strtok (NULL,":"); // PASS
        popo = strtok (NULL,":"); // UID
        if (Quests.UID) QuestFeed.UID = popo;
        else QuestFeed.UID = "x";
        popo = strtok (NULL,":"); // GID
        if (Quests.GID) QuestFeed.GID = popo;
        else QuestFeed.GID = "x";
        popo = strtok (NULL,":"); // NAME
        if (Quests.Name) QuestFeed.Name = popo;
        else QuestFeed.Name = "x";
        popo = strtok (NULL,":"); // HOME
        if (Quests.Home) QuestFeed.Home = popo;
        else QuestFeed.Home = "x";
        popo = strtok (NULL,":"); // SHELL
        if (Quests.Shell) QuestFeed.Shell = popo;
        else QuestFeed.Shell = "x";
        
        // Send answer
        SendAnswer(CS, QuestFeed);

        // Check if found
        Found = true;
    }
    
    // If not found send unkown
    if (!Found) write(CS, "unkown", PACKET_SIZE_UNKOWN);
    
    // End function
    fclose(PasswdFile);
    
    // Send end socket
    if (Quests.LFUID)
    {
        if (write(CS, "end", PACKET_SIZE_END) == -1)
            return RET_FUNCTION_ERROR;
    }
    
    // Return success
    return RET_FUNCTION_SUCCESS;
}
