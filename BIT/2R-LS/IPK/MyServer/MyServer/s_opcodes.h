//
//  opcodes.h
//  MyServer
//
//  Created by Martin Pavelka on 29.3.2015.
//  Copyright (c) 2015 Martin Pavelka. All rights reserved.
//

#ifndef MyServer_opcodes_h
#define MyServer_opcodes_h

#define RET_SUCCESS          0
#define RET_ARGUMENT_ERROR   1
#define RET_CONNECTION_ERROR 2
#define RET_SEND_ERROR       3
#define RET_RECV_ERROR       4
#define RET_SOCCLOSE_ERROR   5
#define RET_FORKING_ERROR   5

#define RET_FUNCTION_SUCCESS 0
#define RET_FUNCTION_ERROR   1
#define RET_FUNCTION_EXTRA   2

#endif
