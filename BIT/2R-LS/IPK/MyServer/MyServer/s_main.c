/* Required headers */
#include "s_global.h"
#include "s_connection.h"
#include "s_send_answer.h"
#include "s_proc_quest.h"
#include "s_recv_answer.h"
#include "s_opcodes.h"

/* Main function  of server program */
int main(int argc, char * argv[])
{
    // Base check for argument number
    if (argc < 3) RET_ARGUMENT_ERROR;
    
    // Define client socket variables
    int CS = 0;
    ClinetSocket ClieSock;
    ClieSock.CSAddrLen = sizeof(ClieSock.CSAddr);
    
    // Create configuration of connection and Server Socket
    ServerSocket ServSock = SetuUpServer(argv);
    if (ServSock.Succ != RET_FUNCTION_SUCCESS)
    {
        fprintf(stderr, "Error creating server socket!\n");
        return RET_CONNECTION_ERROR;
    }
    
    // Wait for incomming connection
    while(1)
    {
        // Accept incomming connection
        if ((CS = accept(ServSock.SS, (struct sockaddr*)&ClieSock.CSAddr, &ClieSock.CSAddrLen)) < 0 )
            continue;
        
        // Fork program
        int pid = fork();
        if (pid < 0)
        {
            fprintf(stderr, "Error forking process!\n");
            return RET_FORKING_ERROR;
        }
        
        // Child program
        if (pid == 0)
        {
            // Wait for receive
            while(1)
            {
                // Receive answer
                int RecvRet = ReceiveAnswer(CS);
                // If error return error
                if (RecvRet == RET_FUNCTION_ERROR) return RET_RECV_ERROR;
                // If end message break waiting for data
                if (RecvRet == RET_FUNCTION_EXTRA) break;
                // If success wait for next data
                if (RecvRet == RET_FUNCTION_SUCCESS) continue;
            }
            
            // Close program
            close(CS);
            return RET_SUCCESS;
        }
    }
    
    // Return success
    return RET_SUCCESS;
}
