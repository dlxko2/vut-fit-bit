/* Required headers */
#include "s_global.h"
#include "s_connection.h"
#include "s_opcodes.h"

/* Function to create server BSD sock comm */
ServerSocket SetuUpServer(char * argv[])
{
    // Function variables
    ServerSocket ServSock;
    ServSock.Succ = RET_FUNCTION_ERROR;
    
    // Get port number as argument
    int ArgPort = atoi(argv[2]);
                       
    
    if ((strcmp("-p", argv[1]) != 0) && (ArgPort< 1))
    {
        fprintf(stderr, "Error while getting arguments!\n");
        ServSock.Succ = RET_FUNCTION_ERROR;
        return ServSock;
    }
    
    // Set address parameters
    memset(&ServSock.SSAddr, 0, sizeof(ServSock.SSAddr));
    ServSock.SSAddr.sin_family = PF_INET;
    ServSock.SSAddr.sin_port = htons(ArgPort);
    ServSock.SSAddr.sin_addr.s_addr = INADDR_ANY;
    
    // Create socket
    if ((ServSock.SS = socket(PF_INET, SOCK_STREAM, 0 )) < 0)
    {
        ServSock.Succ = RET_FUNCTION_ERROR;
        return ServSock;
    }
    
    // Bund socket
    int BindErr = bind(ServSock.SS, (struct sockaddr *)&ServSock.SSAddr, sizeof(ServSock.SSAddr));
    if(BindErr != 0)
    {
        ServSock.Succ = RET_FUNCTION_ERROR;
        return ServSock;
    }
    
    // Listen
    int ListenErr = listen(ServSock.SS, 5);
    if (ListenErr < 0)
    {
        ServSock.Succ = RET_FUNCTION_ERROR;
        return ServSock;
    }
    
    // Return configuration
    ServSock.Succ = RET_FUNCTION_SUCCESS;
    return ServSock;
}
