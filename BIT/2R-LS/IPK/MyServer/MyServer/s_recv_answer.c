/* Required headers */
#include "s_global.h"
#include "s_proc_quest.h"
#include "s_recv_answer.h"
#include "s_opcodes.h"

/* Function to receive answer */
int ReceiveAnswer(int CS)
{
    // Struc of quests
    QuestStruc Quests;
    memset(&Quests, 0, sizeof(Quests));
    
    // Receive
    char Buffer[MAX_BUFFER];
    memset(Buffer, 0, sizeof(Buffer));
    ssize_t RecvLen = read(CS, Buffer, PACKET_SIZE_CLIENT);
    if(RecvLen <= 0) return RET_FUNCTION_ERROR;
    
    // Printf debug message
    if (DEBUG_MODE)
    {
        printf("<---");
        int i = 0;
        for (i = 0; i < PACKET_SIZE_CLIENT; i++)
            printf("[%c]", Buffer[i]);
        printf("\n");
    }
    
    // Chceck for terminating socket
    if (Buffer[0] == 'e' && Buffer[1] == 'n' && Buffer[2] == 'd')
        return RET_FUNCTION_EXTRA;
    
    // Get info about quests
    if (Buffer[0] == 'l') Quests.Loggin = true;
    if (Buffer[1] == 'u') Quests.UID = true;
    if (Buffer[2] == 'g') Quests.GID = true;
    if (Buffer[3] == 'n') Quests.Name = true;
    if (Buffer[4] == 'h') Quests.Home = true;
    if (Buffer[5] == 's') Quests.Shell = true;
    if (Buffer[6] == 'U') Quests.LFUID = true;
    if (Buffer[6] == 'L') Quests.LFLogin = true;
        
    // Get UID or login
    Quests.LookUp = strtok (Buffer,":");
    Quests.LookUp = strtok (NULL,":");
    
    // Process quests
    ProcessQuest(Quests, CS);
    return RET_FUNCTION_SUCCESS;
}
