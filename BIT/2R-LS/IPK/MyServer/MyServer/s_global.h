/* Header for global definitions */
#ifndef MyServer_global_h
#define MyServer_global_h

/* Required headers */
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <regex.h>

/* Defines */
#define PASSWD_FILE "/etc/passwd"
#define DEBUG_MODE 0
#define PACKET_SIZE_CLIENT 28
#define PACKET_SIZE_INFO 10
#define PACKET_SIZE_UNKOWN 10
#define PACKET_SIZE_END 10
#define MAX_BUFFER 100
#define MAX_REGEXP_BUFFER 100

/* Structure of server socket */
typedef struct
{
    struct sockaddr_in SSAddr;
    int SS;
    bool Succ;
} ServerSocket;

/* Structure of client socket */
typedef struct
{
    struct sockaddr_in CSAddr;
    socklen_t CSAddrLen;
} ClinetSocket;

/* Data requested structure */
typedef struct
{
    bool Loggin, UID, GID, Name, Home, Shell;
    bool LFLogin, LFUID;
    char *LookUp;
} QuestStruc;

/* Data extracted structure */
typedef struct
{
    char *Loggin, *UID, *GID, *Name, *Home, *Shell;
} QuestFeedback;

#endif
