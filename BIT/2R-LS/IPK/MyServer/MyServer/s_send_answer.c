/* Reuired headers */
#include "s_global.h"
#include "s_opcodes.h"
#include "s_send_answer.h"

/* Function to send datas to client */
bool SendAnswer(int CS, QuestFeedback QuestFeed)
{
    // Create data information
    char SendMsg[MAX_BUFFER];
    memset(SendMsg, 0, MAX_BUFFER);
    sprintf(SendMsg, "%s:%s:%s:%s:%s:%s", QuestFeed.Loggin, QuestFeed.UID, QuestFeed.GID, QuestFeed.Name, QuestFeed.Home, QuestFeed.Shell);
    
    // Create size information
    char SizeMsg[PACKET_SIZE_INFO];
    memset(SizeMsg, 0, PACKET_SIZE_INFO);
    sprintf(SizeMsg, "%lu", strlen(SendMsg));
    
    // Create final message
    char CompleteMsg[MAX_BUFFER];
    memset(CompleteMsg, 0, MAX_BUFFER);
    memcpy(CompleteMsg, SizeMsg, strlen(SizeMsg));
    memcpy(&CompleteMsg[PACKET_SIZE_INFO], SendMsg, strlen(SendMsg));
    
    // Debug packet
    if (DEBUG_MODE)
    {
        printf("--->");
        int i = 0;
        for (i = 0; i < PACKET_SIZE_INFO + strlen(SendMsg) - 1; i++)
            printf("[%c]", CompleteMsg[i]);
        printf("\n");
    }
    
    // Send it to client
    ssize_t SndLen = write(CS, CompleteMsg, strlen(SendMsg)+PACKET_SIZE_INFO);
    if (SndLen == -1) return RET_FUNCTION_ERROR;
    
    // Return success
    return RET_FUNCTION_SUCCESS;
}
