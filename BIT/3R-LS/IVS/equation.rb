class Equation 
  def self.solve_quadratic(a, b, c)
	
	# Ak je aj A aj B nula vratime neriesitelnu rovnicu
	if (a == 0 and b == 0)
		return nil
	end

	# Ak je A nula potom vratime upravenu rovnicu
	if (a == 0)
		return [-c/b.to_f]
	end

	# Spocitame diskriminant a rozhodneme pocet rieseni kvadratickej
	discriminant = (b*b) - (4*a*c)

	# Ak je mensi ako nula potom nema riesenie
	if (discriminant < 0)
		return nil

	# Ak je nula ma jedno dvojite riesenie
	elsif (discriminant == 0)
		x = -b/2.to_f * a
		return [x]

	# Ak je vecsi ako nula ma dva riesenia
	else
		x = (-b + Math.sqrt(discriminant))/(2*a).to_f
		y = (-b - Math.sqrt(discriminant))/(2*a).to_f
		return [x,y]
	end
  end
end
