require 'test/unit'
require_relative 'fib-sequence'

class FibonacciSequenceTest < Test::Unit::TestCase
  def setup
  end
  
  # Test na inicializaciu parametrov aktualneho prvku a aktualnej hodnoty
  def test_init
  	sequence1 = FibonacciSequence.new
    assert_equal( nil, sequence1.current(), "Initialization of current value don't work!")
    assert_equal( nil, sequence1.current_idx(), "Initialization of current index don't work!")
  end

  # Test na overenie prvej podmienky rovnice, ze nulty prvok je nula
  def test_first
  	sequence2 = FibonacciSequence.new
  	sequence2.next()
    assert_equal( 0, sequence2.current(), "First number is wrong!")
    assert_equal( 0, sequence2.current_idx(), "First number index is wrong!")
  end

  # Test na overenie druhej podmienky rovnice, ze prvy prvok je jedna
  def test_second
  	sequence3 = FibonacciSequence.new
  	sequence3.next()
  	sequence3.next()
    assert_equal( 1, sequence3.current(), "Second number is wrong!")
    assert_equal( 1, sequence3.current_idx(), "Second number index is wrong!")
  end

  # Test na overenie tretej podmienky rovnice pre lubovolny prvok a jeho index
  def test_random
  	sequence4 = FibonacciSequence.new
  	generator = Random.new

  	# Test vykoname pre 5 nahodnych cisiel
  	for i in 0..5 		
		random = generator.rand(10...42)
   		x = sequence4[random]
   		y = sequence4.next()
   		z = sequence4.next()
   		sequence4.reset()
   		assert_equal( 0, z - y - x, "The random fibbonachi index number is wrong!")
	end
  end

  # Test na overenie funkcie resetovania
  def test_reset
  	sequence5 = FibonacciSequence.new
  	generator = Random.new
		
	# Vypocitame nahodne cislo a overime ci sa zmenil index a hodnota
	random = generator.rand(10...42)
   	sequence5[random]
   	assert_not_equal( nil, sequence5.current_idx, "After resolving function index is still nil!")
   	assert_not_equal( nil, sequence5.current, "After resolving function value is still nil!")

   	# Po resete overime ci sa index a hodnota resetli
   	sequence5.reset
   	assert_equal( nil, sequence5.current_idx, "The reset of index don't work!")
   	assert_equal( nil, sequence5.current, "The reset of number value don't work!")

   	# Overenie cinnosti po opetovnom next volani
   	assert_equal( 0, sequence5.next, "Calling next after reset returns wrong number!")
   	assert_equal( 0, sequence5.current_idx, "After calling reset and next the index is wrong!")
   	assert_equal( 0, sequence5.current, "After calling reset and next the value is wrong!")
  end

  # Test na overenie spravnej cinnosti funkcie vracajucej index
  def test_index
  	sequence5 = FibonacciSequence.new
  	generator = Random.new

  	# Vykoname test pre 5 nahodnych cisiel
  	for i in 0..5 	

  		# Vypocitame rovnicu na vypocet indexu z cisla
  		random = generator.rand(10...42)
  		x = sequence5[random]
    	phi = (1 + Math.sqrt(5))/2
    	num = x * Math.sqrt(5) + (1/2)
    	result = Math.log(num,phi).round
    	
    	# Overime ci aktualny index je rovnaky ako vypocitany
    	assert_equal(result, sequence5.current_idx, "Returning actual index is wrong!")
    	sequence5.reset
    end
  end
end