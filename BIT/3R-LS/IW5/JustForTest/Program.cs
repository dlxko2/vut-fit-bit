﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieManager.Model.Object;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace JustForTest
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var countryRepository = new CountryRepo())
            {
                countryRepository.Add(new Country { Name = "druhy"});
                countryRepository.Save();
            }

            Console.ReadKey();
        }
    }
}
