﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace MovieManager.Desktop.CustomElements
{
    public class CustomMultiDataGrid : DataGrid
    {
        public CustomMultiDataGrid()
        {
            this.SelectionChanged += CustomDataGrid_SelectionChanged;
        }

        void CustomDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedItemsList = this.SelectedItems;
        }
        #region SelectedItemsList

        public IList SelectedItemsList
        {
            get { return (IList)GetValue(SelectedItemsListProperty); }
            set { SetValue(SelectedItemsListProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemsListProperty =
                DependencyProperty.Register("SelectedItemsList", typeof(System.Collections.IList), typeof(CustomMultiDataGrid), new PropertyMetadata(null));

        #endregion
    }
}