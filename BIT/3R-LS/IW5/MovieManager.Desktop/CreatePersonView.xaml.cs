﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for CreatePersonView.xaml
    /// </summary>
    public partial class CreatePersonView : Window
    {
        private readonly CreatePersonViewModel _viewModel;

        public CreatePersonView()
        {
            InitializeComponent();
            _viewModel = (CreatePersonViewModel)DataContext;
            Loaded += CreatePersonViewLoaded;
        }
    
        private void CreatePersonViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.PersonViewModelCreate.PersonView = this;
            _viewModel.LoadData();
        }

        protected override void OnClosed(EventArgs e)
        {
            _viewModel.PersonViewModelCreate.Item = new Person();
            base.OnClosed(e);
        }
    }
}
