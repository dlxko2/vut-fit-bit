﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for CreateCountryView.xaml
    /// </summary>
    public partial class CreateCountryView : Window
    {
        private readonly CreateCountryViewModel _viewModel;

        public CreateCountryView()
        {
            InitializeComponent();
            _viewModel = (CreateCountryViewModel)DataContext;
            Loaded += CreateCountryViewLoaded;
        }

        private void CreateCountryViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.CountryViewModelCreate.CountryView = this;
            _viewModel.LoadData();
        }

        protected override void OnClosed(EventArgs e)
        {
            _viewModel.CountryViewModelCreate.Item = new Country();
            base.OnClosed(e);
        }
    }
}
