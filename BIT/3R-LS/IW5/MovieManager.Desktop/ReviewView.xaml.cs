﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for CreateReviewView.xaml
    /// </summary>
    public partial class ReviewView : Window
    {
        public Guid MovieGuid { get; set; }

        private ShowMovieRatingViewModel _viewModel;

        public ReviewView(Guid movieGuid)
        {
            MovieGuid = movieGuid;
            InitializeComponent();
            _viewModel = (ShowMovieRatingViewModel)DataContext;
            Loaded += CreateMovieRatingViewLoaded;
        }
        private void CreateMovieRatingViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadData(MovieGuid);
        }

        private void ButtonCreateReview(object sender, RoutedEventArgs e)
        {
            var view = new CreateReviewView(_viewModel.Movie);
            view.Show();
        }

    }
}
