﻿using MovieManager.Desktop.Framework.Commands;

namespace MovieManager.Desktop.ViewModels
{
    internal class OpenMovieViewCommand : CommandBase<MainViewModel>
    {
        public OpenMovieViewCommand(MainViewModel viewModel) : base(viewModel)
        {
        }
        public override void Execute(object parameter)
        {
            var view = new CreateMovieView();
            view.Show();
        }
    }
}