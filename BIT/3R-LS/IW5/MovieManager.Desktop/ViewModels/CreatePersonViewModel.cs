﻿using System;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CreatePersonViewModel
    {
        public PersonViewModelCreate PersonViewModelCreate { get; set; }
        public PersonsViewModel PersonsViewModel { get; set; }

        public CreatePersonViewModel()
        {
            var context = new MovieManagerDbContex();
            var personRepo = new PersonRepo(context);

            PersonViewModelCreate = new PersonViewModelCreate(personRepo);
            PersonsViewModel = new PersonsViewModel(personRepo);
        }

        public void LoadData()
        {
            PersonsViewModel.LoadData();
        }

        public void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            PersonViewModelCreate.Dispose();
            PersonsViewModel.Dispose();
        }
    }
}