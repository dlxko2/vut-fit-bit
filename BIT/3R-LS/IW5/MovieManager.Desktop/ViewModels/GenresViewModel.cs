﻿using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class GenresViewModel : ViewModelCollection<Genre, GenreRepo>
    {
        public GenresViewModel(GenreRepo service) : base(service)
        {
        }
    }
}