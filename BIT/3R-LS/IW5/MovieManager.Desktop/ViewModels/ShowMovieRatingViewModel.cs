﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MovieManager.Desktop.Annotations;
using MovieManager.Model.Object;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class ShowMovieRatingViewModel: INotifyPropertyChanged
    {
        private Movie _movie;
        public MovieRatingViewModel MovieRatingsViewModel { get; set; }
        public MovieRepo MovieRepo { get; set; }

        public Movie Movie
        {
            get { return _movie; }
            set
            {
                if (Equals(value, _movie)) return;
                _movie = value;
                OnPropertyChanged();
            }
        }

        public ShowMovieRatingViewModel()
        {
            var context = new MovieManagerDbContex();
            var movieratingRepo = new MovieRatingRepo(context);
            MovieRepo = new MovieRepo(context);

            MovieRatingsViewModel = new MovieRatingViewModel(movieratingRepo);
        }


        public void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            MovieRatingsViewModel.Dispose();
        }

        public void LoadData(Guid movieGuid)
        {
            Movie = MovieRepo.GetById(movieGuid);
            MovieRatingsViewModel.LoadData(movieGuid);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}