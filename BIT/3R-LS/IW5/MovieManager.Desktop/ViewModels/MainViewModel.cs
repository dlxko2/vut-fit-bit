﻿using System;
using System.Windows.Input;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class MainViewModel
    {
        public ICommand OpenGenreView { get; set; }

        public ICommand OpenCountryView { get; set; }

        public ICommand OpenPersonView { get; set; }

        public ICommand OpenMovieView { get; set; }

        public MovieViewModel MovieViewModel { get; set; }
        public MovieViewModelCreate MovieViewModelCreate { get; set; }
        public PersonsViewModel PersonsViewModel { get; set; }
        public CountriesViewModel CountriesViewModel { get; set; }
        public GenresViewModel GenresViewModel { get; set; }

        public MainViewModel()
        {
            var context = new MovieManagerDbContex();

            var movieRepo = new MovieRepo(context);
            var personRepo = new PersonRepo(context);
            var countryRepo = new CountryRepo(context);
            var genreRepo = new GenreRepo(context);

            MovieViewModel = new MovieViewModel(movieRepo);

            MovieViewModelCreate = new MovieViewModelCreate(movieRepo);
            PersonsViewModel = new PersonsViewModel(personRepo);
            CountriesViewModel = new CountriesViewModel(countryRepo);
            GenresViewModel = new GenresViewModel(genreRepo);

            OpenGenreView = new OpenGenreViewCommand(this);
            OpenCountryView = new OpenCountryViewCommand(this);
            OpenPersonView = new OpenPersonViewCommand(this);
            OpenMovieView = new OpenMovieViewCommand(this);

        }

        public void LoadData()
        {
            MovieViewModel.LoadData();
            PersonsViewModel.LoadData();
            CountriesViewModel.LoadData();
            GenresViewModel.LoadData();
        }

        public void OnProgramShutdownStarted(object sende, EventArgs e)
        {
            MovieViewModel.SaveData();
            MovieViewModel.Dispose();
            MovieViewModelCreate.Dispose();
            PersonsViewModel.Dispose();
            CountriesViewModel.Dispose();
            GenresViewModel.Dispose();
        }
    }
}