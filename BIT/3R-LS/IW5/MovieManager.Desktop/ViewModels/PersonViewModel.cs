﻿using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class PersonViewModel : ViewModelDetail<Person, PersonRepo>
    {
        public PersonViewModel(Person item, PersonRepo service) : base(item, service)
        {
        }
    }
}