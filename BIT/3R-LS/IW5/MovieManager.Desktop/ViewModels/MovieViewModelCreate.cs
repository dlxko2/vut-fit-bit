﻿using System.Collections;
using System.ComponentModel;
using MovieManager.Desktop.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class MovieViewModelCreate : ViewModelDetail<Movie, MovieRepo>
    {
        public CreateMovieView MovieView { get; set; }

        public SaveMovieCommand SaveMovieCommand { get; set; }
        public IList SelectedActors { get; set; }

        public IList SelectedDirectors { get; set; }

        public MovieViewModelCreate(MovieRepo service) : base(service)
        {
            SaveMovieCommand = new SaveMovieCommand(this);
            Item = new Movie();
            Item.PropertyChanged += NotifyCommands;
        }

        private void NotifyCommands(object sender, PropertyChangedEventArgs e)
        {
            SaveMovieCommand.RaiseCanExecuteChanged();
        }

        public override void LoadData()
        {
        }
    }
}