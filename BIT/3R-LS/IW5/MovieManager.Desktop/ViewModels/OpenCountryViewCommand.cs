﻿using MovieManager.Desktop.Framework.Commands;

namespace MovieManager.Desktop.ViewModels
{
    internal class OpenCountryViewCommand : CommandBase<MainViewModel>
    {
        public OpenCountryViewCommand(MainViewModel viewModel) : base(viewModel)
        {
        }
        public override void Execute(object parameter)
        {
            var view = new CreateCountryView();
            view.Show();
        }
    }
}