﻿using System;
using System.ComponentModel;
using MovieManager.Desktop.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class MovieRatingViewModelCreate : ViewModelDetail<MovieRating, MovieRatingRepo>
    {
        public SaveMovieRatingCommand SaveMovieRatingCommand { get; set; }
        public Movie Movie { get; set; }

        public Int16 MyRating { get; set; }

        public MovieRatingViewModelCreate(MovieRatingRepo service) : base(service)
        {
            SaveMovieRatingCommand = new SaveMovieRatingCommand(this);
            Item = new MovieRating();
            Item.PropertyChanged += NotifyCommands;
        }

        private void NotifyCommands(object sender, PropertyChangedEventArgs e)
        {
            SaveMovieRatingCommand.RaiseCanExecuteChanged();
        }

        public void LoadData(Movie movie)
        {
            Movie = movie;
        }
    }
}