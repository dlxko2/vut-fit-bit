﻿using System.Windows.Input;
using MovieManager.Desktop.Framework.Commands;

namespace MovieManager.Desktop.ViewModels
{
    internal class OpenGenreViewCommand : CommandBase<MainViewModel>
    {
        public OpenGenreViewCommand(MainViewModel viewModel) : base(viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            var view = new CreateGenreView();
            view.Show();
        }
    }
}