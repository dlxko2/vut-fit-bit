﻿using System;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CreateGenreViewModel
    {
        public GenreViewModelCreate GenreViewModelCreate { get; set; }
        public GenresViewModel GenresViewModel { get; set; }

        public CreateGenreViewModel()
        {
            var context = new MovieManagerDbContex();
            var genreRepo = new GenreRepo(context);

            GenreViewModelCreate = new GenreViewModelCreate(genreRepo);
            GenresViewModel = new GenresViewModel(genreRepo);
        }

        public void LoadData()
        {
            GenresViewModel.LoadData();
        }

        public void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            GenreViewModelCreate.Dispose();
            GenresViewModel.Dispose();
        }

    }
}
