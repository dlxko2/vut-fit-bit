﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MovieManager.Desktop.Annotations;
using MovieManager.Model.Object;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CreateMovieRatingViewModel: INotifyPropertyChanged
    {
        private Movie _movie;
        public MovieRatingViewModelCreate MovieRatingViewModelCreate { get; set; }

        public Movie Movie
        {
            get { return _movie; }
            set
            {
                if (Equals(value, _movie)) return;
                _movie = value;
                OnPropertyChanged();
            }
        }

        public CreateMovieRatingViewModel()
        {
            var context = new MovieManagerDbContex();
            var movieratingRepo = new MovieRatingRepo(context);

            MovieRatingViewModelCreate = new MovieRatingViewModelCreate(movieratingRepo);
        }

        public void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            MovieRatingViewModelCreate.Dispose();
        }

        public void LoadData(Movie movie)
        {
            Movie = movie;
            MovieRatingViewModelCreate.LoadData(Movie);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}