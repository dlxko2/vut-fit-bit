﻿using System.ComponentModel;
using System.Windows.Input;
using MovieManager.Desktop.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class GenreViewModelCreate : ViewModelDetail<Genre, GenreRepo>
    {
        public CreateGenreView GenreView { get; set; }

        public SaveGenreCommand SaveGenreCommand { get; set; }

        public GenreViewModelCreate(GenreRepo service) : base(service)
        {
            SaveGenreCommand = new SaveGenreCommand(this);
            Item = new Genre();
            Item.PropertyChanged += NotifyCommands;

        }

        private void NotifyCommands(object sender, PropertyChangedEventArgs e)
        {
            SaveGenreCommand.RaiseCanExecuteChanged();
        }

        public override void LoadData()
        {
        }
    }
}
