﻿using System.ComponentModel;
using MovieManager.Desktop.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class PersonViewModelCreate : ViewModelDetail<Person, PersonRepo>
    {
        public CreatePersonView PersonView { get; set; }

        public SavePersonCommand SavePersonCommand { get; set; }

        public PersonViewModelCreate(PersonRepo service) : base(service)
        {
            SavePersonCommand = new SavePersonCommand(this);
            Item = new Person();
            Item.PropertyChanged += NotifyCommands;
        }

        private void NotifyCommands(object sender, PropertyChangedEventArgs e)
        {
            SavePersonCommand.RaiseCanExecuteChanged();
        }

        public override void LoadData()
        {
        }
    }
}