﻿using System.ComponentModel;
using MovieManager.Desktop.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CountryViewModelCreate : ViewModelDetail<Country, CountryRepo>
    {
        public CreateCountryView CountryView { get; set; }

        public SaveCountryCommand SaveCountryCommand { get; set; }

        public CountryViewModelCreate(CountryRepo service) : base(service)
        {
            SaveCountryCommand = new SaveCountryCommand(this);
            Item = new Country();
            Item.PropertyChanged += NotifyCommands;
        }

        private void NotifyCommands(object sender, PropertyChangedEventArgs e)
        {
            SaveCountryCommand.RaiseCanExecuteChanged();
        }

        public override void LoadData()
        {
        }
    }
}