﻿using System;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CreateCountryViewModel
    {
        public CountryViewModelCreate CountryViewModelCreate { get; set; }
        public CountriesViewModel CountriesViewModel { get; set; }

        public CreateCountryViewModel()
        {
            var context = new MovieManagerDbContex();
            var countryRepo = new CountryRepo(context);

            CountryViewModelCreate = new CountryViewModelCreate(countryRepo);
            CountriesViewModel = new CountriesViewModel(countryRepo);
        }

        public void LoadData()
        {
            CountriesViewModel.LoadData();
        }

        public void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            CountryViewModelCreate.Dispose();
            CountriesViewModel.Dispose();
        }

    }
}