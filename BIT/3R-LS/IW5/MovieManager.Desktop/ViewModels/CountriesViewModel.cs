﻿using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class CountriesViewModel : ViewModelCollection<Country, CountryRepo>
    {
        public CountriesViewModel(CountryRepo service) : base(service)
        {
        }
    }
}