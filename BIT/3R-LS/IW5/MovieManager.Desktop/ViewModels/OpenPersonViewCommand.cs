﻿using MovieManager.Desktop.Framework.Commands;

namespace MovieManager.Desktop.ViewModels
{
    internal class OpenPersonViewCommand : CommandBase<MainViewModel>
    {
        public OpenPersonViewCommand(MainViewModel viewModel) : base(viewModel)
        {
        }
        public override void Execute(object parameter)
        {
            var view = new CreatePersonView();
            view.Show();
        }
    }
}