﻿using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class MovieViewModel : ViewModelCollection<Movie, MovieRepo>
    {
        public MovieViewModel(MovieRepo service) : base(service)
        {
        }
    }
}