﻿using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class PersonsViewModel : ViewModelCollection<Person, PersonRepo>
    {
        public PersonsViewModel(PersonRepo service) : base(service)
        {
        }
    }
}