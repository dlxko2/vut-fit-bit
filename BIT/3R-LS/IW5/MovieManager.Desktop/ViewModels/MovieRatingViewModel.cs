﻿using System;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Object;
using MovieManager.Services.Repos;

namespace MovieManager.Desktop.ViewModels
{
    public class MovieRatingViewModel : ViewModelCollection<MovieRating, MovieRatingRepo>
    {
        public Movie Movie { get; set; }

        public MovieRatingViewModel(MovieRatingRepo service) : base(service)
        {
        }

    
        public void LoadData(Guid movieGuid)
        {
            Service.LoadAll(movieGuid);
            OnPropertyChanged(nameof(Items));
        }
    }
}