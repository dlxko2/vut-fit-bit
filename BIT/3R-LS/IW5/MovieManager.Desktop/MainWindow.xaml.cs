﻿using System.ComponentModel;
using System.Windows;
using MovieManager.Desktop.ViewModels;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = (MainViewModel) this.DataContext;
            Loaded += this.MainWindowLoaded;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            _viewModel.OnProgramShutdownStarted(this, e);
        }

        private void MainWindowLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadData();
        }
    }
}

