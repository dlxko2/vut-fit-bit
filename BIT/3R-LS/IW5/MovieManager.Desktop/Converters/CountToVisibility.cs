﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MovieManager.Desktop.Converters
{
    public class CountToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var count = (int)value;
            var revertHiding = (string) parameter;

            if (revertHiding == "1")
                return count == 0 ? Visibility.Collapsed : Visibility.Visible;
            return count == 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}