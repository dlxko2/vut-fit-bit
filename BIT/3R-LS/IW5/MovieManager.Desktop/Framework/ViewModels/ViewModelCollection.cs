﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Framework.ViewModels
{
    public abstract class ViewModelCollection<T, TR> : ViewModelBase<T, TR>
        where T : class, IModel, new()
        where TR : class, IRepository<T>
    {
        private string _status;

        private T _newItem;

        public ICommand SaveNewItem { get; set; }
        public ICommand DiscardNewItem { get; set; }
        public ICommand RemoveItem { get; set; }

        public ObservableCollection<T> Items => Service.GetObservableCollection();

        public T NewItem
        {
            get
            {
                return _newItem;
            }
            set
            {
                if (Equals(value, _newItem))
                {
                    return;
                }
                _newItem = value;

                OnPropertyChanged();
            }
        }

        protected ViewModelCollection(TR service) : base(service)
        {
            NewItem = new T();
        }

        public override void LoadData()
        {
            Service.LoadAll();
            OnPropertyChanged(nameof(Items));
        }
    }
}