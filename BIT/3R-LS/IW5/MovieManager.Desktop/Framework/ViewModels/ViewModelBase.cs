﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MovieManager.Model.Annotations;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Framework.ViewModels
{
    public abstract class ViewModelBase<T, TR> : INotifyPropertyChanged, IDisposable
        where T : class, IModel
        where TR : class, IRepository<T>
    {
        private bool _disposed;

        public TR Service { get; protected set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected ViewModelBase(TR service)
        {
            Service = service;
        }

        ~ViewModelBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public abstract void LoadData();

        public void SaveData()
        {
            Service.Save();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                Service.Dispose();
            }
            _disposed = true;
        }

        public virtual void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            Service.Save();
            Dispose();
        }
    }
}