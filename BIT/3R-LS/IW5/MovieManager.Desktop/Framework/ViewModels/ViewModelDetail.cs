﻿using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Framework.ViewModels
{
    public abstract class ViewModelDetail<T, TR> : ViewModelBase<T, TR>
        where T : class, IModel
        where TR: class, IRepository<T>
    {
        protected ViewModelDetail(TR service) : base(service)
        {
        }

        protected ViewModelDetail(T item, TR service) : base(service)
        {
            Item = item;
        }

        public T Item { get; set; }

        public override void LoadData()
        {
            Item = Service.GetById(Item.ID);
        }
    }
}
