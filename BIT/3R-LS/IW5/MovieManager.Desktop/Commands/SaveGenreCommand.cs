﻿using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.ViewModels;

namespace MovieManager.Desktop.Commands
{
    public class SaveGenreCommand : CommandBase<GenreViewModelCreate>
    {
        public SaveGenreCommand(GenreViewModelCreate viewModel) : base(viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            var newItem = ViewModel.Item;
            ViewModel.Service.Add(newItem);
            ViewModel.SaveData();
            ViewModel.GenreView.Close();
        }

        public override bool CanExecute(object parameter)
        {
            var item = ViewModel.Item;
            return true;
            //  return !string.IsNullOrEmpty(item.Name);
        }
    }
}