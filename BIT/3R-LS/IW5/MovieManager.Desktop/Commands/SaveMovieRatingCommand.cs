﻿using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.ViewModels;

namespace MovieManager.Desktop.Commands
{
    public class SaveMovieRatingCommand : CommandBase<MovieRatingViewModelCreate>
    {
        public SaveMovieRatingCommand(MovieRatingViewModelCreate viewModel) : base(viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            var newItem = ViewModel.Item;
            newItem.MovieId = ViewModel.Movie.ID;
            newItem.Rating = ViewModel.MyRating;
            ViewModel.Service.Add(newItem);
            ViewModel.SaveData();
        }

        public override bool CanExecute(object parameter)
        {
            var item = ViewModel.Item;
            return true;
            //  return !string.IsNullOrEmpty(item.Name);
        }
    }
}