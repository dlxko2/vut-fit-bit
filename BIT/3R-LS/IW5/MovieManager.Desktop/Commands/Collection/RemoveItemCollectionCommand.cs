﻿using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Commands.Collection
{
    public class RemoveItemCollectionCommand<T, TR> : CommandBase<ViewModelCollection<T, TR>>
        where T : class, IModel, new()
        where TR : class, IRepository<T>
    {
        public RemoveItemCollectionCommand(ViewModelCollection<T, TR> viewModelCollection)
            : base(viewModelCollection)
        {
        }

        public override void Execute(object item)
        {
            var typeItem = item as T;
            if (typeItem != null)
            {
                this.ViewModel.Items.Remove(typeItem);
            }
        }
    }
}