﻿using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Commands.Collection
{
    public class DiscardItemCollectionCommand<T, TR> : CommandBase<ViewModelCollection<T, TR>>
        where T : class, IModel, new()
        where TR : class, IRepository<T>
    {
        public DiscardItemCollectionCommand(ViewModelCollection<T, TR> viewModel)
            : base(viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            ViewModel.NewItem = null;
        }
    }
}