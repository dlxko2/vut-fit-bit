﻿using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.Framework.ViewModels;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Desktop.Commands.Collection
{
    public class SaveItemCollectionCommand<T, TR> : CommandBase<ViewModelCollection<T, TR>>
        where T : class, IModel, new()
        where TR : class, IRepository<T>
    {
        public SaveItemCollectionCommand(ViewModelCollection<T, TR> viewModelCollection)
           : base(viewModelCollection)
        {
        }

        public override void Execute(object item)
        {
            this.ViewModel.Service.Add(this.ViewModel.NewItem);
            this.ViewModel.Items.Add(this.ViewModel.NewItem);
            this.ViewModel.NewItem = null;
        }
    }
}