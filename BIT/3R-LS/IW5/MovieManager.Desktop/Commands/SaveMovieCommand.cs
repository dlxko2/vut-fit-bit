﻿using System.Collections.ObjectModel;
using MovieManager.Desktop.Framework.Commands;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop.Commands
{
    public class SaveMovieCommand : CommandBase<MovieViewModelCreate>
    {
        public SaveMovieCommand(MovieViewModelCreate viewModel) : base(viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            ViewModel.Item.Actors = new ObservableCollection<Person>();
            foreach (var item in ViewModel.SelectedActors)
            {
                ViewModel.Item.Actors.Add(item as Person);
            }

            ViewModel.Item.Directors = new ObservableCollection<Person>();
            foreach (var item in ViewModel.SelectedDirectors)
            {
                ViewModel.Item.Directors.Add(item as Person);
            }

            var newItem = ViewModel.Item;
            ViewModel.Service.Add(newItem);
            ViewModel.SaveData();
            ViewModel.Item = new Movie();
            ViewModel.MovieView.Close();
        }

        public override bool CanExecute(object parameter)
        {
            var item = ViewModel.Item;
            return true;
            //return !string.IsNullOrEmpty(item.Name) &&
            //       !string.IsNullOrEmpty(item.OriginalName) &&
            //       !string.IsNullOrEmpty(item.CoverUrl) &&
            //       !string.IsNullOrEmpty(item.Plot) &&
            //       !(item.Length <= 0) &&
            //       item.Genre != null;
        }
    }
}