﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for ReviewView.xaml
    /// </summary>
    public partial class CreateReviewView : Window
    {
        private readonly CreateMovieRatingViewModel _viewModel;

        public Movie Movie { get; set; }

        public CreateReviewView(Movie movie)
        {
            InitializeComponent();
            _viewModel = (CreateMovieRatingViewModel) DataContext;
            Loaded += CreateMovieRatingViewLoaded;
            Movie = movie;
        }

        private void CreateMovieRatingViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.LoadData(Movie);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}