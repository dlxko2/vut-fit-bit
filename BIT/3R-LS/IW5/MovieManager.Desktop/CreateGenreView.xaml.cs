﻿using System;
using System.Windows;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    /// <summary>
    /// Interaction logic for CreateGenreView.xaml
    /// </summary>
    public partial class CreateGenreView : Window
    {
        private readonly CreateGenreViewModel _viewModel;

        public CreateGenreView()
        {
            InitializeComponent();
            _viewModel = (CreateGenreViewModel)DataContext;
            Loaded += CreateGenreViewLoaded;
        }

        private void CreateGenreViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.GenreViewModelCreate.GenreView = this;
            _viewModel.LoadData();

        }

        protected override void OnClosed(EventArgs e)
        {
            _viewModel.GenreViewModelCreate.Item = new Genre();
            base.OnClosed(e);
        }
    }
}
