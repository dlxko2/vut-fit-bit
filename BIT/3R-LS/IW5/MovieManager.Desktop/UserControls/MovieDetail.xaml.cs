﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace MovieManager.Desktop.UserControls
{
    /// <summary>
    /// Interaction logic for MovieDetail.xaml
    /// </summary>
    public partial class MovieDetail : UserControl
    {
        public MovieDetail()
        {
            InitializeComponent();
        }

        private void Button_Click_Show_Reviews(object sender, RoutedEventArgs e)
        {
            var movieGuid = (Guid) ((Button) sender).Tag;
            var view = new ReviewView(movieGuid);
            view.Show();
        }
    }
}
