﻿using System;
using System.Windows;
using MovieManager.Desktop.ViewModels;
using MovieManager.Model.Object;

namespace MovieManager.Desktop
{
    public partial class CreateMovieView : Window
    {
        private readonly MainViewModel _viewModel;

        public CreateMovieView()
        {
            InitializeComponent();
            _viewModel = (MainViewModel)DataContext;
            Loaded += CreateMovieViewLoaded;
        }

        private void CreateMovieViewLoaded(object sender, RoutedEventArgs e)
        {
            _viewModel.MovieViewModelCreate.MovieView = this;
            _viewModel.LoadData();
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.MovieViewModelCreate.Item = new Movie();
        }

        protected override void OnClosed(EventArgs e)
        {
            _viewModel.MovieViewModelCreate.Item = new Movie();
            base.OnClosed(e);
        }
    }
}
