﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MovieManager.Model.Object;

namespace MovieManager.Model.Base.Implementation
{
    public abstract class BaseNamedMoviesRefModel : NamedModel
    {
        protected BaseNamedMoviesRefModel()
        {
            Movies = new ObservableCollection<Movie>();
        }

        protected BaseNamedMoviesRefModel(string name) : base(name)
        {
            Movies = new ObservableCollection<Movie>();
        }

        public virtual ObservableCollection<Movie> Movies { get; set; }    
    }
}