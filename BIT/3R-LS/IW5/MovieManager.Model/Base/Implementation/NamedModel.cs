﻿using MovieManager.Model.Base.Interface;

namespace MovieManager.Model.Base.Implementation
{
    public abstract class NamedModel : BaseModel, IName
    {
        private string _name;

        protected NamedModel()
        {
        }

        protected NamedModel(string name)
        {
            Name = name;
        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }
    }
}