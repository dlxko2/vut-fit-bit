﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MovieManager.Model.Annotations;
using MovieManager.Model.Base.Interface;

namespace MovieManager.Model.Base.Implementation
{
    public abstract class BaseModel : IModel, INotifyPropertyChanged
    {
        protected BaseModel()
        {
            ID = Guid.NewGuid();
        }

        protected BaseModel(Guid id)
        {
            ID = id;
        }

        public Guid ID { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}