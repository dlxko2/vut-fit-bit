﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace MovieManager.Model.Base.Interface
{
    public interface IModel
    {
        Guid ID { get; set; } 
    }
}