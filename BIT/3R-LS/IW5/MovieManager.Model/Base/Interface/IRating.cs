﻿namespace MovieManager.Model.Base.Interface
{
    public interface IRating
    {
        int Rating { get; } 
    }
}