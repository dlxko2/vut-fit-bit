﻿using System;
using MovieManager.Model.Base.Implementation;
using MovieManager.Model.Base.Interface;

namespace MovieManager.Model.Object
{
    public class MovieRating : BaseModel, IRating
    {
        private int _rating;
        private string _comment;
        private Movie _movie;

        public MovieRating(int rating)
        {
            Rating = rating;
        }

        public MovieRating()
        {
        }

        public int Rating
        {
            get { return _rating; }
            set
            {
                if (value == _rating) return;
                _rating = value;
                OnPropertyChanged();
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                if (value == _comment) return;
                _comment = value;
                OnPropertyChanged();
            }
        }

        public Guid MovieId { get; set; }

        public virtual Movie Movie
        {
            get { return _movie; }
            set
            {
                if (Equals(value, _movie)) return;
                _movie = value;
                OnPropertyChanged();
            }
        }
    }
}