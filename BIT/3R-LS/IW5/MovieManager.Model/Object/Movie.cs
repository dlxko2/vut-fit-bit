﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using MovieManager.Model.Base.Implementation;

namespace MovieManager.Model.Object
{
    public class Movie : NamedModel
    {
        private string _originalName;
        private string _coverUrl;
        private double _length;
        private string _plot;
        private Country _country;
        private Genre _genre;

        public Movie()
        {
            Actors = new ObservableCollection<Person>();
            Directors = new ObservableCollection<Person>();
            MovieRatings = new ObservableCollection<MovieRating>();
        }

        public string OriginalName
        {
            get { return _originalName; }
            set
            {
                if (value == _originalName) return;
                _originalName = value;
                OnPropertyChanged();
            }
        }

        public string CoverUrl
        {
            get { return _coverUrl; }
            set
            {
                if (value == _coverUrl) return;
                _coverUrl = value;
                OnPropertyChanged();
            }
        }

        public double Length
        {
            get { return _length; }
            set
            {
                if (value.Equals(_length)) return;
                _length = value;
                OnPropertyChanged();
            }
        }

        public string Plot
        {
            get { return _plot; }
            set
            {
                if (value == _plot) return;
                _plot = value;
                OnPropertyChanged();
            }
        }

        public Guid GenreId { get; set; }

        public virtual Genre Genre
        {
            get { return _genre; }
            set
            {
                if (Equals(value, _genre)) return;
                _genre = value;
                OnPropertyChanged();
            }
        }

        public Guid CountryId { get; set; }

        public virtual Country Country
        {
            get { return _country; }
            set
            {
                if (Equals(value, _country)) return;
                _country = value;
                OnPropertyChanged();
            }
        }

//        [InverseProperty("ActInMovies")]
        public virtual ObservableCollection<Person> Actors { get; set; }

//        [InverseProperty("DirectInMovies")]
        public virtual ObservableCollection<Person> Directors { get; set; } 

        public virtual ObservableCollection<MovieRating> MovieRatings { get; set; }
    }
}