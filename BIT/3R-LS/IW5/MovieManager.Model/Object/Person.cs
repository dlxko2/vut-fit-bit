﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using MovieManager.Model.Base.Implementation;
using MovieManager.Model.Base.Interface;

namespace MovieManager.Model.Object
{
    public class Person : NamedModel, IPerson
    {
        private string _firstName;
        private string _lastName;
        private string _photoUrl;
        private int _age;

        public Person()
        {
            ActInMovies = new ObservableCollection<Movie>();
            DirectInMovies = new ObservableCollection<Movie>();
        }

        protected Person(string firstName, string lastName) : this()
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Name));
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value == _lastName) return;
                _lastName = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Name));
            }
        }

        public override string Name => $"{FirstName} {LastName}";

        public string PhotoUrl
        {
            get { return _photoUrl; }
            set
            {
                if (value == _photoUrl) return;
                _photoUrl = value;
                OnPropertyChanged();
            }
        }

        public int Age
        {
            get { return _age; }
            set
            {
                if (value == _age) return;
                _age = value;
                OnPropertyChanged();
            }
        }

        public virtual ObservableCollection<Movie> ActInMovies { get; set; }
        public virtual ObservableCollection<Movie> DirectInMovies { get; set; }
    }
}