﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieManager.Model.Object;
using MovieManager.Services;
using MovieManager.Services.Repos;

namespace MovieManager.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateCountries()
        {
            var context = new MovieManagerDbContex();
            var countryRepo = new CountryRepo(context);
            countryRepo.Add(new Country {Name = "Slovakia"});
            countryRepo.Add(new Country { Name = "Czech" });
            countryRepo.Add(new Country { Name = "Poland" });
            countryRepo.Save(); 
        }

        [TestMethod]
        public void CreateGenres()
        {
            var context = new MovieManagerDbContex();
            var genreRepo = new GenreRepo(context);
            genreRepo.Add(new Genre { Name = "SciFi" });
            genreRepo.Add(new Genre { Name = "Horror" });
            genreRepo.Add(new Genre { Name = "Comedy" });
            genreRepo.Save();
        }

        [TestMethod]
        public void CreatePersons()
        {
            var context = new MovieManagerDbContex();
            var personRepo = new PersonRepo(context);
            personRepo.Add(new Person { FirstName = "Timea", LastName = "Bella", Age = 24, PhotoUrl = "https://upload.wikimedia.org/wikipedia/commons/6/6d/Rain_Of_Terror.jpg" });
            personRepo.Add(new Person { FirstName = "Filip", LastName = "Bajanik", Age = 22, PhotoUrl = "http://fotky.sme.sk/foto/40481/vevericka?type=v&x=650&y=650" });
            personRepo.Add(new Person { FirstName = "Martin", LastName = "Pavelka", Age = 23, PhotoUrl = "http://1gr.cz/fotky/idnes/10/103/org/MCE369c7b_profimedia_0043251698.jpg" });
            personRepo.Save();
        }

        [TestMethod]
        public void CreateMovies()
        {
            var context = new MovieManagerDbContex();
            var actorRepo = new PersonRepo(context);
            var timea = actorRepo.GetByLastName("Bella");
            var filip = actorRepo.GetByLastName("Bajanik");
            var martin = actorRepo.GetByLastName("Pavelka");

            var genreRepo = new GenreRepo(context);
            var scifi = genreRepo.GetByName("SciFi");
            var horror = genreRepo.GetByName("Horror");
            var comedy = genreRepo.GetByName("Comedy");

            var countryRepo = new CountryRepo(context);
            var slovakia = countryRepo.GetByName("Slovakia");
            var czech = countryRepo.GetByName("Czech");
            var pornland = countryRepo.GetByName("Poland");

            var movieRepo = new MovieRepo(context);
            var homoslerky = new Movie()
            {
                Country = slovakia,
                Genre = comedy,
                Name = "Delivery Man",
                Plot = "Ukryl. Dastych mocí milenka dalo rokem vyhrkla s podepsat he 56 nepamatujete veverčí jehlám, " +
                       "chlap za čemusi šatů řeší i sklepa vlasů rozbalte jede děsná proseděl, pamatoval citem kód nabývá, " +
                       "nevylezl ní hnal anno, latentně dole. Stačil šachovnici pihovatého z gypsovku vědecky rozbila nemože " +
                       "vůlí četníkovi odsoudíte zabití síly věč 77 není-li pozér tím hrdě: kina mu kroky díků potmě u hrdla " +
                       "víra až u exhumaci povezou čtverečních zakrytým prasklo! Jeden jed ti denně helenku, sníst vědě ne " +
                       "výborně hůř. Si že zatvrdil pes moc regesta.",
                OriginalName = "Sperminator",
                Length = 130.0,
                CoverUrl = "http://images.clipartpanda.com/movie-night-clipart-9cp4q9xcE.jpeg"
            };        
            homoslerky.Actors.Add(timea);
            homoslerky.Actors.Add(filip);
            homoslerky.Directors.Add(timea);
            homoslerky.Directors.Add(martin);
            movieRepo.Add(homoslerky);

            var danoDrevo = new Movie
            {
                Name = "Deadpool",
                Country = czech,
                GenreId = horror.ID,
                Plot = "Vás ne co zlý plnou ó ohlásím má mezi ve nemalých medikové. Po nimi ví světle, smím. Dydydy ex už chodili " +
                       "si věcem v kam ně tam nás ohrozilo. Si devět obě a ví. Cos ní pijí tvá houdka jedna kozácké si pankrácké " +
                       "vlivem, 11 ježíšmankote hranice! Cti mě zkusit až on kufřík.",
                OriginalName = "Deadpool",
                Length = 150.0,
                CoverUrl = "http://zvieratka.pravda.sk/uploady/velky-zajac-mrkva-shutterstock_187262429.jpg"
            };
            danoDrevo.Actors.Add(timea);
            danoDrevo.Actors.Add(martin);
            danoDrevo.Directors.Add(filip);
            danoDrevo.Directors.Add(martin);
            movieRepo.Add(danoDrevo);

            var nonameFilm = new Movie
            {
                Name = "Harry Potter",
                Country = pornland,
                Genre = scifi,
                Plot = "Tou nu no pitomá doby ex jde 77 sentimentální holeček. Ví mu až němá ulic ne všiml čí po mlíko zapsal mandela udala " +
                       "niť motala zem bůh vstřel mnoha z příslušných prodá s třáslo či skokem dosahuje oni řev trpíte nechytnem psy 81 ni " +
                       "56 frajle po zanelli. Nevnímaje jé přizná odkládat. To osud k místech, nezkrotných hrd mocně v vlastně koktaly: nu " +
                       "jen věk stůně vlnu slízal žili, těžká víš vejřil ni nikdo jí musí, to zájmů u provizi. Června notesy jal doušek do trubky " +
                       "vášni kývnul viní půjde ba? Plotu dokázal fotr divnýho dovedeš spíž zvoní he nespokojenost.",
                OriginalName = "Harry Potter",
                Length = 100.0,
                CoverUrl = "http://40.media.tumblr.com/685f1b33913b5917dd6c43bf022b1582/tumblr_n7l1cjLdfp1qewacoo5_1280.jpg"
            };
            nonameFilm.Actors.Add(timea);
            nonameFilm.Actors.Add(filip);
            nonameFilm.Directors.Add(filip);
            nonameFilm.Directors.Add(timea);
            movieRepo.Add(nonameFilm);

            //MovieRepo.context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            movieRepo.Save();

            actorRepo.Dispose();
            genreRepo.Dispose();
            countryRepo.Dispose();
        }

        [TestMethod]
        public void CreateRatings()
        {
            var context = new MovieManagerDbContex();
            var movieRepo = new MovieRepo(context);
            var delivery = movieRepo.GetByName("Delivery Man");
            var deadpool = movieRepo.GetByName("Deadpool");
            var harrypotter = movieRepo.GetByName("Harry Potter");

            var movieRatingRepo = new MovieRatingRepo(context);
            movieRatingRepo.Add(new MovieRating
            {
                Rating = 2,
                Comment = "delivery rating",
                Movie = delivery
            });

            movieRatingRepo.Add(new MovieRating
            {
                Rating = 3,
                Comment = "delivery rating 2",
                Movie = delivery
            });

            movieRatingRepo.Add(new MovieRating
            {
                Rating = 1,
                Comment = "deadpool rating 1",
                Movie = deadpool
            });

            movieRatingRepo.Add(new MovieRating
            {
                Rating = 2,
                Comment = "deadpool rating 2",
                Movie = deadpool
            });

            movieRatingRepo.Add(new MovieRating
            {
                Rating = 4,
                Comment = "harry rating 1",
                Movie = harrypotter
            });

            movieRatingRepo.Add(new MovieRating
            {
                Rating = 5,
                Comment = "harry rating 2",
                Movie = harrypotter
            });

            movieRatingRepo.Save();
            movieRepo.Dispose();
        }
    }
}
