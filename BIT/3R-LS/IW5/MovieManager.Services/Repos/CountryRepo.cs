﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieManager.Model.Object;
using MovieManager.Services.Repos.Base.Implementation;

namespace MovieManager.Services.Repos
{
    public class CountryRepo : NamedRepository<MovieManagerDbContex, Country>
    {
        public CountryRepo(MovieManagerDbContex context) : base(context)
        {
        }
    }
}