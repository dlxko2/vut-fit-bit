﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MovieManager.Model.Base.Interface;
using MovieManager.Services.Repos.Base.Interface;

namespace MovieManager.Services.Repos.Base.Implementation
{
    public class Repository <TContext, T> : IDisposable, IRepository<T>
        where T : class, IModel
        where TContext : DbContext
    {
        private TContext _context;

        private bool _disposed;

        public Repository(TContext context)
        {
            _context = context;
        }

        protected TContext Context
        {
            get
            {
                return this._context;
            }
            set
            {
                this._context = value;
            }
        }

        public virtual T Add(T entity)
        {
            return this._context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            this._context.Set<T>().Remove(entity);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Edit(T entity)
        {
            this._context.Entry(entity).State = EntityState.Modified;
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = this._context.Set<T>().Where(predicate);
            return query;
        }

        public virtual IQueryable<T> GetAll()
        {
            return this._context.Set<T>();
        }

        public virtual T GetById(Guid id)
        {
            return this._context.Set<T>().Single(i => i.ID == id);
        }

        public ObservableCollection<T> GetObservableCollection()
        {
            return this._context.Set<T>().Local;
        }

        public void LoadAll()
        {
            this._context.Set<T>().Load();
        }

        public virtual void Save()
        {
            this._context.SaveChanges();
        }

        private void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    this._context.Dispose();
                }
                this._disposed = true;
            }
        }
    }
}