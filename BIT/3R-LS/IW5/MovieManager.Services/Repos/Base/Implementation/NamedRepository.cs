﻿using System.Data.Entity;
using System.Linq;
using MovieManager.Model.Base.Interface;

namespace MovieManager.Services.Repos.Base.Implementation
{
    public class NamedRepository<TContext, T> : Repository<TContext, T>
        where T : class, IModel, IName
        where TContext : DbContext, new()
    {
        public virtual T GetByName(string name)
        {
            return Context.Set<T>().First(i => i.Name == name);
        }

        public NamedRepository(TContext context) : base(context)
        {
        }
    }
}