﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using MovieManager.Model.Base.Interface;

namespace MovieManager.Services.Repos.Base.Interface
{
    public interface IRepository<T>
        where T : class, IModel
    {
        T Add(T entity);

        void Delete(T entity);

        void Dispose();

        void Edit(T entity);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll();

        T GetById(Guid id);

        ObservableCollection<T> GetObservableCollection();

        void LoadAll();

        void Save();
    }
}