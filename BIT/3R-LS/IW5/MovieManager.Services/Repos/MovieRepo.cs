﻿using System.Linq;
using MovieManager.Model.Object;
using MovieManager.Services.Repos.Base.Implementation;

namespace MovieManager.Services.Repos
{
    public class MovieRepo : NamedRepository<MovieManagerDbContex, Movie>
    {
        public MovieRepo(MovieManagerDbContex context) : base(context)
        {
        }
    }
}