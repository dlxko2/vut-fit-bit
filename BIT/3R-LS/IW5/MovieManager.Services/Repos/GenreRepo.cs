﻿using System.Collections.Generic;
using System.Linq;
using MovieManager.Model.Object;
using MovieManager.Services.Repos.Base.Implementation;

namespace MovieManager.Services.Repos
{
    public class GenreRepo : NamedRepository<MovieManagerDbContex, Genre>
    {
        public GenreRepo(MovieManagerDbContex context) : base(context)
        {
        }
    }
}