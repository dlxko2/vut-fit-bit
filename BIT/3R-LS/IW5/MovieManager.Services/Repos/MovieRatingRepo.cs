﻿using System;
using System.Data.Entity;
using System.Linq;
using MovieManager.Model.Object;
using MovieManager.Services.Repos.Base.Implementation;

namespace MovieManager.Services.Repos
{
    public class MovieRatingRepo : Repository<MovieManagerDbContex, MovieRating>
    {
        public void LoadAll(Guid movieId)
        {
            Context.Set<MovieRating>().Where(m => m.MovieId == movieId).Load();
        }



        public MovieRatingRepo(MovieManagerDbContex context) : base(context)
        {
        }
    }
}