﻿using System.Linq;
using MovieManager.Model.Object;
using MovieManager.Services.Repos.Base.Implementation;

namespace MovieManager.Services.Repos
{
    public class PersonRepo : Repository<MovieManagerDbContex, Person>
    {
        public virtual Person GetByLastName(string lastname)
        {
            return Context.Set<Person>().First(i => i.LastName == lastname);
        }

        public PersonRepo(MovieManagerDbContex context) : base(context)
        {
        }
    }
}