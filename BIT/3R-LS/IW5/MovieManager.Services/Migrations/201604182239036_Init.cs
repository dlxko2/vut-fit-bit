namespace MovieManager.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OriginalName = c.String(),
                        CoverUrl = c.String(),
                        Length = c.Double(nullable: false),
                        Plot = c.String(),
                        GenreId = c.Guid(nullable: false),
                        CountryId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.Genres", t => t.GenreId, cascadeDelete: true)
                .Index(t => t.GenreId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhotoUrl = c.String(),
                        Age = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MovieRatings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Rating = c.Int(nullable: false),
                        Comment = c.String(),
                        MovieId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.MovieId);
            
            CreateTable(
                "dbo.ActorsMovies",
                c => new
                    {
                        MovieId = c.Guid(nullable: false),
                        ActorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MovieId, t.ActorId })
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.ActorId, cascadeDelete: true)
                .Index(t => t.MovieId)
                .Index(t => t.ActorId);
            
            CreateTable(
                "dbo.DirectorsMovies",
                c => new
                    {
                        MovieId = c.Guid(nullable: false),
                        DirectorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MovieId, t.DirectorId })
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.DirectorId, cascadeDelete: true)
                .Index(t => t.MovieId)
                .Index(t => t.DirectorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MovieRatings", "MovieId", "dbo.Movies");
            DropForeignKey("dbo.Movies", "GenreId", "dbo.Genres");
            DropForeignKey("dbo.DirectorsMovies", "DirectorId", "dbo.Persons");
            DropForeignKey("dbo.DirectorsMovies", "MovieId", "dbo.Movies");
            DropForeignKey("dbo.Movies", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.ActorsMovies", "ActorId", "dbo.Persons");
            DropForeignKey("dbo.ActorsMovies", "MovieId", "dbo.Movies");
            DropIndex("dbo.DirectorsMovies", new[] { "DirectorId" });
            DropIndex("dbo.DirectorsMovies", new[] { "MovieId" });
            DropIndex("dbo.ActorsMovies", new[] { "ActorId" });
            DropIndex("dbo.ActorsMovies", new[] { "MovieId" });
            DropIndex("dbo.MovieRatings", new[] { "MovieId" });
            DropIndex("dbo.Movies", new[] { "CountryId" });
            DropIndex("dbo.Movies", new[] { "GenreId" });
            DropTable("dbo.DirectorsMovies");
            DropTable("dbo.ActorsMovies");
            DropTable("dbo.MovieRatings");
            DropTable("dbo.Genres");
            DropTable("dbo.Persons");
            DropTable("dbo.Movies");
            DropTable("dbo.Countries");
        }
    }
}
