﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MovieManager.Model.Object;

namespace MovieManager.Services
{
    public class MovieManagerDbContex : DbContext
    {
        public DbSet<Movie> Movies { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<MovieRating> MovieRatings { get; set; }

        public MovieManagerDbContex()
            : base("Name=MovieManagerDB")
        {  
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Movie>().ToTable("Movies");
            modelBuilder.Entity<Country>().ToTable("Countries");
            modelBuilder.Entity<Genre>().ToTable("Genres");
            modelBuilder.Entity<Person>().ToTable("Persons");
            modelBuilder.Entity<MovieRating>().ToTable("MovieRatings");

            modelBuilder.Entity<Movie>()
                .HasMany(a => a.Actors)
                .WithMany(a => a.ActInMovies)
                .Map(m =>
                {
                    m.ToTable("ActorsMovies");
                    m.MapLeftKey("MovieId");
                    m.MapRightKey("ActorId");
                });

            modelBuilder.Entity<Movie>()
                .HasMany(a => a.Directors)
                .WithMany(a => a.DirectInMovies)
                .Map(m =>
                {
                    m.ToTable("DirectorsMovies");
                    m.MapLeftKey("MovieId");
                    m.MapRightKey("DirectorId");
                });
        }
    }
}
