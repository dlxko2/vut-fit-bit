--------------------------------------------------------------------------------
-- Company: FIT VUT Brno
-- Engineer: Martin Pavelka
--
-- Create Date:   22:04:21 03/23/2016
-- Design Name:   Testbench of BCD counter
-- Module Name:   bcd_tb.vhd
-- Project Name:  CounterBCD
-- Description:   Count the score for the game
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
  
ENTITY bcd_tb IS
END bcd_tb;
 
ARCHITECTURE behavior OF bcd_tb IS 
 
 	-- My component for testing
    COMPONENT bcd
    PORT(
         CLK : IN  std_logic;
         RESET : IN  std_logic;
         NUMBER3 : buffer  std_logic_vector(3 downto 0);
         NUMBER2 : buffer  std_logic_vector(3 downto 0);
         NUMBER1 : buffer  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   -- Inputs
   signal CLK : std_logic := '0';
   signal RESET : std_logic := '1';

   --Outputs
   signal NUMBER3 : std_logic_vector(3 downto 0);
   signal NUMBER2 : std_logic_vector(3 downto 0);
   signal NUMBER1 : std_logic_vector(3 downto 0);

   -- Variables
   shared variable count : integer range 0 to 1000 := 1;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bcd PORT MAP (
          CLK => CLK,
          RESET => RESET,
          NUMBER3 => NUMBER3,
          NUMBER2 => NUMBER2,
          NUMBER1 => NUMBER1
        );

   -- Main test process definition
   CLK_process :process
		variable t_num1 : std_logic_vector(3 downto 0) := "0000";
		variable t_num2 : std_logic_vector(3 downto 0) := "0000";
		variable t_num3 : std_logic_vector(3 downto 0) := "0000";
   begin
	
	-- Check after reset initialization
	if (RESET = '1') then
		count := 1;
		CLK <= '0';
		wait for CLK_period/2;
		assert NUMBER1 = STD_LOGIC_VECTOR'("0000") report "Reset NUMBER1 failed!" severity error;
		assert NUMBER2 = STD_LOGIC_VECTOR'("0000") report "Reset NUMBER2 failed!" severity error;
		assert NUMBER3 = STD_LOGIC_VECTOR'("0000") report "Reset NUMBER3 failed!" severity error;
	end if;
	
	-- Check first 100 numbers
	if (count /= 100) then
		CLK <= '0';
		RESET <= '0';
		wait for CLK_period/2;
		
		-- Check first number 
		if (count mod 1 = 0) then
			if (NUMBER1 < "1001") then t_num1 := NUMBER1 + 1;
			else t_num1 := "0000";
			end if;
		end if;
		
		-- Check second number
		if (count mod 10 = 0) then
			if (NUMBER2 < "1001") then t_num2 := NUMBER2 + 1;
			else t_num2 := "0000";
			end if;
		end if;
		
		-- Check third number
		if (count mod 100 = 0) then
			if (NUMBER3 < "1001") then t_num3 := NUMBER3 + 1;
			else 
				t_num1 := "0000";
				t_num2 := "0000";
				t_num3 := "0000";
			end if;
		end if;
		
		-- Set clock to high and check
		CLK <= '1';
		count := count + 1;
		wait for CLK_period/2;
		assert NUMBER1 = t_num1 and NUMBER2 = t_num2 and NUMBER3 = t_num3  report "Update of number failed!" severity error;
		
	end if;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      wait for 100 ns;	
      wait for CLK_period*10;
      wait;
   end process;

END;
