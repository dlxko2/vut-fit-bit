library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use work.mathe_pack.all;
use work.vypocet_pack.all;

entity net is
    Port 
	 ( 
		O_ACTIVE_CELLS : out std_logic_vector(24 downto 0);
		O_SELECTED_CELLS : out std_logic_vector(24 downto 0);
		
    RANDOM :in std_logic_vector(24 downto 0);
		CLKA :in std_logic;
		RESETA : in std_logic;
		KEYSA : IN  std_logic_vector(4 downto 0)
	 );
end net;

architecture Behavioral of net is

	 -- Popis bunky
    COMPONENT cell
	  GENERIC (
      MASK              : mask_t := (others => '1') 
   );
    PORT(
         INVERT_REQ_IN : IN  std_logic_vector(3 downto 0) := (others => '0');
        	INVERT_REQ_OUT : OUT  std_logic_vector(3 downto 0);
        	KEYS : IN  std_logic_vector(4 downto 0) := (others => '0');
        	SELECT_REQ_IN : IN  std_logic_vector(3 downto 0) := (others => '0');
        	SELECT_REQ_OUT : OUT  std_logic_vector(3 downto 0);
         INIT_ACTIVE : IN  std_logic := '0';
         ACTIVE : OUT  std_logic;
         INIT_SELECTED : IN  std_logic := '0';
         SELECTED : OUT  std_logic;
        	CLK : IN  std_logic := '0';
        	RESET : IN  std_logic := '0'
        );
    END COMPONENT;
	 
	constant IDX_TOP    : NATURAL := 0; 
   constant IDX_LEFT   : NATURAL := 1;
   constant IDX_RIGHT  : NATURAL := 2; 
   constant IDX_BOTTOM : NATURAL := 3; 
   constant IDX_ENTER  : NATURAL := 4; 
	
	
	signal select_request : std_logic_vector(99 downto 0);
	signal invert_request : std_logic_vector(99 downto 0);
	signal active_cells : std_logic_vector(24 downto 0);
	signal selected_cells : std_logic_vector(24 downto 0);
	signal init_selected_cells : std_logic_vector(24 downto 0) := "0000000000001000000000000";
	signal x : integer := 0;
	signal y : integer := 0;
begin
 
	O_ACTIVE_CELLS <= active_cells;
	O_SELECTED_CELLS <= selected_cells;
	
	create_y: 
	for y in 0 to 4 generate
	begin
	
		create_x: 
		for x in 0 to 4 generate
		begin
			
			genericmap: cell generic map
			(
				MASK => getmask(x, y, 5, 5)
			)  port map
			(
				CLK =>  CLKA,
				RESET => RESETA,
				KEYS => KEYSA,
				
				ACTIVE => active_cells(y * 5 + x),
				SELECTED => selected_cells(y * 5 + x),
				INIT_ACTIVE => RANDOM(y * 5 + x),
				INIT_SELECTED => init_selected_cells(y * 5 + x),
				
				INVERT_REQ_IN(IDX_LEFT) => invert_request(vypocet(x-1, y, IDX_RIGHT)),
				INVERT_REQ_IN(IDX_RIGHT) => invert_request(vypocet(x+1, y, IDX_LEFT)),
				INVERT_REQ_IN(IDX_TOP) => invert_request(vypocet(x, y-1, IDX_BOTTOM)),
				INVERT_REQ_IN(IDX_BOTTOM) => invert_request(vypocet(x, y+1, IDX_TOP)),
				
				SELECT_REQ_IN(IDX_LEFT) => select_request(vypocet(x-1, y, IDX_RIGHT)),
				SELECT_REQ_IN(IDX_RIGHT) => select_request(vypocet(x+1, y, IDX_LEFT)),
				SELECT_REQ_IN(IDX_TOP) => select_request(vypocet(x, y-1, IDX_BOTTOM)),
				SELECT_REQ_IN(IDX_BOTTOM) => select_request(vypocet(x, y+1, IDX_TOP)),
				
				INVERT_REQ_OUT => invert_request(vypocet(x,y, IDX_BOTTOM) downto vypocet(x, y, IDX_TOP)),
				SELECT_REQ_OUT => select_request(vypocet(x,y, IDX_BOTTOM) downto vypocet(x, y, IDX_TOP))
			);
			
			
		
		end generate;
	end generate;
	

end Behavioral;

