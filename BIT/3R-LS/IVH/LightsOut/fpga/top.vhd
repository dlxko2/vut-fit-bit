
library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.vga_controller_cfg.all;
use work.clkgen_cfg.all;

architecture behav of tlv_pc_ifc is

signal bcd_clk : std_logic;
 signal   bcd_num1 : std_logic_vector(3 downto 0);
   signal        bcd_num2 :  std_logic_vector(3 downto 0);
    signal       bcd_num3 :  std_logic_vector(3 downto 0);
   signal random_active :  std_logic_vector (24 downto 0) := (others => '0');
   signal vga_mode  : std_logic_vector(60 downto 0); 
   signal irgb : std_logic_vector(8 downto 0);
   signal row : std_logic_vector(11 downto 0);
   signal col : std_logic_vector(11 downto 0);

   signal active_cells :  std_logic_vector(24 downto 0);
   signal selected_cells :  std_logic_vector(24 downto 0);
      
   signal kbrd_data_out : std_logic_vector(15 downto 0);
   signal kbrd_data_vld : std_logic;
   signal keys :  std_logic_vector(4 downto 0);

   signal myrst : std_logic := '1';

   -- Indexy klaves a signalov
   constant IDX_TOP    : NATURAL := 0; 
   constant IDX_LEFT   : NATURAL := 1;
   constant IDX_RIGHT  : NATURAL := 2; 
   constant IDX_BOTTOM : NATURAL := 3; 
   constant IDX_ENTER  : NATURAL := 4; 

begin
   
   -- Nastaveni grafickeho rezimu (640x480, 60 Hz refresh)
   setmode(r640x480x60, vga_mode);

   vga: entity work.vga_controller(arch_vga_controller) 
      generic map (REQ_DELAY => 1)
      port map (
         CLK    => CLK, 
         RST    => RESET,
         ENABLE => '1',
         MODE   => vga_mode,

         DATA_RED    => irgb(8 downto 6),
         DATA_GREEN  => irgb(5 downto 3),
         DATA_BLUE   => irgb(2 downto 0),
         ADDR_COLUMN => col,
         ADDR_ROW    => row,

         VGA_RED   => RED_V,
         VGA_BLUE  => BLUE_V,
         VGA_GREEN => GREEN_V,
         VGA_HSYNC => HSYNC_V,
         VGA_VSYNC => VSYNC_V
   );

    -- Keyboard controller
   kbrd_ctrl: entity work.keyboard_controller(arch_keyboard)
      port map (
         CLK => SMCLK,
         RST => RESET,

         DATA_OUT => kbrd_data_out(15 downto 0),
         DATA_VLD => kbrd_data_vld,
         
         KB_KIN   => KIN,
         KB_KOUT  => KOUT
      );
   
   net: entity work.net
    PORT MAP
    ( 
      RANDOM => random_active,
      O_ACTIVE_CELLS => active_cells,
      O_SELECTED_CELLS => selected_cells,
      CLKA => CLK,
      RESETA => myrst,
      KEYSA => keys
    );

    myvga: entity work.vga 
    PORT MAP 
	 ( 
		irgb => irgb,
		row => row,
    col => col,
    active_cells => active_cells,
    selected_cells => selected_cells,
    number3 => bcd_num3,
    number2 => bcd_num2,
    number1 => bcd_num1
	 );
   
   random: entity work.random_gen
   PORT MAP
  (
      clk => CLK,
      random_num => random_active             
    );

   mybcd: entity work.bcd 
   PORT MAP
    ( 
      CLK => bcd_clk,
      RESET => myrst,
      NUMBER3 => bcd_num3,
      NUMBER2 => bcd_num2,
      NUMBER1 => bcd_num1
    );

   
   -- cursor controller, move to CLK
   cursor: process
      variable in_access : std_logic := '0';
   begin
       
      if CLK'event and CLK='1' then
      keys <= (others => '0'); 
      myrst <= '0'; 
      bcd_clk <= '0';
         if in_access='0' then
            if kbrd_data_vld='1' then 
               in_access:='1';
               if kbrd_data_out(4)='1' then  -- key 6
                  keys <= (IDX_TOP => '1', others => '0');
               elsif kbrd_data_out(6)='1' then  -- key 4
                  keys <= (IDX_BOTTOM => '1', others => '0');
               elsif kbrd_data_out(9)='1'then  -- key 2
                  keys <= (IDX_RIGHT => '1', others => '0');
               elsif kbrd_data_out(1)='1'then  -- key 8
                  keys <= (IDX_LEFT => '1', others => '0');
               elsif kbrd_data_out(5)='1' then   -- key 5
                  keys <= (IDX_ENTER => '1', others => '0');
                  bcd_clk <= '1';
               elsif kbrd_data_out(3)='1' then   -- key 5
                  myrst <= '1';
                                  
               end if;
            end if;
         else
            if kbrd_data_vld='0' then 
               in_access:='0';
            end if;
         end if;
      end if;
                                                                                              
   end process;
         
   
   
end behav;
