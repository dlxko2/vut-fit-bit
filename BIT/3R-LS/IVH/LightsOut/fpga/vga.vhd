library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use work.mathe_pack.all;
use work.vypocet_pack.all;

entity vga is
    Port 
	 ( 
		irgb : out std_logic_vector(8 downto 0);
		row : in std_logic_vector(11 downto 0);
    col : in std_logic_vector(11 downto 0);
    active_cells : in std_logic_vector(24 downto 0);
    selected_cells : in std_logic_vector(24 downto 0);
    number3 : in  std_logic_vector(3 downto 0);
    number2 : in  std_logic_vector(3 downto 0);
    number1 : in  std_logic_vector(3 downto 0)
	 );
end vga;

architecture Behavioral of vga is

 signal rom_col : integer range 0 to 15;
 signal rom_row : integer range 0 to 15;
  signal digger_clr1 : std_logic;
  signal digger_clr2 : std_logic;
  signal digger_clr3 : std_logic;
  type pamet is array(0 to 16*10-1) of std_logic_vector(0 to 15);
signal rom_digger: pamet := ("0111111111111110",
                             "0111111111111110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0110000000000110",
                             "0111111111111110",
                             "0111111111111110",
                             
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             "0000000000000110",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1111111111111111",
                             "1111111111111111",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1100000000000000",
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             
                             "1111111111111111",
                             "1111111111111111",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1100000000000011",
                             "1111111111111111",
                             "1111111111111111",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011",
                             "0000000000000011"
       
                              );



begin

rom_col <= conv_integer(col(3 downto 0));
rom_row <= conv_integer(row(3 downto 0));   

   digger_clr1 <= rom_digger(conv_integer(number1) * 16 + rom_row)(rom_col);
  digger_clr2 <= rom_digger(conv_integer(number2) * 16 + rom_row)(rom_col);
digger_clr3 <= rom_digger(conv_integer(number3) * 16 + rom_row)(rom_col);

    irgb <= 
      
        -- 0/0
        "000000111"  when  selected_cells(0) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(0) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000001"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        
        -- 1/0
        "000000111"  when  selected_cells(1) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(1) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000001"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
       
        -- 2/0 
        "000000111"  when  selected_cells(2) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(2) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000001"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
       
        -- 3/0
        "000000111"  when  selected_cells(3) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(3) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000001"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
       
        -- 4/0
        "000000111"  when  selected_cells(4) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(4) = '1' and row(11 downto 6)= "000001"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000001"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
          
        -- 0/1
        "000000111"  when  selected_cells(5) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(5) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000010"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 1/1
        "000000111"  when  selected_cells(6) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(6) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000010"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 2/1
        "000000111"  when  selected_cells(7) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(7) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000010"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 3/1
        "000000111"  when  selected_cells(8) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(8) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000010"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 4/1
        "000000111"  when  selected_cells(9) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(9) = '1' and row(11 downto 6)= "000010"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000010"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 0/2
        "000000111"  when  selected_cells(10) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(10) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000011"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 1/2
        "000000111"  when  selected_cells(11) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(11) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000011"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 2/2
        "000000111"  when  selected_cells(12) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(12) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000011"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 3/2
        "000000111"  when  selected_cells(13) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(13) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000011"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 4/2
        "000000111"  when  selected_cells(14) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(14) = '1' and row(11 downto 6)= "000011"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000011"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 0/3
        "000000111"  when  selected_cells(15) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(15) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000100"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 1/3
        "000000111"  when  selected_cells(16) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(16) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000100"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 2/3
        "000000111"  when  selected_cells(17) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(17) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000100"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 3/3
        "000000111"  when  selected_cells(18) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(18) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000100"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 4/3
        "000000111"  when  selected_cells(19) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(19) = '1' and row(11 downto 6)= "000100"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000100"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 0/4
        "000000111"  when  selected_cells(20) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(20) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000101"  and col(11 downto 6)="000011" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
      
        -- 1/4
        "000000111"  when  selected_cells(21) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(21) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000101"  and col(11 downto 6)="000100" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
            
        -- 2/4
        "000000111"  when  selected_cells(22) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(22) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000101"  and col(11 downto 6)="000101" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
         
        -- 3/4
        "000000111"  when  selected_cells(23) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(23) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000101"  and col(11 downto 6)="000110" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
              
        -- 4/4
        "000000111"  when  selected_cells(24) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "111000000"  when  active_cells(24) = '1' and row(11 downto 6)= "000101"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111" else
        "000111000"  when  row(11 downto 6)= "000101"  and col(11 downto 6)="000111" and col(5 downto 3) /= "111" and row(5 downto 3) /= "111"  else
        
        
       "111010000" when digger_clr3='1' and (col(11 downto 4) = "00011001") and (row(11 downto 4) = "00011001")  else
        "111010000" when digger_clr2='1' and (col(11 downto 4) = "00011010") and (row(11 downto 4) = "00011001")  else
       "111010000" when digger_clr1='1' and (col(11 downto 4) = "00011011") and (row(11 downto 4) = "00011001")  
      
        else "000000000";
   
	

end Behavioral;

