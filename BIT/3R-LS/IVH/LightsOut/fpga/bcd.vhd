----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:16:34 04/01/2016 
-- Design Name: 
-- Module Name:    bcd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd is

	---Port ( vystupny_sygnal : out mask_t);
	Port (
		CLK		: IN std_logic;
		RESET		: IN std_logic;
		NUMBER3  : BUFFER std_logic_vector(3 downto 0);
		NUMBER2  : BUFFER std_logic_vector(3 downto 0);
		NUMBER1  : BUFFER std_logic_vector(3 downto 0)
	);

end bcd;

architecture Behavioral of bcd is	

begin
	process(CLK, RESET)
		begin
		if (RESET = '1') then
				NUMBER3 <= "0000";
				NUMBER2 <= "0000";
				NUMBER1 <= "0000";
				
		elsif (CLK'event) and (CLK='1') then
			if (NUMBER1 <= "1000") then
				NUMBER1 <= NUMBER1 + "0001";
			else
				NUMBER1 <= "0000";
				if (NUMBER2 <= "1000") then
					NUMBER2 <= NUMBER2 + "0001";
				else
					NUMBER2 <= "0000";
					if (NUMBER3 <= "1000") then
						NUMBER3 <= NUMBER3 + "0001" ;
					else
						NUMBER3 <= "0000";
					end if;
				end if;
			end if;
		end if;
	end process;
end Behavioral;

