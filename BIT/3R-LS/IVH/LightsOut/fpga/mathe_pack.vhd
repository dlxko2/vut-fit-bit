--
--	Math pack package
--
--	Purpose: This package is used to find neighboor cells avaiability 
--		      for each distance when rows and cols are specified.
--
-- Author: Martin Pavelka
-- Date: 20.03.2016
-- Email: xpavel27@stud.fit.vutbr.cz

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- This is package declaration
package mathe_pack is

 type mask_t is
  record
    top           : std_logic;
	 left          : std_logic;
	 right         : std_logic;
	 bottom        : std_logic;
 end record;

 function getmask(x,y : natural; COLUMNS, ROWS : natural) return mask_t;

end mathe_pack;

-- This is package implementation
package body mathe_pack is

  function getmask  (x,y : natural; COLUMNS, ROWS : natural) return mask_t is
    variable result : mask_t;
	 
  begin
  
	 -- Left border 
	 if (x = 0) then result.left := '0';
	 else result.left := '1';
	 end if;
	
	 -- Right border
	 if (x = (COLUMNS - 1)) then result.right := '0';
	 else result.right := '1';
	 end if;
	 
	 -- Top border
	 if (y = 0) then result.top := '0';
	 else result.top := '1';
	 end if;
	 
	 -- Bottom border
	 if (y = (ROWS - 1)) then result.bottom := '0';
	 else result.bottom := '1';
	 end if;
	 
    return result; 
  end getmask;

end mathe_pack;
