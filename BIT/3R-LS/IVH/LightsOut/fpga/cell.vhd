----------------------------------------------------------------------------------
-- Engineer: Martin Pavelka
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.mathe_pack.ALL;

-- Definicia entity bunky
entity cell is
   GENERIC (
      MASK              : mask_t := (others => '1') 
   );
   Port ( 
      INVERT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
      INVERT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
      KEYS              : in   STD_LOGIC_VECTOR (4 downto 0);
      SELECT_REQ_IN     : in   STD_LOGIC_VECTOR (3 downto 0);
      SELECT_REQ_OUT    : out  STD_LOGIC_VECTOR (3 downto 0);
      INIT_ACTIVE       : in   STD_LOGIC;
      ACTIVE            : out  STD_LOGIC;
      INIT_SELECTED     : in   STD_LOGIC;
      SELECTED          : out  STD_LOGIC;
      CLK               : in   STD_LOGIC;
      RESET             : in   STD_LOGIC
   );
end cell;

-- Definicia architektury bunky
architecture Behavioral of cell is
 
	-- Indexy klaves a signalov
   constant IDX_TOP    : NATURAL := 0; 
   constant IDX_LEFT   : NATURAL := 1;
   constant IDX_RIGHT  : NATURAL := 2; 
   constant IDX_BOTTOM : NATURAL := 3; 
   constant IDX_ENTER  : NATURAL := 4; 
	
	-- Signali zaistujuce citanie vystupnej hodnoty
  signal var_selected : std_logic;
  signal var_active   : std_logic;

-- Definicia chovania  
begin
	
	-- Linkovanie pomocnych signalov na vystupne
	ACTIVE <= var_active;
	SELECTED <= var_selected;

-- Definicia zakladneho synchronneho procesu
main : process(CLK, RESET)
begin
		
	-- Definovanie asynchronneho resetu
	-- Resetujem aj vystupne signali kvoli 'undefined'
	if (RESET = '1') then
		var_active   <= INIT_ACTIVE;
		var_selected <= INIT_SELECTED;
		SELECT_REQ_OUT <= (others => '0');
		INVERT_REQ_OUT <= (others => '0');
		
	-- Definovanie synchronneho chovania (vzostupna hrana)
	elsif rising_edge(CLK) then 	
		
			INVERT_REQ_OUT <= (others => '0');
			SELECT_REQ_OUT <= (others => '0');

			-- Ak nie je bunka vybrana (selected = 0)
			if (var_selected = '0') then
			
				-- Ak signal inverzie kontrolujeme masku ci moze v danom smere vstupit a zmenime aktivitu
				if ((INVERT_REQ_IN and (MASK.BOTTOM & MASK.RIGHT & MASK.LEFT & MASK.TOP)) /= "0000" ) then
					var_active <= not var_active;	
				end if;
				
				-- Al signal vyberu kontrolujeme masku a zmenime stav
				if ((SELECT_REQ_IN and (MASK.BOTTOM & MASK.RIGHT & MASK.LEFT & MASK.TOP)) /= "0000" ) then
					var_selected <= not var_selected;
				end if;			

			-- Ak je vybrana (selected)
			else

				-- Ak stlacime enter posle signal zmeny aktivity okoliu a zmeni svoj stav (active)
				if (KEYS(IDX_ENTER) = '1') then 
					var_active <= not var_active;
					INVERT_REQ_OUT <= (IDX_TOP    => MASK.TOP, 
											 IDX_LEFT   => MASK.LEFT, 
											 IDX_RIGHT  => MASK.RIGHT, 
											 IDX_BOTTOM => MASK.BOTTOM);

				-- Ak stlacime sipky smeru posle vyber dalej a zmeny svoj stav
				elsif (KEYS(IDX_TOP) = '1' and MASK.TOP = '1') then
					SELECT_REQ_OUT <= (IDX_TOP => '1', others => '0');
					var_selected <= '0';
				elsif (KEYS(IDX_LEFT) = '1' and MASK.LEFT = '1') then
					SELECT_REQ_OUT <= (IDX_LEFT => '1', others => '0');
					var_selected <= '0'; 
				elsif (KEYS(IDX_RIGHT) = '1' and MASK.RIGHT = '1') then
					SELECT_REQ_OUT <= (IDX_RIGHT => '1', others => '0');
					var_selected <= '0';
				elsif (KEYS(IDX_BOTTOM) = '1' and MASK.BOTTOM = '1') then
					SELECT_REQ_OUT <= (IDX_BOTTOM => '1', others => '0');
					var_selected <= '0';
 			end if;

		end if;

	end if;
	
end process;
end Behavioral;