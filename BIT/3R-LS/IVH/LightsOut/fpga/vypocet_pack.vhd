--
--	Math pack package
--
--	Purpose: This package is used to find neighboor cells avaiability 
--		      for each distance when rows and cols are specified.
--
-- Author: Martin Pavelka
-- Date: 20.03.2016
-- Email: xpavel27@stud.fit.vutbr.cz

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- This is package declaration
package vypocet_pack is

 function vypocet(x,y : integer; direction : natural) return natural;

end vypocet_pack;

-- This is package implementation
package body vypocet_pack is
 
  function vypocet(x,y : integer; direction : natural) return natural is 
		variable newX : natural;
		variable newY : natural;
  begin
	
	newX := x mod 5;
	newY := y mod 5;
	
	return newX * 4 + newY * 20 + direction;
	
  end vypocet;

end vypocet_pack;
