----------------------------------------------------------------------------------
-- Engineer: Martin Pavelka
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.math_pack.ALL;

-- Entity pre testench
ENTITY cell_tb IS
END cell_tb;
 
-- Popis architektury pre test
ARCHITECTURE behavior OF cell_tb IS 
	
	 -- Popis bunky
    COMPONENT cell
    PORT(
         INVERT_REQ_IN : IN  std_logic_vector(3 downto 0);
         INVERT_REQ_OUT : OUT  std_logic_vector(3 downto 0);
         KEYS : IN  std_logic_vector(4 downto 0);
         SELECT_REQ_IN : IN  std_logic_vector(3 downto 0);
         SELECT_REQ_OUT : OUT  std_logic_vector(3 downto 0);
         INIT_ACTIVE : IN  std_logic;
         ACTIVE : OUT  std_logic;
         INIT_SELECTED : IN  std_logic;
         SELECTED : OUT  std_logic;
         CLK : IN  std_logic;
         RESET : IN  std_logic
        );
    END COMPONENT;
    
   -- Vstupne signaly
   signal INVERT_REQ_IN : std_logic_vector(3 downto 0) := (others => '0');
   signal KEYS : std_logic_vector(4 downto 0) := (others => '0');
   signal SELECT_REQ_IN : std_logic_vector(3 downto 0) := (others => '0');
   signal INIT_ACTIVE : std_logic := '0';
   signal INIT_SELECTED : std_logic := '0';
   signal CLK : std_logic := '0';
   signal RESET : std_logic := '0';

 	-- Vystupne signaly
   signal INVERT_REQ_OUT : std_logic_vector(3 downto 0);
   signal SELECT_REQ_OUT : std_logic_vector(3 downto 0);
   signal ACTIVE : std_logic;
   signal SELECTED : std_logic;

   -- Konstanta pre krokovanie hodin
   constant CLK_period : time := 10 ns;
	
	-- Indexy pre signaly
   constant IDX_TOP    : NATURAL := 0; 
   constant IDX_LEFT   : NATURAL := 1; 
   constant IDX_RIGHT  : NATURAL := 2; 
   constant IDX_BOTTOM : NATURAL := 3; 
   constant IDX_ENTER  : NATURAL := 4;

-- Popis chovania
BEGIN
 
	-- Mapovanie portov
   uut: cell PORT MAP (
          INVERT_REQ_IN  => INVERT_REQ_IN,
          INVERT_REQ_OUT => INVERT_REQ_OUT,
          KEYS           => KEYS,
          SELECT_REQ_IN  => SELECT_REQ_IN,
          SELECT_REQ_OUT => SELECT_REQ_OUT,
          INIT_ACTIVE    => INIT_ACTIVE,
          ACTIVE         => ACTIVE,
          INIT_SELECTED  => INIT_SELECTED,
          SELECTED       => SELECTED,
          CLK            => CLK,
          RESET          => RESET
        );

	-- Krokovanie hodin po pol periode
	CLK <= not CLK after CLK_period/2;
	
   -- Definicia testovacieho procesu
   tb :process
   begin
	
		-- Test resetu
		INIT_SELECTED <= '1';
		INIT_ACTIVE <= '1';
		RESET <= '1';
	
		wait for CLK_period ;
		assert ACTIVE = '1' and SELECTED = '1' report "Chyba inicializacie!" severity error;
		RESET <= '0';
	 
		wait for CLK_period ;
		INIT_SELECTED <= '0';
		INIT_ACTIVE <= '0';
		RESET <= '1';
	
		wait for CLK_period ;
		assert ACTIVE = '0' and SELECTED = '0' report "Chyba inicializacie!" severity error;
		RESET <= '0';
	
		-- Ak jej pride signal inverzie od suseda invertuje svoj stav
		wait for CLK_period ;
		INVERT_REQ_IN <= "1000";
	
		wait for CLK_period;
		INVERT_REQ_IN <= "0000";
		assert ACTIVE = '1' report "Chyba inverzie aktivity!" severity error;
	
		-- Ak pride signal o posuve kurzora spravne sa aktivuje
		wait for CLK_period ;
		SELECT_REQ_IN <= "1000";
	
		wait for CLK_period;
		SELECT_REQ_IN <= "0000";
		assert SELECTED = '1' report "Chyba inverzie vyberu!" severity error;
	
		-- Bunka pri stisknuti klavesi posle kurzor spravne dalej
		wait for CLK_period ;
		KEYS <= (IDX_TOP => '1', others => '0');
	
		wait for CLK_period;
		KEYS <= (others => '0');
		assert SELECT_REQ_OUT(IDX_TOP) = '1' report "Chyba posuvu kurzora!" severity error;

		-- Pre ucelenost ukoncime resetom a cakame na koniec simulacie
		wait for CLK_period;
		RESET <= '1';
		wait;
		end process;

END;
