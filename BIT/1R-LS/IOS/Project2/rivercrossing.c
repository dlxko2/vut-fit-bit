// Includes for fork and wait functions
#include <unistd.h>
#include <sys/wait.h>

// Semaphores and shared memory
#include <sys/types.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <fcntl.h>

// Signals
#include <signal.h>

// Basic includes
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>

// Structure of arguments
typedef struct
{
    int NumPersons;
    int IntervalHackers;
    int IntervalSerfs;
    int RiverInterval;
} ArgumentStruct;

typedef struct
{
    sem_t *BoardingSem;
    sem_t *LandingSem;
    sem_t *boarding;
    sem_t *HackerSem;
    sem_t *SerfSem;
    sem_t *FinishingSem;
    sem_t *MemSem;
    sem_t *MalyPrdola;
    int hackers;
    int serfs;
    int OperationCount;
    int BoardPeople;
    int TravelPeople;
} SemaphoreStruc;

// Return values
enum ReturnErrors
{
    EERR=-1,
    EOK,
    EBADARGS,
    EBADARGNUMS
};

// Types of persons
enum PersonTypes
{
    HACKER,
    SERF
};

// Better to use prototypes
ArgumentStruct GetArgs(char const *argv[]);
void SendSignal(int sighandler);
void CloseChannels(void);
void GenHackers(SemaphoreStruc *mem, ArgumentStruct args);
void GenSerfs(SemaphoreStruc *mem, ArgumentStruct args);
void OpenFile();

// File variable
FILE *MyOutputFile;

// Function to open file
void OpenFile(int mode)
{
    if (mode == 0)
        MyOutputFile = fopen("rivercrossing.out","w");
    else
        fclose(MyOutputFile);
}

// Function to generate hackers
void GenHackers(SemaphoreStruc *mem, ArgumentStruct args)
{
    // Local variables
    int Interval;
    bool Captain = false;
    
    // For every person cycle
    for (int i = 0; i < args.NumPersons; ++i)
    {
        // Make proces to sleep for some time
        srand(time(NULL) ^ (getpid()<<16));
        Interval = rand() % (1000*args.IntervalHackers+1);
        usleep(Interval);
        
        // Create one hacker 
        pid_t hacker = fork();
        if (hacker == 0)
        {
            // With semaphore start hacker
            sem_wait(mem->MemSem);
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile, "%d: hacker: %d: started.\n", mem->OperationCount++, i+1);
            sem_post(mem->MalyPrdola);
            sem_post(mem->MemSem);
            
            // Wait for boardinf
            sem_wait(mem->boarding);
            
            // Add new hacker to the qeue
            sem_wait(mem->MemSem);
            mem->hackers++;
            
            // Write message about boarding
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile, "%d: hacker: %d: waiting for boarding: %d: %d\n", mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            sem_post(mem->MemSem);
            
            // If four of hackers
            if (mem->hackers == 4)
            {
                // Make them to go
                sem_wait(mem->MemSem);
                mem->hackers = 0;
                sem_post(mem->MemSem);
                
                // Enable four times to run process
                sem_post(mem->HackerSem);
                sem_post(mem->HackerSem);
                sem_post(mem->HackerSem);
                sem_post(mem->HackerSem);
                
                // Create hacker captain
                Captain = true;
            }
            // For 2 of hackers and more serfs
            else if (mem->hackers == 2 && mem->serfs >= 2)
            {
                // Recet numbers of count
                sem_wait(mem->MemSem);
                mem->hackers = 0;
                mem->serfs -= 2;
                sem_post(mem->MemSem);
                
                // Make 2 of each to go
                sem_post(mem->HackerSem);
                sem_post(mem->HackerSem);
                sem_post(mem->SerfSem);
                sem_post(mem->SerfSem);
                
                // Make hacker as captain
                Captain = true;
            }
            else
                sem_post(mem->boarding);
            
            // Wait for hacker semaphore
            sem_wait(mem->HackerSem);
            
            // Start boarding
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile, "%d: hacker: %d: boarding: %d: %d\n", mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            
            // Board people
            sem_wait(mem->MemSem);
            mem->BoardPeople++;
            sem_post(mem->MemSem);
            
            // If full boat make it go
            if (mem->BoardPeople == 4)
            {
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
            }
            
            // Write members
            sem_wait(mem->BoardingSem);
            sem_wait(mem->MemSem);
            
            if (Captain)
            {
                // Write captain
                sem_wait(mem->MalyPrdola);
                fprintf(MyOutputFile, "%d: hacker: %d: captain\n", mem->OperationCount++, i+1);
                sem_post(mem->MalyPrdola);
                
                // Sleep captain
                Interval = rand() % (1000*args.RiverInterval+1);
                usleep(Interval);
            }
            else
                // Print member
                sem_wait(mem->MalyPrdola);
                fprintf(MyOutputFile, "%d: hacker: %d: member\n", mem->OperationCount++, i+1);
                sem_post(mem->MalyPrdola);
            
            // Enable semaphore for members
            sem_post(mem->MemSem);
            sem_wait(mem->MemSem);
            
            // Decrease number of boarded
            mem->BoardPeople--;
            
            // If everybody on boat land them
            if (mem->BoardPeople == 0)
            {
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
            }
            
            // Semaphores
            sem_post(mem->MemSem);
            sem_wait(mem->LandingSem);
            sem_wait(mem->MemSem);
            
            // Landing
            mem->BoardPeople++;
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile, "%d: hacker: %d: landing: %d: %d\n",mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            mem->TravelPeople--;
            sem_post(mem->MemSem);
            
            // Stop landing
            if (mem->BoardPeople == 4)
            {
                sem_post(mem->boarding);
                mem->BoardPeople = 0;
            }
            
            // Finishing
            if (mem->TravelPeople == 0)
                for (int i = 0; i < args.NumPersons*2; ++i)
                    sem_post(mem->FinishingSem);
            
            // Write message
            sem_wait(mem->FinishingSem);
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile, "%d: hacker: %d: finishing\n", mem->OperationCount++, i+1);
            sem_post(mem->MalyPrdola);
            exit(0);
        }
        else if (hacker == -1)
        {
            fprintf(stderr, "Error with creating hacker\n");
            OpenFile(1);
            exit(1);
        }
    }
    
    // Wait for childs and then end
    while(wait(NULL))
    {
        if(errno == ECHILD)
            break;
    }
    exit(0);
}

// Function to generate serfs
void GenSerfs(SemaphoreStruc *mem, ArgumentStruct args)
{
    // Local variables
    int Interval;
    bool Captain = false;
    
    // Cycle for every serf
    for (int i = 0; i < args.NumPersons; ++i)
    {
        // Sleep the process
        srand(time(NULL) ^ (getpid()<<16));
        Interval = rand() % (1000*args.IntervalSerfs+1);
        usleep(Interval);
        
        // Create one serf
        pid_t serf = fork();
        if (serf == 0)
        {
            // Show as started
            sem_wait(mem->MemSem);
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile,"%d: serf: %d: started.\n", mem->OperationCount++, i+1);
            sem_post(mem->MalyPrdola);
            sem_post(mem->MemSem);
            
            // Wait for bording
            sem_wait(mem->boarding);
            
            // Add person to queue
            sem_wait(mem->MemSem);
            mem->serfs++;
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile,"%d: serf: %d: waiting for boarding: %d: %d\n",mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            sem_post(mem->MemSem);
            
            // If four serfs
            if (mem->serfs == 4)
            {
                // Set count to zero
                sem_wait(mem->MemSem);
                mem->serfs = 0;
                sem_post(mem->MemSem);
                
                // Enable four times serf
                sem_post(mem->SerfSem);
                sem_post(mem->SerfSem);
                sem_post(mem->SerfSem);
                sem_post(mem->SerfSem);
                
                // Create captain
                Captain = true;
            }
            // If two of serfs and more hackers
            else if (mem->serfs == 2 && mem->hackers >= 2)
            {
                // Set counts
                sem_wait(mem->MemSem);
                mem->serfs = 0;
                mem->hackers -= 2;
                sem_post(mem->MemSem);
                
                // Enable two o each other
                sem_post(mem->SerfSem);
                sem_post(mem->SerfSem);
                sem_post(mem->HackerSem);
                sem_post(mem->HackerSem);
                
                // Create captain
                Captain = true;
            }
            else
                // Set boarding
                sem_post(mem->boarding);
            
            // Wait for boarding
            sem_wait(mem->SerfSem);
            
            // Show message and save the count
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile,"%d: serf: %d: boarding: %d: %d\n",mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            sem_wait(mem->MemSem);
            mem->BoardPeople++;
            sem_post(mem->MemSem);
            
            // If everybody boarded
            if (mem->BoardPeople == 4)
            {
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
                sem_post(mem->BoardingSem);
            }
            
            // Show members
            sem_wait(mem->BoardingSem);
            sem_wait(mem->MemSem);
            
            if (Captain)
            {
                // Show captain
                sem_wait(mem->MalyPrdola);
                fprintf(MyOutputFile,"%d: serf: %d: captain\n", mem->OperationCount++, i+1);
                sem_post(mem->MalyPrdola);
                
                // Sleep him
                Interval = rand() % (1000*args.RiverInterval+1);
                usleep(Interval);
            }
            else
                // Another memeber
                sem_wait(mem->MalyPrdola);
                fprintf(MyOutputFile,"%d: serf: %d: member\n", mem->OperationCount++, i+1);
                sem_post(mem->MalyPrdola);
            
            // Semaphores
            sem_post(mem->MemSem);
            sem_wait(mem->MemSem);
            
            // Decrease boarded ones
            mem->BoardPeople--;
            if (mem->BoardPeople == 0)
            {
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
                sem_post(mem->LandingSem);
            }
            
            // Change semaphores
            sem_post(mem->MemSem);
            sem_wait(mem->LandingSem);
            sem_wait(mem->MemSem);
            
            // Landing
            mem->BoardPeople++;
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile,"%d: serf: %d: landing: %d: %d\n",mem->OperationCount++, i+1, mem->hackers, mem->serfs);
            sem_post(mem->MalyPrdola);
            mem->TravelPeople--;
            sem_post(mem->MemSem);
            
            // End of landing
            if (mem->BoardPeople == 4)
            {
                sem_post(mem->boarding);
                mem->BoardPeople = 0;
            }
            
            // Finishing
            if (mem->TravelPeople == 0)
                for (int i = 0; i < args.NumPersons*2; ++i)
                    sem_post(mem->FinishingSem);
            
            // Show message
            sem_wait(mem->FinishingSem);
            sem_wait(mem->MalyPrdola);
            fprintf(MyOutputFile,"%d: serf: %d: finishing\n", mem->OperationCount++, i+1);
            sem_post(mem->MalyPrdola);
            exit(0);
        }
        else if (serf == -1)
        {
            fprintf(stderr, "Cant create serf!\n");
            OpenFile(1);
            exit(1);
        }
    }
    
    // Wait for childs and exit
    while(wait(NULL))
    {
        if(errno == ECHILD)
            break;
    }
    exit(0);
}

// Main function of program
int main(int argc, char const *argv[])
{
    // Check for arguments number
    if (argc != 5)
    {
        fprintf(stderr, "Wrong number of arguments!\n");
        return 1;
    }
    
    // Open file
    OpenFile(0);
    
    // Get arguments into structure
    ArgumentStruct args = GetArgs(argv);
    
    // Check the result
    if (errno == EBADARGS)
    {
        fprintf(stderr, "Arguments are wrong!\n");
        return 1;
    }
    
    // Using of signals
    signal(SIGTERM, SendSignal);
    signal(SIGINT, SendSignal);
    
    // Create shared memory
    SemaphoreStruc *mem;
    int SharedMem = shm_open("/sharedMemory", O_CREAT | O_EXCL | O_RDWR, 0644);
    ftruncate(SharedMem, sizeof(SemaphoreStruc));
    mem = mmap(NULL, sizeof(SemaphoreStruc), PROT_READ | PROT_WRITE, MAP_SHARED, SharedMem, 0);
    
    // Init the semaphores and another handlers
    mem->BoardingSem = sem_open("/semafor1", O_CREAT | O_EXCL, 0666, 0);
    mem->LandingSem = sem_open("/semafor2", O_CREAT | O_EXCL, 0666, 0);
    mem->boarding = sem_open("/semafor3", O_CREAT | O_EXCL, 0666, 1);
    mem->HackerSem = sem_open("/semafor4", O_CREAT | O_EXCL, 0666, 0);
    mem->SerfSem = sem_open("/semafor5", O_CREAT | O_EXCL, 0666, 0);
    mem->FinishingSem = sem_open("/semafor6", O_CREAT | O_EXCL, 0666, 0);
    mem->MemSem = sem_open("/semafor7", O_CREAT | O_EXCL, 0666, 1);
    mem->OperationCount = 1;
    mem->BoardPeople = 0;
    mem->TravelPeople = args.NumPersons*2;
    
    // Create generator for serfs
    pid_t GeneratorOfSerfs = fork();
    if (GeneratorOfSerfs == 0)
    {
        GenSerfs(mem, args);
    }
    else if (GeneratorOfSerfs == -1)
    {
        // Write error message
        fprintf(stderr, "Error while creating serf generator process\n");
        OpenFile(1);
        exit(1);
    }
    else if (GeneratorOfSerfs > 0)
    {
        // Create hackers generator
        pid_t GeneratorOfHackers = fork();
        if (GeneratorOfHackers == 0) {
            GenHackers(mem, args);
        }
        else if (GeneratorOfHackers == -1)
        {
            // Write error message
            fprintf(stderr, "Error while creating hacker generator process\n");
            OpenFile(1);
            exit(1);
        }
        else if (GeneratorOfHackers > 0)
            wait(NULL);
    }
    
    while(wait(NULL))
    {
        if(errno == ECHILD)
            break;
    }
    
    // Close semaphores
    sem_close(mem->BoardingSem);
    sem_close(mem->LandingSem);
    sem_close(mem->boarding);
    sem_close(mem->HackerSem);
    sem_close(mem->SerfSem);
    sem_close(mem->FinishingSem);
    sem_close(mem->MemSem);
    
    // Close channels
    CloseChannels();
    
    // Unmap memory and close it
    munmap(mem, sizeof(SemaphoreStruc));
    close(SharedMem);
    
    // Close file
    OpenFile(1);
    
    // If ok just sucess
    return EXIT_SUCCESS;
}

// Function to get arguments
ArgumentStruct GetArgs(char const *argv[])
{
    // Local variables
    ArgumentStruct args;
    char *error[4];
    
    // Get numbers for structure
    args.NumPersons = strtod(argv[1], &error[0]);
    args.IntervalHackers = strtod(argv[2], &error[1]);
    args.IntervalSerfs = strtod(argv[3], &error[2]);
    args.RiverInterval = strtod(argv[4], &error[3]);
    
    // Test values
    if (args.NumPersons < 0 || args.NumPersons%2 != 0
        || args.IntervalHackers < 0 || args.IntervalHackers > 5000
        || args.IntervalSerfs < 0 || args.IntervalSerfs > 5000
        || args.RiverInterval < 0 || args.RiverInterval > 5000) {
        errno = EBADARGS;
    }
    return args;
}

// Function to signal people
void SendSignal(int sighandler)
{
    (void)sighandler;
    
    // Kill processes
    kill(getpid(), SIGTERM);
    CloseChannels();
    OpenFile(1);
    exit(1);
}

// Function to close semaphotres and memory
void CloseChannels(void)
{
    // Unlink semaphores
    sem_unlink("/semafor1");
    sem_unlink("/semafor2");
    sem_unlink("/semafor3");
    sem_unlink("/semafor4");
    sem_unlink("/semafor5");
    sem_unlink("/semafor6");
    sem_unlink("/semafor7");
    
    // Close shared memory
    shm_unlink("/sharedMemory");
}