#!/bin/bash

# ----------------------------------- ARGUMENTS ---------------------------------
# Null every argument handler 
ARG_G=false; ARG_D=false; ARG_R=false

# Parsing program arguments
while getopts :gd:r: selected 
do case "$selected" in
	g) ARG_G=true;;
	r) ARG_R=true; ARGOPT=$OPTARG;; 
    d) ARG_D=true; ARGOPT=$OPTARG;;
    :) echo "Option -$OPTARG requires an object file!" >&2; exit 1;;
	*) echo "Wrong argument -$OPTARG!" >&2; exit 1;;
esac
done


# ----------------------------------- TEST PART ---------------------------------
# Write error if -d and -r entered
if [ "$ARG_R" = true ] && [ "$ARG_D" = true ]
then echo "Cant use -r with -d option!" >&2; exit 1
fi

# Decrement argument number
((OPTIND--)); shift $OPTIND

# Check if file used as argument
if [ "$*" =  "" ]
then echo "No file used as argument!" >&2; exit 1
fi

# Check if files exist and is object
for CHECK_FILE in $*; do
    if [ -e "$CHECK_FILE" ]
    then :
    else echo "File $CHECK_FILE doesn't exist!" >&2; exit 1
    fi

    if [ "$(echo $CHECK_FILE | egrep -E '.+[.]o$')" =  "" ]
    then echo "File $CHECK_FILE has wrong type!" >&2; exit 1
    fi
done

# Check if ARGOPT file exsists and is object
if [ "$ARG_R" = true ] || [ "$ARG_D" = true ]
then
    if [ -e "$ARGOPT" ]
    then :
    else echo "File $ARGOPT doesn't exist!" >&2; exit 1
    fi

    if [ "$(echo $ARGOPT | egrep -E '.+[.]o$')" =  "" ]
    then echo "File $ARGOPT has wrong type!" >&2; exit 1
    fi
fi

# Create cleaned ARGOPT
if [ "$ARG_R" = true ] || [ "$ARG_D" = true ]
then ARGOPT_CLR=$(echo "$ARGOPT" |  egrep -o "[^/]*\.o"); fi


# ----------------------------------- SCRIPT ---------------------------------
# For every file do from first line cycle
for FROM_FILE in $*; do FROM_LINE=1

    # Create cleaned FROM_FILE
    FROM_FILE_CLR=$(echo "$FROM_FILE" |  egrep -o "[^/]*\.o")

    # Make optimized source file dump and get lines count
    if $ARG_D
    then FROM_FILE_DUMP=$(nm $FROM_FILE | sed -nr "s/^[0-9a-z]+ [BCDGT] (.+)$/\1/p")
    else FROM_FILE_DUMP=$(nm $FROM_FILE | sed -nr "s/ +U (.+)$/\1/p")
    fi
    FROM_FILE_LINES=$(echo "$FROM_FILE_DUMP" | wc -l)

    # Check dump file
    [ "$FROM_FILE_DUMP" = "" ] && continue

    # Go throught the selected file till end
    while [ "$FROM_LINE" -le "$FROM_FILE_LINES" ]; do

        # Do first search for selected file and increase line counter
        FROM_FUNCTION=$(echo "$FROM_FILE_DUMP" | sed -n ${FROM_LINE}p)
        ((FROM_LINE++))

        # If generating graph save file name of object / Find symbol in target file
        [ "$ARG_G" = true ] && TEMP_DEFINITIONS=$TEMP_DEFINITIONS"$FROM_FILE_CLR ($FROM_FILE)*"
        [ "$ARG_R" = true ] && TO_FUNCTION=$(nm $ARGOPT | sed -nr "s/^[0-9a-z]+ [BCDGT] ($FROM_FUNCTION)$/\1/p")
        [ "$ARG_D" = true ] && TO_FUNCTION=$(nm $ARGOPT | sed -nr "s/ +U ($FROM_FUNCTION)$/\1/p")

        # Check the result and save dependencies 
        if [ "$ARG_R" = true ] || [ "$ARG_D" = true ]
        then
            [ "$TO_FUNCTION" = "" ] && continue
            
            if [ "$ARG_R" = true ]
            then TEMP_DUMP=$TEMP_DUMP"$FROM_FILE_CLR -> $ARGOPT_CLR ($TO_FUNCTION)*"; fi

            if [ "$ARG_D" = true ]
            then TEMP_DUMP=$TEMP_DUMP"$ARGOPT_CLR -> $FROM_FILE_CLR ($TO_FUNCTION)*"; fi

        # Search if no argument found
        else for TO_FILE in $*; do

            # Create cleaned FROM_FILE
            TO_FILE_CLR=$(echo "$TO_FILE" |  egrep -o "[^/]*\.o")

			# Search the target for requires
			TO_FUNCTION=$(nm $TO_FILE | sed -nr "s/^[0-9a-z]+ [BCDGT] ($FROM_FUNCTION)$/\1/p")

            # Check the result and save dependencies with defines
            [ "$TO_FUNCTION" = "" ] && continue
            TEMP_DEFINITIONS=$TEMP_DEFINITIONS"$TO_FILE_CLR ($TO_FILE)*"
            TEMP_DUMP=$TEMP_DUMP"$FROM_FILE_CLR -> $TO_FILE_CLR ($TO_FUNCTION)*"
            done
        fi
    done
done


# ----------------------------------- OUTPUT ---------------------------------
if $ARG_G
then

     # If argument R or D found get defines for graph
     if [ "$ARG_R" = true ] || [ "$ARG_D" = true ]
     then TEMP_DEFINITIONS=$TEMP_DEFINITIONS"$ARGOPT_CLR ($ARGOPT)*"; fi

     # Convert output to graph format
     echo "digraph GSYM {" 
     echo "$TEMP_DUMP" | tr '*' '\n' | sort -u | sed '/^$/d;s/.o[ ]/Do /g;s/(/[label="/g;s/)/"];/g'
     echo "$TEMP_DEFINITIONS" | tr '*' '\n' | sort -u | sed '/^$/d;s/.o[ ]/Do /g;s/(/[label="/g;s/)/"];/g'
     echo "}"
else echo "$TEMP_DUMP" | tr '*' '\n' | sort -u | sed "/^$/d" 
fi



