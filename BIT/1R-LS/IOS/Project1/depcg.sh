#!/bin/bash

# ----------------------------------- ARGUMENTS ---------------------------------
# Null every argument handler
ARG_G=false; ARG_D=false; ARG_R=false; ARG_P=false;

# Parsing program arguments
while getopts :gpd:r: selected
do case "$selected" in
    g) ARG_G=true;;
    p) ARG_P=true;;
    r) ARG_R=true; ARGOPT_R=$OPTARG;;
    d) ARG_D=true; ARGOPT_D=$OPTARG;;
    :) echo "Option -$OPTARG requires an object file!" >&2; exit 1;;
    *) echo "Chyba argumentov!" >&2; exit 1;;
esac
done


# ----------------------------------- TEST PART ---------------------------------
# Write error if -d and -r entered
if [ "$ARG_R" = true ] && [ "$ARG_D" = true ]; then
    echo "Cant use -r with -d option!" >&2; exit 1
fi

# Decrement argument number
((OPTIND--)); shift $OPTIND

# Check if file used as argument
if [ "$*" =  "" ]; then
    echo "No file used as argument!" >&2; exit 1
fi

# Check if files exist and is object
for CHECK_FILE in $*; do
    if [ -e "$CHECK_FILE" ]; then
    :
    else echo "File $CHECK_FILE doesn't exist!" >&2; exit 1
    fi
done


# ----------------------------------- SCRIPT ---------------------------------
# For every file do from first line cycle
for FROM_FILE in $*; do FROM_LINE=1; 

    # Filtrate output 
    DUMP_OUTPUT=$(objdump -d -j .text $FROM_FILE | egrep -E " +[0-9a-z]+:.+callq +[0-9a-z]+ <([a-zA-Z0-9@_-]+)>$|^[0-9a-z]+ <[a-zA-Z0-9_-]+>:$")
    
    # Check if file is not empty
    if [ "$DUMP_OUTPUT" = "" ]; then
        echo "Prazdny alebo poskodeny subor recfun"
        continue
    fi

    # Lines count
    DUMP_LINES=$(echo "$DUMP_OUTPUT" | wc -l)

    # Cycle every line of output
    while [ "$FROM_LINE" -le "$DUMP_LINES" ]; do

        # Look if function definition found
        FOUND_FUNCTION=$(echo "$DUMP_OUTPUT" | sed -n ${FROM_LINE}p | sed -nr 's/^[0-9a-z]+ <(.+)>:$/\1/p')

        # Check result of search for function
        if [ "$FOUND_FUNCTION" != "" ]
        then ACTUAL_FUNCTION="$FOUND_FUNCTION";

        # Check result of search to execute callq search
        else
            if $ARG_P
            then FOUND_CALLQ=$(echo "$DUMP_OUTPUT" | sed -n ${FROM_LINE}p | sed -nr 's/ +[0-9a-z]+:.*callq +[0-9a-z]+ <(.+)>$/\1/p')
            else FOUND_CALLQ=$(echo "$DUMP_OUTPUT" | sed -n ${FROM_LINE}p | sed -nr 's/ * [0-9a-z]*:.*callq *[0-9a-z]* <([^@]+)>$/\1/p')
            fi

            # Check the result of search
            if [ "$ARG_D" = true ] && [ "$FOUND_CALLQ" != "" ] && [ "$ACTUAL_FUNCTION" = "$ARGOPT_D" ]
                then TEMP_DUMP=$TEMP_DUMP"$ACTUAL_FUNCTION -> $FOUND_CALLQ*"
            elif [ "$ARG_R" = true ] && [ "$FOUND_CALLQ" = "$ARGOPT_R" ] && [ "$ACTUAL_FUNCTION" != "" ]
                then TEMP_DUMP=$TEMP_DUMP"$ACTUAL_FUNCTION -> $FOUND_CALLQ*"
            elif [ "$ARG_D" = false ] && [ "$ARG_R" = false ] && [ "$FOUND_CALLQ" != "" ]
                then TEMP_DUMP=$TEMP_DUMP"$ACTUAL_FUNCTION -> $FOUND_CALLQ*"
            fi
        fi

        # increase line number
        ((FROM_LINE++))
    done


# ----------------------------------- OUTPUT ---------------------------------
    # Generate output
    if $ARG_G; then
         echo "digraph CG {"
         echo "$TEMP_DUMP" | tr '*' '\n' | sort -u | sed '/^$/d;s/$/;/g;s/@/_/g;s@_plt@_PLT@g'
         echo "}"
    else echo "$TEMP_DUMP" | tr '*' '\n' | sort -u | sed "/^$/d" 
    fi
done