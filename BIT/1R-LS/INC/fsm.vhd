-- fsm.vhd: Finite State Machine
-- Author(s): Martin Pavelka
--            FIT VUT Brno 2014
--            xpavel27@stud.fit.vutbr.cz

-- ----------------------------------------------------------------------------
-- 								LIBRARY INCLUDES
-- ----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-- ----------------------------------------------------------------------------
--                        	DECLARATION OF ENTITY FSM
-- ----------------------------------------------------------------------------
entity fsm is
port
(
	-- Time signals
	CLK         : in  std_logic;
    RESET       : in  std_logic;
 
    -- Input signals
    KEY         : in  std_logic_vector(15 downto 0);
    CNT_OF      : in  std_logic;
 
    -- Output signals
    FSM_CNT_CE  : out std_logic;
    FSM_MX_MEM  : out std_logic;
    FSM_MX_LCD  : out std_logic;
    FSM_LCD_WR  : out std_logic;
    FSM_LCD_CLR : out std_logic
);
end entity fsm;
 
-- ----------------------------------------------------------------------------
--                      	DECLARATION OF STATES
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
	type t_state is (IDLE, FAIL, TEST1, TEST2, TEST3, TEST4, TEST5_FIRST, TEST5_SECOND, TEST6_FIRST, TEST6_SECOND,
                 	 TEST7_FIRST, TEST7_SECOND, TEST8_FIRST, TEST8_SECOND, TEST9_FIRST, TEST9_SECOND, TEST10_FIRST, 
                 	 TEST10_SECOND,TEST11, PRINT_MESSAGE_FAIL, PRINT_MESSAGE_OK, FINISH);
	signal present_state, next_state : t_state;
 
-- ----------------------------------------------------------------------------
-- 							BEGIN OF MY PROGRAM
-- 								  DEFINES
-- ----------------------------------------------------------------------------
begin
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= TEST1;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;
 
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is
   when IDLE =>
      next_state <= TEST1;
   when FAIL =>
      next_state <= FAIL;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE_FAIL;
      end if;

-- ----------------------------------------------------------------------------
-- 						 TESTING OF KEYBORD INPUT
-- ----------------------------------------------------------------------------
   
-- Set the state to Test1
when TEST1 => next_state <= TEST1;
	
	-- If key "1" pressed go to next test      
	if (KEY(1) = '1') then 
		next_state <= TEST2;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test2
when TEST2 => next_state <= TEST2;
	
	-- If key "5" pressed go to next test      
	if (KEY(5) = '1') then 
		next_state <= TEST3;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if; 
   
   -- Set the state to Test3
when TEST3 => next_state <= TEST3;
	
	-- If key "7" pressed go to next test      
	if (KEY(7) = '1') then 
		next_state <= TEST4;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test4
when TEST4 => next_state <= TEST4;
	
	-- If key "2" pressed go to next test      
	if (KEY(2) = '1') then 
		next_state <= TEST5_FIRST;

	-- If key "4" pressed go to next test too
	elsif (KEY(4) = '1') then
		next_state <= TEST5_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test5 first
when TEST5_FIRST => next_state <= TEST5_FIRST;
	
	-- If key "3" pressed go to next test      
	if (KEY(3) = '1') then 
		next_state <= TEST6_FIRST;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test5 second
when TEST5_SECOND => next_state <= TEST5_SECOND;
	
	-- If key "6" pressed go to next test      
	if (KEY(6) = '1') then 
		next_state <= TEST6_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test6 first
when TEST6_FIRST => next_state <= TEST6_FIRST;
	
	-- If key "1" pressed go to next test      
	if (KEY(1) = '1') then 
		next_state <= TEST7_FIRST;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test6 second
when TEST6_SECOND => next_state <= TEST6_SECOND;
	
	-- If key "3" pressed go to next test      
	if (KEY(3) = '1') then 
		next_state <= TEST7_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test7 first
when TEST7_FIRST => next_state <= TEST7_FIRST;
	
	-- If key "8" pressed go to next test      
	if (KEY(8) = '1') then 
		next_state <= TEST8_FIRST;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test7 second
when TEST7_SECOND => next_state <= TEST7_SECOND;
	
	-- If key "7" pressed go to next test      
	if (KEY(7) = '1') then 
		next_state <= TEST8_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test8 first
when TEST8_FIRST => next_state <= TEST8_FIRST;
	
	-- If key "6" pressed go to next test      
	if (KEY(6) = '1') then 
		next_state <= TEST9_FIRST;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test8 second
when TEST8_SECOND => next_state <= TEST8_SECOND;
	
	-- If key "2" pressed go to next test      
	if (KEY(2) = '1') then 
		next_state <= TEST9_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test9 first
when TEST9_FIRST => next_state <= TEST9_FIRST;
	
	-- If key "1" pressed go to next test      
	if (KEY(1) = '1') then 
		next_state <= TEST10_FIRST;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test9 second
when TEST9_SECOND => next_state <= TEST9_SECOND;
	
	-- If key "3" pressed go to next test      
	if (KEY(3) = '1') then 
		next_state <= TEST10_SECOND;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test9 first
when TEST10_FIRST => next_state <= TEST10_FIRST;
	
	-- If key "7" pressed go to next test      
	if (KEY(7) = '1') then 
		next_state <= TEST11;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test9 second
when TEST10_SECOND => next_state <= TEST10_SECOND;
	
	-- If key "5" pressed go to next test      
	if (KEY(5) = '1') then 
		next_state <= TEST11;

	-- If key "#" pressed show wrong code
	elsif (KEY(15) = '1') then
		next_state <= PRINT_MESSAGE_FAIL;

	-- If another key pressed then just show next char
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;

-- Set the state to Test11
when TEST11 => next_state <= TEST11;
    
	-- If accept key pressed show OK
    if (KEY(15) = '1') then
    	next_state <= PRINT_MESSAGE_OK;

    -- If another key pressed continue
    elsif (KEY(15 downto 0) /= "0000000000000000") then
    	next_state <= FAIL;
    end if;
 
-- ----------------------------------------------------------------------------
-- 						 DEFINITIONS OF FUNCTIONS
-- ----------------------------------------------------------------------------

-- Definition of function to print OK message
when PRINT_MESSAGE_OK => next_state <= PRINT_MESSAGE_OK;
    if (CNT_OF = '1') then
    	next_state <= FINISH;
    end if;
 
   -- --------------------------------------------
   when PRINT_MESSAGE_FAIL =>
      next_state <= PRINT_MESSAGE_FAIL;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
 
   -- --------------------------------------------
   when FINISH =>
      next_state <= FINISH;
      if (KEY(15) = '1') then
         next_state <= TEST1;
      end if;
 
   -- --------------------------------------------
   when others =>
      next_state <= IDLE;
   end case;
end process next_state_logic;
 
-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';
 
   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE_FAIL =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_MX_MEM     <= '0';
      FSM_LCD_WR     <= '1';
 
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE_OK =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_MX_MEM     <= '1';
      FSM_LCD_WR     <= '1';
 
   -- - - - - - - - - - - - - - - - - - - - - - -
   when FINISH =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
 
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
   if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   end case;
end process output_logic;
 
end architecture behavioral;